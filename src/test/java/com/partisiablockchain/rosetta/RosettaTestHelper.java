package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.AccountCoin;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.StakeDelegation;
import com.partisiablockchain.blockchain.StakesFromOthers;
import com.partisiablockchain.blockchain.TransferInformation;
import com.partisiablockchain.blockchain.VestingAccount;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.PublicKeyDto;
import com.partisiablockchain.rosetta.dto.SignatureDto;
import com.partisiablockchain.rosetta.dto.SigningPayloadDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.server.BetanetAddresses;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.junit.jupiter.api.Assertions;

/** Helper methods for testing the Rosetta implementation. */
public final class RosettaTestHelper {

  @SuppressWarnings("unused")
  private RosettaTestHelper() {}

  static final SignatureDto DUMMY_SIGNATURE_DTO =
      createSignatureDtoFromMessageHash(
          Hash.create((s) -> SafeDataOutputStream.serialize(stream -> {})));

  static SignatureDto createSignatureDtoFromMessageHash(Hash messageHash) {
    Signature sign = TestValues.KEY_PAIR_ONE.sign(messageHash);
    int recoveryId = sign.getRecoveryId();
    byte[] signatureHex =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.write(zeroPad(sign.getR()));
              stream.write(zeroPad(sign.getS()));
              stream.writeByte(recoveryId);
            });

    return new SignatureDto(
        new SigningPayloadDto(
            new byte[0],
            new AccountIdentifierDto(TestValues.ACCOUNT_ONE),
            null,
            SignatureDto.SIGNATURE_TYPE),
        new PublicKeyDto(new byte[0], PublicKeyDto.CURVE_TYPE),
        SignatureDto.SIGNATURE_TYPE,
        signatureHex);
  }

  static byte[] zeroPad(BigInteger value) {
    byte[] bytes = value.toByteArray();
    int offset;
    if (bytes[0] == 0) {
      // The byte is only included to contain sign
      offset = 1;
    } else {
      offset = 0;
    }

    int bytesToWrite = bytes.length - offset;
    int destination = 32 - bytesToWrite;
    byte[] result = new byte[32];
    System.arraycopy(bytes, offset, result, destination, bytesToWrite);
    return result;
  }

  /** Invocation bytes only used for tests. */
  public static final class DummyInvocations {

    public static final int MINT = 99;

    static final int MPC_TOKEN_BYOC_TRANSFER = 29;

    static final int ADD_COIN_BALANCE = 0;
    static final byte INIT_DELEGATE_STAKES = 21;
    static final byte INIT_RECEIVE_DELEGATED_STAKES = 22;
    static final int INIT_RETRACT_DELEGATED_STAKES = 23;
    static final int INIT_RETURN_DELEGATED_STAKES = 24;
    static final byte INIT_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION = 87;
    static final int ADD_MPC_TOKEN_BALANCE = 4;
    static final int DEDUCT_MPC_TOKEN_BALANCE = 5;
    static final int BURN_MPC_TOKEN_BALANCE = 6;
    static final int CREATE_VESTING_ACCOUNT = 7;
  }

  static void checkError(Runnable requester, ErrorDto error) {
    try {
      requester.run();
      Assertions.fail("WebApplicationException not thrown");
    } catch (WebApplicationException e) {
      Response response = e.getResponse();
      assertThat(response.getStatus()).isEqualTo(500);
      ErrorDto errorDto = (ErrorDto) response.getEntity();
      assertThat(errorDto).usingRecursiveComparison().isEqualTo(error);
    }
  }

  static SignedTransaction createTransfer(
      BlockchainLedger ledger, KeyPair sender, BlockchainAddress receiver, long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createTransferWithMpc20(
      BlockchainLedger ledger, KeyPair sender, BlockchainAddress receiver, long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenMpc20Invocations.TRANSFER);
          receiver.write(s);
          s.write(toNonZeroUnsigned128Bytes(Unsigned256.create(amount)));
        });
  }

  static SignedTransaction createTransferFromWithMpc20(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress senderAddress,
      BlockchainAddress receiverAddress,
      long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenMpc20Invocations.TRANSFER_FROM);
          senderAddress.write(s);
          receiverAddress.write(s);
          s.write(toNonZeroUnsigned128Bytes(Unsigned256.create(amount)));
        });
  }

  static byte[] toNonZeroUnsigned128Bytes(Unsigned256 unsigned256) {
    byte[] unsigned128 = new byte[16];
    System.arraycopy(unsigned256.serialize(), 16, unsigned128, 0, 16);
    return unsigned128;
  }

  static SignedTransaction createTransferSmallMemo(
      BlockchainLedger ledger, KeyPair sender, BlockchainAddress receiver, long amount, long memo) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO);
          receiver.write(s);
          s.writeLong(amount);
          s.writeLong(memo);
        });
  }

  static SignedTransaction createTransferLargeMemo(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress receiver,
      long amount,
      String memo) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO);
          receiver.write(s);
          s.writeLong(amount);
          s.writeString(memo);
        });
  }

  static SignedTransaction createDelegateTransfer(
      BlockchainLedger ledger, KeyPair sender, BlockchainAddress receiver, long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.DELEGATE_STAKES);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createDelegateTransferOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.DELEGATE_STAKES_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createDelegateTransferWithExpiration(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress receiver,
      long amount,
      long expirationTimestamp) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION);
          receiver.write(s);
          s.writeLong(amount);
          s.writeLong(expirationTimestamp);
        });
  }

  static SignedTransaction createDelegateTransferWithExpirationOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount,
      long expirationTimestamp) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
          s.writeLong(expirationTimestamp);
        });
  }

  static SignedTransaction createDelegateRetract(
      BlockchainLedger ledger, KeyPair sender, BlockchainAddress receiver, long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.RETRACT_DELEGATE_STAKES);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createDelegateRetractOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.RETRACT_DELEGATED_STAKES_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createByocTransfer(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress receiver,
      long amount,
      String symbol) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.BYOC_TRANSFER_OLD);
          receiver.write(s);
          s.writeLong(amount);
          s.writeString(symbol);
        });
  }

  static SignedTransaction createTransferOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount) {

    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
        });
  }

  static SignedTransaction createTransferSmallMemoOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount,
      long memo) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
          s.writeLong(memo);
        });
  }

  static SignedTransaction createTransferLargeMemoOnBehalfOf(
      BlockchainLedger ledger,
      KeyPair sender,
      BlockchainAddress target,
      BlockchainAddress receiver,
      long amount,
      String memo) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger,
        BetanetAddresses.MPC_TOKEN_CONTRACT,
        sender,
        s -> {
          s.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF);
          target.write(s);
          receiver.write(s);
          s.writeLong(amount);
          s.writeString(memo);
        });
  }

  static Hash addPendingTransaction(BlockchainLedger ledger, SignedTransaction transaction) {
    boolean addedTransaction = ledger.addPendingTransaction(transaction);
    assertThat(addedTransaction).isTrue();
    return transaction.identifier();
  }

  static NetworkIdentifierDto createNetworkIdentifier(String network, String subNetwork) {
    SubNetworkIdentifierDto subNetworkIdentifier = new SubNetworkIdentifierDto(subNetwork);
    return new NetworkIdentifierDto(
        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN, network, subNetworkIdentifier);
  }

  private static RootDirectory createTempDb(String testDir) {
    return ExceptionConverter.call(
        () ->
            new RootDirectory(
                Files.createTempDirectory(testDir).toFile(),
                (path, immutable) -> new MapBackedStorage(immutable)),
        "Could not create temp directory for test DB");
  }

  static Map<String, BlockchainLedger> createLedgers(
      List<String> shards, Consumer<BlockchainLedger> register) {
    HashMap<String, BlockchainLedger> ledgers = new HashMap<>();
    for (String shard : shards) {
      BlockchainLedger ledger =
          BlockchainLedgerHelper.createLedger(
              createTempDb("TestDir" + (shard == null ? "Gov" : "Shard1")),
              BlockchainLedgerHelper.createNetworkConfig(),
              BlockchainLedgerHelper.createChainState(),
              shard);
      ledgers.put(shard, ledger);
      register.accept(ledger);
    }
    return ledgers;
  }

  /** Test token minter contract. */
  public static final class TestTokenMinterContract extends SysContract<StateVoid> {

    @Override
    @SuppressWarnings("EnumOrdinal")
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      final BlockchainAddress account = BlockchainAddress.read(rpc);
      final long tokens = rpc.readLong();
      final long stakedTokens = rpc.readLong();

      final List<StakeDelegation> storedPendingStakeDelegations = new ArrayList<>();
      int numberOfStoredPendingStakeDelegations = rpc.readInt();
      for (int i = 0; i < numberOfStoredPendingStakeDelegations; i++) {
        long amount = rpc.readLong();
        StakeDelegation.DelegationType delegationType =
            StakeDelegation.DelegationType.values()[rpc.readInt()];
        storedPendingStakeDelegations.add(StakeDelegation.create(amount, delegationType));
      }

      final List<Long> delegatedStakesToOthers = new ArrayList<>();
      int numberOfDelegatedStakesToOthers = rpc.readInt();
      for (int i = 0; i < numberOfDelegatedStakesToOthers; i++) {
        delegatedStakesToOthers.add(rpc.readLong());
      }

      final Map<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers = new HashMap<>();
      int numberOfDelegatedStakesFromOthers = rpc.readInt();
      for (int i = 0; i < numberOfDelegatedStakesFromOthers; i++) {
        delegatedStakesFromOthers.put(
            BlockchainAddress.read(rpc), StakesFromOthers.create(rpc.readLong(), rpc.readLong()));
      }

      final List<Long> pendingRetractedDelegatedStakes = new ArrayList<>();
      int numberOfPendingRetractedDelegatedStakes = rpc.readInt();
      for (int i = 0; i < numberOfPendingRetractedDelegatedStakes; i++) {
        pendingRetractedDelegatedStakes.add(rpc.readLong());
      }

      List<VestingAccount> vestingAccounts = new ArrayList<>();
      int numberOfVestingAccounts = rpc.readInt();
      for (int i = 0; i < numberOfVestingAccounts; i++) {
        vestingAccounts.add(new VestingAccount(rpc.readLong()));
      }

      Map<Hash, TransferInformation> storedPendingTransfers = new HashMap<>();
      int numberOfStoredPendingTransfers = rpc.readInt();
      for (int i = 0; i < numberOfStoredPendingTransfers; i++) {
        storedPendingTransfers.put(
            Hash.read(rpc),
            new TransferInformation(rpc.readLong(), rpc.readBoolean(), rpc.readInt()));
      }

      Map<Long, Long> pendingUnstakes = new HashMap<>();
      int numberOfPendingUnstakes = rpc.readInt();
      for (int i = 0; i < numberOfPendingUnstakes; i++) {
        pendingUnstakes.put(rpc.readLong(), rpc.readLong());
      }

      List<AccountCoin> accountCoins = new ArrayList<>();
      int numberOfCoins = rpc.readInt();
      for (int i = 0; i < numberOfCoins; i++) {
        accountCoins.add(new AccountCoin(Unsigned256.read(rpc)));
      }

      SystemEventCreator invocationCreator = context.getInvocationCreator();
      invocationCreator.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              account,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.MINT);
                    stream.writeLong(tokens);
                    stream.writeLong(stakedTokens);

                    stream.writeInt(storedPendingStakeDelegations.size());
                    for (StakeDelegation storedPendingStakeDelegation :
                        storedPendingStakeDelegations) {
                      stream.writeLong(storedPendingStakeDelegation.getAmount());
                      stream.writeInt(storedPendingStakeDelegation.getDelegationType().ordinal());
                    }

                    stream.writeInt(delegatedStakesToOthers.size());
                    for (long delegatedStakesToOther : delegatedStakesToOthers) {
                      stream.writeLong(delegatedStakesToOther);
                    }

                    stream.writeInt(delegatedStakesFromOthers.size());
                    for (BlockchainAddress address : delegatedStakesFromOthers.keySet()) {
                      address.write(stream);
                      StakesFromOthers stakesFromOthers = delegatedStakesFromOthers.get(address);
                      stream.writeLong(stakesFromOthers.acceptedDelegatedStakes);
                      stream.writeLong(stakesFromOthers.pendingDelegatedStakes);
                    }

                    stream.writeInt(pendingRetractedDelegatedStakes.size());
                    for (long pendingRetractedDelegatedStake : pendingRetractedDelegatedStakes) {
                      stream.writeLong(pendingRetractedDelegatedStake);
                    }

                    stream.writeInt(vestingAccounts.size());
                    for (VestingAccount vestingAccount : vestingAccounts) {
                      stream.writeLong(vestingAccount.tokens);
                    }

                    stream.writeInt(storedPendingTransfers.size());
                    for (Hash transaction : storedPendingTransfers.keySet()) {
                      transaction.write(stream);
                      TransferInformation transferInformation =
                          storedPendingTransfers.get(transaction);
                      stream.writeLong(transferInformation.amount);
                      stream.writeBoolean(transferInformation.addTokensOrCoinsIfTransferSuccessful);
                      stream.writeInt(transferInformation.coinIndex);
                    }

                    stream.writeInt(pendingUnstakes.size());
                    for (Long pendingUnstake : pendingUnstakes.keySet()) {
                      stream.writeLong(pendingUnstake);
                      stream.writeLong(pendingUnstakes.get(pendingUnstake));
                    }

                    stream.writeInt(accountCoins.size());
                    for (AccountCoin accountCoin : accountCoins) {
                      accountCoin.balance.write(stream);
                    }
                  })));
      return state;
    }
  }

  /** Test MPC token contract. */
  public static final class TestMpcTokenContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      int invocationByte = rpc.readUnsignedByte(); // invocation byte
      SystemEventCreator eventManager = context.getInvocationCreator();

      if (invocationByte == MpcTokenInvocations.TRANSFER) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, null);
      } else if (invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        rpc.readLong(); // small memo
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, null);
      } else if (invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount-
        rpc.readString(); // large memo
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, null);
      } else if (invocationByte == DummyInvocations.MPC_TOKEN_BYOC_TRANSFER) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        rpc.readString(); // symbol
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, null);
      } else if (invocationByte == MpcTokenInvocations.DELEGATE_STAKES) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeDelegateStakes(context, eventManager, recipient, amount, transferId);
      } else if (invocationByte == MpcTokenInvocations.DELEGATE_STAKES_ON_BEHALF_OF) {
        BlockchainAddress target = BlockchainAddress.read(rpc);
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeDelegateStakes(context, eventManager, recipient, amount, transferId, target);
      } else if (invocationByte == MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        long expirationTimestamp = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeDelegateStakesWithExpiration(
            context, eventManager, recipient, amount, expirationTimestamp, transferId);
      } else if (invocationByte
          == MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF) {
        BlockchainAddress target = BlockchainAddress.read(rpc);
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        long expirationTimestamp = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeDelegateStakesWithExpiration(
            context, eventManager, recipient, amount, expirationTimestamp, transferId, target);
      } else if (invocationByte == MpcTokenInvocations.RETRACT_DELEGATE_STAKES) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeRetractDelegateStakes(context, eventManager, recipient, amount, transferId);
      } else if (invocationByte == MpcTokenInvocations.RETRACT_DELEGATED_STAKES_ON_BEHALF_OF) {
        BlockchainAddress target = BlockchainAddress.read(rpc);
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        long amount = rpc.readLong();
        Hash transferId = context.getCurrentTransactionHash();
        invokeRetractDelegateStakesOnBehalfOf(
            context, eventManager, target, recipient, amount, transferId);
      } else if (invocationByte == MpcTokenInvocations.TRANSFER_ON_BEHALF_OF) {
        BlockchainAddress targetAddress = BlockchainAddress.read(rpc);
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, targetAddress);
      } else if (invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        BlockchainAddress targetAddress = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        rpc.readLong(); // small memo
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, targetAddress);
      } else if (invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        BlockchainAddress targetAddress = BlockchainAddress.read(rpc);
        rpc.readLong(); // amount
        rpc.readString(); // large memo
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, targetAddress);
      } else {
        long amount = rpc.readLong();

        byte[] pluginStateUpdateRpc;
        if (invocationByte == DummyInvocations.ADD_MPC_TOKEN_BALANCE) {
          pluginStateUpdateRpc =
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.ADD_MPC_TOKEN_BALANCE);
                    stream.writeLong(amount);
                  });
        } else if (invocationByte == DummyInvocations.DEDUCT_MPC_TOKEN_BALANCE) {
          pluginStateUpdateRpc =
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.DEDUCT_MPC_TOKEN_BALANCE);
                    stream.writeLong(amount);
                  });
        } else if (invocationByte == DummyInvocations.BURN_MPC_TOKEN_BALANCE) {
          pluginStateUpdateRpc =
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.BURN_MPC_TOKENS);
                    stream.writeLong(amount);
                  });
        } else if (invocationByte == DummyInvocations.CREATE_VESTING_ACCOUNT) {
          pluginStateUpdateRpc =
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.CREATE_VESTING_ACCOUNT);
                    stream.writeLong(amount);
                  });
        } else { // invocationByte == DummyInvocations.DUMMY_ADD_COIN_BALANCE
          pluginStateUpdateRpc =
              SafeDataOutputStream.serialize(
                  stream -> stream.writeByte(DummyInvocations.ADD_COIN_BALANCE));
        }
        eventManager.updateLocalAccountPluginState(
            LocalPluginStateUpdate.create(context.getFrom(), pluginStateUpdateRpc));
      }
      return state;
    }

    private void invokeDelegateStakes(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        long amount,
        Hash transactionId) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              context.getFrom(),
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_DELEGATE_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RECEIVE_DELEGATED_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void invokeDelegateStakes(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        long amount,
        Hash transactionId,
        BlockchainAddress target) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              target,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_DELEGATE_STAKES);
                    stream.writeOptional(BlockchainAddress::write, context.getFrom());
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RECEIVE_DELEGATED_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void invokeDelegateStakesWithExpiration(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        long amount,
        long expirationTimestamp,
        Hash transactionId) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              context.getFrom(),
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_DELEGATE_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(
                        DummyInvocations.INIT_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                    stream.writeLong(expirationTimestamp);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void invokeDelegateStakesWithExpiration(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        long amount,
        long expirationTimestamp,
        Hash transactionId,
        BlockchainAddress target) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              target,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_DELEGATE_STAKES);
                    stream.writeOptional(BlockchainAddress::write, context.getFrom());
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(
                        DummyInvocations.INIT_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                    stream.writeLong(expirationTimestamp);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void invokeRetractDelegateStakes(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        long amount,
        Hash transactionId) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              context.getFrom(),
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RETRACT_DELEGATED_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RETURN_DELEGATED_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void invokeRetractDelegateStakesOnBehalfOf(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress target,
        BlockchainAddress recipient,
        long amount,
        Hash transactionId) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              target,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RETRACT_DELEGATED_STAKES);
                    stream.writeOptional(BlockchainAddress::write, context.getFrom());
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    recipient.write(stream);
                  })));

      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(DummyInvocations.INIT_RETURN_DELEGATED_STAKES);
                    transactionId.write(stream);
                    stream.writeLong(amount);
                    context.getFrom().write(stream);
                  })));

      addCallback(context, eventManager, recipient, transactionId);
    }

    private void addCallback(
        SysContractContext context,
        SystemEventCreator eventManager,
        BlockchainAddress recipient,
        Hash transactionId) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              context.getFrom(),
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.COMMIT_DELEGATE_STAKES);
                    transactionId.write(stream);
                  })));
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              SafeDataOutputStream.serialize(
                  stream -> {
                    stream.writeByte(AccountPluginInvocations.COMMIT_DELEGATE_STAKES);
                    transactionId.write(stream);
                  })));
    }
  }

  /** Test MPC token MPC20 contract. */
  public static final class TestMpcTokenMpc20Contract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      int invocationByte = rpc.readUnsignedByte(); // invocation byte
      SystemEventCreator eventManager = context.getInvocationCreator();

      if (invocationByte == MpcTokenMpc20Invocations.TRANSFER) {
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        Unsigned256.create(rpc.readBytes(16)); // amount as 128 bytes
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, null);
      } else if (invocationByte == MpcTokenMpc20Invocations.TRANSFER_FROM) {
        BlockchainAddress sender = BlockchainAddress.read(rpc);
        BlockchainAddress recipient = BlockchainAddress.read(rpc);
        Unsigned256.create(rpc.readBytes(16));
        Hash transferId = context.getCurrentTransactionHash();
        invokeCommit(context, eventManager, recipient, transferId, sender);
      }

      return state;
    }
  }

  /** Invoke commit on account-plugin. */
  public static void invokeCommit(
      SysContractContext context,
      SystemEventCreator eventManager,
      BlockchainAddress recipient,
      Hash transferId,
      BlockchainAddress target) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            target == null ? context.getFrom() : target,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(AccountPluginInvocations.COMMIT_TRANSFER);
                  transferId.write(stream);
                })));
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(AccountPluginInvocations.COMMIT_TRANSFER);
                  transferId.write(stream);
                })));
  }
}
