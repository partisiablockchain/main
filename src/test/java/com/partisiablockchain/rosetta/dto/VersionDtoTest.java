package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class VersionDtoTest {

  @Test
  void getNodeVersion() {
    String correctVersion = VersionDto.getNodeVersionFromProperties("/project.properties");
    Assertions.assertThat(correctVersion).isNotNull();
    Assertions.assertThat(correctVersion).isNotEqualTo("");
    Assertions.assertThat(correctVersion).isNotEqualTo("${project.version}");

    Assertions.assertThatThrownBy(
            () -> VersionDto.getNodeVersionFromProperties("project.properties"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to get version");
  }

  @Test
  void versionDefault() {
    VersionDto version = VersionDto.DEFAULT;
    String currentVersion = VersionDto.getNodeVersionFromProperties("/project.properties");
    Assertions.assertThat(version.rosettaVersion()).isEqualTo("1.4.12");
    Assertions.assertThat(version.nodeVersion()).isEqualTo(currentVersion);
  }
}
