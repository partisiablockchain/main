package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.rest.ObjectMapperProvider;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

/** Class for testing serialization and deserialization. */
public final class SerializeDeserializeTester {

  private static final ObjectMapper OBJECT_MAPPER =
      new ObjectMapperProvider()
          .getContext(SerializeDeserializeTester.class)
          .enable(SerializationFeature.INDENT_OUTPUT)
          .setDefaultPrettyPrinter(new GoogleStylePrettyPrinter());

  private static final String TEST_JSON_FOLDER = "dto-json";

  /**
   * Test that an object is serialized to the expected JSON and can be deserialized back to an
   * identical object.
   *
   * @param dto the object to test
   * @param dtoClass the class of the object to test
   * @param jsonFileName name of file containing expected json
   * @param <T> the type of the object
   */
  static <T> void test(T dto, Class<T> dtoClass, String jsonFileName) {
    String serialized = serialize(dto);
    String json = getJson(jsonFileName);
    assertThat(serialized).isEqualTo(json);
    T deserialized = deserialize(dtoClass, serialized);
    assertThat(deserialized).usingRecursiveComparison().isEqualTo(dto);
  }

  static String getJson(String jsonFileName) {
    ClassLoader classLoader = SerializeDeserializeTester.class.getClassLoader();
    String filePath = Paths.get(TEST_JSON_FOLDER, jsonFileName).toString();
    byte[] jsonBytes =
        ExceptionConverter.call(
            () -> classLoader.getResourceAsStream(filePath).readAllBytes(),
            "Could not read test json file");
    return new String(jsonBytes, StandardCharsets.UTF_8);
  }

  static <T> String serialize(T dto) {
    return ExceptionConverter.call(
        () -> OBJECT_MAPPER.writeValueAsString(dto), "Error in serialization");
  }

  private static <T> T deserialize(Class<T> dtoClass, String serialized) {
    return ExceptionConverter.call(
        () -> OBJECT_MAPPER.readValue(serialized, dtoClass), "Error in deserialization");
  }
}
