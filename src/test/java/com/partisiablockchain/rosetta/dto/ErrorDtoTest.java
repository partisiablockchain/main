package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ErrorDtoTest {

  @Test
  void uniqueErrors() {
    List<ErrorDto> declaredErrors = ErrorDto.errorDtos;
    assertThat(declaredErrors.stream().map(ErrorDto::getCode).distinct().toList())
        .describedAs("Duplicate error code")
        .hasSize(declaredErrors.size());
    assertThat(declaredErrors.stream().map(ErrorDto::getMessage).distinct().toList())
        .describedAs("Duplicate error message")
        .hasSize(declaredErrors.size());
  }

  @Test
  void createErrorDto() {
    ErrorDto error = ErrorDto.errorDtos.get(11);
    assertThat(error.getCode()).isEqualTo(103);
    assertThat(error.getMessage()).isEqualTo("Invalid invocation byte");
    assertThat(error.isRetriable()).isEqualTo(false);
    assertThat(ErrorDto.INVALID_HEX_BYTES.getCode()).isEqualTo(100);
  }

  @Test
  void allErrorsAreAllowed() {
    assertThat(AllowDto.ERRORS).isEqualTo(ErrorDto.errorDtos);
  }

  @Test
  void toStringTest() {
    assertThat(ErrorDto.UNKNOWN_OPERATION_TYPE.toString())
        .isEqualTo("ErrorDto{code=1, message='Unknown operation type', retriable=false}");
    assertThat(ErrorDto.UNKNOWN_OPERATION_TYPE.describe("description").toString())
        .isEqualTo(
            "ErrorDto{code=1, message='Unknown operation type', retriable=false,"
                + " description=description}");
  }
}
