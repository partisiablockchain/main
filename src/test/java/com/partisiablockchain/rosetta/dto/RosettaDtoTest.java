package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.rosetta.RosettaOnlineResource;
import com.partisiablockchain.rosetta.TestValues;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionRequest;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionResponse;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountRequest;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountResponse;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test serialization/deserialization of DTOs. */
public final class RosettaDtoTest {

  private static final NetworkIdentifierDto DUMMY_NETWORK_IDENTIFIER =
      new NetworkIdentifierDto("blockchain", "network", new SubNetworkIdentifierDto("subNetwork"));

  private static final AccountIdentifierDto DUMMY_ACCOUNT_IDENTIFIER =
      new AccountIdentifierDto(TestValues.ACCOUNT_ONE);

  private static final BlockIdentifierDto DUMMY_BLOCK_IDENTIFIER =
      new BlockIdentifierDto(1, TestValues.HASH_ONE);

  private static final PartialBlockIdentifierDto DUMMY_PARTIAL_BLOCK_IDENTIFIER =
      new PartialBlockIdentifierDto(1L, TestValues.HASH_ONE);

  private static final TransactionDto DUMMY_TRANSACTION =
      new TransactionDto(new TransactionIdentifierDto(TestValues.HASH_ONE), List.of(), null, null);

  private static final PublicKeyDto DUMMY_PUBLIC_KEY =
      new PublicKeyDto(TestValues.KEY_PAIR_ONE.getPublic().asBytes(), PublicKeyDto.CURVE_TYPE);

  @Test
  void accountBalanceRequestDto() {
    AccountBalanceRequestDto accountBalanceRequestDto =
        new AccountBalanceRequestDto(
            DUMMY_NETWORK_IDENTIFIER,
            DUMMY_ACCOUNT_IDENTIFIER,
            DUMMY_PARTIAL_BLOCK_IDENTIFIER,
            List.of(CurrencyDto.MPC));
    SerializeDeserializeTester.test(
        accountBalanceRequestDto, AccountBalanceRequestDto.class, "AccountBalanceRequestDto.json");
  }

  @Test
  void accountBalanceResponseDto() {
    AccountBalanceResponseDto accountBalanceResponseDto =
        new AccountBalanceResponseDto(
            DUMMY_BLOCK_IDENTIFIER, List.of(new AmountDto("1000", CurrencyDto.MPC)));
    SerializeDeserializeTester.test(
        accountBalanceResponseDto,
        AccountBalanceResponseDto.class,
        "AccountBalanceResponseDto.json");
  }

  @Test
  void accountIdentifierDto() {
    SerializeDeserializeTester.test(
        DUMMY_ACCOUNT_IDENTIFIER, AccountIdentifierDto.class, "AccountIdentifierDto.json");
  }

  @Test
  void allowDto() {
    SerializeDeserializeTester.test(AllowDto.DEFAULT, AllowDto.class, "AllowDto.json");
  }

  @Test
  void amountDto() {
    AmountDto amountDto = new AmountDto("1000", new CurrencyDto("symbol", 10));
    SerializeDeserializeTester.test(amountDto, AmountDto.class, "AmountDto.json");
  }

  @Test
  void balanceExemptionDto() {
    BalanceExemptionDto balanceExemption = new BalanceExemptionDto();
    SerializeDeserializeTester.test(
        balanceExemption, BalanceExemptionDto.class, "BalanceExemptionDto.json");
  }

  @Test
  void blockDto() {
    BlockIdentifierDto blockIdentifier = new BlockIdentifierDto(1, TestValues.HASH_ONE);
    BlockIdentifierDto parentBlockIdentifier = new BlockIdentifierDto(2, TestValues.HASH_TWO);
    BlockDto blockDto =
        new BlockDto(blockIdentifier, parentBlockIdentifier, 1, List.of(DUMMY_TRANSACTION));
    SerializeDeserializeTester.test(blockDto, BlockDto.class, "BlockDto.json");
  }

  @Test
  void blockIdentifierDto() {
    BlockIdentifierDto blockIdentifierDto = new BlockIdentifierDto(10, TestValues.HASH_ONE);
    SerializeDeserializeTester.test(
        blockIdentifierDto, BlockIdentifierDto.class, "BlockIdentifierDto.json");
  }

  @Test
  void blockRequestDto() {
    BlockRequestDto blockRequestDto =
        new BlockRequestDto(DUMMY_NETWORK_IDENTIFIER, new PartialBlockIdentifierDto(1L, null));
    SerializeDeserializeTester.test(blockRequestDto, BlockRequestDto.class, "BlockRequestDto.json");
  }

  @Test
  void blockResponseDto() {
    BlockIdentifierDto blockIdentifier = new BlockIdentifierDto(2, TestValues.HASH_TWO);
    BlockIdentifierDto parentBlockIdentifier = new BlockIdentifierDto(1, TestValues.HASH_ONE);
    BlockDto block = new BlockDto(blockIdentifier, parentBlockIdentifier, 123, List.of());
    BlockResponseDto blockResponseDto =
        new BlockResponseDto(block, List.of(new TransactionIdentifierDto(TestValues.HASH_ONE)));
    SerializeDeserializeTester.test(
        blockResponseDto, BlockResponseDto.class, "BlockResponseDto.json");
  }

  @Test
  void blockTransactionRequestDto() {
    BlockTransactionRequestDto blockTransactionRequestDto =
        new BlockTransactionRequestDto(
            DUMMY_NETWORK_IDENTIFIER,
            new BlockIdentifierDto(1, TestValues.HASH_ONE),
            new TransactionIdentifierDto(TestValues.HASH_TWO));
    SerializeDeserializeTester.test(
        blockTransactionRequestDto,
        BlockTransactionRequestDto.class,
        "BlockTransactionRequestDto.json");
  }

  @Test
  void blockTransactionResponseDto() {
    BlockTransactionResponse blockTransactionResponse =
        new BlockTransactionResponse(DUMMY_TRANSACTION);
    SerializeDeserializeTester.test(
        blockTransactionResponse, BlockTransactionResponse.class, "BlockTransactionResponse.json");
  }

  @Test
  void callRequest() {
    CallRequestDto callRequestDto =
        new CallRequestDto(
            DUMMY_NETWORK_IDENTIFIER,
            "get_shard_for_account",
            new GetShardForAccountRequest(DUMMY_ACCOUNT_IDENTIFIER.address()));
    SerializeDeserializeTester.test(callRequestDto, CallRequestDto.class, "CallRequest.json");
  }

  @Test
  void callResponse() {
    CallResponseDto callResponseDto =
        new CallResponseDto(
            new GetShardForAccountResponse(
                DUMMY_NETWORK_IDENTIFIER.subNetworkIdentifier().network()),
            false);
    String serialized = SerializeDeserializeTester.serialize(callResponseDto);
    String json = SerializeDeserializeTester.getJson("CallResponse.json");
    Assertions.assertThat(serialized).isEqualTo(json);
  }

  @Test
  void getShardForAccountCallRequest() {
    GetShardForAccountRequest getShardForAccountRequest =
        new GetShardForAccountRequest(DUMMY_ACCOUNT_IDENTIFIER.address());
    SerializeDeserializeTester.test(
        getShardForAccountRequest,
        GetShardForAccountRequest.class,
        "GetShardForAccountCallRequest.json");
  }

  @Test
  void getBlockFromTransactionCallRequest() {
    GetBlockFromTransactionRequest getBlockFromTransactionRequest =
        new GetBlockFromTransactionRequest(
            Hash.create(
                s -> {
                  s.writeLong(1);
                }));
    SerializeDeserializeTester.test(
        getBlockFromTransactionRequest,
        GetBlockFromTransactionRequest.class,
        "GetBlockFromTransactionCallRequest.json");
  }

  @Test
  void getShardForAccountCallResponse() {
    GetShardForAccountResponse getShardForAccountResponse = new GetShardForAccountResponse("Gov");
    SerializeDeserializeTester.test(
        getShardForAccountResponse,
        GetShardForAccountResponse.class,
        "GetShardForAccountCallResponse.json");
  }

  @Test
  void getBlockFromTransactionCallResponse() {
    GetBlockFromTransactionResponse getBlockFromTransactionResponse =
        new GetBlockFromTransactionResponse(
            1,
            Hash.create(
                s -> {
                  s.writeLong(1);
                }));
    SerializeDeserializeTester.test(
        getBlockFromTransactionResponse,
        GetBlockFromTransactionResponse.class,
        "GetBlockFromTransactionCallResponse.json");
  }

  @Test
  void coinIdentifierDto() {
    CoinIdentifierDto identifier = new CoinIdentifierDto("identifier");
    SerializeDeserializeTester.test(identifier, CoinIdentifierDto.class, "CoinIdentifierDto.json");
  }

  @Test
  void constructionCombineRequestDto() {
    ConstructionCombineRequestDto constructionCombineRequestDto =
        new ConstructionCombineRequestDto(DUMMY_NETWORK_IDENTIFIER, new byte[] {123}, List.of());
    SerializeDeserializeTester.test(
        constructionCombineRequestDto,
        ConstructionCombineRequestDto.class,
        "ConstructionCombineRequestDto.json");
  }

  @Test
  void constructionCombineResponseDto() {
    ConstructionCombineResponseDto constructionCombineResponseDto =
        new ConstructionCombineResponseDto(new byte[] {123});
    SerializeDeserializeTester.test(
        constructionCombineResponseDto,
        ConstructionCombineResponseDto.class,
        "ConstructionCombineResponseDto.json");
  }

  @Test
  void constructionDeriveRequestDto() {
    ConstructionDeriveRequestDto constructionDeriveRequestDto =
        new ConstructionDeriveRequestDto(DUMMY_NETWORK_IDENTIFIER, DUMMY_PUBLIC_KEY);
    SerializeDeserializeTester.test(
        constructionDeriveRequestDto,
        ConstructionDeriveRequestDto.class,
        "ConstructionDeriveRequestDto.json");
  }

  @Test
  void constructionDeriveResponseDto() {
    ConstructionDeriveResponseDto constructionDeriveResponseDto =
        new ConstructionDeriveResponseDto(new AccountIdentifierDto(TestValues.ACCOUNT_ONE));
    SerializeDeserializeTester.test(
        constructionDeriveResponseDto,
        ConstructionDeriveResponseDto.class,
        "ConstructionDeriveResponseDto.json");
  }

  @Test
  void constructionHashRequestDto() {
    ConstructionHashRequestDto constructionHashRequestDto =
        new ConstructionHashRequestDto(DUMMY_NETWORK_IDENTIFIER, new byte[] {123});
    SerializeDeserializeTester.test(
        constructionHashRequestDto,
        ConstructionHashRequestDto.class,
        "ConstructionHashRequestDto.json");
  }

  @Test
  void constructionMetadataDto() {
    ConstructionMetadataResponseDto constructionMetadataResponse =
        new ConstructionMetadataResponseDto(
            new ConstructionMetadataResponseDto.Metadata(1, 2, "this is a memo"));
    SerializeDeserializeTester.test(
        constructionMetadataResponse,
        ConstructionMetadataResponseDto.class,
        "ConstructionMetadataResponseDto.json");
  }

  @Test
  void constructionMetadataRequestDto() {
    BlockchainAddress address =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    ConstructionOptionsDto constructionPayloadInformation =
        new ConstructionOptionsDto(address, "this is a memo");
    ConstructionMetadataRequestDto constructionMetadataRequest =
        new ConstructionMetadataRequestDto(
            DUMMY_NETWORK_IDENTIFIER, constructionPayloadInformation);
    SerializeDeserializeTester.test(
        constructionMetadataRequest,
        ConstructionMetadataRequestDto.class,
        "ConstructionMetadataRequestDto.json");
  }

  @Test
  void constructionMetadataResponseDto() {
    ConstructionMetadataResponseDto constructionMetadataResponseDto =
        new ConstructionMetadataResponseDto(
            new ConstructionMetadataResponseDto.Metadata(1, 2, "this is a memo"));
    SerializeDeserializeTester.test(
        constructionMetadataResponseDto,
        ConstructionMetadataResponseDto.class,
        "ConstructionMetadataResponseDto.json");
  }

  @Test
  void constructionParseRequestDto() {
    ConstructionParseRequestDto constructionParseRequestDto =
        new ConstructionParseRequestDto(DUMMY_NETWORK_IDENTIFIER, true, new byte[] {123});
    SerializeDeserializeTester.test(
        constructionParseRequestDto,
        ConstructionParseRequestDto.class,
        "ConstructionParseRequestDto.json");
  }

  @Test
  void constructionParseResponseDto() {
    ConstructionParseResponseDto constructionParseResponseDto =
        new ConstructionParseResponseDto(
            List.of(
                new OperationDto(
                    new OperationIdentifierDto(1),
                    OperationDto.Type.ADD,
                    DUMMY_ACCOUNT_IDENTIFIER,
                    new AmountDto("1000", CurrencyDto.MPC),
                    OperationDto.Status.SUCCESS,
                    null)),
            null);
    SerializeDeserializeTester.test(
        constructionParseResponseDto,
        ConstructionParseResponseDto.class,
        "ConstructionParseResponseDto.json");
  }

  @Test
  void constructionPayloadInformationDto() {
    ConstructionOptionsDto constructionOptionsDto =
        new ConstructionOptionsDto(TestValues.ACCOUNT_ONE, "this is a memo");
    SerializeDeserializeTester.test(
        constructionOptionsDto,
        ConstructionOptionsDto.class,
        "ConstructionPayloadInformationDto.json");
  }

  @Test
  void constructionPreprocessRequestDto() {
    ConstructionPreprocessRequestDto constructionPreprocessRequest =
        new ConstructionPreprocessRequestDto(
            DUMMY_NETWORK_IDENTIFIER,
            List.of(),
            new ConstructionPreprocessRequestDto.Metadata("this is a memo"));
    SerializeDeserializeTester.test(
        constructionPreprocessRequest,
        ConstructionPreprocessRequestDto.class,
        "ConstructionPreprocessRequestDto.json");
  }

  @Test
  void constructionPreprocessResponseDto() {
    ConstructionPreprocessResponseDto constructionPreprocessResponseDto =
        new ConstructionPreprocessResponseDto(
            new ConstructionOptionsDto(TestValues.CONTRACT_ONE, "this is a memo"));
    SerializeDeserializeTester.test(
        constructionPreprocessResponseDto,
        ConstructionPreprocessResponseDto.class,
        "ConstructionPreprocessResponseDto.json");
  }

  @Test
  void currencyDto() {
    SerializeDeserializeTester.test(CurrencyDto.MPC, CurrencyDto.class, "CurrencyDto.json");
  }

  @Test
  void dummyHashesDto() {
    DummyHashesDto transactionIdentifierDto =
        new DummyHashesDto(List.of(TestValues.HASH_ONE, TestValues.HASH_TWO));
    SerializeDeserializeTester.test(
        transactionIdentifierDto, DummyHashesDto.class, "DummyHashesDto.json");
  }

  @Test
  void mempoolTransactionRequestDto() {
    MempoolTransactionRequestDto mempoolTransactionRequestDto =
        new MempoolTransactionRequestDto(
            DUMMY_NETWORK_IDENTIFIER, new TransactionIdentifierDto(TestValues.HASH_ONE));
    SerializeDeserializeTester.test(
        mempoolTransactionRequestDto,
        MempoolTransactionRequestDto.class,
        "MempoolTransactionRequestDto.json");
  }

  @Test
  void mempoolTransactionResponseDto() {
    MempoolTransactionResponseDto mempoolTransactionResponseDto =
        new MempoolTransactionResponseDto(DUMMY_TRANSACTION);
    SerializeDeserializeTester.test(
        mempoolTransactionResponseDto,
        MempoolTransactionResponseDto.class,
        "MempoolTransactionResponseDto.json");
  }

  @Test
  void networkIdentifierDto() {
    String subNetworkIdentifier = "Shard1";
    SubNetworkIdentifierDto subNetworkIdentifierDto =
        new SubNetworkIdentifierDto(subNetworkIdentifier);
    String network = "Partisia Blockchain";
    NetworkIdentifierDto networkIdentifierDto =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN, network, subNetworkIdentifierDto);
    SerializeDeserializeTester.test(
        networkIdentifierDto, NetworkIdentifierDto.class, "NetworkIdentifierDto.json");
  }

  @Test
  void networkListResponseDto() {
    NetworkListResponseDto networkListResponseDto =
        new NetworkListResponseDto(List.of(DUMMY_NETWORK_IDENTIFIER));
    SerializeDeserializeTester.test(
        networkListResponseDto, NetworkListResponseDto.class, "NetworkListResponseDto.json");
  }

  @Test
  void networkOptionsResponseDto() {
    AllowDto allow =
        new AllowDto(List.of(), List.of(), List.of(), List.of(), false, List.of(), false);
    VersionDto version = new VersionDto("0.0.0", "0.0.0");
    NetworkOptionsResponseDto networkOptionsResponseDto =
        new NetworkOptionsResponseDto(version, allow);
    SerializeDeserializeTester.test(
        networkOptionsResponseDto,
        NetworkOptionsResponseDto.class,
        "NetworkOptionsResponseDto.json");
  }

  @Test
  void networkRequestDto() {
    NetworkRequestDto networkRequestDto = new NetworkRequestDto(DUMMY_NETWORK_IDENTIFIER);
    SerializeDeserializeTester.test(
        networkRequestDto, NetworkRequestDto.class, "NetworkRequestDto.json");
  }

  @Test
  void networkStatusResponseDto() {
    BlockIdentifierDto currentBlockIdentifier = new BlockIdentifierDto(1, TestValues.HASH_TWO);
    BlockIdentifierDto genesisBlockIdentifier = new BlockIdentifierDto(0, TestValues.HASH_ONE);
    NetworkStatusResponseDto networkStatusResponseDto =
        new NetworkStatusResponseDto(
            currentBlockIdentifier,
            0L,
            genesisBlockIdentifier,
            genesisBlockIdentifier,
            NetworkStatusResponseDto.PEERS);
    SerializeDeserializeTester.test(
        networkStatusResponseDto, NetworkStatusResponseDto.class, "NetworkStatusResponseDto.json");
  }

  @Test
  void operationDto() {
    OperationDto operationDto =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            new AccountIdentifierDto(TestValues.ACCOUNT_ONE),
            new AmountDto("100", new CurrencyDto("MPC", 10)),
            OperationDto.Status.SUCCESS,
            new OperationDto.Metadata(
                "this is a memo", new AccountIdentifierDto(TestValues.ACCOUNT_TWO)));
    SerializeDeserializeTester.test(operationDto, OperationDto.class, "OperationDto.json");
  }

  @Test
  void operationIdentifierDto() {
    OperationIdentifierDto operationIdentifier = new OperationIdentifierDto(10);
    SerializeDeserializeTester.test(
        operationIdentifier, OperationIdentifierDto.class, "OperationIdentifierDto.json");
  }

  @Test
  void operationStatusDto() {
    OperationStatusDto operationStatusDto = new OperationStatusDto("status", true);
    SerializeDeserializeTester.test(
        operationStatusDto, OperationStatusDto.class, "OperationStatusDto.json");
  }

  @Test
  void partialBlockIdentifierDto() {
    SerializeDeserializeTester.test(
        new PartialBlockIdentifierDto(null, TestValues.HASH_ONE),
        PartialBlockIdentifierDto.class,
        "PartialBlockIdentifierDto.json");
  }

  @Test
  void peerDto() {
    PeerDto peerDto = new PeerDto("peerId");
    SerializeDeserializeTester.test(peerDto, PeerDto.class, "PeerDto.json");
  }

  @Test
  void publicKeyDto() {
    byte[] bytes = new KeyPair(BigInteger.TWO).getPublic().asBytes();
    PublicKeyDto publicKeyDto = new PublicKeyDto(bytes, PublicKeyDto.CURVE_TYPE);
    SerializeDeserializeTester.test(publicKeyDto, PublicKeyDto.class, "PublicKeyDto.json");
  }

  @Test
  void signatureDto() {
    SignatureDto signatureDto =
        new SignatureDto(
            new SigningPayloadDto(
                new byte[] {123},
                new AccountIdentifierDto(TestValues.ACCOUNT_ONE),
                null,
                SignatureDto.SIGNATURE_TYPE),
            DUMMY_PUBLIC_KEY,
            SignatureDto.SIGNATURE_TYPE,
            new byte[] {123});
    SerializeDeserializeTester.test(signatureDto, SignatureDto.class, "SignatureDto.json");
  }

  @Test
  void signingPayloadDto() {
    SigningPayloadDto signingPayloadDto =
        new SigningPayloadDto(
            new byte[1],
            new AccountIdentifierDto(TestValues.ACCOUNT_ONE),
            TestValues.ACCOUNT_ONE,
            SignatureDto.SIGNATURE_TYPE);
    Assertions.assertThat(signingPayloadDto.address()).isEqualTo(TestValues.ACCOUNT_ONE);
  }

  @Test
  void subNetworkIdentifierDto() {
    SubNetworkIdentifierDto subNetworkIdentifierDto = new SubNetworkIdentifierDto("network");
    SerializeDeserializeTester.test(
        subNetworkIdentifierDto, SubNetworkIdentifierDto.class, "SubNetworkIdentifierDto.json");
  }

  @Test
  void transactionDto() {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationIdentifierDto operationIdentifier = new OperationIdentifierDto(0);
    AmountDto amount = new AmountDto("100", CurrencyDto.MPC);
    OperationDto operationDto =
        new OperationDto(
            operationIdentifier,
            OperationDto.Type.ADD,
            recipient,
            amount,
            OperationDto.Status.SUCCESS,
            null);
    TransactionDto transactionDto =
        new TransactionDto(
            new TransactionIdentifierDto(TestValues.HASH_ONE), List.of(operationDto), null, null);
    SerializeDeserializeTester.test(transactionDto, TransactionDto.class, "TransactionDto.json");
  }

  @Test
  void relatedTransactionDto() {
    RelatedTransactionDto relatedTransactionDto =
        new RelatedTransactionDto(
            DUMMY_NETWORK_IDENTIFIER,
            new TransactionIdentifierDto(TestValues.HASH_ONE),
            RelatedTransactionDto.Direction.backward);
    SerializeDeserializeTester.test(
        relatedTransactionDto, RelatedTransactionDto.class, "RelatedTransactionDto.json");
  }

  @Test
  void transactionIdentifierDto() {
    TransactionIdentifierDto transactionIdentifierDto =
        new TransactionIdentifierDto(TestValues.HASH_ONE);
    SerializeDeserializeTester.test(
        transactionIdentifierDto, TransactionIdentifierDto.class, "TransactionIdentifierDto.json");
  }

  @Test
  void versionDto() {
    SerializeDeserializeTester.test(
        new VersionDto(VersionDto.ROSETTA_VERSION, "3.0.20"), VersionDto.class, "VersionDto.json");
  }
}
