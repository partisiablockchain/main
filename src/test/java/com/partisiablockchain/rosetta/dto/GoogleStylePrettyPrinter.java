package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import java.io.IOException;

/** Custom pretty printer matching GoogleStyle formatting of JSON files. */
@SuppressWarnings("serial")
public final class GoogleStylePrettyPrinter extends DefaultPrettyPrinter {

  @Override
  public DefaultPrettyPrinter createInstance() {
    DefaultPrettyPrinter printer = new GoogleStylePrettyPrinter();
    DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter();
    printer.indentObjectsWith(indenter);
    printer.indentArraysWith(indenter);
    return printer;
  }

  @Override
  public void writeObjectFieldValueSeparator(JsonGenerator g) throws IOException {
    g.writeRaw(": ");
  }

  @Override
  public void writeEndArray(JsonGenerator g, int nrOfValues) throws IOException {
    if (!_arrayIndenter.isInline()) {
      --_nesting;
    }
    if (nrOfValues > 0) {
      _arrayIndenter.writeIndentation(g, _nesting);
    }
    g.writeRaw("]");
  }

  @Override
  public void writeEndObject(JsonGenerator jg, int nrOfEntries) throws IOException {
    if (!_objectIndenter.isInline()) {
      --_nesting;
    }
    if (nrOfEntries > 0) {
      _objectIndenter.writeIndentation(jg, _nesting);
    }
    jg.writeRaw('}');
  }
}
