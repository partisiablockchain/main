package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.Transaction;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.RelatedTransactionDto;
import com.partisiablockchain.rosetta.dto.TransactionDto;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Tests related to the {@link TransactionParser}. */
public final class TransactionParserTest extends CloseableTest {

  private ShardedBlockchain ledgers;

  @BeforeEach
  void setup() {
    List<String> shards = new ArrayList<>();
    shards.add(null);
    shards.add("Shard1");
    ledgers = new ShardedBlockchain(RosettaTestHelper.createLedgers(shards, this::register));
  }

  @Test
  void parse_unknownTransaction() {
    Hash transactionHash = Hash.create(h -> h.writeString("UNKNOWN"));
    RosettaTestHelper.checkError(
        () -> TransactionParser.parse(transactionHash, "Gov", ledgers),
        ErrorDto.INVALID_TRANSACTION);
  }

  @Test
  void parse_allAccountPluginInvocations() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");

    List<Integer> dummyInvocations =
        List.of(
            RosettaTestHelper.DummyInvocations.ADD_COIN_BALANCE,
            RosettaTestHelper.DummyInvocations.ADD_MPC_TOKEN_BALANCE,
            RosettaTestHelper.DummyInvocations.DEDUCT_MPC_TOKEN_BALANCE,
            RosettaTestHelper.DummyInvocations.BURN_MPC_TOKEN_BALANCE,
            RosettaTestHelper.DummyInvocations.CREATE_VESTING_ACCOUNT);

    ArrayList<SignedTransaction> transactions = new ArrayList<>();
    for (Integer invocation : dummyInvocations) {
      SignedTransaction interact =
          BlockchainLedgerHelper.createInteractWithContractTransaction(
              shardOneLedger,
              BetanetAddresses.MPC_TOKEN_CONTRACT,
              TestValues.KEY_PAIR_ONE,
              s -> {
                s.writeByte(invocation);
                s.writeLong(1000);
              });

      boolean valid = shardOneLedger.addPendingTransaction(interact);
      assertThat(valid).isTrue();
      transactions.add(interact);
    }

    BlockchainLedgerHelper.appendBlock(shardOneLedger, transactions, List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    List<OperationDto> operations = parseOperationsFromLatestExecutedState(shardOneLedger);
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    assertThat(operations).hasSize(4);
    List<OperationDto.Type> operationTypes =
        operations.stream().map(OperationDto::type).collect(Collectors.toList());
    assertThat(operationTypes)
        .containsExactlyInAnyOrder(
            OperationDto.Type.ADD,
            OperationDto.Type.DEDUCT,
            OperationDto.Type.BURN,
            OperationDto.Type.VEST);
    OperationDto deductOperation = operations.get(1);
    assertThat(deductOperation.type()).isEqualTo(OperationDto.Type.DEDUCT);
    assertThat(deductOperation.amount().value()).isEqualTo(Long.toString(-1000));
    OperationDto burnOperation = operations.get(2);
    assertThat(burnOperation.type()).isEqualTo(OperationDto.Type.BURN);
    assertThat(burnOperation.amount().value()).isEqualTo(Long.toString(-1000));
  }

  @Test
  void parse_transfer() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, amount);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);

    ensureTransferParsedCorrect(
        operations,
        TestValues.KEY_PAIR_ONE.getPublic().createAddress(),
        TestValues.ACCOUNT_ONE,
        amount);
  }

  @Test
  void parse_transfer_mpc20() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createTransferWithMpc20(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, amount);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);

    ensureTransferParsedCorrect(
        operations,
        TestValues.KEY_PAIR_ONE.getPublic().createAddress(),
        TestValues.ACCOUNT_ONE,
        amount);
  }

  @Test
  void parse_transferFrom_mpc20() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createTransferFromWithMpc20(
            shardOneLedger,
            TestValues.KEY_PAIR_ONE,
            TestValues.ACCOUNT_ONE,
            TestValues.ACCOUNT_TWO,
            amount);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);

    ensureTransferParsedCorrect(operations, TestValues.ACCOUNT_ONE, TestValues.ACCOUNT_TWO, amount);
  }

  @Test
  void parse_transferOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress targetAccount = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;

    SignedTransaction interact =
        RosettaTestHelper.createTransferOnBehalfOf(
            shardOneLedger, custodian, targetAccount, recipient, amount);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);

    ensureTransferParsedCorrect(operations, TestValues.ACCOUNT_TWO, TestValues.ACCOUNT_ONE, amount);
  }

  private void ensureTransferParsedCorrect(
      List<OperationDto> operations,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      long amount) {
    OperationDto deduct = operations.get(1);
    assertThat(deduct.type()).isEqualTo(OperationDto.Type.DEDUCT);
    assertThat(deduct.amount().value()).isEqualTo(Long.toString(-amount));
    assertThat(deduct.account().address()).isEqualTo(sender);

    OperationDto add = operations.get(0);
    assertThat(add.type()).isEqualTo(OperationDto.Type.ADD);
    assertThat(add.amount().value()).isEqualTo(Long.toString(amount));
    assertThat(add.account().address()).isEqualTo(receiver);
  }

  @Test
  void parse_transferSmallMemoOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress targetAddress = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;
    int smallMemo = 123;

    SignedTransaction interact =
        RosettaTestHelper.createTransferSmallMemoOnBehalfOf(
            shardOneLedger, custodian, targetAddress, recipient, amount, smallMemo);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    OperationDto deduct = operations.get(1);
    assertThat(deduct.type()).isEqualTo(OperationDto.Type.DEDUCT);
    assertThat(deduct.metadata().memo()).isEqualTo(String.valueOf(smallMemo));
    assertThat(deduct.amount().value()).isEqualTo(Long.toString(-amount));
    assertThat(deduct.account().address()).isEqualTo(TestValues.ACCOUNT_TWO);

    OperationDto add = operations.get(0);
    assertThat(add.type()).isEqualTo(OperationDto.Type.ADD);
    assertThat(add.metadata().memo()).isEqualTo(String.valueOf(smallMemo));
    assertThat(add.amount().value()).isEqualTo(Long.toString(amount));
    assertThat(add.account().address()).isEqualTo(TestValues.ACCOUNT_ONE);
  }

  @Test
  void parse_transferLargeMemoOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress targetAddress = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;
    String largeMemo = "this is a memo";

    SignedTransaction interact =
        RosettaTestHelper.createTransferLargeMemoOnBehalfOf(
            shardOneLedger, custodian, targetAddress, recipient, amount, largeMemo);

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    OperationDto deduct = operations.get(1);
    assertThat(deduct.type()).isEqualTo(OperationDto.Type.DEDUCT);
    assertThat(deduct.metadata().memo()).isEqualTo(largeMemo);
    assertThat(deduct.amount().value()).isEqualTo(Long.toString(-amount));
    assertThat(deduct.account().address()).isEqualTo(TestValues.ACCOUNT_TWO);

    OperationDto add = operations.get(0);
    assertThat(add.type()).isEqualTo(OperationDto.Type.ADD);
    assertThat(add.metadata().memo()).isEqualTo(largeMemo);
    assertThat(add.amount().value()).isEqualTo(Long.toString(amount));
    assertThat(add.account().address()).isEqualTo(TestValues.ACCOUNT_ONE);
  }

  @Test
  void parseDelegateTransfer() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createDelegateTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, amount);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    checkDelegationOperations(
        operations,
        amount,
        TestValues.ACCOUNT_ONE,
        TestValues.KEY_PAIR_ONE.getPublic().createAddress());
  }

  @Test
  @DisplayName("Parsing a delegation on behalf of, generates expected operations.")
  void parseDelegateTransferOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress target = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;

    SignedTransaction interact =
        RosettaTestHelper.createDelegateTransferOnBehalfOf(
            shardOneLedger, custodian, target, recipient, amount);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    checkDelegationOperations(operations, amount, recipient, target);
  }

  @Test
  @DisplayName("Parsing a delegation with an expiration timestamp, generates expected operations.")
  void parseDelegateTransferWithExpiration() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;
    long expirationTimestamp = 1000L;

    SignedTransaction interact =
        RosettaTestHelper.createDelegateTransferWithExpiration(
            shardOneLedger,
            TestValues.KEY_PAIR_ONE,
            TestValues.ACCOUNT_ONE,
            amount,
            expirationTimestamp);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    checkDelegationOperations(
        operations,
        amount,
        TestValues.ACCOUNT_ONE,
        TestValues.KEY_PAIR_ONE.getPublic().createAddress());
  }

  @Test
  @DisplayName(
      "Parsing a delegation with an expiration timestamp on behalf of, generates expected"
          + " operations.")
  void parseDelegateTransferWithExpirationOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;
    long expirationTimestamp = 1000L;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress target = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;
    SignedTransaction interact =
        RosettaTestHelper.createDelegateTransferWithExpirationOnBehalfOf(
            shardOneLedger, custodian, target, recipient, amount, expirationTimestamp);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    checkDelegationOperations(operations, amount, recipient, target);
  }

  private static void checkDelegationOperations(
      List<OperationDto> operations,
      int receiverAmount,
      BlockchainAddress expectedReceiver,
      BlockchainAddress expectedSender) {
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    OperationDto sender = operations.get(0);
    assertThat(sender.type()).isEqualTo(OperationDto.Type.DELEGATE);
    assertThat(sender.account().address()).isEqualTo(expectedSender);
    assertThat(sender.amount().value()).isEqualTo(Long.toString(-receiverAmount));

    OperationDto receiver = operations.get(1);
    assertThat(receiver.type()).isEqualTo(OperationDto.Type.RECEIVE);
    assertThat(receiver.account().address()).isEqualTo(expectedReceiver);
    assertThat(receiver.amount().value()).isEqualTo(Long.toString(receiverAmount));
  }

  private List<OperationDto> getOperationDtos(
      BlockchainLedger shardOneLedger, SignedTransaction interact) {
    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(interact), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    List<OperationDto> operations = parseOperationsFromLatestExecutedState(shardOneLedger);

    assertThat(operations).hasSize(2);
    return operations;
  }

  @Test
  void parseRetractDelegateTransfer() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createDelegateRetract(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, amount);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    OperationDto add = operations.get(0);
    assertThat(add.type()).isEqualTo(OperationDto.Type.RETRACT);
    assertThat(add.account().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE.getPublic().createAddress());
    assertThat(add.amount().value()).isEqualTo(Long.toString(amount));

    OperationDto deduct = operations.get(1);
    assertThat(deduct.type()).isEqualTo(OperationDto.Type.RETURN);
    assertThat(deduct.account().address()).isEqualTo(TestValues.ACCOUNT_ONE);
    assertThat(deduct.amount().value()).isEqualTo(Long.toString(-amount));
  }

  @Test
  @DisplayName("Parsing retract delegation on behalf of, generates expected operations.")
  void parseRetractDelegateTransferOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress target = TestValues.ACCOUNT_TWO;
    BlockchainAddress recipient = TestValues.ACCOUNT_ONE;

    SignedTransaction interact =
        RosettaTestHelper.createDelegateRetractOnBehalfOf(
            shardOneLedger, custodian, target, recipient, amount);
    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    List<OperationDto> operations = getOperationDtos(shardOneLedger, interact);
    assertThat(operations.stream().map(OperationDto::status))
        .containsOnly(OperationDto.Status.SUCCESS);

    OperationDto deduct = operations.get(0);
    assertThat(deduct.type()).isEqualTo(OperationDto.Type.RETRACT);
    assertThat(deduct.account().address()).isEqualTo(target);
    assertThat(deduct.amount().value()).isEqualTo(Long.toString(amount));

    OperationDto add = operations.get(1);
    assertThat(add.type()).isEqualTo(OperationDto.Type.RETURN);
    assertThat(add.account().address()).isEqualTo(TestValues.ACCOUNT_ONE);
    assertThat(add.amount().value()).isEqualTo(Long.toString(-amount));
  }

  @Test
  void parseTransferByoc() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");
    int amount = 1000;

    SignedTransaction interact =
        RosettaTestHelper.createByocTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, amount, "MPC_ROPSTEN");

    boolean valid = shardOneLedger.addPendingTransaction(interact);
    assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(interact), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    List<OperationDto> operations = parseOperationsFromLatestExecutedState(shardOneLedger);

    assertThat(operations).hasSize(0);
  }

  @Test
  void parse_unknownInvocation_mpc20() {
    byte[] rpc = SafeDataOutputStream.serialize(s -> s.writeByte(123));

    Transaction transaction =
        InteractWithContractTransaction.create(BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT, rpc);
    EventTransaction eventTransaction =
        EventTransaction.createStandalone(
            TestValues.HASH_ONE,
            BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
            0,
            transaction,
            new ShardRoute(null, 0),
            0,
            0);
    ExecutableEvent executableEvent = new ExecutableEvent(null, eventTransaction);
    ExecutedTransaction commitExecutedTransaction =
        ExecutedTransaction.create(null, executableEvent, true, null);
    TransactionParser.TransactionSearcher txSearcher = (h) -> commitExecutedTransaction;

    OperationDto nonExistentOperation =
        TransactionParser.handleCommitTransferInvocation(
            null, txSearcher, TestValues.ACCOUNT_ONE, true);
    assertThat(nonExistentOperation).isNull();
  }

  @Test
  void parse_isolatedCommitEvent() {
    int amount = 1000;
    byte[] rpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(MpcTokenInvocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(s);
              s.writeLong(amount);
            });
    parseTransferEvent(rpc, BetanetAddresses.MPC_TOKEN_CONTRACT);
  }

  @Test
  void parse_isolatedCommitEvent_transferSmallMemo() {
    int amount = 1000;
    byte[] rpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO);
              TestValues.ACCOUNT_ONE.write(s);
              s.writeLong(amount);
              s.writeLong(99);
            });
    parseTransferEvent(rpc, BetanetAddresses.MPC_TOKEN_CONTRACT);
  }

  @Test
  void parse_isolatedCommitEvent_transferLargeMemo() {
    int amount = 1000;
    byte[] rpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO);
              TestValues.ACCOUNT_ONE.write(s);
              s.writeLong(amount);
              s.writeString("Memo");
            });
    parseTransferEvent(rpc, BetanetAddresses.MPC_TOKEN_CONTRACT);
  }

  @Test
  public void returnNullIfNotMpcTokenContract() {
    InteractWithContractTransaction interaction =
        InteractWithContractTransaction.create(TestValues.CONTRACT_MINT, new byte[0]);
    EventTransaction eventTransaction =
        EventTransaction.createStandalone(
            TestValues.HASH_THREE,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            0,
            interaction,
            new ShardRoute(null, 0),
            0,
            0);
    ExecutableEvent executableEvent = new ExecutableEvent(null, eventTransaction);
    ExecutedTransaction commitExecutedTransaction =
        ExecutedTransaction.create(null, executableEvent, true, null);
    TransactionParser.TransactionSearcher transactionSearcher = (h) -> commitExecutedTransaction;
    assertThat(
            TransactionParser.handleCommitTransferInvocation(
                null, transactionSearcher, TestValues.ACCOUNT_ONE, true))
        .isNull();
    ExecutedTransaction signedExecutedTransaction =
        ExecutedTransaction.create(
            null,
            SignedTransaction.create(CoreTransactionPart.create(1, 20000L, 0L), interaction)
                .sign(TestValues.KEY_PAIR_ONE, ""),
            true,
            null);
    assertThat(
            TransactionParser.handleCommitTransferInvocation(
                null, (h) -> signedExecutedTransaction, TestValues.ACCOUNT_ONE, true))
        .isNull();
  }

  private static void parseTransferEvent(byte[] rpc, BlockchainAddress tokenContract) {
    Transaction interact = InteractWithContractTransaction.create(tokenContract, rpc);
    EventTransaction eventTransaction =
        EventTransaction.createStandalone(
            TestValues.HASH_ONE, tokenContract, 0, interact, new ShardRoute(null, 0), 0, 0);
    ExecutableEvent executableEvent = new ExecutableEvent(null, eventTransaction);
    ExecutedTransaction commitExecutedTransaction =
        ExecutedTransaction.create(null, executableEvent, true, null);
    TransactionParser.TransactionSearcher txSearcher = (h) -> commitExecutedTransaction;

    OperationDto recipientMatch =
        TransactionParser.handleCommitTransferInvocation(
            null, txSearcher, TestValues.ACCOUNT_ONE, true);
    assertThat(recipientMatch.type()).isEqualTo(OperationDto.Type.ADD);

    OperationDto recipientMismatch =
        TransactionParser.handleCommitTransferInvocation(
            null, txSearcher, TestValues.ACCOUNT_TWO, true);
    assertThat(recipientMismatch.type()).isEqualTo(OperationDto.Type.DEDUCT);
  }

  private List<OperationDto> parseOperationsFromLatestExecutedState(
      BlockchainLedger shardOneLedger) {
    List<OperationDto> operations = new ArrayList<>();
    ExecutedState finalState = shardOneLedger.getChainState().getExecutedState();
    for (Hash transactionHash : finalState.getSpawnedEvents().keySet()) {
      TransactionDto parse = TransactionParser.parse(transactionHash, "Shard1", ledgers);
      List<RelatedTransactionDto> relatedTransactionDtos = parse.relatedTransactions();
      if (!relatedTransactionDtos.isEmpty()) {
        for (RelatedTransactionDto relatedTransactionDto : relatedTransactionDtos) {
          Hash hash = relatedTransactionDto.transactionIdentifier().hash();
          EventTransaction eventTransaction = shardOneLedger.getEventTransaction(hash).getEvent();
          String shard = eventTransaction.getDestinationShard();
          if (shard.equals("Shard1")) {
            assertThat(relatedTransactionDto.networkIdentifier()).isNull();
          } else {
            assertThat(relatedTransactionDto.networkIdentifier()).isNotNull();
          }
          assertThat(relatedTransactionDto.direction())
              .isEqualTo(RelatedTransactionDto.Direction.forward);
        }
      }
      operations.addAll(parse.operations());
    }
    operations.sort(Comparator.comparing(OperationDto::type));
    return operations;
  }

  @Test
  void findExecutedTransaction() {
    Hash unknownTxIdentifier = Hash.create(h -> h.writeString("UNKNOWN"));
    RosettaTestHelper.checkError(
        () -> TransactionParser.findTransactionForContract(unknownTxIdentifier, ledgers),
        ErrorDto.UNABLE_TO_FIND_TRANSACTION);
  }

  @Test
  void tryParseLocalPluginStateUpdate_globalPluginStateUpdate() {
    InnerSystemEvent.UpdateGlobalPluginStateEvent updateGlobalPluginStateEvent =
        new InnerSystemEvent.UpdateGlobalPluginStateEvent(
            ChainPluginType.ACCOUNT, GlobalPluginStateUpdate.create(new byte[0]));
    EventTransaction eventTransaction =
        new EventTransaction(null, null, 0, 0, 0, null, updateGlobalPluginStateEvent);
    ExecutableEvent executableEvent = new ExecutableEvent("originShard", eventTransaction);
    LocalPluginStateUpdate localPluginStateUpdate =
        TransactionParser.tryParseLocalPluginStateUpdate(executableEvent);
    assertThat(localPluginStateUpdate).isNull();
  }

  /** If the executed transaction has failed the operation will have status of failed. */
  @Test
  public void operationStatusFailedIfTransactionNotSuccessful() {
    BlockchainLedger shardOneLedger = ledgers.getShard("Shard1");

    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shardOneLedger,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            s -> {
              s.writeByte(RosettaTestHelper.DummyInvocations.DEDUCT_MPC_TOKEN_BALANCE);
              s.writeLong(-1000);
            });

    boolean valid = shardOneLedger.addPendingTransaction(transaction);
    assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transaction), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    List<OperationDto> operations = parseOperationsFromLatestExecutedState(shardOneLedger);
    assertThat(operations.stream().map(OperationDto::status))
        .containsExactly(OperationDto.Status.FAILED);
  }
}
