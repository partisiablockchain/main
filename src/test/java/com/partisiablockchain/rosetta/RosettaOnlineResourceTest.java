package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.AccountCoin;
import com.partisiablockchain.blockchain.AccountHolderAccountPlugin;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.Fraction;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.StakeDelegation;
import com.partisiablockchain.blockchain.StakesFromOthers;
import com.partisiablockchain.blockchain.TransferInformation;
import com.partisiablockchain.blockchain.VestingAccount;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.dto.AccountBalanceRequestDto;
import com.partisiablockchain.rosetta.dto.AccountBalanceResponseDto;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.BlockDto;
import com.partisiablockchain.rosetta.dto.BlockIdentifierDto;
import com.partisiablockchain.rosetta.dto.BlockRequestDto;
import com.partisiablockchain.rosetta.dto.BlockResponseDto;
import com.partisiablockchain.rosetta.dto.BlockTransactionRequestDto;
import com.partisiablockchain.rosetta.dto.BlockTransactionResponse;
import com.partisiablockchain.rosetta.dto.CallRequestDto;
import com.partisiablockchain.rosetta.dto.CallResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionOptionsDto;
import com.partisiablockchain.rosetta.dto.ConstructionSubmitRequest;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.MempoolTransactionRequestDto;
import com.partisiablockchain.rosetta.dto.MempoolTransactionResponseDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.NetworkRequestDto;
import com.partisiablockchain.rosetta.dto.NetworkStatusResponseDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.PartialBlockIdentifierDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierResponse;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionRequest;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionResponse;
import com.partisiablockchain.rosetta.dto.call.GetGasForAccountRequest;
import com.partisiablockchain.rosetta.dto.call.GetGasForAccountResponse;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountRequest;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountResponse;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RosettaOnlineResourceTest extends CloseableTest {

  private ShardedBlockchain ledgers;
  private RosettaOnlineResource resource;
  private String network;

  private static final String SUB_CHAIN = "Shard1";

  @BeforeEach
  void setup() {
    ArrayList<String> shards = new ArrayList<>();
    shards.add(null);
    shards.add("Shard1");
    Map<String, BlockchainLedger> shardLedgers =
        RosettaTestHelper.createLedgers(shards, this::register);
    ledgers = new ShardedBlockchain(shardLedgers);
    resource = new RosettaOnlineResource(shardLedgers);
    network = this.ledgers.getGovShard().getChainId();
  }

  @Test
  void networkStatus() {
    String shard = "Shard1";
    BlockchainLedger shardOneLedger = ledgers.getShard(shard);
    NetworkRequestDto networkRequest =
        createNetworkRequest(RosettaOnlineResource.subChainToSubNetworkId(shard));
    NetworkStatusResponseDto networkStatus = resource.networkStatus(networkRequest);

    Assertions.assertThat(networkStatus.currentBlockTimestamp())
        .isEqualTo(shardOneLedger.getLatestBlock().getProductionTime());
  }

  @Test
  void networkStatus_withActivity() {
    BlockchainLedger governanceLedger = ledgers.getGovShard();

    BlockchainLedgerHelper.appendBlock(governanceLedger);
    BlockchainLedgerHelper.appendBlock(governanceLedger);

    Block genesisBlock = governanceLedger.getBlock(0);
    Block currentBlock = governanceLedger.getLatestBlock();

    NetworkRequestDto networkRequest =
        createNetworkRequest(RosettaOnlineResource.subChainToSubNetworkId(null));
    NetworkStatusResponseDto networkStatusGov = resource.networkStatus(networkRequest);

    Assertions.assertThat(networkStatusGov.currentBlockTimestamp())
        .isEqualTo(currentBlock.getProductionTime());

    BlockIdentifierDto currentBlockIdentifier =
        new BlockIdentifierDto(currentBlock.getBlockTime(), currentBlock.identifier());
    Assertions.assertThat(networkStatusGov.currentBlockIdentifier())
        .isEqualTo(currentBlockIdentifier);

    BlockIdentifierDto genesisBlockIdentifier =
        new BlockIdentifierDto(genesisBlock.getBlockTime(), genesisBlock.identifier());
    Assertions.assertThat(networkStatusGov.genesisBlockIdentifier())
        .isEqualTo(genesisBlockIdentifier);
    Assertions.assertThat(networkStatusGov.oldestBlockIdentifier())
        .isEqualTo(genesisBlockIdentifier);

    Assertions.assertThat(networkStatusGov.peers()).isEmpty();
  }

  @Test
  void unknownNetworkIdentifier() {
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            "some blockchain", "network", new SubNetworkIdentifierDto("Shard1"));
    RosettaTestHelper.checkError(
        () -> resource.mempool(new NetworkRequestDto(networkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
    RosettaTestHelper.checkError(
        () ->
            resource.mempoolTransaction(
                new MempoolTransactionRequestDto(
                    networkIdentifier,
                    new TransactionIdentifierDto(Hash.create(s -> s.writeInt(123))))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
    RosettaTestHelper.checkError(
        () -> resource.networkStatus(new NetworkRequestDto(networkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(networkIdentifier, null, null, null)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
    assertNetworkError(
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN, network, null),
        "No sub network identifier");
  }

  @Test
  void mempool() {
    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    final byte[] rpc = new byte[] {0};
    Hash firstTransaction =
        RosettaTestHelper.addPendingTransaction(
            shard1,
            SignedTransaction.create(
                    CoreTransactionPart.create(1, 20000L, 0L),
                    InteractWithContractTransaction.create(TestValues.CONTRACT_ONE, rpc))
                .sign(TestValues.KEY_PAIR_ONE, shard1.getChainId()));
    final byte[] rpc1 = new byte[] {1};
    Hash secondTransaction =
        RosettaTestHelper.addPendingTransaction(
            shard1,
            SignedTransaction.create(
                    CoreTransactionPart.create(1, 20000L, 0L),
                    InteractWithContractTransaction.create(TestValues.CONTRACT_ONE, rpc1))
                .sign(TestValues.KEY_PAIR_ONE, shard1.getChainId()));
    Assertions.assertThat(
            resource
                .mempool(new NetworkRequestDto(networkIdentifier))
                .transactionIdentifiers()
                .size())
        .isEqualTo(2);
    Assertions.assertThat(
            resource.mempool(new NetworkRequestDto(networkIdentifier)).transactionIdentifiers())
        .contains(
            new TransactionIdentifierDto(firstTransaction),
            new TransactionIdentifierDto(secondTransaction));
  }

  @Test
  void mempoolTransaction_populateOperations() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(20);
            });
    Hash firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    Hash secondTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    MempoolTransactionResponseDto response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().transactionIdentifier().hash())
        .isEqualTo(firstTransaction);
    response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(secondTransaction)));
    Assertions.assertThat(response.transaction().transactionIdentifier().hash())
        .isEqualTo(secondTransaction);
    List<OperationDto> operations = response.transaction().operations();
    Assertions.assertThat(operations.size()).isEqualTo(2);
    OperationDto operationDto = operations.get(0);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto.metadata().memo()).isNull();
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    SignedTransaction transactionOnMpc20Contract =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenMpc20Invocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.write(RosettaTestHelper.toNonZeroUnsigned128Bytes(Unsigned256.create(20)));
            });

    firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transactionOnMpc20Contract);
    response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().transactionIdentifier().hash())
        .isEqualTo(firstTransaction);
    operations = response.transaction().operations();
    Assertions.assertThat(operations.size()).isEqualTo(2);
    operationDto = operations.get(0);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto.metadata().memo()).isNull();
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);
  }

  @Test
  void mempoolTransaction_memo() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);

    int smallMemo = 123;
    SignedTransaction smallMemoTransfer =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(20);
              stream.writeLong(smallMemo);
            });
    Hash smallMemoTransferTx = RosettaTestHelper.addPendingTransaction(shard1, smallMemoTransfer);
    MempoolTransactionResponseDto smallMemoResponse =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(smallMemoTransferTx)));
    Assertions.assertThat(smallMemoResponse.transaction().transactionIdentifier().hash())
        .isEqualTo(smallMemoTransferTx);
    List<OperationDto> smallMemoOperations = smallMemoResponse.transaction().operations();
    Assertions.assertThat(smallMemoOperations.size()).isEqualTo(2);
    Assertions.assertThat(smallMemoOperations.get(0).metadata().memo())
        .isEqualTo(Long.toString(smallMemo));
    Assertions.assertThat(smallMemoOperations.get(0).metadata().counterpart().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    OperationDto.Metadata metadata = smallMemoOperations.get(1).metadata();
    Assertions.assertThat(metadata.memo()).isEqualTo(Long.toString(smallMemo));
    Assertions.assertThat(metadata.counterpart().address()).isEqualTo(TestValues.ACCOUNT_ONE);

    String largeMemo = "this is a memo";
    SignedTransaction largeMemoTransfer =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(20);
              stream.writeString(largeMemo);
            });
    Hash largeMemoTransferTx = RosettaTestHelper.addPendingTransaction(shard1, largeMemoTransfer);
    MempoolTransactionResponseDto largeMemoResponse =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(largeMemoTransferTx)));
    Assertions.assertThat(largeMemoResponse.transaction().transactionIdentifier().hash())
        .isEqualTo(largeMemoTransferTx);
    List<OperationDto> largeMemoOperations = largeMemoResponse.transaction().operations();
    Assertions.assertThat(largeMemoOperations.size()).isEqualTo(2);
    Assertions.assertThat(largeMemoOperations.get(0).metadata().memo()).isEqualTo(largeMemo);
  }

  @Test
  void mempoolTransactionOnBehalfOf() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress receiver = TestValues.ACCOUNT_ONE;
    BlockchainAddress target = TestValues.ACCOUNT_TWO;
    long amount = 20;

    SignedTransaction transferOnBehalfOf =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            custodian,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_ON_BEHALF_OF);
              target.write(stream);
              receiver.write(stream);
              stream.writeLong(amount);
            });

    Hash transferOnBehalfOfTx = RosettaTestHelper.addPendingTransaction(shard1, transferOnBehalfOf);

    MempoolTransactionResponseDto transferOnBehalfOfResponse =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(transferOnBehalfOfTx)));

    Assertions.assertThat(transferOnBehalfOfResponse.transaction().transactionIdentifier().hash())
        .isEqualTo(transferOnBehalfOfTx);

    List<OperationDto> transferOnBehalfOfOperations =
        transferOnBehalfOfResponse.transaction().operations();
    Assertions.assertThat(transferOnBehalfOfOperations.size()).isEqualTo(2);

    Assertions.assertThat(transferOnBehalfOfOperations.get(0).metadata().counterpart().address())
        .isEqualTo(target);
    Assertions.assertThat(transferOnBehalfOfOperations.get(1).metadata().counterpart().address())
        .isEqualTo(receiver);
  }

  @Test
  void mempoolTransaction_memoOnBehalfOf() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);

    KeyPair custodian = TestValues.KEY_PAIR_ONE;
    BlockchainAddress receiver = TestValues.ACCOUNT_ONE;
    BlockchainAddress target = TestValues.ACCOUNT_TWO;
    long amount = 20;
    long smallMemo = 123;

    SignedTransaction transferSmallMemoOnBehalfOf =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            custodian,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF);
              target.write(stream);
              receiver.write(stream);
              stream.writeLong(amount);
              stream.writeLong(smallMemo);
            });

    Hash transferSmallMemoOnBehalfOfTx =
        RosettaTestHelper.addPendingTransaction(shard1, transferSmallMemoOnBehalfOf);

    MempoolTransactionResponseDto transferSmallMemoOnBehalfOfResponse =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(transferSmallMemoOnBehalfOfTx)));

    Assertions.assertThat(
            transferSmallMemoOnBehalfOfResponse.transaction().transactionIdentifier().hash())
        .isEqualTo(transferSmallMemoOnBehalfOfTx);

    List<OperationDto> transferSmallMemoOnBehalfOfOperations =
        transferSmallMemoOnBehalfOfResponse.transaction().operations();
    Assertions.assertThat(transferSmallMemoOnBehalfOfOperations.size()).isEqualTo(2);

    Assertions.assertThat(
            transferSmallMemoOnBehalfOfOperations.get(0).metadata().counterpart().address())
        .isEqualTo(target);
    Assertions.assertThat(transferSmallMemoOnBehalfOfOperations.get(0).metadata().memo())
        .isEqualTo(Long.toString(smallMemo));
    Assertions.assertThat(
            transferSmallMemoOnBehalfOfOperations.get(1).metadata().counterpart().address())
        .isEqualTo(receiver);
    Assertions.assertThat(transferSmallMemoOnBehalfOfOperations.get(1).metadata().memo())
        .isEqualTo(Long.toString(smallMemo));

    String largeMemo = "this is a memo";

    SignedTransaction transferLargeMemoOnBehalfOf =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            custodian,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF);
              target.write(stream);
              receiver.write(stream);
              stream.writeLong(amount);
              stream.writeString(largeMemo);
            });

    Hash transferLargeMemoOnBehalfOfTx =
        RosettaTestHelper.addPendingTransaction(shard1, transferLargeMemoOnBehalfOf);

    MempoolTransactionResponseDto transferLargeMemoOnBehalfOfResponse =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(transferLargeMemoOnBehalfOfTx)));

    Assertions.assertThat(
            transferLargeMemoOnBehalfOfResponse.transaction().transactionIdentifier().hash())
        .isEqualTo(transferLargeMemoOnBehalfOfTx);

    List<OperationDto> transferLargeMemoOnBehalfOfOperations =
        transferLargeMemoOnBehalfOfResponse.transaction().operations();
    Assertions.assertThat(transferLargeMemoOnBehalfOfOperations.size()).isEqualTo(2);

    Assertions.assertThat(
            transferLargeMemoOnBehalfOfOperations.get(0).metadata().counterpart().address())
        .isEqualTo(target);
    Assertions.assertThat(transferLargeMemoOnBehalfOfOperations.get(0).metadata().memo())
        .isEqualTo(largeMemo);
    Assertions.assertThat(
            transferLargeMemoOnBehalfOfOperations.get(1).metadata().counterpart().address())
        .isEqualTo(receiver);
    Assertions.assertThat(transferLargeMemoOnBehalfOfOperations.get(1).metadata().memo())
        .isEqualTo(largeMemo);
  }

  @Test
  void mempoolTransaction_incorrectContractAddress() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.LARGE_ORACLE_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(20);
            });
    Hash firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    MempoolTransactionResponseDto response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().operations().size()).isEqualTo(0);
  }

  @Test
  void mempoolTransaction_notTransfer() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(100);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(20);
            });
    Hash firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    MempoolTransactionResponseDto response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().operations().size()).isEqualTo(0);
  }

  @Test
  void mempoolTransaction_transferFromOnMpc20() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(MpcTokenMpc20Invocations.TRANSFER_FROM);
              TestValues.ACCOUNT_ONE.write(stream);
              TestValues.ACCOUNT_TWO.write(stream);
              stream.write(RosettaTestHelper.toNonZeroUnsigned128Bytes(Unsigned256.create(20)));
            });
    Hash firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    MempoolTransactionResponseDto response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().transactionIdentifier().hash())
        .isEqualTo(firstTransaction);
    List<OperationDto> operations = response.transaction().operations();
    Assertions.assertThat(operations.size()).isEqualTo(2);
    OperationDto operationDto = operations.get(0);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_ONE);
    operationDto = operations.get(1);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.DEDUCT);
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_TWO);
  }

  @Test
  void mempool_unknownInvocationOnMpc20() {
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shard1,
            BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT,
            TestValues.KEY_PAIR_ONE,
            stream -> {
              stream.writeByte(45);
              TestValues.ACCOUNT_ONE.write(stream);
              TestValues.ACCOUNT_TWO.write(stream);
              stream.write(RosettaTestHelper.toNonZeroUnsigned128Bytes(Unsigned256.create(20)));
            });
    Hash firstTransaction = RosettaTestHelper.addPendingTransaction(shard1, transaction);
    MempoolTransactionResponseDto response =
        resource.mempoolTransaction(
            new MempoolTransactionRequestDto(
                networkIdentifier, new TransactionIdentifierDto(firstTransaction)));
    Assertions.assertThat(response.transaction().operations().size()).isEqualTo(0);
  }

  @Test
  public void unknownNetworkIdentifierBlock() {
    RosettaTestHelper.checkError(
        () ->
            resource.getBlock(
                new BlockRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, "UNKNOWN"),
                    new PartialBlockIdentifierDto(99L, null))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(
            "No Shard exists for the given network ChainId"));
  }

  @Test
  public void mempoolTransactionNoTransaction() {
    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);
    RosettaTestHelper.checkError(
        () ->
            resource.mempoolTransaction(
                new MempoolTransactionRequestDto(
                    networkIdentifier,
                    new TransactionIdentifierDto(
                        Hash.create(
                            (stream) ->
                                SafeDataOutputStream.serialize(s -> s.writeString("string")))))),
        ErrorDto.INVALID_TRANSACTION.setRetriable().describe("Transaction does not exist yet"));
  }

  @Test
  void callGetShardForAccount() {
    CallResponseDto call =
        resource.call(
            new CallRequestDto(
                RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                "get_shard_for_account",
                new GetShardForAccountRequest(TestValues.ACCOUNT_ONE)));
    Assertions.assertThat(call.idempotent()).isFalse();
    GetShardForAccountResponse response = (GetShardForAccountResponse) call.result();
    Assertions.assertThat(response.network()).isEqualTo("Shard1");
  }

  @Test
  void callInvalidMethod() {
    RosettaTestHelper.checkError(
        () ->
            resource.call(
                new CallRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, "Gov"),
                    "not_a_method",
                    new GetShardForAccountRequest(TestValues.ACCOUNT_ONE))),
        ErrorDto.UNKNOWN_CALL_METHOD);
  }

  @Test
  void callInvalidSubNetworkIdentifier() {
    RosettaTestHelper.checkError(
        () ->
            resource.call(
                new CallRequestDto(
                    new NetworkIdentifierDto("a", "b", new SubNetworkIdentifierDto("Gov")),
                    "get_shard_for_account",
                    new GetShardForAccountRequest(TestValues.ACCOUNT_ONE))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  public void mempoolTransactionInvalidNetworkIdentifier() {
    SubNetworkIdentifierDto subNetworkIdentifier = new SubNetworkIdentifierDto(SUB_CHAIN);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto("NonExistingBlockChain", network, subNetworkIdentifier);
    RosettaTestHelper.checkError(
        () ->
            resource.mempoolTransaction(
                new MempoolTransactionRequestDto(
                    networkIdentifier,
                    new TransactionIdentifierDto(
                        Hash.create(
                            (stream) ->
                                SafeDataOutputStream.serialize(s -> s.writeString("string")))))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void block() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    Block latestBlock = shardOneLedger.getLatestBlock();

    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    long blockTime = latestBlock.getBlockTime();
    BlockRequestDto blockTimeRequest =
        new BlockRequestDto(
            RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
            new PartialBlockIdentifierDto(blockTime, null));
    BlockResponseDto blockFromBlockTime = resource.getBlock(blockTimeRequest);
    Assertions.assertThat(blockFromBlockTime.block().blockIdentifier().index())
        .isEqualTo(blockTime);
    Assertions.assertThat(blockFromBlockTime.block().parentBlockIdentifier().index())
        .isEqualTo(blockTime - 1);

    BlockRequestDto blockHashRequest =
        new BlockRequestDto(
            RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
            new PartialBlockIdentifierDto(null, latestBlock.identifier()));
    BlockResponseDto blockFromHash = resource.getBlock(blockHashRequest);
    Assertions.assertThat(blockFromHash.block().blockIdentifier().hash())
        .isEqualTo(latestBlock.identifier());

    Assertions.assertThat(blockFromBlockTime).isEqualTo(blockFromHash);
  }

  @Test
  void block_bothHashAndIndexCorrectValues() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    Block latestBlock = shardOneLedger.getLatestBlock();
    long blockTime = latestBlock.getBlockTime();
    BlockRequestDto blockTimeRequest =
        new BlockRequestDto(
            RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
            new PartialBlockIdentifierDto(blockTime, latestBlock.identifier()));
    BlockResponseDto blockFromBlockTime = resource.getBlock(blockTimeRequest);
    Assertions.assertThat(blockFromBlockTime.block().blockIdentifier().index())
        .isEqualTo(blockTime);
    Assertions.assertThat(blockFromBlockTime.block().parentBlockIdentifier().index())
        .isEqualTo(blockTime - 1);
  }

  @Test
  void getBlock_nonMatchingBlockTimeAndHash() {
    BlockRequestDto blockHashRequest =
        new BlockRequestDto(
            RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
            new PartialBlockIdentifierDto(0L, TestValues.HASH_TWO));
    RosettaTestHelper.checkError(
        () -> resource.getBlock(blockHashRequest), ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void block_genesis() {
    BlockRequestDto blockRequest =
        new BlockRequestDto(
            RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
            new PartialBlockIdentifierDto(0L, null));
    BlockDto blockResponse = resource.getBlock(blockRequest).block();
    Assertions.assertThat(blockResponse.blockIdentifier())
        .isEqualTo(blockResponse.parentBlockIdentifier());
  }

  @Test
  void block_unknownBlock() {
    RosettaTestHelper.checkError(
        () ->
            resource.getBlock(
                new BlockRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    new PartialBlockIdentifierDto(99L, null))),
        ErrorDto.UNKNOWN_BLOCK);
  }

  @Test
  void block_nullForBothIndexAndHash() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    BlockResponseDto block =
        resource.getBlock(
            new BlockRequestDto(
                RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                new PartialBlockIdentifierDto(null, null)));
    Hash blockResponseHash = block.block().blockIdentifier().hash();
    Hash latestBlockHash = shardOneLedger.getLatestBlock().identifier();
    Assertions.assertThat(blockResponseHash).isEqualTo(latestBlockHash);
  }

  @Test
  void blockTransaction() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    SignedTransaction transfer =
        RosettaTestHelper.createTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 1000);

    boolean valid = shardOneLedger.addPendingTransaction(transfer);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transfer), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    Block latestBlock = shardOneLedger.getLatestBlock();

    Assertions.assertThat(shardOneLedger.getTransaction(transfer.identifier())).isNotNull();

    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);

    BlockResponseDto blockResponse =
        resource.getBlock(
            new BlockRequestDto(
                networkIdentifier,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), null)));

    BlockIdentifierDto blockIdentifier =
        new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());

    List<OperationDto> operations = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifierDto : blockResponse.otherTransactions()) {
      BlockTransactionResponse blockTransactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  networkIdentifier, blockIdentifier, transactionIdentifierDto));
      operations.addAll(blockTransactionResponse.transaction().operations());
    }

    Assertions.assertThat(operations).hasSize(2);
    OperationDto operationDto = operations.get(1);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.DEDUCT);
    Assertions.assertThat(operationDto.metadata().memo()).isNull();
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_ONE);

    OperationDto operationDto1 = operations.get(0);
    Assertions.assertThat(operationDto1.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto1.metadata().memo()).isNull();
    Assertions.assertThat(operationDto1.metadata().counterpart().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);
  }

  /** Failed transactions are included when fetching a block. */
  @Test
  void blockTransaction_failedTransaction() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    BlockchainAddress nonExistingContract =
        BlockchainAddress.fromString("02A000000000000000000000000000000000000002");
    SignedTransaction transaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            shardOneLedger,
            nonExistingContract,
            TestValues.KEY_PAIR_ONE,
            FunctionUtility.noOpConsumer());

    boolean valid = shardOneLedger.addPendingTransaction(transaction);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transaction), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    Block latestBlock = shardOneLedger.getLatestBlock();

    Hash eventIdentifier =
        shardOneLedger.getTransaction(transaction.identifier()).getEvents().get(0);
    ExecutedTransaction executedEvent = shardOneLedger.getTransaction(eventIdentifier);
    Assertions.assertThat(executedEvent.didExecutionSucceed()).isFalse();

    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);

    BlockResponseDto blockResponse =
        resource.getBlock(
            new BlockRequestDto(
                networkIdentifier,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), null)));

    List<Hash> executedTransactions =
        blockResponse.otherTransactions().stream().map(TransactionIdentifierDto::hash).toList();
    Assertions.assertThat(executedTransactions)
        .containsExactlyInAnyOrder(transaction.identifier(), eventIdentifier);
  }

  @Test
  void blockTransactionOnBehalfOf() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transfer =
        RosettaTestHelper.createTransferOnBehalfOf(
            shardOneLedger,
            TestValues.KEY_PAIR_ONE,
            TestValues.ACCOUNT_THREE,
            TestValues.ACCOUNT_ONE,
            1000);

    boolean valid = shardOneLedger.addPendingTransaction(transfer);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transfer), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    Block latestBlock = shardOneLedger.getLatestBlock();

    Assertions.assertThat(shardOneLedger.getTransaction(transfer.identifier())).isNotNull();

    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);

    BlockIdentifierDto blockIdentifier =
        new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());

    BlockResponseDto blockResponse =
        resource.getBlock(
            new BlockRequestDto(
                networkIdentifier,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), null)));

    List<OperationDto> operations = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifierDto : blockResponse.otherTransactions()) {
      BlockTransactionResponse blockTransactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  networkIdentifier, blockIdentifier, transactionIdentifierDto));
      operations.addAll(blockTransactionResponse.transaction().operations());
    }

    Assertions.assertThat(operations).hasSize(2);

    OperationDto operationDto1 = operations.get(0);
    Assertions.assertThat(operationDto1.type()).isEqualTo(OperationDto.Type.DEDUCT);
    Assertions.assertThat(operationDto1.metadata().memo()).isNull();
    Assertions.assertThat(operationDto1.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_ONE);

    OperationDto operationDto = operations.get(1);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto.metadata().memo()).isNull();
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_THREE);
  }

  @Test
  void blockTransaction_memo() {
    // Small memo
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    int smallMemo = 123;
    SignedTransaction transferSmallMemo =
        RosettaTestHelper.createTransferSmallMemo(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 1000, smallMemo);

    shardOneLedger.addPendingTransaction(transferSmallMemo);
    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transferSmallMemo), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    Block latestBlock = shardOneLedger.getLatestBlock();
    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN);
    BlockResponseDto blockResponse =
        resource.getBlock(
            new BlockRequestDto(
                networkIdentifier,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), null)));
    BlockIdentifierDto blockIdentifier =
        new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());

    List<OperationDto> smallMemoOperations = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifierDto : blockResponse.otherTransactions()) {
      BlockTransactionResponse blockTransactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  networkIdentifier, blockIdentifier, transactionIdentifierDto));
      smallMemoOperations.addAll(blockTransactionResponse.transaction().operations());
    }

    Assertions.assertThat(smallMemoOperations).hasSize(2);
    OperationDto operationDto0 = smallMemoOperations.get(1);
    Assertions.assertThat(operationDto0.type()).isEqualTo(OperationDto.Type.DEDUCT);
    OperationDto.Metadata metadata = operationDto0.metadata();
    Assertions.assertThat(metadata.memo()).isEqualTo(Long.toString(smallMemo));
    Assertions.assertThat(metadata.counterpart().address()).isEqualTo(TestValues.ACCOUNT_ONE);

    OperationDto operationDto1 = smallMemoOperations.get(0);
    Assertions.assertThat(operationDto1.type()).isEqualTo(OperationDto.Type.ADD);
    Assertions.assertThat(operationDto1.metadata().counterpart().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    // Large memo
    String largeMemo = "this is a memo";
    SignedTransaction transferLargeMemo =
        RosettaTestHelper.createTransferLargeMemo(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 1000, largeMemo);

    shardOneLedger.addPendingTransaction(transferLargeMemo);
    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transferLargeMemo), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    latestBlock = shardOneLedger.getLatestBlock();
    blockResponse =
        resource.getBlock(
            new BlockRequestDto(
                networkIdentifier,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), null)));
    blockIdentifier = new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());

    List<OperationDto> largeMemoOperations = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifierDto : blockResponse.otherTransactions()) {
      BlockTransactionResponse blockTransactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  networkIdentifier, blockIdentifier, transactionIdentifierDto));
      largeMemoOperations.addAll(blockTransactionResponse.transaction().operations());
    }

    Assertions.assertThat(largeMemoOperations).hasSize(2);
    OperationDto operationDto = largeMemoOperations.get(0);
    Assertions.assertThat(operationDto.metadata().memo()).isEqualTo(largeMemo);
    Assertions.assertThat(operationDto.type()).isEqualTo(OperationDto.Type.DEDUCT);
    Assertions.assertThat(operationDto.metadata().counterpart().address())
        .isEqualTo(TestValues.ACCOUNT_ONE);
  }

  @Test
  void blockTransaction_unknownTransaction() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    SignedTransaction transfer =
        RosettaTestHelper.createTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 1000);

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transfer), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    Block latestBlock = shardOneLedger.getLatestBlock();

    BlockIdentifierDto blockIdentifier =
        new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());

    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    blockIdentifier,
                    new TransactionIdentifierDto(TestValues.HASH_ONE))),
        ErrorDto.UNKNOWN_TRANSACTION);
  }

  @Test
  void blockTransaction_invalidBlockTime() {
    BlockIdentifierDto blockIdentifier = new BlockIdentifierDto(0, TestValues.HASH_TWO);
    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    blockIdentifier,
                    new TransactionIdentifierDto(TestValues.HASH_ONE))),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void blockTransaction_invalidBlockHash() {
    BlockIdentifierDto blockIdentifier = new BlockIdentifierDto(0, null);
    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    blockIdentifier,
                    new TransactionIdentifierDto(TestValues.HASH_ONE))),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void blockTransaction_unknownLedger() {
    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, "UNKNOWN"),
                    new BlockIdentifierDto(0, null),
                    new TransactionIdentifierDto(null))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(
            "No Shard exists for the given network ChainId"));
  }

  @Test
  void blockTransaction_acrossShards() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    SignedTransaction transfer =
        RosettaTestHelper.createTransfer(
            shardOneLedger, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_GOV, 1000);

    boolean valid = shardOneLedger.addPendingTransaction(transfer);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(transfer), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    final Block latestBlockShardOne = shardOneLedger.getLatestBlock();

    ExecutedTransaction transferTransaction = shardOneLedger.getTransaction(transfer.identifier());
    Assertions.assertThat(transferTransaction).isNotNull();
    Assertions.assertThat(transferTransaction.numberOfEvents()).isEqualTo(1);

    ExecutedTransaction transferEventTransaction =
        shardOneLedger.getTransaction(transferTransaction.getEvents().get(0));
    Assertions.assertThat(transferEventTransaction).isNotNull();
    Assertions.assertThat(transferEventTransaction.numberOfEvents()).isEqualTo(2);

    List<ExecutableEvent> governanceEvents =
        shardOneLedger.getChainState().getExecutedState().getSpawnedEvents().keySet().stream()
            .map(shardOneLedger::getEventTransaction)
            .filter(event -> event.getEvent().getDestinationShard() == null)
            .toList();

    BlockchainLedger governanceLedger = ledgers.getGovShard();
    FloodableEvent floodableEvent =
        FloodableEvent.create(
            governanceEvents.get(0),
            shardOneLedger.getFinalizedBlock(2),
            shardOneLedger.getStateStorage());
    BlockchainLedgerHelper.appendBlock(governanceLedger, List.of(), List.of(floodableEvent));
    BlockchainLedgerHelper.appendBlock(governanceLedger);
    final Block latestBlockGov = governanceLedger.getLatestBlock();

    // Get the latest final block for both shards
    BlockResponseDto blockResponseGov =
        resource.getBlock(
            new BlockRequestDto(
                RosettaTestHelper.createNetworkIdentifier(network, "Gov"),
                new PartialBlockIdentifierDto(latestBlockGov.getBlockTime(), null)));

    BlockResponseDto blockResponseShardOne =
        resource.getBlock(
            new BlockRequestDto(
                RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                new PartialBlockIdentifierDto(latestBlockShardOne.getBlockTime(), null)));

    BlockIdentifierDto blockIdentifierGov =
        new BlockIdentifierDto(latestBlockGov.getBlockTime(), latestBlockGov.identifier());
    // Get all operations for transactions in the block on both shards
    List<OperationDto> operationsGov = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifier : blockResponseGov.otherTransactions()) {
      BlockTransactionResponse transactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  RosettaTestHelper.createNetworkIdentifier(network, "Gov"),
                  blockIdentifierGov,
                  transactionIdentifier));
      operationsGov.addAll(transactionResponse.transaction().operations());
    }

    BlockIdentifierDto blockIdentifierShard =
        new BlockIdentifierDto(
            latestBlockShardOne.getBlockTime(), latestBlockShardOne.identifier());
    List<OperationDto> operationsShardOne = new ArrayList<>();
    for (TransactionIdentifierDto transactionIdentifier :
        blockResponseShardOne.otherTransactions()) {
      BlockTransactionResponse transactionResponse =
          resource.getBlockTransaction(
              new BlockTransactionRequestDto(
                  RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                  blockIdentifierShard,
                  transactionIdentifier));
      operationsShardOne.addAll(transactionResponse.transaction().operations());
    }

    List<OperationDto> operations = new ArrayList<>();
    operations.addAll(operationsGov);
    operations.addAll(operationsShardOne);

    Assertions.assertThat(operations).hasSize(2);
    List<OperationDto.Type> types = operations.stream().map(OperationDto::type).toList();
    Assertions.assertThat(types)
        .containsExactlyInAnyOrder(OperationDto.Type.ADD, OperationDto.Type.DEDUCT);
  }

  @Test
  void blockTransaction_nullBlockIdentifier() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    null,
                    new TransactionIdentifierDto(TestValues.HASH_ONE))),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  /** Test that a BlockIdentifier with an invalid index returns an error. */
  @Test
  void blockTransaction_invalidIndex() {
    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    RosettaTestHelper.checkError(
        () ->
            resource.getBlockTransaction(
                new BlockTransactionRequestDto(
                    RosettaTestHelper.createNetworkIdentifier(network, SUB_CHAIN),
                    new BlockIdentifierDto(-1, Hash.create(s -> s.writeLong(1))),
                    new TransactionIdentifierDto(TestValues.HASH_ONE))),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void getAccountBalance() {
    long tokens = 5;
    long stakedTokens = 7;
    List<AccountCoin> accountCoins = List.of(new AccountCoin(Unsigned256.create(42)));

    List<StakeDelegation> storedPendingStakeDelegations =
        List.of(
            StakeDelegation.create(0L, StakeDelegation.DelegationType.DELEGATE_STAKES),
            StakeDelegation.create(1L, StakeDelegation.DelegationType.RETURN_DELEGATED_STAKES),
            StakeDelegation.create(3L, StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES));

    List<Long> delegatedStakesToOthers = List.of(4L, 5L, 3L);
    Map<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
        Map.of(
            TestValues.ACCOUNT_ONE,
            StakesFromOthers.create(2, 2),
            TestValues.ACCOUNT_TWO,
            StakesFromOthers.create(2, 0),
            TestValues.ACCOUNT_GOV,
            StakesFromOthers.create(0, 2));

    List<Long> pendingRetractedDelegatedStakes = List.of(5L, 3L, 3L);

    List<VestingAccount> vestingAccounts = List.of(new VestingAccount(3), new VestingAccount(6));
    Map<Hash, TransferInformation> storedPendingTransfers =
        Map.of(
            TestValues.HASH_ONE,
            new TransferInformation(5, false, -1),
            TestValues.HASH_TWO,
            new TransferInformation(10, true, -1),
            TestValues.HASH_THREE,
            new TransferInformation(10, false, 1),
            TestValues.HASH_FOUR,
            new TransferInformation(5, true, 1));
    Map<Long, Long> pendingUnstakes = Map.of(0L, 2L, 1L, 3L);

    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    SignedTransaction mint =
        createMintTransaction(
            accountCoins,
            tokens,
            stakedTokens,
            storedPendingStakeDelegations,
            delegatedStakesToOthers,
            delegatedStakesFromOthers,
            pendingRetractedDelegatedStakes,
            vestingAccounts,
            storedPendingTransfers,
            pendingUnstakes,
            shardOneLedger);

    boolean valid = shardOneLedger.addPendingTransaction(mint);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(mint), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifierDto =
        RosettaTestHelper.createNetworkIdentifier(
            ledgers.getShard(SUB_CHAIN).getChainId(), SUB_CHAIN);
    AccountBalanceResponseDto accountBalance =
        resource.getAccountBalance(
            new AccountBalanceRequestDto(
                networkIdentifierDto,
                accountIdentifierDto,
                new PartialBlockIdentifierDto(null, null),
                null));
    Assertions.assertThat(accountBalance.blockIdentifierDto().index())
        .isEqualTo(shardOneLedger.getLatestBlock().getBlockTime());
    Assertions.assertThat(accountBalance.blockIdentifierDto().hash())
        .isEqualTo(shardOneLedger.getLatestBlock().identifier());
    Assertions.assertThat(accountBalance.balances()).hasSize(1);
    Assertions.assertThat(accountBalance.balances().get(0).currency()).isEqualTo(CurrencyDto.MPC);

    int storedPendingStakeDelegationsResult = 1;
    int delegatedStakesFromOthersResult = 8;
    int pendingRetractedDelegatedStakesResult = 11;

    long expected =
        tokens
            + stakedTokens
            + vestingAccounts.stream().mapToLong(v -> v.tokens).sum()
            + storedPendingTransfers.values().stream()
                .mapToLong(
                    t -> t.addTokensOrCoinsIfTransferSuccessful || t.coinIndex != -1 ? 0 : t.amount)
                .sum()
            + pendingUnstakes.values().stream().mapToLong(p -> p).sum()
            + storedPendingStakeDelegationsResult
            + delegatedStakesFromOthersResult
            + pendingRetractedDelegatedStakesResult;
    Assertions.assertThat(asLong(accountBalance.balances().get(0).value())).isEqualTo(expected);
  }

  @Test
  void getGasBalance() {
    final Unsigned256 coinBalanceA = Unsigned256.create(40);
    final Unsigned256 coinBalanceB = Unsigned256.create(10);
    final List<AccountCoin> accountCoins =
        List.of(new AccountCoin(coinBalanceA), new AccountCoin(coinBalanceB));
    final Fraction conversionRateA = AccountHolderAccountPlugin.TEST_COINS.get(0).conversionRate;
    final Fraction conversionRateB = AccountHolderAccountPlugin.TEST_COINS.get(1).conversionRate;
    final BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    final SignedTransaction mint =
        createMintTransaction(
            accountCoins,
            0,
            0,
            List.of(),
            List.of(),
            Map.of(),
            List.of(),
            List.of(),
            Map.of(),
            Map.of(),
            shardOneLedger);

    boolean valid = shardOneLedger.addPendingTransaction(mint);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(mint), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifierDto =
        RosettaTestHelper.createNetworkIdentifier(
            ledgers.getShard(SUB_CHAIN).getChainId(), SUB_CHAIN);
    CallRequestDto requestDto =
        new CallRequestDto(
            networkIdentifierDto,
            "get_gas_for_account",
            new GetGasForAccountRequest(accountIdentifierDto.address()));
    CallResponseDto responseDto = resource.call(requestDto);
    GetGasForAccountResponse response = (GetGasForAccountResponse) responseDto.result();
    long coinGasA =
        coinBalanceA
            .multiply(Unsigned256.create(conversionRateA.numerator))
            .divide(Unsigned256.create(conversionRateA.denominator))
            .longValueExact();
    long coinGasB =
        coinBalanceB
            .multiply(Unsigned256.create(conversionRateB.numerator))
            .divide(Unsigned256.create(conversionRateB.denominator))
            .longValueExact();
    Assertions.assertThat(response.gas()).isEqualTo(coinGasA + coinGasB);
  }

  @Test
  void gasForNonExistentAccount() {
    AccountIdentifierDto accountIdentifierDto =
        new AccountIdentifierDto(
            BlockchainAddress.fromHash(
                BlockchainAddress.Type.ACCOUNT,
                Hash.create(s -> s.writeString("nonExistentAccount"))));
    NetworkIdentifierDto networkIdentifierDto =
        RosettaTestHelper.createNetworkIdentifier(
            ledgers.getShard(SUB_CHAIN).getChainId(), SUB_CHAIN);
    CallRequestDto requestDto =
        new CallRequestDto(
            networkIdentifierDto,
            "get_gas_for_account",
            new GetGasForAccountRequest(accountIdentifierDto.address()));
    RosettaTestHelper.checkError(() -> resource.call(requestDto), ErrorDto.UNKNOWN_ACCOUNT);
  }

  @SuppressWarnings("EnumOrdinal")
  private static SignedTransaction createMintTransaction(
      List<AccountCoin> accountCoins,
      long tokens,
      long stakedTokens,
      List<StakeDelegation> storedPendingStakeDelegations,
      List<Long> delegatedStakesToOthers,
      Map<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers,
      List<Long> pendingRetractedDelegatedStakes,
      List<VestingAccount> vestingAccounts,
      Map<Hash, TransferInformation> storedPendingTransfers,
      Map<Long, Long> pendingUnstakes,
      BlockchainLedger shardOneLedger) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        shardOneLedger,
        TestValues.CONTRACT_MINT,
        TestValues.KEY_PAIR_ONE,
        rpc -> {
          TestValues.ACCOUNT_ONE.write(rpc);
          rpc.writeLong(tokens);
          rpc.writeLong(stakedTokens);

          rpc.writeInt(storedPendingStakeDelegations.size());
          for (StakeDelegation storedPendingStakeDelegation : storedPendingStakeDelegations) {
            rpc.writeLong(storedPendingStakeDelegation.getAmount());
            rpc.writeInt(storedPendingStakeDelegation.getDelegationType().ordinal());
          }

          rpc.writeInt(delegatedStakesToOthers.size());
          for (long delegatedStakesToOther : delegatedStakesToOthers) {
            rpc.writeLong(delegatedStakesToOther);
          }

          rpc.writeInt(delegatedStakesFromOthers.size());
          for (BlockchainAddress address : delegatedStakesFromOthers.keySet()) {
            address.write(rpc);
            StakesFromOthers stakesFromOthers = delegatedStakesFromOthers.get(address);
            rpc.writeLong(stakesFromOthers.acceptedDelegatedStakes);
            rpc.writeLong(stakesFromOthers.pendingDelegatedStakes);
          }

          rpc.writeInt(pendingRetractedDelegatedStakes.size());
          for (long pendingRetractedDelegatedStake : pendingRetractedDelegatedStakes) {
            rpc.writeLong(pendingRetractedDelegatedStake);
          }

          rpc.writeInt(vestingAccounts.size());
          for (VestingAccount vestingAccount : vestingAccounts) {
            rpc.writeLong(vestingAccount.tokens);
          }

          rpc.writeInt(storedPendingTransfers.size());
          for (Hash transaction : storedPendingTransfers.keySet()) {
            transaction.write(rpc);
            TransferInformation transferInformation = storedPendingTransfers.get(transaction);
            rpc.writeLong(transferInformation.amount);
            rpc.writeBoolean(transferInformation.addTokensOrCoinsIfTransferSuccessful);
            rpc.writeInt(transferInformation.coinIndex);
          }

          rpc.writeInt(pendingUnstakes.size());
          for (Long pendingUnstake : pendingUnstakes.keySet()) {
            rpc.writeLong(pendingUnstake);
            rpc.writeLong(pendingUnstakes.get(pendingUnstake));
          }

          rpc.writeInt(accountCoins.size());
          for (AccountCoin accountCoin : accountCoins) {
            accountCoin.balance.write(rpc);
          }
        });
  }

  private SignedTransaction createMint(long tokens, long stakedTokens, BlockchainLedger ledger) {
    return createMintTransaction(
        List.of(),
        tokens,
        stakedTokens,
        List.of(),
        List.of(),
        Map.of(),
        List.of(),
        List.of(),
        Map.of(),
        Map.of(),
        ledger);
  }

  @Test
  void getAccountBalanceWithCurrencies() {
    long tokens = 5;
    long stakedTokens = 10;

    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);
    SignedTransaction mint = createMint(tokens, stakedTokens, shardOneLedger);
    boolean valid = shardOneLedger.addPendingTransaction(mint);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(mint), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(shardOneLedger.getChainId(), SUB_CHAIN);
    AccountBalanceResponseDto accountBalance =
        resource.getAccountBalance(
            new AccountBalanceRequestDto(
                networkIdentifier,
                accountIdentifierDto,
                new PartialBlockIdentifierDto(null, null),
                List.of(CurrencyDto.MPC)));
    Assertions.assertThat(accountBalance.balances()).hasSize(1);
    Assertions.assertThat(asLong(accountBalance.balances().get(0).value()))
        .isEqualTo(tokens + stakedTokens);

    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(null, null),
                    List.of(new CurrencyDto("not_a_currency", 42)))),
        ErrorDto.UNKNOWN_CURRENCY);

    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(null, null),
                    List.of(CurrencyDto.MPC, CurrencyDto.MPC))),
        ErrorDto.UNKNOWN_CURRENCY);
  }

  @Test
  void getAccountBalanceHistoric() {
    long tokens = 10;
    long stakedTokens = 2;

    BlockchainLedger shardOneLedger = ledgers.getShard(SUB_CHAIN);

    SignedTransaction mint = createMint(tokens, stakedTokens, shardOneLedger);

    boolean valid = shardOneLedger.addPendingTransaction(mint);
    Assertions.assertThat(valid).isTrue();

    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(mint), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    final Block historic = shardOneLedger.getLatestBlock();

    mint = createMint(tokens, stakedTokens, shardOneLedger);
    valid = shardOneLedger.addPendingTransaction(mint);
    Assertions.assertThat(valid).isTrue();
    BlockchainLedgerHelper.appendBlock(shardOneLedger, List.of(mint), List.of());
    BlockchainLedgerHelper.appendBlock(shardOneLedger);
    BlockchainLedgerHelper.appendBlock(shardOneLedger);

    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier(SUB_CHAIN);

    AccountBalanceResponseDto accountBalance =
        resource.getAccountBalance(
            new AccountBalanceRequestDto(
                networkIdentifier,
                accountIdentifierDto,
                new PartialBlockIdentifierDto(historic.getBlockTime(), historic.identifier()),
                null));
    Assertions.assertThat(accountBalance.balances()).hasSize(1);
    Assertions.assertThat(asLong(accountBalance.balances().get(0).value()))
        .isEqualTo(tokens + stakedTokens);

    Block latestBlock = shardOneLedger.getLatestBlock();
    accountBalance =
        resource.getAccountBalance(
            new AccountBalanceRequestDto(
                networkIdentifier,
                accountIdentifierDto,
                new PartialBlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier()),
                null));
    Assertions.assertThat(accountBalance.balances()).hasSize(1);
    Assertions.assertThat(asLong(accountBalance.balances().get(0).value()))
        .isEqualTo((tokens + stakedTokens) * 2);
  }

  @Test
  void getAccountBalanceWithInvalidBlockIndex() {
    BlockchainLedger gov = ledgers.getGovShard();
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    BlockchainLedgerHelper.appendBlock(gov);
    BlockchainLedgerHelper.appendBlock(gov);

    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(1L, gov.getBlock(0).identifier()),
                    null)),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  /** Test that a PartialBlockIdentifier with no index and an invalid hash returns an error. */
  @Test
  void partialBlockIdentifierWithInvalidHash() {
    BlockchainLedger gov = ledgers.getGovShard();
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    BlockchainLedgerHelper.appendBlock(gov);
    BlockchainLedgerHelper.appendBlock(gov);

    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(
                        null, Hash.create(s -> s.writeString("nonExistentBlock"))),
                    null)),
        ErrorDto.UNKNOWN_BLOCK);
  }

  @Test
  void getAccountBalanceWithInvalidBlockTime() {
    BlockchainLedger gov = ledgers.getGovShard();
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    BlockchainLedgerHelper.appendBlock(gov);
    BlockchainLedgerHelper.appendBlock(gov);
    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(0L, gov.getBlock(1).identifier()),
                    null)),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void getAccountBalanceWithNullHash() {
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(0L, TestValues.HASH_TWO),
                    null)),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void getAccountBalanceWithNegativeBlockTime() {
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    accountIdentifierDto,
                    new PartialBlockIdentifierDto(-1L, TestValues.HASH_TWO),
                    null)),
        ErrorDto.INVALID_BLOCK_IDENTIFIER);
  }

  @Test
  void getAccountBalanceWithNullIdentifier() {
    BlockchainLedger gov = ledgers.getGovShard();
    AccountIdentifierDto accountIdentifierDto = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");
    BlockchainLedgerHelper.appendBlock(gov);
    BlockchainLedgerHelper.appendBlock(gov);

    AccountBalanceResponseDto accountBalance =
        resource.getAccountBalance(
            new AccountBalanceRequestDto(networkIdentifier, accountIdentifierDto, null, null));
    Assertions.assertThat(accountBalance.blockIdentifierDto().index())
        .isEqualTo(gov.getLatestBlock().getBlockTime());
  }

  @Test
  void getAccountBalanceWithInvalidAccount() {
    BlockchainLedger gov = ledgers.getGovShard();
    AccountIdentifierDto nonExistingAccount =
        new AccountIdentifierDto(
            BlockchainAddress.fromString("000000000000000000000000000000000000000042"));
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");

    RosettaTestHelper.checkError(
        () ->
            resource.getAccountBalance(
                new AccountBalanceRequestDto(
                    networkIdentifier,
                    nonExistingAccount,
                    new PartialBlockIdentifierDto(0L, gov.getBlock(0).identifier()),
                    null)),
        ErrorDto.UNKNOWN_ACCOUNT);
  }

  @Test
  void getAccountBalanceWithExistingAccountWithNoFunds() {
    AccountIdentifierDto accountIdentifierDto =
        new AccountIdentifierDto(TestValues.KEY_PAIR_ONE.getPublic().createAddress());
    NetworkIdentifierDto networkIdentifier = createNetworkIdentifier("Gov");

    AccountBalanceRequestDto request =
        new AccountBalanceRequestDto(
            networkIdentifier,
            accountIdentifierDto,
            new PartialBlockIdentifierDto(null, null),
            null);

    AccountBalanceResponseDto response = resource.getAccountBalance(request);
    Assertions.assertThat(response.balances()).isEmpty();
  }

  private long asLong(String s) {
    return Long.parseLong(s);
  }

  private NetworkRequestDto createNetworkRequest(String subNetwork) {
    return new NetworkRequestDto(createNetworkIdentifier(subNetwork));
  }

  private NetworkIdentifierDto createNetworkIdentifier(String subNetwork) {
    return RosettaTestHelper.createNetworkIdentifier(network, subNetwork);
  }

  @Test
  void submit() {
    byte[] signedTx =
        SafeDataOutputStream.serialize(
            createSignedTransaction(ledgers.getGovShard(), false)::write);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionSubmitRequest request = new ConstructionSubmitRequest(networkIdentifier, signedTx);
    TransactionIdentifierResponse response = resource.submit(request);
    SignedTransaction expectedSignedTransaction =
        SignedTransaction.read(
            ledgers.getGovShard().getChainId(), SafeDataInputStream.createFromBytes(signedTx));
    Assertions.assertThat(response.transactionIdentifierDto().hash())
        .isEqualTo(expectedSignedTransaction.identifier());
  }

  @Test
  void submit_invalidNetworkIdentifier() {
    byte[] signedTx =
        SafeDataOutputStream.serialize(
            createSignedTransaction(ledgers.getGovShard(), false)::write);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            "invalidNetworkIdentifier",
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionSubmitRequest request = new ConstructionSubmitRequest(networkIdentifier, signedTx);
    RosettaTestHelper.checkError(
        () -> resource.submit(request),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void submitFailsIfAddingFails() {
    byte[] signedTx =
        SafeDataOutputStream.serialize(createSignedTransaction(ledgers.getGovShard(), true)::write);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionSubmitRequest request = new ConstructionSubmitRequest(networkIdentifier, signedTx);
    RosettaTestHelper.checkError(() -> resource.submit(request), ErrorDto.SUBMIT_FAILURE);
  }

  @Test
  void metadata_invalid_networkIdentifier() {
    RosettaTestHelper.checkError(
        () ->
            resource.metadata(
                new ConstructionMetadataRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto("nonExistingShard")),
                    new ConstructionOptionsDto(TestValues.ACCOUNT_ONE, null))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(
            "No Shard exists for the given network ChainId"));
  }

  @Test
  void metadata_accountNotOnShard() {
    RosettaTestHelper.checkError(
        () ->
            resource.metadata(
                new ConstructionMetadataRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto("Gov")),
                    new ConstructionOptionsDto(
                        BlockchainAddress.fromHash(
                            BlockchainAddress.Type.ACCOUNT,
                            Hash.create(h -> h.writeString("UnknownAccount"))),
                        null))),
        ErrorDto.UNKNOWN_ACCOUNT.describe(
            String.format("Account was not found on the given shard %s", "Gov")));
  }

  @Test
  void searchTransaction() {
    BlockchainLedger shard1 = ledgers.getShard(SUB_CHAIN);
    SignedTransaction transfer =
        RosettaTestHelper.createTransfer(
            shard1, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 42);
    shard1.addPendingTransaction(transfer);
    BlockchainLedgerHelper.appendBlock(shard1, List.of(transfer), List.of());
    BlockchainLedgerHelper.appendBlock(shard1);
    Block latestBlock = shard1.getLatestBlock();
    Assertions.assertThat(shard1.getTransaction(transfer.identifier())).isNotNull();

    CallRequestDto getBlockFromTransactionRequest =
        new CallRequestDto(
            createNetworkIdentifier(SUB_CHAIN),
            "get_block_from_transaction",
            new GetBlockFromTransactionRequest(transfer.identifier()));
    CallResponseDto response = resource.call(getBlockFromTransactionRequest);
    GetBlockFromTransactionResponse responseDto =
        (GetBlockFromTransactionResponse) response.result();
    Assertions.assertThat(responseDto.blockHash()).isEqualTo(latestBlock.identifier());
    Assertions.assertThat(responseDto.blockIndex()).isEqualTo(latestBlock.getBlockTime());

    SignedTransaction nonExistentTransfer =
        RosettaTestHelper.createTransfer(
            shard1, TestValues.KEY_PAIR_ONE, TestValues.ACCOUNT_ONE, 101);
    RosettaTestHelper.checkError(
        () ->
            resource.call(
                new CallRequestDto(
                    createNetworkIdentifier(SUB_CHAIN),
                    "get_block_from_transaction",
                    new GetBlockFromTransactionRequest(nonExistentTransfer.identifier()))),
        ErrorDto.INVALID_TRANSACTION);

    NetworkIdentifierDto invalidNetwork = createNetworkIdentifier("invalidShard");
    RosettaTestHelper.checkError(
        () ->
            resource.call(
                new CallRequestDto(
                    invalidNetwork,
                    "get_block_from_transaction",
                    new GetBlockFromTransactionRequest(nonExistentTransfer.identifier()))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(
            "No Shard exists for the given network " + invalidNetwork.network()));
  }

  private SignedTransaction createSignedTransaction(
      BlockchainLedger ledger, boolean useWrongNonce) {

    InteractWithContractTransaction interactWithContractTransaction =
        InteractWithContractTransaction.create(TestValues.CONTRACT_ONE, new byte[0]);

    BlockchainLedger.BlockAndState blockAndState = ledger.latest();
    ImmutableChainState immutableChainState = blockAndState.getState();
    KeyPair signer = TestValues.KEY_PAIR_ONE;
    BlockchainAddress signerAddress = signer.getPublic().createAddress();
    AccountState accountState = immutableChainState.getAccount(signerAddress);
    long nonce = useWrongNonce ? 2L : accountState.getNonce();
    CoreTransactionPart coreTransactionPart =
        CoreTransactionPart.create(nonce, System.currentTimeMillis() + 100_000, 100_000);

    return SignedTransaction.create(coreTransactionPart, interactWithContractTransaction)
        .sign(TestValues.KEY_PAIR_ONE, ledgers.getGovShard().getChainId());
  }

  void assertNetworkError(NetworkIdentifierDto identifierDto, String description) {
    RosettaTestHelper.checkError(
        () -> ledgers.validateNetworkIdentifier(identifierDto),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(description));
  }

  @Test
  void validateNetworkIdentifier() {
    SubNetworkIdentifierDto gov = new SubNetworkIdentifierDto("Gov");
    NetworkIdentifierDto invalidBlockchain = new NetworkIdentifierDto("invalid", network, gov);
    assertNetworkError(invalidBlockchain, "Invalid blockchain name");

    NetworkIdentifierDto invalidNetwork =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN, "invalid", gov);
    assertNetworkError(invalidNetwork, "Invalid network");

    NetworkIdentifierDto invalidShard =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            network,
            new SubNetworkIdentifierDto("x"));
    assertNetworkError(invalidShard, "No Shard exists for the given network ChainId");

    NetworkIdentifierDto shardMissing =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN, network, null);
    assertNetworkError(shardMissing, "No sub network identifier");
  }
}
