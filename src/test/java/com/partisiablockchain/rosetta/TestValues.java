package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import java.util.List;

/** Test values used for tests. */
public final class TestValues {
  public static final BlockchainAddress ACCOUNT_ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  public static final BlockchainAddress ACCOUNT_TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  public static final BlockchainAddress ACCOUNT_THREE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");
  public static final KeyPair KEY_ACCOUNT_GOV = new KeyPair(BigInteger.TWO);
  public static final BlockchainAddress ACCOUNT_GOV = KEY_ACCOUNT_GOV.getPublic().createAddress();
  public static final Hash HASH_ONE =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
  public static final Hash HASH_TWO =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000002");
  public static final Hash HASH_THREE =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000003");
  public static final Hash HASH_FOUR =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000004");
  public static final KeyPair KEY_PAIR_ONE = new KeyPair(BigInteger.ONE);
  public static final BlockchainAddress KEY_PAIR_ONE_ADDRESS =
      KEY_PAIR_ONE.getPublic().createAddress();
  public static final BlockchainAddress CONTRACT_ONE =
      BlockchainAddress.fromString("04FF00000000000000000000000000000000000001");
  public static final BlockchainAddress CONTRACT_MINT =
      BlockchainAddress.fromString("04FF00000000000000000000000000000000000002");
  public static final List<BlockchainAddress> GOV_SHARD_ADDRESSES = List.of(ACCOUNT_GOV);
}
