package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.storage.Storage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/** A (concurrent) map backed storage implementation. */
public final class MapBackedStorage implements Storage {

  private final Map<Hash, byte[]> db;
  private final boolean immutable;

  /**
   * Creates a new storage object backed by a concurrent hash map.
   *
   * @param immutable true if ignore overwrite
   */
  public MapBackedStorage(boolean immutable) {
    this.immutable = immutable;
    db = new ConcurrentHashMap<>();
  }

  @Override
  public <T> T read(Hash hash, Function<SafeDataInputStream, T> reader) {
    byte[] bytes = readRaw(hash);
    if (bytes != null) {
      return reader.apply(SafeDataInputStream.createFromBytes(bytes));
    } else {
      return null;
    }
  }

  private byte[] lookup(Hash key) {
    return db.get(key);
  }

  @Override
  public <T> Stream<T> stream(Function<SafeDataInputStream, T> reader) {
    return db.keySet().stream()
        .sorted(Hash::compareTo)
        .map(db::get)
        .map(bytes -> reader.apply(SafeDataInputStream.createFromBytes(bytes)));
  }

  @Override
  public byte[] readRaw(Hash hash) {
    return lookup(hash);
  }

  @Override
  public boolean has(Hash hash) {
    byte[] bytes = lookup(hash);
    return bytes != null;
  }

  @Override
  public boolean isEmpty() {
    return db.isEmpty();
  }

  @Override
  public void remove(Hash hash) {
    db.remove(hash);
  }

  @Override
  public void write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    boolean shouldWrite = !immutable || !has(hash);
    if (shouldWrite) {
      db.put(hash, SafeDataOutputStream.serialize(writer));
    }
  }

  @Override
  public void clear() {
    db.clear();
  }
}
