package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.AllowDto;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionHashRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsRequest;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsResponse;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessResponseDto;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.NetworkOptionsResponseDto;
import com.partisiablockchain.rosetta.dto.NetworkRequestDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.OperationIdentifierDto;
import com.partisiablockchain.rosetta.dto.PublicKeyDto;
import com.partisiablockchain.rosetta.dto.SignatureDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierResponse;
import com.partisiablockchain.rosetta.dto.VersionDto;
import com.partisiablockchain.server.BetanetAddresses;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RosettaOfflineResourceTest {

  private RosettaOfflineResource resource;
  private ShardedBlockchain ledgers;
  private static final String SUB_CHAIN = "Shard1";

  private String network;

  @BeforeEach
  void setup() {
    ledgers = new ShardedBlockchain(new HashMap<>(createLedgers()));
    resource =
        new RosettaOfflineResource(
            new ArrayList<>(ledgers.getShards().keySet()), ledgers.getGovShard().getChainId());
    network = ledgers.getGovShard().getChainId();
  }

  static Map<String, BlockchainLedger> createLedgers() {
    BlockchainLedger governanceLedger =
        BlockchainLedgerHelper.createLedger(
            createTempDb("TestDirGov"),
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Gov");
    BlockchainLedger shardOneLedger =
        BlockchainLedgerHelper.createLedger(
            createTempDb("TestDirShard1"),
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            SUB_CHAIN);
    HashMap<String, BlockchainLedger> ledgers = new HashMap<>();
    ledgers.put(null, governanceLedger);
    ledgers.put(SUB_CHAIN, shardOneLedger);
    return ledgers;
  }

  private static RootDirectory createTempDb(String testDir) {
    return ExceptionConverter.call(
        () ->
            new RootDirectory(
                Files.createTempDirectory(testDir).toFile(),
                (ignored, immutable) -> new MapBackedStorage(immutable)),
        "Could not create temp directory for test DB");
  }

  @Test
  void derive() {
    byte[] bytes = new KeyPair(BigInteger.ONE).getPublic().asBytes();
    PublicKeyDto publicKey = new PublicKeyDto(bytes, PublicKeyDto.CURVE_TYPE);
    ConstructionDeriveRequestDto request =
        new ConstructionDeriveRequestDto(
            RosettaTestHelper.createNetworkIdentifier(
                ledgers.getGovShard().getChainId(), SUB_CHAIN),
            publicKey);

    ConstructionDeriveResponseDto response = resource.derive(request);
    BlockchainPublicKey blockchainPublicKey = BlockchainPublicKey.fromEncodedEcPoint(bytes);
    Assertions.assertThat(blockchainPublicKey.createAddress())
        .isEqualTo(response.accountIdentifier().address());

    NetworkIdentifierDto invalid =
        new NetworkIdentifierDto("invalid", "invalid", new SubNetworkIdentifierDto("invalid"));
    RosettaTestHelper.checkError(
        () -> resource.derive(new ConstructionDeriveRequestDto(invalid, publicKey)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void deriveWrongCurveType() {
    byte[] bytes = new KeyPair(BigInteger.ONE).getPublic().asBytes();
    String wrongCurveType = "wrong curve type";
    PublicKeyDto publicKey = new PublicKeyDto(bytes, wrongCurveType);
    ConstructionDeriveRequestDto request =
        new ConstructionDeriveRequestDto(
            RosettaTestHelper.createNetworkIdentifier(
                ledgers.getGovShard().getChainId(), SUB_CHAIN),
            publicKey);
    RosettaTestHelper.checkError(() -> resource.derive(request), ErrorDto.UNKNOWN_CURVE_TYPE);
  }

  @Test
  void hexSerialization() {
    RosettaAdapter.HexBytesFromString decoder = new RosettaAdapter.HexBytesFromString();
    RosettaTestHelper.checkError(
        () -> decoder._deserialize("xx", null), ErrorDto.INVALID_HEX_BYTES);
    byte[] bts = decoder._deserialize("0f", null);
    Assertions.assertThat(bts).isEqualTo(new byte[] {15});

    RosettaAdapter.HexBytesToString encoder = new RosettaAdapter.HexBytesToString();
    String convert = encoder.convert(new byte[] {15});
    Assertions.assertThat(convert).isEqualTo("0f");
  }

  @Test
  void hash() {
    byte[] signedTx = SafeDataOutputStream.serialize(createSignedTransaction(false)::write);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionHashRequestDto request =
        new ConstructionHashRequestDto(networkIdentifier, signedTx);
    TransactionIdentifierResponse response = resource.hash(request);
    SignedTransaction expectedSignedTransaction =
        SignedTransaction.read(
            ledgers.getGovShard().getChainId(), SafeDataInputStream.createFromBytes(signedTx));
    Assertions.assertThat(response.transactionIdentifierDto().hash())
        .isEqualTo(expectedSignedTransaction.identifier());
  }

  @Test
  void networkOptions() {
    NetworkRequestDto networkRequest =
        new NetworkRequestDto(RosettaTestHelper.createNetworkIdentifier(network, "Gov"));
    NetworkOptionsResponseDto networkOptions = resource.networkOptions(networkRequest);

    Assertions.assertThat(networkOptions.version()).isEqualTo(VersionDto.DEFAULT);
    Assertions.assertThat(networkOptions.allow()).isEqualTo(AllowDto.DEFAULT);

    RosettaTestHelper.checkError(
        () ->
            resource.networkOptions(
                new NetworkRequestDto(
                    new NetworkIdentifierDto(
                        "invalid", "invalid", new SubNetworkIdentifierDto("invalid")))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void networkOptions_invalidNetworkIdentifier() {
    NetworkIdentifierDto invalidBlockchainNetworkIdentifier =
        new NetworkIdentifierDto(
            "NotPartisiaBlockchain", network, new SubNetworkIdentifierDto(SUB_CHAIN));

    RosettaTestHelper.checkError(
        () -> resource.networkOptions(new NetworkRequestDto(invalidBlockchainNetworkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));

    NetworkIdentifierDto invalidNetworkNetworkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            "UnknownNetwork",
            new SubNetworkIdentifierDto("Gov"));

    RosettaTestHelper.checkError(
        () -> resource.networkOptions(new NetworkRequestDto(invalidNetworkNetworkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid network"));

    NetworkIdentifierDto invalidSubNetworkNetworkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            network,
            new SubNetworkIdentifierDto("UnknownShard"));

    RosettaTestHelper.checkError(
        () -> resource.networkOptions(new NetworkRequestDto(invalidSubNetworkNetworkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe(
            "No Shard exists for the given network ChainId"));
  }

  @Test
  void networkList() {
    List<NetworkIdentifierDto> networkIdentifiers = resource.networkList().networkIdentifiers();

    for (NetworkIdentifierDto networkIdentifierDto : networkIdentifiers) {
      Assertions.assertThat(networkIdentifierDto.blockchain())
          .isEqualTo(RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN);
      Assertions.assertThat(networkIdentifierDto.network()).isEqualTo(network);
    }

    List<String> subNetworkIdentifiers =
        networkIdentifiers.stream()
            .map(networkIdentifierDto -> networkIdentifierDto.subNetworkIdentifier().network())
            .toList();
    Assertions.assertThat(subNetworkIdentifiers).isEqualTo(ledgers.getShardNames());
  }

  @Test
  void constructionPayloads() {
    NetworkIdentifierDto networkIdentifierDto =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            network,
            new SubNetworkIdentifierDto(SUB_CHAIN));
    OperationIdentifierDto operationIdentifierDto = new OperationIdentifierDto(0);
    AccountIdentifierDto signer = new AccountIdentifierDto(TestValues.ACCOUNT_TWO);
    ConstructionMetadataResponseDto.Metadata metadata =
        new ConstructionMetadataResponseDto.Metadata(9, 10, null);
    OperationDto operationAdd =
        new OperationDto(
            operationIdentifierDto,
            OperationDto.Type.ADD,
            new AccountIdentifierDto(TestValues.ACCOUNT_ONE),
            new AmountDto("100", CurrencyDto.MPC),
            null,
            null);
    OperationDto operationDeduct =
        new OperationDto(
            operationIdentifierDto,
            OperationDto.Type.DEDUCT,
            signer,
            new AmountDto("-100", CurrencyDto.MPC),
            null,
            null);

    ConstructionPayloadsRequest request =
        new ConstructionPayloadsRequest(
            networkIdentifierDto, List.of(operationAdd, operationDeduct), metadata);
    ConstructionPayloadsResponse response = resource.constructionPayloads(request);
    SafeDataInputStream inputStreamUnsignedTransaction =
        SafeDataInputStream.createFromBytes(response.unsignedTransaction());
    Assertions.assertThat(BlockchainAddress.read(inputStreamUnsignedTransaction))
        .isEqualTo(signer.address());
    Assertions.assertThat(inputStreamUnsignedTransaction.readLong()).isEqualTo(9L);
    Assertions.assertThat(inputStreamUnsignedTransaction.readLong()).isEqualTo(10L);
    Assertions.assertThat(inputStreamUnsignedTransaction.readLong()).isEqualTo(16000L);
    Assertions.assertThat(BlockchainAddress.read(inputStreamUnsignedTransaction))
        .isEqualTo(BetanetAddresses.MPC_TOKEN_CONTRACT);
    // Length of rpc
    Assertions.assertThat(inputStreamUnsignedTransaction.readInt()).isEqualTo(30);
    Assertions.assertThat(inputStreamUnsignedTransaction.readUnsignedByte())
        .isEqualTo(MpcTokenInvocations.TRANSFER);
    Assertions.assertThat(BlockchainAddress.read(inputStreamUnsignedTransaction))
        .isEqualTo(TestValues.ACCOUNT_ONE);
    Assertions.assertThat(inputStreamUnsignedTransaction.readLong()).isEqualTo(100);

    Hash signingPayloadHash =
        Hash.read(SafeDataInputStream.createFromBytes(response.payloads().get(0).hexBytes()));
    Hash expected =
        Hash.create(
            (s) -> {
              s.write(
                  RosettaOfflineResource.splitTransaction(response.unsignedTransaction())
                      .unsignedTransaction());
              s.writeString(networkIdentifierDto.network());
            });
    Assertions.assertThat(signingPayloadHash).isEqualTo(expected);

    // Large memo
    String largeMemoValue = "this is a large memo";
    ConstructionMetadataResponseDto.Metadata metadataLargeMemo =
        new ConstructionMetadataResponseDto.Metadata(9, 10, largeMemoValue);
    ConstructionPayloadsRequest requestLargeMemo =
        new ConstructionPayloadsRequest(
            networkIdentifierDto, List.of(operationAdd, operationDeduct), metadataLargeMemo);
    ConstructionPayloadsResponse responseLargeMemo =
        resource.constructionPayloads(requestLargeMemo);
    SafeDataInputStream inputStreamLargeMemo =
        SafeDataInputStream.createFromBytes(responseLargeMemo.unsignedTransaction());
    Assertions.assertThat(BlockchainAddress.read(inputStreamLargeMemo)).isEqualTo(signer.address());
    Assertions.assertThat(inputStreamLargeMemo.readLong()).isEqualTo(9L);
    Assertions.assertThat(inputStreamLargeMemo.readLong()).isEqualTo(10L);
    Assertions.assertThat(inputStreamLargeMemo.readLong()).isEqualTo(16_240L);
    Assertions.assertThat(BlockchainAddress.read(inputStreamLargeMemo))
        .isEqualTo(BetanetAddresses.MPC_TOKEN_CONTRACT);
    // Length of rpc
    Assertions.assertThat(inputStreamLargeMemo.readInt()).isEqualTo(54);
    Assertions.assertThat(inputStreamLargeMemo.readUnsignedByte())
        .isEqualTo(MpcTokenInvocations.TRANSFER_LARGE_MEMO);
    Assertions.assertThat(BlockchainAddress.read(inputStreamLargeMemo))
        .isEqualTo(TestValues.ACCOUNT_ONE);
    Assertions.assertThat(inputStreamLargeMemo.readLong()).isEqualTo(100);
    Assertions.assertThat(inputStreamLargeMemo.readString()).isEqualTo(largeMemoValue);

    // Small memo
    long smallMemoValue = 123L;
    ConstructionMetadataResponseDto.Metadata metadataSmallMemo =
        new ConstructionMetadataResponseDto.Metadata(9, 10, Long.toString(smallMemoValue));
    ConstructionPayloadsRequest requestSmallMemo =
        new ConstructionPayloadsRequest(
            networkIdentifierDto, List.of(operationAdd, operationDeduct), metadataSmallMemo);
    ConstructionPayloadsResponse responseSmallMemo =
        resource.constructionPayloads(requestSmallMemo);
    SafeDataInputStream inputStreamSmallMemo =
        SafeDataInputStream.createFromBytes(responseSmallMemo.unsignedTransaction());
    Assertions.assertThat(BlockchainAddress.read(inputStreamSmallMemo)).isEqualTo(signer.address());
    Assertions.assertThat(inputStreamSmallMemo.readLong()).isEqualTo(9L);
    Assertions.assertThat(inputStreamSmallMemo.readLong()).isEqualTo(10L);
    Assertions.assertThat(inputStreamSmallMemo.readLong()).isEqualTo(16_080L);
    Assertions.assertThat(BlockchainAddress.read(inputStreamSmallMemo))
        .isEqualTo(BetanetAddresses.MPC_TOKEN_CONTRACT);
    // Length of rpc
    Assertions.assertThat(inputStreamSmallMemo.readInt()).isEqualTo(38);
    Assertions.assertThat(inputStreamSmallMemo.readUnsignedByte())
        .isEqualTo(MpcTokenInvocations.TRANSFER_SMALL_MEMO);
    Assertions.assertThat(BlockchainAddress.read(inputStreamSmallMemo))
        .isEqualTo(TestValues.ACCOUNT_ONE);
    Assertions.assertThat(inputStreamSmallMemo.readLong()).isEqualTo(100);
    Assertions.assertThat(inputStreamSmallMemo.readLong()).isEqualTo(smallMemoValue);
  }

  @Test
  void constructionPayloads_errors() {
    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(ledgers.getGovShard().getChainId(), SUB_CHAIN);
    CurrencyDto currencyDto = CurrencyDto.MPC;
    AmountDto amountDto = new AmountDto("100", currencyDto);
    OperationIdentifierDto operationIdentifierDto = new OperationIdentifierDto(0);
    AccountIdentifierDto account = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    ConstructionMetadataResponseDto.Metadata metadataDto =
        new ConstructionMetadataResponseDto.Metadata(10, 10, null);
    List<OperationDto> operations =
        List.of(
            new OperationDto(
                operationIdentifierDto, OperationDto.Type.ADD, account, amountDto, null, null),
            new OperationDto(
                operationIdentifierDto, OperationDto.Type.DEDUCT, account, amountDto, null, null),
            new OperationDto(
                operationIdentifierDto, OperationDto.Type.DEDUCT, account, amountDto, null, null));
    ConstructionPayloadsRequest request =
        new ConstructionPayloadsRequest(networkIdentifier, operations, metadataDto);
    RosettaTestHelper.checkError(
        () -> resource.constructionPayloads(request), ErrorDto.UNKNOWN_OPERATION_TYPE);
    operations =
        List.of(
            new OperationDto(
                operationIdentifierDto,
                OperationDto.Type.ADD,
                account,
                amountDto,
                OperationDto.Status.SUCCESS,
                null),
            new OperationDto(
                operationIdentifierDto,
                OperationDto.Type.ADD,
                account,
                amountDto,
                OperationDto.Status.SUCCESS,
                null));
    ConstructionPayloadsRequest request2 =
        new ConstructionPayloadsRequest(networkIdentifier, operations, metadataDto);
    RosettaTestHelper.checkError(
        () -> resource.constructionPayloads(request2), ErrorDto.UNKNOWN_OPERATION_TYPE);

    ConstructionPayloadsRequest request3 =
        new ConstructionPayloadsRequest(
            new NetworkIdentifierDto("BLOCKCHAIN", "network", new SubNetworkIdentifierDto("Gov")),
            List.of(
                new OperationDto(
                    operationIdentifierDto,
                    OperationDto.Type.ADD,
                    account,
                    amountDto,
                    OperationDto.Status.SUCCESS,
                    null),
                new OperationDto(
                    operationIdentifierDto,
                    OperationDto.Type.DEDUCT,
                    account,
                    amountDto,
                    OperationDto.Status.SUCCESS,
                    null)),
            metadataDto);
    RosettaTestHelper.checkError(
        () -> resource.constructionPayloads(request3),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void constructionParse() {
    NetworkIdentifierDto networkIdentifierDto =
        RosettaTestHelper.createNetworkIdentifier(ledgers.getGovShard().getChainId(), SUB_CHAIN);

    ConstructionMetadataResponseDto.Metadata metadataDto =
        new ConstructionMetadataResponseDto.Metadata(10, 10, null);

    final OperationDto addOperation =
        operation(
            0,
            TestValues.ACCOUNT_TWO,
            OperationDto.Type.ADD,
            100,
            null,
            new OperationDto.Metadata(
                null, new AccountIdentifierDto(TestValues.KEY_PAIR_ONE_ADDRESS)));
    final OperationDto deductOperation =
        operation(
            1,
            TestValues.KEY_PAIR_ONE_ADDRESS,
            OperationDto.Type.DEDUCT,
            -100,
            null,
            new OperationDto.Metadata(null, new AccountIdentifierDto(TestValues.ACCOUNT_TWO)));
    ConstructionPayloadsRequest request =
        new ConstructionPayloadsRequest(
            networkIdentifierDto, List.of(addOperation, deductOperation), metadataDto);
    ConstructionPayloadsResponse response = resource.constructionPayloads(request);

    ConstructionParseResponseDto parseResponse =
        resource.constructionParse(
            new ConstructionParseRequestDto(
                networkIdentifierDto, false, response.unsignedTransaction()));

    List<OperationDto> operations = parseResponse.operations();
    Assertions.assertThat(operations.get(0)).usingRecursiveComparison().isEqualTo(addOperation);
    Assertions.assertThat(operations.get(1)).usingRecursiveComparison().isEqualTo(deductOperation);

    Hash messageHash =
        Hash.read(SafeDataInputStream.createFromBytes(response.payloads().get(0).hexBytes()));
    Signature signature = TestValues.KEY_PAIR_ONE.sign(messageHash);
    byte[] payload =
        SafeDataOutputStream.serialize(
            stream -> {
              signature.write(stream);
              stream.write(
                  RosettaOfflineResource.splitTransaction(response.unsignedTransaction())
                      .unsignedTransaction());
            });

    ConstructionParseRequestDto request1 =
        new ConstructionParseRequestDto(networkIdentifierDto, true, payload);
    ConstructionParseResponseDto parseResponseSigned = resource.constructionParse(request1);

    final OperationDto deductOperationSigned =
        operation(
            1,
            TestValues.KEY_PAIR_ONE_ADDRESS,
            OperationDto.Type.DEDUCT,
            -100,
            null,
            new OperationDto.Metadata(null, new AccountIdentifierDto(TestValues.ACCOUNT_TWO)));
    Assertions.assertThat(parseResponseSigned.operations().get(0))
        .usingRecursiveComparison()
        .isEqualTo(addOperation);
    Assertions.assertThat(parseResponseSigned.operations().get(1))
        .usingRecursiveComparison()
        .isEqualTo(deductOperationSigned);
    Assertions.assertThat(request1.networkIdentifier())
        .usingRecursiveComparison()
        .isEqualTo(networkIdentifierDto);
  }

  @Test
  void constructionParseWithShortMemo() {

    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(ledgers.getGovShard().getChainId(), SUB_CHAIN);

    OperationDto add =
        operation(
            0,
            TestValues.ACCOUNT_TWO,
            OperationDto.Type.ADD,
            10,
            null,
            new OperationDto.Metadata(
                "100", new AccountIdentifierDto(TestValues.KEY_PAIR_ONE_ADDRESS)));
    OperationDto deduct =
        operation(
            1,
            TestValues.KEY_PAIR_ONE_ADDRESS,
            OperationDto.Type.DEDUCT,
            -10,
            null,
            new OperationDto.Metadata("100", new AccountIdentifierDto(TestValues.ACCOUNT_TWO)));

    ConstructionPayloadsResponse payloads =
        resource.constructionPayloads(
            new ConstructionPayloadsRequest(
                networkIdentifier,
                List.of(add, deduct),
                new ConstructionMetadataResponseDto.Metadata(10, 10, "100")));

    ConstructionParseResponseDto parsed =
        resource.constructionParse(
            new ConstructionParseRequestDto(
                networkIdentifier, false, payloads.unsignedTransaction()));

    Assertions.assertThat(parsed.operations()).hasSize(2);
    Assertions.assertThat(parsed.operations().get(0)).usingRecursiveComparison().isEqualTo(add);
  }

  @Test
  void constructionParseWithLargeMemo() {

    NetworkIdentifierDto networkIdentifier =
        RosettaTestHelper.createNetworkIdentifier(ledgers.getGovShard().getChainId(), SUB_CHAIN);

    OperationDto add =
        operation(
            0,
            TestValues.ACCOUNT_TWO,
            OperationDto.Type.ADD,
            123,
            null,
            new OperationDto.Metadata(
                "hello", new AccountIdentifierDto(TestValues.KEY_PAIR_ONE_ADDRESS)));
    OperationDto deduct =
        operation(
            1,
            TestValues.KEY_PAIR_ONE_ADDRESS,
            OperationDto.Type.DEDUCT,
            -123,
            null,
            new OperationDto.Metadata("hello", new AccountIdentifierDto(TestValues.ACCOUNT_TWO)));

    ConstructionPayloadsResponse payloads =
        resource.constructionPayloads(
            new ConstructionPayloadsRequest(
                networkIdentifier,
                List.of(add, deduct),
                new ConstructionMetadataResponseDto.Metadata(10, 10, "hello")));

    ConstructionParseResponseDto parsed =
        resource.constructionParse(
            new ConstructionParseRequestDto(
                networkIdentifier, false, payloads.unsignedTransaction()));

    Assertions.assertThat(parsed.operations()).hasSize(2);
    Assertions.assertThat(parsed.operations().get(0)).isEqualTo(add);
  }

  @Test
  void constructionParseErrors() {
    NetworkIdentifierDto networkIdentifierDto =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            network,
            new SubNetworkIdentifierDto(SUB_CHAIN));
    byte[] payload =
        SafeDataOutputStream.serialize(
            s -> {
              TestValues.ACCOUNT_ONE.write(s);
              s.writeLong(10);
              s.writeLong(11);
              s.writeLong(12);
              BetanetAddresses.LARGE_ORACLE_CONTRACT.write(s);
            });
    ConstructionParseRequestDto request =
        new ConstructionParseRequestDto(networkIdentifierDto, false, payload);
    RosettaTestHelper.checkError(
        () -> resource.constructionParse(request), ErrorDto.INVALID_BLOCKCHAIN_ADDRESS);
    byte[] transaction =
        SafeDataOutputStream.serialize(
            s -> {
              TestValues.ACCOUNT_ONE.write(s);
              s.writeLong(10);
              s.writeLong(11);
              s.writeLong(12);
              BetanetAddresses.MPC_TOKEN_CONTRACT.write(s);
              s.writeInt(10);
              s.writeByte(5);
            });
    ConstructionParseRequestDto request2 =
        new ConstructionParseRequestDto(networkIdentifierDto, false, transaction);
    RosettaTestHelper.checkError(
        () -> resource.constructionParse(request2), ErrorDto.INVALID_INVOCATION_BYTE);
    ConstructionParseRequestDto request3 =
        new ConstructionParseRequestDto(
            new NetworkIdentifierDto("BLOCKCHAIN", "network", new SubNetworkIdentifierDto("Gov")),
            false,
            transaction);
    RosettaTestHelper.checkError(
        () -> resource.constructionParse(request3),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  private OperationDto operation(
      int operationIndex, BlockchainAddress address, OperationDto.Type type, int amount) {
    return operation(operationIndex, address, type, amount, OperationDto.Status.SUCCESS, null);
  }

  private OperationDto operation(
      int operationIndex,
      BlockchainAddress address,
      OperationDto.Type type,
      int amount,
      OperationDto.Status status,
      OperationDto.Metadata metadata) {
    return new OperationDto(
        new OperationIdentifierDto(operationIndex),
        type,
        new AccountIdentifierDto(address),
        new AmountDto(Long.toString(amount), CurrencyDto.MPC),
        status,
        metadata);
  }

  @Test
  void validateOperationAmounts() {
    OperationDto op1 = operation(0, TestValues.ACCOUNT_ONE, OperationDto.Type.ADD, 0);
    OperationDto op2 = operation(1, TestValues.ACCOUNT_TWO, OperationDto.Type.DEDUCT, 1);
    RosettaTestHelper.checkError(
        () -> RosettaOfflineResource.validateTransfer(List.of(op1, op2)), ErrorDto.INVALID_AMOUNT);
  }

  @Test
  void checkInvalidIndividualAmounts() {
    Assertions.assertThat(RosettaOfflineResource.validateTransferAmounts(1, 2)).isTrue();
    Assertions.assertThat(RosettaOfflineResource.validateTransferAmounts(0, 2)).isTrue();
    Assertions.assertThat(RosettaOfflineResource.validateTransferAmounts(-2, -1)).isTrue();
    Assertions.assertThat(RosettaOfflineResource.validateTransferAmounts(-2, 0)).isTrue();
  }

  @Test
  void checkOperationAmountsAreValid() {
    OperationIdentifierDto identifier = new OperationIdentifierDto(0);
    CurrencyDto currency1 = new CurrencyDto("MPC", 10);
    AmountDto amount1 = new AmountDto("100", currency1);
    AccountIdentifierDto accountIdentifier = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    final OperationDto recipient =
        new OperationDto(
            identifier,
            OperationDto.Type.ADD,
            accountIdentifier,
            amount1,
            OperationDto.Status.SUCCESS,
            null);
    CurrencyDto currency2 = new CurrencyDto("MPCC", 10);
    AmountDto amount2 = new AmountDto("-100", currency2);
    OperationDto sender =
        new OperationDto(
            identifier,
            OperationDto.Type.DEDUCT,
            accountIdentifier,
            amount2,
            OperationDto.Status.SUCCESS,
            null);
    RosettaTestHelper.checkError(
        () -> RosettaOfflineResource.validateTransfer(List.of(sender, recipient)),
        ErrorDto.UNKNOWN_OPERATION_TYPE);

    CurrencyDto currencyDecimal1 = new CurrencyDto("MPC", 10);
    AmountDto amountDecimal1 = new AmountDto("100", currencyDecimal1);
    final OperationDto recipientDecimal =
        new OperationDto(
            identifier,
            OperationDto.Type.ADD,
            accountIdentifier,
            amountDecimal1,
            OperationDto.Status.SUCCESS,
            null);
    CurrencyDto currencyDecimal2 = new CurrencyDto("MPC", 100);
    AmountDto amountDecimal2 = new AmountDto("-100", currencyDecimal2);
    OperationDto senderDecimal =
        new OperationDto(
            identifier,
            OperationDto.Type.DEDUCT,
            accountIdentifier,
            amountDecimal2,
            OperationDto.Status.SUCCESS,
            null);
    RosettaTestHelper.checkError(
        () -> RosettaOfflineResource.validateTransfer(List.of(senderDecimal, recipientDecimal)),
        ErrorDto.UNKNOWN_OPERATION_TYPE);
  }

  @Test
  void combine() {
    long nonce = 101;
    long validToTime = 10;
    long cost = 100;
    byte[] rpc =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(10);
            });

    // Create unsigned transaction
    Consumer<SafeDataOutputStream> unsignedTransactionConsumer =
        stream -> {
          stream.writeLong(nonce);
          stream.writeLong(validToTime);
          stream.writeLong(cost);
          BetanetAddresses.MPC_TOKEN_CONTRACT.write(stream);
          stream.writeDynamicBytes(rpc);
        };
    byte[] transaction = SafeDataOutputStream.serialize(unsignedTransactionConsumer);

    SignatureDto signature =
        RosettaTestHelper.createSignatureDtoFromMessageHash(
            Hash.create(
                s -> {
                  s.write(SafeDataOutputStream.serialize(unsignedTransactionConsumer));
                  s.writeString(network);
                }));

    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    byte[] paddedTx =
        SafeDataOutputStream.serialize(
            s -> {
              TestValues.KEY_PAIR_ONE_ADDRESS.write(s);
              s.write(transaction);
            });
    ConstructionCombineRequestDto request =
        new ConstructionCombineRequestDto(networkIdentifier, paddedTx, List.of(signature));

    ConstructionCombineResponseDto response = resource.combine(request);

    SignedTransaction deserialize =
        SafeDataInputStream.deserialize(
            stream -> SignedTransaction.read(request.networkIdentifier().network(), stream),
            response.signedTransaction());

    Assertions.assertThat(deserialize.getCore().getNonce()).isEqualTo(nonce);
    Assertions.assertThat(deserialize.getCore().getValidToTime()).isEqualTo(validToTime);
    Assertions.assertThat(deserialize.getCore().getCost()).isEqualTo(cost);
    Assertions.assertThat(deserialize.getTransaction().getAddress())
        .isEqualTo(BetanetAddresses.MPC_TOKEN_CONTRACT);
    Assertions.assertThat(deserialize.getTransaction().getRpc()).isEqualTo(rpc);
  }

  @Test
  void invalidSenderInTransaction() {
    byte[] rpc =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER);
              TestValues.ACCOUNT_ONE.write(stream);
              stream.writeLong(10);
            });

    // Create unsigned transaction
    Consumer<SafeDataOutputStream> unsignedTransactionConsumer =
        stream -> {
          stream.writeLong(101);
          stream.writeLong(10);
          stream.writeLong(100);
          BetanetAddresses.MPC_TOKEN_CONTRACT.write(stream);
          stream.writeDynamicBytes(rpc);
        };
    SignatureDto signature =
        RosettaTestHelper.createSignatureDtoFromMessageHash(
            Hash.create(unsignedTransactionConsumer));
    byte[] paddedTx =
        SafeDataOutputStream.serialize(
            s -> {
              TestValues.ACCOUNT_ONE.write(s);
              s.write(SafeDataOutputStream.serialize(unsignedTransactionConsumer));
            });

    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    paddedTx,
                    List.of(signature))),
        ErrorDto.INVALID_SIGNATURE);
  }

  @Test
  void combine_twoSignatures() {
    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    null,
                    List.of(
                        RosettaTestHelper.DUMMY_SIGNATURE_DTO,
                        RosettaTestHelper.DUMMY_SIGNATURE_DTO))),
        ErrorDto.INVALID_SIGNATURE);
  }

  @Test
  void combine_wrongNetworkIdentifier() {
    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        "InvalidNetworkIdentifier",
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    null,
                    List.of(RosettaTestHelper.DUMMY_SIGNATURE_DTO))),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void combine_malformedSignature() {
    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    new byte[3],
                    List.of(
                        new SignatureDto(null, null, SignatureDto.SIGNATURE_TYPE, new byte[0])))),
        ErrorDto.INVALID_SIGNATURE);
  }

  @Test
  void combine_malformedTransaction() {
    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    SafeDataOutputStream.serialize(TestValues.KEY_PAIR_ONE_ADDRESS::write),
                    List.of(
                        RosettaTestHelper.createSignatureDtoFromMessageHash(
                            Hash.create(
                                (s) ->
                                    SafeDataOutputStream.serialize(
                                        stream -> {
                                          stream.write(new byte[0]);
                                          stream.writeString(network);
                                        })))))),
        ErrorDto.INVALID_TRANSACTION);
  }

  @Test
  void combine_wrongSignatureType() {
    RosettaTestHelper.checkError(
        () ->
            resource.combine(
                new ConstructionCombineRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    null,
                    List.of(new SignatureDto(null, null, "wrongSignatureType", new byte[0])))),
        ErrorDto.INVALID_SIGNATURE);
  }

  @Test
  void preprocessMetadata() {
    ConstructionPreprocessRequestDto request = createDummyPreprocessRequest(null);
    ConstructionPreprocessResponseDto response = resource.preprocess(request);
    OperationDto deductOperation = request.operations().get(1);
    Assertions.assertThat(deductOperation.type()).isEqualTo(OperationDto.Type.DEDUCT);
    BlockchainAddress senderAddress = deductOperation.account().address();
    Assertions.assertThat(response.options().senderAddress()).isEqualTo(senderAddress);

    String memo = "this is a memo";
    ConstructionPreprocessRequestDto requestMemo =
        createDummyPreprocessRequest(new ConstructionPreprocessRequestDto.Metadata(memo));
    ConstructionPreprocessResponseDto responseMemo = resource.preprocess(requestMemo);
    Assertions.assertThat(responseMemo.options().memo()).isEqualTo(memo);

    // Use response in metadata
    RosettaOnlineResource onlineResource = new RosettaOnlineResource(ledgers.getShards());
    NetworkIdentifierDto networkIdentifier = request.networkIdentifier();
    ConstructionMetadataRequestDto metadataRequest =
        new ConstructionMetadataRequestDto(networkIdentifier, response.options());
    ConstructionMetadataResponseDto metadataResponse = onlineResource.metadata(metadataRequest);

    ConstructionMetadataRequestDto metadataRequestMemo =
        new ConstructionMetadataRequestDto(networkIdentifier, responseMemo.options());
    ConstructionMetadataResponseDto metadataResponseMemo =
        onlineResource.metadata(metadataRequestMemo);

    BlockchainLedger blockchainLedger = ledgers.getGovShard();
    Assertions.assertThat(metadataResponse.metadata().nonce())
        .isEqualTo(blockchainLedger.getAccountState(senderAddress).getNonce());
    Assertions.assertThat(metadataResponse.metadata().validToTime())
        .isEqualTo(
            blockchainLedger.getLatestBlock().getProductionTime()
                + RosettaOnlineResource.VALID_TIME);
    Assertions.assertThat(metadataResponseMemo.metadata().memo()).isEqualTo(memo);
  }

  @Test
  void preprocess_onlyDeduct() {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.DEDUCT,
            recipient,
            new AmountDto("-10", new CurrencyDto("MPC", 4)),
            null,
            null);

    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionPreprocessRequestDto request =
        new ConstructionPreprocessRequestDto(networkIdentifier, List.of(deductOperation), null);
    RosettaTestHelper.checkError(
        () -> resource.preprocess(request), ErrorDto.UNKNOWN_OPERATION_TYPE);
  }

  @Test
  void preprocess_sameOperationType() {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("10", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);

    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));
    ConstructionPreprocessRequestDto request =
        new ConstructionPreprocessRequestDto(
            networkIdentifier, List.of(addOperation, addOperation), null);
    RosettaTestHelper.checkError(
        () -> resource.preprocess(request), ErrorDto.UNKNOWN_OPERATION_TYPE);
  }

  @Test
  void unknownNetworkIdentifier() {
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            "some blockchain", "network", new SubNetworkIdentifierDto("Shard1"));
    RosettaTestHelper.checkError(
        () -> resource.networkOptions(new NetworkRequestDto(networkIdentifier)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
    RosettaTestHelper.checkError(
        () ->
            resource.hash(
                new ConstructionHashRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        "network",
                        new SubNetworkIdentifierDto("Shard1")),
                    new byte[0])),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid network"));
  }

  @Test
  void preprocess_secondAccountIsDeduct() {
    ConstructionPreprocessRequestDto request = createDummyPreprocessRequest(null);
    ConstructionPreprocessResponseDto response = resource.preprocess(request);
    OperationDto deductOperation = request.operations().get(1);
    Assertions.assertThat(deductOperation.type()).isEqualTo(OperationDto.Type.DEDUCT);
    Assertions.assertThat(response.options().senderAddress())
        .isEqualTo(deductOperation.account().address());
  }

  @Test
  void preprocess_invalid_networkIdentifier() {
    RosettaTestHelper.checkError(
        () ->
            resource.preprocess(
                new ConstructionPreprocessRequestDto(
                    new NetworkIdentifierDto(
                        "wrongBlockchain",
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto("Gov")),
                    List.of(),
                    null)),
        ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name"));
  }

  @Test
  void preprocess_nullMemo() {
    ConstructionPreprocessRequestDto request =
        createDummyPreprocessRequest(new ConstructionPreprocessRequestDto.Metadata(null));
    ConstructionPreprocessResponseDto response = resource.preprocess(request);
    Assertions.assertThat(response.options().memo()).isNull();
  }

  @Test
  void preprocess_positiveDeduct() {
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    assertIsInvalidAddDeduct(addOperation, deductOperation);
  }

  @Test
  void preprocess_negativeAdd() {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("-11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("-11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    assertIsInvalidAddDeduct(addOperation, deductOperation);
  }

  @Test
  void preprocess_zeroDeduct() {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("0", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    assertIsInvalidAddDeduct(addOperation, deductOperation);
  }

  @Test
  void preprocess_zeroAdd() {
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("-11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("0", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    assertIsInvalidAddDeduct(addOperation, deductOperation);
  }

  @Test
  void preprocess_sumNotZero() {
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("-123", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("11", new CurrencyDto("MPC", 4)),
            OperationDto.Status.SUCCESS,
            null);
    assertIsInvalidAddDeduct(addOperation, deductOperation);
  }

  private void assertIsInvalidAddDeduct(OperationDto addOperation, OperationDto deductOperation) {
    RosettaTestHelper.checkError(
        () ->
            resource.preprocess(
                new ConstructionPreprocessRequestDto(
                    new NetworkIdentifierDto(
                        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
                        ledgers.getGovShard().getChainId(),
                        new SubNetworkIdentifierDto(SUB_CHAIN)),
                    List.of(addOperation, deductOperation),
                    null)),
        ErrorDto.INVALID_AMOUNT);
  }

  private SignedTransaction createSignedTransaction(boolean useWrongNonce) {

    InteractWithContractTransaction interactWithContractTransaction =
        InteractWithContractTransaction.create(TestValues.CONTRACT_ONE, new byte[0]);
    long nonce = useWrongNonce ? 2L : 1L;
    CoreTransactionPart coreTransactionPart =
        CoreTransactionPart.create(nonce, System.currentTimeMillis() + 100_000, 100_000);

    return SignedTransaction.create(coreTransactionPart, interactWithContractTransaction)
        .sign(TestValues.KEY_PAIR_ONE, ledgers.getGovShard().getChainId());
  }

  private ConstructionPreprocessRequestDto createDummyPreprocessRequest(
      ConstructionPreprocessRequestDto.Metadata metadata) {
    AccountIdentifierDto recipient = new AccountIdentifierDto(TestValues.ACCOUNT_TWO);
    AccountIdentifierDto sender = new AccountIdentifierDto(TestValues.ACCOUNT_ONE);
    OperationDto addOperation =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipient,
            new AmountDto("11", new CurrencyDto("MPC", 4)),
            null,
            null);
    OperationDto deductOperation =
        new OperationDto(
            new OperationIdentifierDto(1),
            OperationDto.Type.DEDUCT,
            sender,
            new AmountDto("-11", new CurrencyDto("MPC", 4)),
            null,
            null);
    NetworkIdentifierDto networkIdentifier =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto(SUB_CHAIN));

    return new ConstructionPreprocessRequestDto(
        networkIdentifier, List.of(addOperation, deductOperation), metadata);
  }
}
