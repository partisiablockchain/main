package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.blockchain.VestingAccount;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class BalanceCalculatorTest {

  @Test
  void compute() {
    OldBalance oldBalance =
        new OldBalance(
            1,
            2,
            AvlTree.create(Map.of(0L, 3L)),
            FixedList.create(List.of(new VestingAccount(4 + 9, 9))),
            AvlTree.create(
                Map.of(
                    TestValues.HASH_ONE,
                    new TransferInformation(5, false),
                    TestValues.HASH_TWO,
                    new TransferInformation(6, true))));
    List<AmountDto> amounts = BalanceCalculator.compute(oldBalance, List.of(CurrencyDto.MPC));
    long result = 1 + 2 + 3 + 4 + 5;
    Assertions.assertThat(amounts.get(0).value()).isEqualTo(Long.toString(result));
  }

  /** Balance state from version 3.0.6 of account plugin. */
  @SuppressWarnings("unused")
  private record OldBalance(
      long mpcTokens,
      long stakedTokens,
      AvlTree<Long, Long> pendingUnstakes,
      FixedList<VestingAccount> vestingAccounts,
      AvlTree<Hash, TransferInformation> storedPendingTransactions)
      implements StateSerializable {}

  @Immutable
  @SuppressWarnings("unused")
  private record TransferInformation(long amount, boolean addTokensIfTransferSuccessful)
      implements StateSerializable {}
}
