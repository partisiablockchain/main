package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.rosetta.dto.ErrorDto;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.internal.MapPropertiesDelegate;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.internal.routing.UriRoutingContext;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ExceptionProviderRosettaTest {

  @Test
  void toResponseUnknownError() {
    UriRoutingContext uriInfo =
        new UriRoutingContext(
            new ContainerRequest(
                URI.create("http:localhost:8000/"),
                URI.create("rosetta"),
                "something",
                null,
                new MapPropertiesDelegate(),
                null));
    ExceptionProviderRosetta provider = new ExceptionProviderRosetta(uriInfo);
    Response response = provider.toResponse(new Throwable("Av"));
    Assertions.assertThat(response.getStatus())
        .isEqualTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    Assertions.assertThat(response.getEntity())
        .usingRecursiveComparison()
        .isEqualTo(ErrorDto.GENERIC_ERROR.describe("Av"));
  }

  @Test
  void toResponseCallDelegate() {
    UriRoutingContext uriInfo =
        new UriRoutingContext(
            new ContainerRequest(
                URI.create("http:localhost:8000/"),
                URI.create("blockchain"),
                "something",
                null,
                new MapPropertiesDelegate(),
                null));
    ExceptionProviderRosetta provider = new ExceptionProviderRosetta(uriInfo);
    Response response = provider.toResponse(new Throwable());
    Assertions.assertThat(response.getStatus())
        .isEqualTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
  }
}
