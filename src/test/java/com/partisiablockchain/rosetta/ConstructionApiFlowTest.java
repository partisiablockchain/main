package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionHashRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsRequest;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsResponse;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionSubmitRequest;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.OperationIdentifierDto;
import com.partisiablockchain.rosetta.dto.PublicKeyDto;
import com.partisiablockchain.rosetta.dto.SignatureDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierResponse;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * This class simply shows the full construction API flow to demonstrate how to construct an
 * MPC.TRANSFER. It also validates the endpoints throughout the calls.
 */
public final class ConstructionApiFlowTest {

  @Test
  public void constructionApiFlow() {
    Map<String, BlockchainLedger> ledgersMap = RosettaOfflineResourceTest.createLedgers();
    final ShardedBlockchain ledgers = new ShardedBlockchain(new HashMap<>(ledgersMap));
    final RosettaOfflineResource resource =
        new RosettaOfflineResource(
            new ArrayList<>(ledgers.getShards().keySet()), ledgers.getGovShard().getChainId());
    final String network = ledgers.getGovShard().getChainId();
    final NetworkIdentifierDto networkDto =
        new NetworkIdentifierDto(
            RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
            ledgers.getGovShard().getChainId(),
            new SubNetworkIdentifierDto("Shard1"));
    final AccountIdentifierDto recipientDto = new AccountIdentifierDto(TestValues.ACCOUNT_TWO);
    final RosettaOnlineResource onlineResource = new RosettaOnlineResource(ledgersMap);

    ConstructionDeriveResponseDto derive =
        resource.derive(
            new ConstructionDeriveRequestDto(
                networkDto,
                new PublicKeyDto(
                    TestValues.KEY_PAIR_ONE.getPublic().asBytes(), PublicKeyDto.CURVE_TYPE)));
    Assertions.assertThat(derive.accountIdentifier().address())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    // Preprocess
    final String amount = "50";
    OperationDto sender =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.DEDUCT,
            derive.accountIdentifier(),
            new AmountDto("-" + amount, CurrencyDto.MPC),
            null,
            null);
    OperationDto recipient =
        new OperationDto(
            new OperationIdentifierDto(0),
            OperationDto.Type.ADD,
            recipientDto,
            new AmountDto(amount, CurrencyDto.MPC),
            null,
            null);
    ConstructionPreprocessRequestDto preprocessDto =
        new ConstructionPreprocessRequestDto(networkDto, List.of(sender, recipient), null);
    ConstructionPreprocessResponseDto preprocessResponse = resource.preprocess(preprocessDto);
    Assertions.assertThat(preprocessResponse.options().senderAddress())
        .isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    // Metadata (online)
    ConstructionMetadataResponseDto metadata =
        onlineResource.metadata(
            new ConstructionMetadataRequestDto(networkDto, preprocessResponse.options()));
    Assertions.assertThat(metadata.metadata().nonce()).isEqualTo(1);

    // Payloads
    ConstructionPayloadsResponse payloadsResponse =
        resource.constructionPayloads(
            new ConstructionPayloadsRequest(
                networkDto, List.of(sender, recipient), metadata.metadata()));
    Assertions.assertThat(payloadsResponse.payloads().size()).isEqualTo(1);

    // Parse
    ConstructionParseResponseDto parseResponse =
        resource.constructionParse(
            new ConstructionParseRequestDto(
                networkDto, false, payloadsResponse.unsignedTransaction()));
    Assertions.assertThat(parseResponse.operations().size()).isEqualTo(2);

    // Combine - helper functions signs with KEY_PAIR_ONE as we need.
    Hash messageHash =
        Hash.read(
            SafeDataInputStream.createFromBytes(payloadsResponse.payloads().get(0).hexBytes()));
    SignatureDto signature = RosettaTestHelper.createSignatureDtoFromMessageHash(messageHash);
    ConstructionCombineResponseDto combineResponse =
        resource.combine(
            new ConstructionCombineRequestDto(
                networkDto, payloadsResponse.unsignedTransaction(), List.of(signature)));
    SignedTransaction signedTransaction =
        SignedTransaction.read(
            network, SafeDataInputStream.createFromBytes(combineResponse.signedTransaction()));
    Assertions.assertThat(signedTransaction.getSender()).isEqualTo(TestValues.KEY_PAIR_ONE_ADDRESS);

    // Throw it through the parse endpoint to validate again
    ConstructionParseResponseDto signedParseResponse =
        resource.constructionParse(
            new ConstructionParseRequestDto(networkDto, true, combineResponse.signedTransaction()));
    Assertions.assertThat(signedParseResponse.accountIdentifierSigners().size()).isEqualTo(1);
    Assertions.assertThat(signedParseResponse.accountIdentifierSigners().get(0))
        .isEqualTo(derive.accountIdentifier());

    // Submit
    TransactionIdentifierResponse hashResponse =
        resource.hash(
            new ConstructionHashRequestDto(networkDto, combineResponse.signedTransaction()));
    TransactionIdentifierResponse submit =
        onlineResource.submit(
            new ConstructionSubmitRequest(networkDto, combineResponse.signedTransaction()));
    Assertions.assertThat(hashResponse.transactionIdentifierDto().hash())
        .isEqualTo(submit.transactionIdentifierDto().hash());

    BlockchainLedger ledger = ledgersMap.get("Shard1");
    Assertions.assertThat(ledger.getPendingTransactions().size()).isEqualTo(1);
    Assertions.assertThat(ledger.getPendingTransactions().get(0)).isEqualTo(signedTransaction);
  }
}
