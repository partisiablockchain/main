package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.function.Supplier;

/** Package helper class. */
public final class NetworkConfigHelper {

  /**
   * Returns the node address for the given network config.
   *
   * @param config config to get address on.
   * @return node address.
   */
  public static Address getNodeAddress(NetworkConfig config) {
    return config.getNodeAddress();
  }

  /**
   * Returns the known nodes for this network config.
   *
   * @param config config to get known nodes on.
   * @return known nodes.
   */
  public static Supplier<Address> getKnownNodes(NetworkConfig config) {
    return config.getKnownNodes();
  }
}
