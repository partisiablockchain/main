package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.monitor.DefaultEthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.WithdrawIdWrapper;
import com.partisiablockchain.math.Unsigned256;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PersistentByocEventCacheTest {

  private final ByocEventStorage storage = new MockStorage();
  private final PersistentByocEventCache persistentCache = new PersistentByocEventCache(storage);

  @Test
  void emptyCache() {
    Assertions.assertThat(persistentCache.loadLatestCheckedBlock()).isNull();
    Assertions.assertThat(persistentCache.loadAllWithdrawals()).isEmpty();
    Assertions.assertThat(persistentCache.loadDeposit(0)).isNull();
    Assertions.assertThat(persistentCache.loadDeposit(1)).isNull();
  }

  @Test
  void saveLatestCheckedBlock() {
    Unsigned256 block = Unsigned256.create(156);
    persistentCache.saveLatestCheckedBlock(block);
    EthereumEventsCache storedView = storage.readEvents();
    Assertions.assertThat(persistentCache.loadLatestCheckedBlock()).isEqualTo(block);
    Assertions.assertThat(persistentCache.loadLatestCheckedBlock())
        .isEqualTo(storedView.loadLatestCheckedBlock());
  }

  @Test
  void saveDeposits() {
    List<DepositTransaction> deposits =
        List.of(
            new DepositTransaction(
                0, new KeyPair(BigInteger.ZERO).getPublic().createAddress(), Unsigned256.ONE),
            new DepositTransaction(
                1,
                new KeyPair(BigInteger.TEN).getPublic().createAddress(),
                Unsigned256.create(1561)));
    persistentCache.saveDeposits(deposits);

    // Inner storage is not saved until last check block is updated
    EthereumEventsCache storedView = storage.readEvents();
    Assertions.assertThat(storedView.loadAllDeposits()).isEmpty();
    persistentCache.saveLatestCheckedBlock(Unsigned256.TEN);
    storedView = storage.readEvents();
    Assertions.assertThat(storedView.loadAllDeposits()).containsAll(deposits);

    Assertions.assertThat(persistentCache.loadAllDeposits()).containsAll(deposits);
    Assertions.assertThat(persistentCache.loadDeposit(0)).isEqualTo(deposits.get(0));
    Assertions.assertThat(persistentCache.loadDeposit(0)).isEqualTo(storedView.loadDeposit(0));
    Assertions.assertThat(persistentCache.loadDeposit(1)).isEqualTo(deposits.get(1));
    Assertions.assertThat(persistentCache.loadDeposit(1)).isEqualTo(storedView.loadDeposit(1));
    Assertions.assertThat(persistentCache.loadDeposit(2)).isNull();
    Assertions.assertThat(persistentCache.loadDeposit(2)).isEqualTo(storedView.loadDeposit(2));
  }

  @Test
  void saveWithdrawals() {
    WithdrawalTransaction withdraw1 =
        new WithdrawalTransaction(1, "SomeEth", Unsigned256.ONE, 1, 3);
    WithdrawalTransaction withdraw2 =
        new WithdrawalTransaction(2, "SomeEth", Unsigned256.ONE, 1, 5);
    WithdrawalTransaction withdraw3 =
        new WithdrawalTransaction(1, "SomeOtherEth", Unsigned256.ZERO, 2, 7);
    WithdrawalTransaction withdraw4 =
        new WithdrawalTransaction(2, "SomeEth", Unsigned256.TEN, 3, 5);

    persistentCache.saveWithdrawals(List.of(withdraw1, withdraw2, withdraw3, withdraw4));
    Assertions.assertThat(persistentCache.loadAllWithdrawals())
        .contains(withdraw1, withdraw2, withdraw3, withdraw4);

    // Inner storage is not saved until last check block is updated
    EthereumEventsCache storedView = storage.readEvents();
    Assertions.assertThat(storedView.loadAllWithdrawals()).isEmpty();
    persistentCache.saveLatestCheckedBlock(Unsigned256.ONE);
    storedView = storage.readEvents();

    Assertions.assertThat(storedView.loadAllWithdrawals())
        .contains(withdraw1, withdraw2, withdraw3, withdraw4);
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(1, 1)))
        .isEqualTo(storedView.loadWithdrawal(WithdrawIdWrapper.createId(1, 1)));
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(1, 2)))
        .isEqualTo(storedView.loadWithdrawal(WithdrawIdWrapper.createId(1, 2)));
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(2, 1)))
        .isEqualTo(storedView.loadWithdrawal(WithdrawIdWrapper.createId(2, 1)));
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(3, 2)))
        .isEqualTo(storedView.loadWithdrawal(WithdrawIdWrapper.createId(3, 2)));
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(1, 3)))
        .isNull();
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(2, 2)))
        .isNull();
    Assertions.assertThat(persistentCache.loadWithdrawal(WithdrawIdWrapper.createId(3, 1)))
        .isNull();
  }

  private static final class MockStorage implements ByocEventStorage {

    EthereumEventsCache view;

    @Override
    public EthereumEventsCache readEvents() {
      if (view == null) {
        view = new DefaultEthereumEventsCache();
      }
      DefaultEthereumEventsCache cacheCopy = new DefaultEthereumEventsCache();
      cacheCopy.saveLatestCheckedBlock(view.loadLatestCheckedBlock());
      cacheCopy.saveDeposits(view.loadAllDeposits());
      cacheCopy.saveWithdrawals(view.loadAllWithdrawals());
      return cacheCopy;
    }

    @Override
    public void writeEvents(EthereumEventsCache writeEvents) {
      view = writeEvents;
    }
  }
}
