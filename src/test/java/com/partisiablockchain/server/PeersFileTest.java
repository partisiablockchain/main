package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.serialization.StateObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PeersFileTest {

  @Test
  public void readPeers() throws IOException {
    File file = new File("");
    PeersFile peersFile = new PeersFile(file);
    Map<Hash, Address> read = peersFile.readPeers();
    assertThat(read).isEqualTo(new HashMap<Hash, Address>());
    assertThat(read.keySet().getClass().getSimpleName()).isEqualTo("");
    ObjectMapper mapper = StateObjectMapper.createObjectMapper();

    Map<Hash, Address> writtenMap =
        Map.of(Hash.create(s -> s.writeString("123")), new Address("some", 2000));
    File tempFile = Files.createTempFile("tempFile", "").toFile();

    mapper.writeValue(tempFile, writtenMap);
    assertThat(new PeersFile(tempFile).readPeers()).isEqualTo(writtenMap);
  }

  @Test
  public void exists() throws IOException {
    File file = new File("");
    PeersFile peersFile = new PeersFile(file);
    assertThat(peersFile.exists()).isFalse();

    File tempFile = Files.createTempFile("tempFile", "").toFile();

    peersFile = new PeersFile(tempFile);
    assertThat(peersFile.exists()).isTrue();
  }
}
