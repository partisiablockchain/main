package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.mockito.ArgumentMatchers.any;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.CloseableTest;
import com.secata.tools.thread.ExecutorFactory;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

/** Test. */
public final class ThreadDumpModuleTest extends CloseableTest {

  private File dataDir;

  @BeforeEach
  void setUp() throws IOException {
    dataDir = Files.createTempDirectory("threadmodule").toFile();
  }

  @Test
  public void couldNotCreateDumpThread() {
    ThreadDumpModule.Config threadDumpModuleConfig = new ThreadDumpModule.Config();
    threadDumpModuleConfig.enabled = true;
    File file = Mockito.mock(File.class);
    Path path = Mockito.mock(Path.class);
    Mockito.when(file.toPath()).thenReturn(path);
    Mockito.when(path.resolve("threaddumps")).thenReturn(path);
    var serverConfig = new Main.CollectingServerConfig();

    Assertions.assertThatThrownBy(
            () -> new ThreadDumpModule(file, threadDumpModuleConfig).register(serverConfig))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not create thread dump output folder");
  }

  @Test
  public void couldNotDump() throws Exception {
    ThreadDumpModule.Config threadDumpModuleConfig = new ThreadDumpModule.Config();
    threadDumpModuleConfig.enabled = true;
    threadDumpModuleConfig.initialDelay = 1L;
    threadDumpModuleConfig.period = 9L;
    File file = Mockito.mock(File.class);
    Path path = Mockito.spy(Path.class);

    final var serverConfig = new Main.CollectingServerConfig();

    Mockito.when(file.toPath()).thenReturn(path);
    Mockito.doAnswer(answer -> path).when(path).resolve("threaddumps");
    final ThreadDumpModule threadDumpModule = new ThreadDumpModule(file, threadDumpModuleConfig);
    MockedStatic<Files> mockedStatic = Mockito.mockStatic(Files.class);
    register(mockedStatic);
    AtomicBoolean flag = new AtomicBoolean(false);
    mockedStatic.when(() -> Files.createDirectories(path)).thenAnswer(answer -> path);
    Mockito.doAnswer(
            answer -> {
              flag.set(true);
              throw new ThrowableTest();
            })
        .when(path)
        .resolve(any(String.class));
    threadDumpModule.register(serverConfig);
    Thread.sleep(100);
    Assertions.assertThat(flag).isTrue();
    for (AutoCloseable closeable : serverConfig.closeable) {
      closeable.close();
    }
  }

  /** Needed because simply throwing an exception would cause an error. */
  @SuppressWarnings("serial")
  static final class ThrowableTest extends Throwable {}

  @Test
  public void setSchedulerToDumpThreads() throws InterruptedException {
    ThreadDumpModule.Config threadDumpModuleConfig = new ThreadDumpModule.Config();
    threadDumpModuleConfig.enabled = true;
    threadDumpModuleConfig.initialDelay = 1L;
    threadDumpModuleConfig.period = 1000L;
    ThreadDumpModule dumpModule = new ThreadDumpModule(dataDir, threadDumpModuleConfig);
    Path path = Mockito.spy(Path.class);
    AtomicInteger counter = new AtomicInteger(0);
    Mockito.doAnswer(
            answer -> {
              counter.incrementAndGet();
              return path;
            })
        .when(path)
        .resolve(any(String.class));

    ScheduledExecutorService threadDump = ExecutorFactory.newScheduledSingleThread("ThreadDump");
    dumpModule.setSchedulerToDumpThreads(threadDump, path);
    Thread.sleep(200);
    Assertions.assertThat(counter.get()).isEqualTo(1);

    int lengthBeforeRun = dataDir.list().length;
    dumpModule.setSchedulerToDumpThreads(threadDump, dataDir.toPath());
    Thread.sleep(200L);
    Assertions.assertThat(lengthBeforeRun).isEqualTo(dataDir.list().length - 1);
    threadDump.shutdown();
  }

  @Test
  public void toFile() {
    ThreadDumpModule.toFile(dataDir.toPath());

    Assertions.assertThatThrownBy(() -> ThreadDumpModule.toFile(Paths.get("something")))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("IO error");
  }

  @Test
  void dump() {
    var rawDump = ThreadDumpModule.threadDump();
    var asString = new String(rawDump, StandardCharsets.UTF_8);
    Assertions.assertThat(asString).contains(ThreadDumpModuleTest.class.getName());
  }

  @Test
  void nullConfig() {
    var config = new Main.CollectingServerConfig();
    new ThreadDumpModule(dataDir, null).register(config);
    Assertions.assertThat(config.closeable).isEmpty();
  }

  @Test
  void notEnabled() {
    var config = new Main.CollectingServerConfig();
    new ThreadDumpModule(dataDir, new ThreadDumpModule.Config()).register(config);
    Assertions.assertThat(config.closeable).isEmpty();
  }

  @Test
  void enabled() throws Exception {
    var threadDumpModuleConfig = new ThreadDumpModule.Config();
    threadDumpModuleConfig.enabled = true;

    var serverConfig = new Main.CollectingServerConfig();
    new ThreadDumpModule(dataDir, threadDumpModuleConfig).register(serverConfig);
    Assertions.assertThat(serverConfig.closeable).hasSize(1);

    serverConfig.closeable.get(0).close();
  }

  @Test
  void initialDelayDefault() {
    var config = new ThreadDumpModule.Config();
    Assertions.assertThat(config.initialDelay).isNull();
    Assertions.assertThat(config.determineInitialDelay()).isEqualTo(30000L);
    config.initialDelay = 100L;
    Assertions.assertThat(config.determineInitialDelay()).isEqualTo(100L);
  }

  @Test
  void periodDefault() {
    var config = new ThreadDumpModule.Config();
    Assertions.assertThat(config.period).isNull();
    Assertions.assertThat(config.determinePeriod()).isEqualTo(300000L);
    config.period = 100L;
    Assertions.assertThat(config.determinePeriod()).isEqualTo(100L);
  }

  @Test
  void jsonSerialization() throws JsonProcessingException {
    var expected = new ThreadDumpModule.Config();
    expected.enabled = true;
    expected.initialDelay = 100L;
    expected.period = 200L;

    ObjectMapper mapper = BetanetConfigFiles.mapper;
    String string = mapper.writeValueAsString(expected);

    ThreadDumpModule.Config actual = mapper.readValue(string, ThreadDumpModule.Config.class);
    Assertions.assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
  }
}
