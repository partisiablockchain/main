package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.rosetta.MapBackedStorage;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import com.partisiablockchain.storage.RootDirectory;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardResourceTest extends CloseableTest {
  @Test
  public void shardResourceTest() throws IOException {
    Map<String, BlockchainResource> blockchainResourceMap = new HashMap<>();
    RootDirectory rootDirectory =
        new RootDirectory(
            Files.createTempDirectory("myDirectory").toFile(),
            (ignored, immutable) -> new MapBackedStorage(immutable));
    BlockchainResource resource =
        new BlockchainResource(
            register(
                BlockchainLedgerHelper.createLedger(
                    rootDirectory,
                    BlockchainLedgerHelper.createNetworkConfig(),
                    BlockchainLedgerHelper.createChainState(),
                    "Shard0")));
    blockchainResourceMap.put("shard1", null);
    blockchainResourceMap.put("shard0", resource);
    ShardResource shardResource = new ShardResource(blockchainResourceMap, new HashMap<>());
    Assertions.assertThat(shardResource.shardResource("shard1")).isEqualTo(null);
    Assertions.assertThat(shardResource.shardResource("shard0")).isEqualTo(resource);
  }

  @Test
  public void metricsResourceTest() throws IOException {
    Map<String, MetricsResource> blockchainMetricsMap = new HashMap<>();
    RootDirectory rootDirectory =
        new RootDirectory(
            Files.createTempDirectory("myDirectory").toFile(),
            (ignored, immutable) -> new MapBackedStorage(immutable));
    MetricsResource metrics =
        new MetricsResource(
            register(
                BlockchainLedgerHelper.createLedger(
                    rootDirectory,
                    BlockchainLedgerHelper.createNetworkConfig(),
                    BlockchainLedgerHelper.createChainState(),
                    "Shard0")));
    blockchainMetricsMap.put("shard1", null);
    blockchainMetricsMap.put("shard0", metrics);
    ShardResource resource = new ShardResource(new HashMap<>(), blockchainMetricsMap);
    Assertions.assertThat(resource.metricsResource("shard1")).isEqualTo(null);
    Assertions.assertThat(resource.metricsResource("shard0")).isEqualTo(metrics);
  }
}
