package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Test. */
public final class CompositeNodeConfigDtoTest {

  private static final String stringA = "1.1.1.1:1111";
  private static final String stringB = "2.2.2.2:2222";
  private static final String stringC = "3.3.3.3:3333";

  private static final Address addressFloodA = Address.parseAddress("1.1.1.1:1111");
  private static final Address addressFloodB = Address.parseAddress("2.2.2.2:2222");
  private static final Address addressFloodC = Address.parseAddress("3.3.3.3:3333");
  private static final KeyPair keyA = new KeyPair(BigInteger.ONE);
  private static final KeyPair keyB = new KeyPair(BigInteger.TWO);
  private static final KeyPair keyC = new KeyPair(BigInteger.TEN);

  private static final CompositeNodeConfigDto.ProducerPeer peerA =
      new CompositeNodeConfigDto.ProducerPeer(
          BlockchainPublicKey.fromEncodedEcPoint(keyA.getPublic().asBytes()), addressFloodA);
  private static final CompositeNodeConfigDto.ProducerPeer peerB =
      new CompositeNodeConfigDto.ProducerPeer(
          BlockchainPublicKey.fromEncodedEcPoint(keyB.getPublic().asBytes()), addressFloodB);
  private static final CompositeNodeConfigDto.ProducerPeer peerC =
      new CompositeNodeConfigDto.ProducerPeer(
          BlockchainPublicKey.fromEncodedEcPoint(keyC.getPublic().asBytes()), addressFloodC);

  @Test
  public void getKnownPeers() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();

    dto.knownPeers = List.of(stringA, stringB, stringC);
    assertThat(dto.getKnownPeers())
        .containsExactlyInAnyOrder(addressFloodA, addressFloodB, addressFloodC);

    dto.knownPeers = null;
    assertThat(dto.getKnownPeers().size()).isEqualTo(0);
  }

  @Test
  public void getProducerPeers() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();
    dto.knownPeers =
        List.of(
            Hex.toHexString(keyA.getPublic().asBytes()) + ":" + stringA,
            Hex.toHexString(keyB.getPublic().asBytes()) + ":" + stringB,
            Hex.toHexString(keyC.getPublic().asBytes()) + ":" + stringC);
    assertThat(dto.getProducerPeers()).containsExactlyInAnyOrder(peerA, peerB, peerC);

    dto.knownPeers = null;
    assertThat(dto.getProducerPeers().size()).isEqualTo(0);
    dto.knownPeers = List.of("hej:med");
    assertThat(dto.getProducerPeers().size()).isEqualTo(0);
  }

  @Test
  public void producerPeer() {
    CompositeNodeConfigDto.ProducerPeer peer =
        new CompositeNodeConfigDto.ProducerPeer(keyA.getPublic(), addressFloodA);
    assertThat(peer.address()).isEqualTo(addressFloodA);
    assertThat(peer.key()).isEqualTo(keyA.getPublic());
  }

  @Test
  void getOverrideOrDefault() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();
    dto.overrides = Map.of("override", "10");
    assertThat(dto.getOverrideOrDefault("override", 9)).isEqualTo(10);
    assertThat(dto.getOverrideOrDefault("override unknown", 9)).isEqualTo(9);
  }

  @Test
  void getGenesisFilePath() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();
    assertThat(dto.getGenesisFilePath("/file")).isEqualTo("/file");
    dto.overrides = Map.of("genesisPath", "path");
    assertThat(dto.getGenesisFilePath("/file")).isEqualTo("path/file");
  }

  @Test
  public void blockProducerConfigDto() {
    BlockProducerConfigDto dto = new BlockProducerConfigDto();
    dto.accountKey = "accountKey";
    dto.ethereumUrl = "ethereumUrl";
    dto.finalizationKey = "finalizationKey";
    dto.host = "host";
    assertThat(dto.accountKey).isEqualTo("accountKey");
    assertThat(dto.ethereumUrl).isEqualTo("ethereumUrl");
    assertThat(dto.finalizationKey).isEqualTo("finalizationKey");
    assertThat(dto.host).isEqualTo("host");
  }

  @Test
  void restAddress() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.restPort = 1234;
    String address = config.restAddress();
    Assertions.assertThat(address).isEqualTo("http://localhost:1234");
  }

  @Test
  void persistentPeer() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    Assertions.assertThat(config.getPersistentPeer(0)).isNull();

    config.overrides = Map.of("flooding.persistent.peer", "invalid address");
    Assertions.assertThat(config.getPersistentPeer(0)).isNull();

    config.overrides = Map.of("flooding.persistent.peer", "host:990");
    Assertions.assertThat(config.getPersistentPeer(10)).isEqualTo(new Address("host", 1000));
    Assertions.assertThat(config.getPersistentPeer(100)).isEqualTo(new Address("host", 1090));
  }

  /**
   * Retrieving an endpoint for a config that does not exist returns null. Adding an endpoint to a
   * config that does not exist, creates a new config with the endpoint added to it.
   */
  @Test
  void nullConfigMapIsHandled() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.producerConfig = new BlockProducerConfigDto();
    Assertions.assertThat(config.producerConfig.getChainEndpoint("Chain")).isNull();
    config.producerConfig.addChainEndpoint("Chain", "Endpoint");
    Assertions.assertThat(config.producerConfig.getChainEndpoint("Chain")).isEqualTo("Endpoint");
  }

  /** Ensure that we retrieve the old config field names correctly. */
  @Test
  void fieldNameOfOldConfigs() {
    Assertions.assertThat(
            BlockProducerConfigDto.getOldConfigFieldFromChain(
                BlockProducerConfigDto.Chains.ETHEREUM))
        .isEqualTo("ethereumUrl");
    Assertions.assertThat(
            BlockProducerConfigDto.getOldConfigFieldFromChain(
                BlockProducerConfigDto.Chains.POLYGON))
        .isEqualTo("polygonUrl");
    Assertions.assertThat(
            BlockProducerConfigDto.getOldConfigFieldFromChain(
                BlockProducerConfigDto.Chains.BNB_SMART_CHAIN))
        .isEqualTo("bnbSmartChainUrl");
  }

  /** Validate that the logger logs a warning if and only if both endpoints exist. */
  @Test
  void validateEndpoints() {
    final String chain = "Ethereum";
    final String endpoint = "EthEndpoint";
    final Logger loggerSpy = Mockito.spy(LoggerFactory.getLogger(ShardedMain.class));
    final String[] logMessage =
        new String[] {
          "Warning: Multiple endpoints was found for the chain '{}'. The configuration field '{}'"
              + " should no longer be used. Please update your block producer configuration to only"
              + " use the 'chainConfigs' map with an entry for '{}'.",
          "Ethereum",
          "ethereumUrl",
          "Ethereum"
        };

    BlockProducerConfigDto config = new BlockProducerConfigDto();

    // Endpoint does not exist in either config
    config.ethereumUrl = null;
    config.addChainEndpoint(chain, null);
    Assertions.assertThat(config.getChainEndpoint(chain, loggerSpy)).isNull();
    // No warning is logged
    Mockito.verify(loggerSpy, Mockito.times(0))
        .warn(logMessage[0], logMessage[1], logMessage[2], logMessage[3]);

    // Endpoint exists in new config
    config.addChainEndpoint(chain, endpoint);
    Assertions.assertThat(config.getChainEndpoint(chain, loggerSpy)).isEqualTo(endpoint);
    // No warning is logged
    Mockito.verify(loggerSpy, Mockito.times(0))
        .warn(logMessage[0], logMessage[1], logMessage[2], logMessage[3]);

    // Endpoint exists in old config
    config.addChainEndpoint(chain, null);
    config.ethereumUrl = endpoint;
    Assertions.assertThat(config.getChainEndpoint(chain, loggerSpy)).isEqualTo(endpoint);
    // No warning is logged
    Mockito.verify(loggerSpy, Mockito.times(0))
        .warn(logMessage[0], logMessage[1], logMessage[2], logMessage[3]);

    // Endpoint exists in both configs
    config.addChainEndpoint(chain, endpoint);
    Assertions.assertThat(config.getChainEndpoint(chain, loggerSpy)).isEqualTo(endpoint);
    // Warning *is* logged
    Mockito.verify(loggerSpy, Mockito.times(1))
        .warn(logMessage[0], logMessage[1], logMessage[2], logMessage[3]);
  }
}
