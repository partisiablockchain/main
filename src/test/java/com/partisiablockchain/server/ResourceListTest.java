package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ResourceListTest {

  @Test
  void getGitProperties() throws IOException {
    String propertiesName = "test-git.properties";
    String directoryName = "target/classes/test/";

    File directory = new File(directoryName);
    File propertiesFile = new File(directoryName + propertiesName);
    directory.deleteOnExit();
    propertiesFile.deleteOnExit();

    Path of = Path.of(directoryName);
    if (!Files.exists(of)) {
      Files.createDirectory(of);
    }

    WithResource.accept(
        () -> new FileOutputStream(propertiesFile),
        fileOutputStream -> new Properties().store(fileOutputStream, null),
        "Could not store properties in file " + propertiesFile);

    List<String> propertiesNames = new ArrayList<>();

    ResourceList.consumeGitProperties((name, properties) -> propertiesNames.add(name));

    assertThat(propertiesNames)
        .contains(propertiesName, "com.secata.tools.coverage-git.properties");
  }

  @Test
  void propertiesContainGitInfo() {
    List<Properties> gitProperties = new ArrayList<>();
    ResourceList.consumeGitProperties((name, properties) -> gitProperties.add(properties));

    Properties properties = gitProperties.get(2);

    assertThat(properties).hasSize(3);
    assertThat(properties.getProperty("git.build.version")).isNotNull();
    assertThat(properties.getProperty("git.commit.id.abbrev")).isNotNull();
    assertThat(properties.getProperty("git.build.time")).isNotNull();
  }
}
