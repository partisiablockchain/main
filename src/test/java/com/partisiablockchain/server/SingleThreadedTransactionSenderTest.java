package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SenderAuthenticationKeyPair;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test of {@link SingleThreadedTransactionSende}. */
public final class SingleThreadedTransactionSenderTest {

  private static final KeyPair keyPair = new KeyPair(BigInteger.ONE);
  private static final ServerModule.ServerConfig serverConfig = new Main.CollectingServerConfig();

  @Test
  public void registerCloseable() {
    ServerModule.ServerConfig config = Mockito.mock(ServerModule.ServerConfig.class);
    AtomicInteger counter = new AtomicInteger(0);
    Mockito.doAnswer(answer -> counter.getAndIncrement())
        .when(config)
        .registerCloseable(Mockito.any(AutoCloseable.class));
    new SingleThreadedTransactionSender(config, null);
    Assertions.assertThat(counter.get()).isEqualTo(1);
  }

  @Test
  public void sendTransaction() throws InterruptedException {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(keyPair),
            100,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(serverConfig, client);
    dummyClient.addDefaultExecutedTransactions();

    sender.sendTransaction(
        keyPair.getPublic().createAddress(),
        (serialize) -> SafeDataOutputStream.serialize(s -> s.writeString("hej")));
    Thread.sleep(100);
    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
  }

  @Test
  public void sendFeeDist() throws InterruptedException {
    DummyPbcClient dummyClient = new DummyPbcClient();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(keyPair),
            100,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    Duration transactionRetryDuration = Duration.ofMillis(0);
    Instant instant = Instant.ofEpochMilli(0);
    SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(
            serverConfig, client, 0L, transactionRetryDuration, () -> instant);
    sender.sendFeeDistributionTransaction(
        (s) -> SafeDataOutputStream.serialize(stream -> stream.writeString("fee")));
    Thread.sleep(100);
    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
    Assertions.assertThat(dummyClient.getAllSentTransactions().get(0).getInner().getAddress())
        .isEqualTo(BetanetAddresses.FEE_DISTRIBUTION);
  }

  @Test
  public void sendAndWaitEvents() {
    DummyPbcClient dummyClient = new DummyPbcClient();
    dummyClient.addDefaultExecutedTransactions();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient, new SenderAuthenticationKeyPair(keyPair), 10000, 1);
    SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(serverConfig, client);
    Transaction interact = Transaction.create(keyPair.getPublic().createAddress(), new byte[10]);

    Assertions.assertThat(dummyClient.getAllSentTransactions()).isEmpty();
    Assertions.assertThat(dummyClient.numTransactionsRequested())
        .as("numTransactionsRequested")
        .isEqualTo(0);

    sender.sendAndWaitForEvents(interact, 100);

    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
    Assertions.assertThat(dummyClient.numTransactionsRequested())
        .as("numTransactionsRequested")
        .isEqualTo(1);
  }

  @Test
  public void sendAndWaitForEventsFails() {
    DummyPbcClient dummyClient = new DummyPbcClient();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient, new SenderAuthenticationKeyPair(keyPair), 100, 100);
    Duration transactionRetryDuration = Duration.ofMillis(0);
    Instant instant = Instant.ofEpochMilli(0);
    SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(
            serverConfig, client, 0L, transactionRetryDuration, () -> instant);
    Transaction interact = Transaction.create(keyPair.getPublic().createAddress(), new byte[10]);
    Assertions.assertThatThrownBy(() -> sender.sendAndWaitForEvents(interact, 100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error sending transaction");
  }

  @Test
  void sampleFeeDistributionDelay() {
    long maximumWait = Duration.ofMinutes(5).toMillis();
    Set<Long> samples = new HashSet<>();
    for (int i = 0; i < 10; i++) {
      long l = SingleThreadedTransactionSender.sampleFeeDistributionDelay(maximumWait);
      Assertions.assertThat(samples.add(l)).isTrue();
    }
    Assertions.assertThat(samples).allMatch(l -> l >= 0 && l <= maximumWait);

    Assertions.assertThat(SingleThreadedTransactionSender.sampleFeeDistributionDelay(0L))
        .isEqualTo(0L);
  }
}
