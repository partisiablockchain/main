package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.zknetwork.ConnectionAddress;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class PersistentPeerDatabaseTest {

  @TempDir File tempStorage;

  private static final BlockchainPublicKey publicKeyA = new KeyPair(BigInteger.ONE).getPublic();
  private static final BlockchainPublicKey publicKeyB = new KeyPair(BigInteger.TWO).getPublic();
  private static final BlockchainPublicKey publicKeyC =
      new KeyPair(BigInteger.valueOf(3)).getPublic();

  private static final Map<BlockchainPublicKey, Address> initialPeers =
      Map.of(
          publicKeyA, Address.parseAddress("1.1.1.1:1111"),
          publicKeyB, Address.parseAddress("2.2.2.2:2222"),
          publicKeyC, Address.parseAddress("3.3.3.3:3333"));

  private static final BlockchainPublicKey otherPublicKey =
      new KeyPair(BigInteger.valueOf(99)).getPublic();

  private PeersFile peersFile;
  private PersistentPeerDatabase peerDatabase;

  @BeforeEach
  void setup() {
    File peersDatabaseFile = new File(tempStorage, "tempPeers");
    peersFile = new PeersFile(peersDatabaseFile);
    peerDatabase = new PersistentPeerDatabase(initialPeers, peersDatabaseFile);
  }

  @Test
  void createPeerDatabaseWithExistingPeersFile() {
    com.partisiablockchain.demo.byoc.Address updatedEndpoint =
        com.partisiablockchain.demo.byoc.Address.parseAddress("4.4.4.4:4444");
    Map<Hash, com.partisiablockchain.demo.byoc.Address> updatedPeers =
        Map.of(
            Hash.create(publicKeyA::write),
            updatedEndpoint,
            Hash.create(otherPublicKey::write),
            com.partisiablockchain.demo.byoc.Address.parseAddress("5.5.5.5:5555"));
    File updatedPeersDatabaseFile = new File(tempStorage, "updatedPeers");
    PeersFile updatedPeersFile = new PeersFile(updatedPeersDatabaseFile);
    updatedPeersFile.writePeers(updatedPeers);

    PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(initialPeers, updatedPeersDatabaseFile);
    peerDatabase.setRegisteredBlockProducers(
        List.of(publicKeyA, publicKeyB, publicKeyC, otherPublicKey));

    Assertions.assertThat(peerDatabase.getBlockProducersWithEndpoint())
        .hasSize(initialPeers.size() + 1);
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(publicKeyA))
        .isEqualTo(updatedEndpoint);
  }

  @Test
  void emptyFilesExists() {
    File updatedPeersDatabaseFile = new File(tempStorage, "updatedPeers");
    ExceptionConverter.run(
        updatedPeersDatabaseFile::createNewFile, "Could not create empty peers file");

    PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(initialPeers, updatedPeersDatabaseFile);

    Assertions.assertThat(peerDatabase.getBlockProducersWithEndpoint()).hasSize(0);
  }

  @Test
  void setRegisteredBlockProducers() {
    Supplier<Boolean> isRegistered = () -> peerDatabase.isRegisteredBlockProducer(publicKeyA);
    Assertions.assertThat(isRegistered.get()).isFalse();
    peerDatabase.setRegisteredBlockProducers(List.of(publicKeyA));
    Assertions.assertThat(isRegistered.get()).isTrue();
  }

  @Test
  void isRegisteredBlockProducer() {
    BlockchainPublicKey registered = publicKeyA;
    peerDatabase.setRegisteredBlockProducers(List.of(registered));
    Assertions.assertThat(peerDatabase.isRegisteredBlockProducer(registered)).isTrue();
    Assertions.assertThat(peerDatabase.isRegisteredBlockProducer(publicKeyB)).isFalse();
  }

  @Test
  void getBlockProducerEndpoint() {
    BlockchainPublicKey registeredWithEndpoint = publicKeyA;
    BlockchainPublicKey registeredWithoutEndpoint = otherPublicKey;

    peerDatabase.setRegisteredBlockProducers(
        List.of(registeredWithEndpoint, registeredWithoutEndpoint));
    com.partisiablockchain.demo.byoc.Address expected =
        peerDatabase.convertAddress(initialPeers.get(publicKeyA));
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(registeredWithEndpoint))
        .isEqualTo(expected);
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(registeredWithoutEndpoint))
        .isNull();
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(otherPublicKey)).isNull();
  }

  @Test
  void getBlockProducersWithEndpoint() {
    peerDatabase.setRegisteredBlockProducers(List.of(publicKeyA, otherPublicKey));
    List<BlockchainPublicKey> blockProducersWithEndpoint =
        peerDatabase.getBlockProducersWithEndpoint();
    Assertions.assertThat(blockProducersWithEndpoint).hasSize(1);
    Assertions.assertThat(blockProducersWithEndpoint.get(0)).isEqualTo(publicKeyA);
  }

  @Test
  void containsBlockProducer() {
    Assertions.assertThat(peerDatabase.containsBlockProducer(publicKeyA)).isTrue();
    Assertions.assertThat(peerDatabase.containsBlockProducer(otherPublicKey)).isFalse();
  }

  @Test
  void updateBlockProducerEndpoint() {
    com.partisiablockchain.demo.byoc.Address addressBefore =
        peersFile.readPeers().get(Hash.create(publicKeyA::write));

    com.partisiablockchain.demo.byoc.Address updatedAddress =
        com.partisiablockchain.demo.byoc.Address.parseAddress("99.99.99.99:9999");
    peerDatabase.updateBlockProducerEndpoint(publicKeyA, updatedAddress);

    com.partisiablockchain.demo.byoc.Address addressAfter =
        peersFile.readPeers().get(Hash.create(publicKeyA::write));
    Assertions.assertThat(addressBefore)
        .isEqualTo(peerDatabase.convertAddress(initialPeers.get(publicKeyA)));
    Assertions.assertThat(addressAfter).isEqualTo(updatedAddress);
  }

  @Test
  void getPeerRealAddress() {
    int portOffset = 9;
    for (Map.Entry<BlockchainPublicKey, Address> entry : initialPeers.entrySet()) {
      Address address = entry.getValue();
      ConnectionAddress expected =
          new ConnectionAddress(address.hostname(), address.port() + portOffset);
      Assertions.assertThat(peerDatabase.getPeerRealAddress(entry.getKey())).isEqualTo(expected);
    }
  }

  @Test
  void getNonExistingRealPeer() {
    Assertions.assertThat(peerDatabase.getPeerRealAddress(otherPublicKey)).isNull();
  }

  @Test
  void getPeers() {
    Assertions.assertThat(peerDatabase.getPeers())
        .containsExactlyInAnyOrder(
            com.partisiablockchain.demo.byoc.Address.parseAddress("1.1.1.1:1111"),
            com.partisiablockchain.demo.byoc.Address.parseAddress("2.2.2.2:2222"),
            com.partisiablockchain.demo.byoc.Address.parseAddress("3.3.3.3:3333"));
  }
}
