package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BetanetAddressesTest {

  @Test
  public void createAddress() {
    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "SystemUpdate"))
        .isEqualTo(BetanetAddresses.SYSTEM_UPDATE);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_GOV, "block-producer-orchestration-contract"))
        .isEqualTo(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_GOV, "large-oracle-contract"))
        .isEqualTo(BetanetAddresses.LARGE_ORACLE_CONTRACT);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-incoming"))
        .isEqualTo(BetanetAddresses.BYOC_INCOMING);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-outgoing"))
        .isEqualTo(BetanetAddresses.BYOC_OUTGOING);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "pub-deploy-wasm"))
        .isEqualTo(BetanetAddresses.PUB_DEPLOY_WASM);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "RootDeploy"))
        .isEqualTo(BetanetAddresses.ROOT_DEPLOY);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "FeeDistribution"))
        .isEqualTo(BetanetAddresses.FEE_DISTRIBUTION);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "MpcTokenContract"))
        .isEqualTo(BetanetAddresses.MPC_TOKEN_CONTRACT);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationEcosystem"))
        .isEqualTo(BetanetAddresses.FOUNDATION_ECOSYSTEM);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationSale"))
        .isEqualTo(BetanetAddresses.FOUNDATION_SALE);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationTeamTokens"))
        .isEqualTo(BetanetAddresses.FOUNDATION_TEAM);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationReserve"))
        .isEqualTo(BetanetAddresses.FOUNDATION_RESERVE);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_SYSTEM, "ZkNodeRegistry"))
        .isEqualTo(BetanetAddresses.ZK_NODE_REGISTRY);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "RealDeploy"))
        .isEqualTo(BetanetAddresses.REAL_DEPLOY);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_GOV, "byoc-polygon-usdc-outgoing"))
        .isEqualTo(BetanetAddresses.BYOC_POLYGON_USDC_OUTGOING);

    Assertions.assertThat(
            BetanetAddresses.createAddress(
                BlockchainAddress.Type.CONTRACT_GOV, "byoc-polygon-usdc-incoming"))
        .isEqualTo(BetanetAddresses.BYOC_POLYGON_USDC_INCOMING);

    Assertions.assertThat(
            BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "mpc-wrap"))
        .isEqualTo(BetanetAddresses.MPC_WRAP_CONTRACT);
  }
}
