package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BetanetConfigFilesTest {

  @Test
  void createFromArgs_wrongNumberOfArguments() {
    Assertions.assertThatThrownBy(() -> BetanetConfigFiles.createFromArgs(new String[1]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 2 or 3 arguments, received 1");
    Assertions.assertThatThrownBy(() -> BetanetConfigFiles.createFromArgs(new String[4]))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void createFromArgs() throws IOException {
    Path file = Files.createTempFile("myFile", "tmp");
    Path directory = Files.createTempDirectory("myDirectory");

    String directoryPath = directory.toAbsolutePath().toString();
    String filePath = file.toAbsolutePath().toString();
    Assertions.assertThatThrownBy(
            () ->
                BetanetConfigFiles.createFromArgs(
                    new String[] {directoryPath, "true", directoryPath}))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(directoryPath + " is not a file");
    Assertions.assertThatThrownBy(
            () -> BetanetConfigFiles.createFromArgs(new String[] {filePath, "true", filePath}))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(filePath + " is not a directory");
  }

  @Test
  void config() throws IOException {
    Path file = Files.createTempFile("myFile", "tmp");
    Path directory = Files.createTempDirectory("myDirectory");

    CompositeNodeConfigDto expectedConfig = new CompositeNodeConfigDto();
    BetanetConfigFiles.mapper.writeValue(file.toFile(), expectedConfig);

    String directoryPath = directory.toAbsolutePath().toString();
    String filePath = file.toAbsolutePath().toString();
    BetanetConfigFiles config =
        BetanetConfigFiles.createFromArgs(new String[] {filePath, directoryPath});
    Assertions.assertThat(config).isNotNull();
    Assertions.assertThat(config.getCompositeNodeConfig())
        .usingRecursiveComparison()
        .isEqualTo(expectedConfig);
    Assertions.assertThat(config.getDataDirectory().createFile("someFile").getParentFile())
        .isEqualTo(directory.toFile());
  }
}
