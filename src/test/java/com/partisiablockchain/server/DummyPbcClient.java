package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainClientForTransaction;
import com.partisiablockchain.api.transactionclient.ShardId;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.ExecutionStatus;
import com.partisiablockchain.api.transactionclient.model.SerializedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Spy implementation of {@link BlockchainClientForTransaction}. */
public final class DummyPbcClient implements BlockchainClientForTransaction {

  private final ArrayDeque<ExecutedTransaction> executedTransactions = new ArrayDeque<>();
  private final ArrayList<SignedTransaction> transactionsSent = new ArrayList<>();
  private int transactionsRequested = 0;

  /**
   * Amount of transactions that have been requested.
   *
   * @return Amount of transactions that have been requested.
   */
  public int numTransactionsRequested() {
    return transactionsRequested;
  }

  void addDefaultExecutedTransactions() {
    this.executedTransactions.add(createExecutedTransaction(1));
    this.executedTransactions.add(createExecutedTransaction(2));
  }

  private static ExecutedTransaction createExecutedTransaction(int id) {
    final ExecutionStatus status = new ExecutionStatus();
    status.setSuccess(true);
    status.setFinalized(true);
    status.setBlockId(Hash.create(s -> s.writeInt(id)));

    final ExecutedTransaction executedTransaction = new ExecutedTransaction();
    executedTransaction.setContent(SafeDataOutputStream.serialize(s -> s.writeInt(id)));
    executedTransaction.setExecutionStatus(status);
    executedTransaction.setIsEvent(false);
    return executedTransaction;
  }

  @Override
  public TransactionPointer putTransaction(SerializedTransaction serializedTransactions)
      throws ApiException {
    final SignedTransaction signedTransaction =
        SafeDataInputStream.readFully(
            serializedTransactions.getPayload(), s -> SignedTransaction.read(getChainId(), s));
    transactionsSent.add(signedTransaction);

    final TransactionPointer ptr = new TransactionPointer();
    ptr.setIdentifier(signedTransaction.identifier());
    return ptr;
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash hash) throws ApiException {
    transactionsRequested++;
    if (executedTransactions.isEmpty()) {
      return null;
    } else {
      return executedTransactions.remove();
    }
  }

  /**
   * Returns all sent {@link SignedTransaction}s.
   *
   * @return All sent {@link SignedTransaction}s.
   */
  public List<SignedTransaction> getAllSentTransactions() {
    return transactionsSent;
  }

  @Override
  public long getLatestProductionTime(ShardId shardId) {
    return 0L;
  }

  @Override
  public long getNonce(BlockchainAddress address) {
    return 0L;
  }

  @Override
  public String getChainId() {
    return "chain chain";
  }

  /**
   * Searches through all transactions put and returns first transaction that matches {@code rpc}.
   *
   * @param rpc rpc to match
   * @return transaction
   */
  public Transaction findTransaction(byte[] rpc) {
    for (SignedTransaction transaction : transactionsSent) {
      if (Arrays.equals(transaction.getInner().getRpc(), rpc)) {
        return transaction.getInner();
      }
    }
    return null;
  }
}
