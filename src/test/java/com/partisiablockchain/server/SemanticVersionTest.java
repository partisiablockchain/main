package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class SemanticVersionTest {

  /** Semantic version can be read from string. */
  @Test
  public void readSemanticVersion() {
    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion("1.2.3");
    Assertions.assertThat(nodeVersion.major()).isEqualTo(1);
    Assertions.assertThat(nodeVersion.minor()).isEqualTo(2);
    Assertions.assertThat(nodeVersion.patch()).isEqualTo(3);
  }

  /** Read node version does not contain any beta version identifiers. */
  @Test
  public void readBetaVersionWithoutIdentifiers() {
    SemanticVersion nodeVersion =
        SemanticVersion.readSemanticVersion("1.2.3-beta-par-15235-1730110982-82abe077");
    Assertions.assertThat(nodeVersion.major()).isEqualTo(1);
    Assertions.assertThat(nodeVersion.minor()).isEqualTo(2);
    Assertions.assertThat(nodeVersion.patch()).isEqualTo(3);
  }

  /** Invalid node version is not read as a SemanticVersion. */
  @Test
  public void doNotReadInvalidVersion() {
    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion("invalid.node.version");
    Assertions.assertThat(nodeVersion).isNull();
  }
}
