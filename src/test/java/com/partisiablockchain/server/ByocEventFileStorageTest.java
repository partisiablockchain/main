package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.WithdrawIdWrapper;
import com.partisiablockchain.math.Unsigned256;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class ByocEventFileStorageTest {

  @TempDir Path temp;

  File file;

  @BeforeEach
  void setUp() {
    file = new File(temp.toAbsolutePath().toString(), "cache.json");
    Assertions.assertThat(file.exists()).isFalse();
  }

  @Test
  void readingFromNonExistingFile() {
    ByocEventFileStorage storage = new ByocEventFileStorage(file);
    Assertions.assertThat(file.exists()).isFalse();

    EthereumEventsCache emptyCache = storage.readEvents();
    Assertions.assertThat(file.exists()).isTrue();
    Assertions.assertThat(emptyCache.loadLatestCheckedBlock()).isNull();
    Assertions.assertThat(emptyCache.loadAllDeposits()).isEmpty();
    Assertions.assertThat(emptyCache.loadAllWithdrawals()).isEmpty();
  }

  @Test
  void readingFromExistingFile() throws IOException {
    Files.writeString(
        file.toPath(),
        "{\"deposits\":{\"1\":{\"amount\":\"10000000\",\"nonce\":\"1\",\"receiver\":"
            + "\"00bb780a2c78901d3fb33738768511a30617afa01d\"}},\"latestCheckedBlock\":\"150510\","
            + "\"withdrawals\":{\"1-1\":{\"amount\":\"5000000\",\"bitmask\":7,\"destination\":"
            + "\"SomeEthAccount\",\"oracleNonce\":\"1\",\"withdrawalNonce\":\"1\"}}}\n");
    ByocEventFileStorage storage = new ByocEventFileStorage(file);
    EthereumEventsCache cache = storage.readEvents();
    Assertions.assertThat(cache.loadLatestCheckedBlock()).isEqualTo(Unsigned256.create(150510));
    Assertions.assertThat(cache.loadDeposit(1)).isNotNull();
    Assertions.assertThat(cache.loadDeposit(2)).isNull();
    Assertions.assertThat(cache.loadWithdrawal(WithdrawIdWrapper.createId(1, 1))).isNotNull();
    Assertions.assertThat(cache.loadWithdrawal(WithdrawIdWrapper.createId(1, 2))).isNull();
    Assertions.assertThat(cache.loadWithdrawal(WithdrawIdWrapper.createId(2, 1))).isNull();
  }

  @Test
  void writingAndReadingFile() {
    ByocEventFileStorage storage = new ByocEventFileStorage(file);
    EthereumEventsCache cache = storage.readEvents();

    Unsigned256 latestCheckedBlock = Unsigned256.create(1510L);
    cache.saveLatestCheckedBlock(latestCheckedBlock);

    DepositTransaction deposit1 =
        new DepositTransaction(
            1, new KeyPair(BigInteger.ZERO).getPublic().createAddress(), Unsigned256.ZERO);
    DepositTransaction deposit2 =
        new DepositTransaction(
            2, new KeyPair(BigInteger.TWO).getPublic().createAddress(), Unsigned256.TEN);
    cache.saveDeposits(List.of(deposit1, deposit2));

    WithdrawalTransaction withdraw1 =
        new WithdrawalTransaction(1, "SomeEth", Unsigned256.ONE, 1, 3);
    WithdrawalTransaction withdraw2 =
        new WithdrawalTransaction(2, "SomeEth", Unsigned256.ONE, 1, 5);
    WithdrawalTransaction withdraw3 =
        new WithdrawalTransaction(1, "SomeOtherEth", Unsigned256.ZERO, 2, 7);
    WithdrawalTransaction withdraw4 =
        new WithdrawalTransaction(2, "SomeEth", Unsigned256.TEN, 3, 5);
    cache.saveWithdrawals(List.of(withdraw1, withdraw2, withdraw3, withdraw4));

    storage.writeEvents(cache);

    EthereumEventsCache newCache = storage.readEvents();
    Assertions.assertThat(newCache.loadLatestCheckedBlock()).isEqualTo(latestCheckedBlock);

    Assertions.assertThat(newCache.loadDeposit(1)).isEqualTo(deposit1);
    Assertions.assertThat(newCache.loadDeposit(2)).isEqualTo(deposit2);
    Assertions.assertThat(newCache.loadDeposit(3)).isNull();

    Assertions.assertThat(newCache.loadWithdrawal(WithdrawIdWrapper.createId(1, 1)))
        .isEqualTo(withdraw1);
    Assertions.assertThat(newCache.loadWithdrawal(WithdrawIdWrapper.createId(1, 2)))
        .isEqualTo(withdraw2);
    Assertions.assertThat(newCache.loadWithdrawal(WithdrawIdWrapper.createId(2, 1)))
        .isEqualTo(withdraw3);
    Assertions.assertThat(newCache.loadWithdrawal(WithdrawIdWrapper.createId(3, 2)))
        .isEqualTo(withdraw4);
  }

  @Test
  void readBadlyFormattedFileReturnsEmptyCache() throws IOException {
    Files.writeString(file.toPath(), "Foobar");
    ByocEventFileStorage storage = new ByocEventFileStorage(file);
    EthereumEventsCache cache = storage.readEvents();
    Assertions.assertThat(cache).isNotNull();
  }

  @Test
  void temporaryFileIsUsedToWriteEvents() throws IOException {
    String tmpFileName = file.getCanonicalPath() + ".tmp";
    System.out.println(tmpFileName);
    Path tmpPath = Path.of(tmpFileName);
    Files.createFile(tmpPath);
    String tmpContent = "tempy temp";
    Files.writeString(tmpPath, tmpContent);
    Assertions.assertThat(Files.readString(tmpPath)).isEqualTo(tmpContent);

    Files.writeString(file.toPath(), "{\"deposits\":{},\"withdrawals\":{}}");
    ByocEventFileStorage storage = new ByocEventFileStorage(file);
    EthereumEventsCache cache = storage.readEvents();
    Unsigned256 latestCheckedBlock = Unsigned256.create(701L);
    cache.saveLatestCheckedBlock(latestCheckedBlock);
    DepositTransaction deposit =
        new DepositTransaction(
            51,
            new KeyPair(BigInteger.TWO).getPublic().createAddress(),
            Unsigned256.create(1000000000L));
    cache.saveDeposits(List.of(deposit));
    storage.writeEvents(cache);

    Assertions.assertThat(Files.exists(tmpPath)).isFalse();
  }
}
