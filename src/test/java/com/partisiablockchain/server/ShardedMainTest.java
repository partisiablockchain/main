package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SenderAuthenticationKeyPair;
import com.partisiablockchain.blockchain.BlockchainHelper;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.RoutingPluginHelper;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducerConfigDto;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.monitor.ByocProcessRunner;
import com.partisiablockchain.demo.byoc.monitor.ContractStateHandler;
import com.partisiablockchain.demo.byoc.monitor.SystemTimeProviderImpl;
import com.partisiablockchain.demo.byoc.settings.EvmPriceOracleChainConfig;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.flooding.NetworkConfigHelper;
import com.partisiablockchain.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.main.voting.CommitteeVotes;
import com.partisiablockchain.main.voting.PendingUpdate;
import com.partisiablockchain.main.voting.Poll;
import com.partisiablockchain.main.voting.PollUpdateType;
import com.partisiablockchain.main.voting.Vote;
import com.partisiablockchain.main.voting.VotingState;
import com.partisiablockchain.oracle.EvmOracleChainConfig;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.partisiablockchain.rosetta.dto.NetworkListResponseDto;
import com.partisiablockchain.rosetta.dto.VersionDto;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto;
import com.partisiablockchain.server.rest.chain.ChainController;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import com.partisiablockchain.server.rest.shard.ShardController;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.synaps.SynapsResource;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zknetwork.ConnectionAddress;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.rest.OpenApiResource;
import com.secata.tools.rest.RestServer;
import com.secata.tools.thread.ShutdownHook;
import jakarta.ws.rs.client.ClientBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardedMainTest {

  private static final KeyPair producer = new KeyPair(BigInteger.ONE);
  private static final String peerPath =
      "src/test/resources/com/partisiablockchain/server/genesis/peers.txt";
  private static final Map<Hash, com.partisiablockchain.demo.byoc.Address> peersMap =
      Map.of(
          Hash.create(s -> s.writeString("123")),
          new com.partisiablockchain.demo.byoc.Address("some", 2001),
          Hash.create(s -> s.writeString("1234")),
          new com.partisiablockchain.demo.byoc.Address("some", 2002),
          Hash.create(s -> s.writeString("12345")),
          new com.partisiablockchain.demo.byoc.Address("some", 2003));
  private static final BlockchainAddress MISCONFIGURED_BYOC =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(s -> s.writeString("Misconfigured BYOC")));
  private static Path genesis;
  private static Path genesisConfigTmp;
  private static RootDirectory rootDirectory;
  private final Random random = new Random(1199);
  private static final long GENESIS_BLOCK_PRODUCTION_TIME = 1657259535997L;
  private static final int UPDATE_NODE_VERSION_INVOCATION = 15;

  private static void stateWithByocOrchestrator(MutableChainState mutable) {
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ROUTING,
        RoutingPluginHelper.createJar(),
        new byte[0]);
    BlockchainAddress incoming =
        BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "my-incoming");
    BlockchainAddress outgoing =
        BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "my-outgoing");
    BlockchainAddress priceOracle =
        BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "my-priceOracle");
    BlockchainAddress tokenBridgeIncoming =
        BetanetAddresses.createAddress(
            BlockchainAddress.Type.CONTRACT_GOV, "token-bridge-incoming");
    BlockchainAddress tokenBridgeOutgoing =
        BetanetAddresses.createAddress(
            BlockchainAddress.Type.CONTRACT_GOV, "token-bridge-outgoing");

    BlockchainLedgerHelper.createContract(
        BetanetAddresses.BYOC_ORCHESTRATOR,
        () ->
            JarBuilder.buildJar(
                MockByocOrchestrator.class, MockByocOrchestratorState.class, MockBridge.class),
        () ->
            new MockByocOrchestratorState(
                AvlTree.create(
                    Map.of(
                        "ETH",
                        new MockBridge(
                            "Ethereum",
                            incoming,
                            outgoing,
                            priceOracle,
                            FixedList.create(
                                List.of(
                                    new TokenBridgeMock(
                                        "Polygon", tokenBridgeIncoming, tokenBridgeOutgoing)))),
                        "Unknown",
                        new MockBridge("UnknownChain", incoming, outgoing, null, null)))),
        mutable);
    BlockchainLedgerHelper.createContract(
        outgoing,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000003")))),
        mutable);
    BlockchainLedgerHelper.createContract(
        tokenBridgeOutgoing,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000004")))),
        mutable);
  }

  private static void stateWithByocOutgoing(MutableChainState mutable) {
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ROUTING,
        RoutingPluginHelper.createJar(),
        new byte[0]);
    BlockchainLedgerHelper.createContract(
        BetanetAddresses.BYOC_OUTGOING,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000003")))),
        mutable);
    BlockchainLedgerHelper.createContract(
        BetanetAddresses.BYOC_POLYGON_USDC_OUTGOING,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000003")))),
        mutable);
    BlockchainLedgerHelper.createContract(
        BetanetAddresses.BYOC_BNB_SMARTCHAIN_BNB_OUTGOING,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000003")))),
        mutable);
    BlockchainLedgerHelper.createContract(
        BetanetAddresses.BYOC_ETHEREUM_PARTI_OUTGOING,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("0000000000000000000000000000000000000003")))),
        mutable);
    BlockchainLedgerHelper.createContract(
        MISCONFIGURED_BYOC,
        () -> JarBuilder.buildJar(DummyByocOutgoingContract.class),
        () ->
            new ByocOutgoingContractState(
                new EthereumAddress(
                    new LargeByteArray(Hex.decode("97a2b3856bebd8fd0b65841481777a4666d1e7cd")))),
        mutable);
  }

  static JerseyWebClient createRawClient() {
    return new JerseyWebClient(
        ClientBuilder.newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build());
  }

  private static Consumer<MutableChainState> stateWithVotingContract(
      Map<Hash, Poll> polls, KeyPair votingKeyForTest) {
    return mutable -> {
      mutable.setPlugin(
          FunctionUtility.noOpBiConsumer(),
          null,
          ChainPluginType.ROUTING,
          RoutingPluginHelper.createJar(),
          new byte[0]);
      BlockchainLedgerHelper.createContract(
          BetanetAddresses.SYSTEM_UPDATE,
          () -> JarBuilder.buildJar(DummyVotingContract.class),
          () ->
              new VotingState(
                  BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
                  AvlTree.create(polls),
                  createCommittee(
                      List.of(
                          votingKeyForTest.getPublic().createAddress(),
                          new KeyPair(BigInteger.valueOf(9)).getPublic().createAddress(),
                          new KeyPair(BigInteger.valueOf(8)).getPublic().createAddress()))),
          mutable);
    };
  }

  private static Consumer<MutableChainState> stateWithBpoContract(
      Map<BlockchainAddress, BpOrchestrationContractState.BlockProducerDto> producers) {
    return mutable -> {
      mutable.setPlugin(
          FunctionUtility.noOpBiConsumer(),
          null,
          ChainPluginType.ROUTING,
          RoutingPluginHelper.createJar(),
          new byte[0]);
      BlockchainLedgerHelper.createContract(
          BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
          () -> JarBuilder.buildJar(DummyBpoContract.class),
          () -> new BpOrchestrationContractState(AvlTree.create(producers)),
          mutable);
    };
  }

  private static void runAndAssertByocServiceShutdownHooks(
      ShardedMain.ByocConfig ethereumByocConfig,
      CompositeNodeConfigDto nodeConfigDto,
      int numberOfHooks)
      throws Exception {
    ShutdownHook shutdownHook = new ShutdownHook();
    ethereumByocConfig.runByocOracleServices(
        rootDirectory,
        nodeConfigDto,
        List.of("Shard1"),
        BigInteger.TEN,
        new KeyPair(BigInteger.ONE).getPublic().createAddress(),
        createTestSender(),
        shutdownHook,
        new ContractStateHandler(List.of()),
        new AdditionalStatusProvidersImpl());
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(numberOfHooks);
    for (AutoCloseable closeable : shutdownHook.closeables()) {
      closeable.close();
    }
  }

  private static SingleThreadedTransactionSender createTestSender() {
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    final DummyPbcClient dummyClient = new DummyPbcClient();
    BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(producer),
            10,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    return new SingleThreadedTransactionSender(serverConfig, client);
  }

  private static CommitteeVotes createCommittee(List<BlockchainAddress> bpList) {
    AvlTree<BlockchainAddress, Long> committee = AvlTree.create();
    for (BlockchainAddress address : bpList) {
      committee = committee.set(address, 1L);
    }
    return new CommitteeVotes(committee);
  }

  /**
   * Helper function to get Main CollectingServerConfig.
   *
   * @return Main.CollectingServerConfig
   */
  public static Main.CollectingServerConfig getServerConfig() {
    return new Main.CollectingServerConfig();
  }

  /**
   * Setup before each test.
   *
   * @throws IOException exception
   */
  @BeforeEach
  public void setup() throws IOException {
    // Create the peer file
    new PeersFile(new File(peerPath)).writePeers(peersMap);
    rootDirectory =
        MemoryStorage.createRootDirectory(
            Files.createTempDirectory("myDirectory").toFile(), new ConcurrentHashMap<>());
    genesis = Files.createTempDirectory("genesis");
    genesisConfigTmp = Files.createTempFile("genesis_config", "tmp");
  }

  /** Restore peers file. */
  @AfterEach
  public void tearDown() {
    new PeersFile(new File(peerPath)).writePeers(Map.of());
  }

  @Test
  public void classDefinition() {
    Assertions.assertThat(new ShardedMain()).isInstanceOf(ShardedMain.class);
  }

  @Test
  public void isRestServerRunning() {
    Assertions.assertThat(ShardedMain.isRestServerRunning(null)).isEqualTo("");
    Main.CollectingServerConfig serverConfig = getServerConfig();
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1");
    serverConfig.registerRestComponent(new MetricsResource(ledger));
    RestServer restServer = Main.restServer(serverConfig, ledger, 12345);
    Assertions.assertThat(ShardedMain.isRestServerRunning(restServer)).isEqualTo("false");
  }

  @Test
  public void createPersistentByocEventCache() {
    ShardedMain.ByocConfig byocConfig =
        new ShardedMain.ByocConfig(
            "prefix",
            "url",
            "address",
            BigInteger.ZERO,
            BetanetAddresses.BYOC_INCOMING,
            BetanetAddresses.BYOC_OUTGOING);

    PersistentByocEventCache byocEventCache =
        byocConfig.createPersistentByocEventCache(rootDirectory);
    Assertions.assertThat(byocEventCache).isNotNull();
  }

  @Test
  public void getIntervalsAndErrorRequests() {
    ShardedMain.ByocConfig byocConfig =
        new ShardedMain.ByocConfig("prefix", "", "", BigInteger.ONE, null, null);
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.overrides =
        Map.of(
            "prefix.delay_on_error",
            "10",
            "prefix.deposit.eth.check_interval",
            "10",
            "prefix.deposit.check_interval",
            "10",
            "prefix.withdrawal.check_interval",
            "10",
            "byoc.large_oracle.delay_on_error",
            "10",
            "byoc.large_oracle.check_interval",
            "10",
            "fasttrack.producer.network_delay",
            "10",
            "fasttrack.producer.timeout",
            "10",
            "price_oracle.delay_on_error",
            "10",
            "price_oracle.check_interval",
            "10");
    Assertions.assertThat(byocConfig.getDelayOnError(config)).isEqualTo(10);
    Assertions.assertThat(byocConfig.getEthRequestInterval(config)).isEqualTo(10);
    Assertions.assertThat(byocConfig.getDepositRequestInterval(config)).isEqualTo(10);
    Assertions.assertThat(byocConfig.getWithdrawalCheckInterval(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getLargeOracleDelayOnError(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getLargeOracleCheckInterval(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getPriceOracleDelayOnError(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getPriceOracleCheckInterval(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getTimeout(config)).isEqualTo(10);
    Assertions.assertThat(ShardedMain.getNetworkDelay(config)).isEqualTo(10);
  }

  @Test
  public void runByocAndSystemUpdateWithByocOutgoing() {
    final ShutdownHook shutdownHook = new ShutdownHook();
    final CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.accountKey = BigInteger.TWO.toString(16);
    bpConfig.finalizationKey = BigInteger.TWO.toString(16);
    bpConfig.polygonUrl = "polyUrl";
    bpConfig.ethereumUrl = "ethUrl";
    bpConfig.bnbSmartChainUrl = "bnbUrl";
    nodeConfig.producerConfig = bpConfig;
    nodeConfig.networkKey = BigInteger.ONE.toString(16);
    nodeConfig.overrides =
        Map.of("genesisPath", "src/test/resources/com/partisiablockchain/server/");
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    final BetanetConfigFiles betanetConfigFiles =
        BetanetConfigFiles.createFromArgs(
            new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});

    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            ShardedMainTest::stateWithByocOutgoing,
            "Shard1");
    ShardedMain.runByocAndSystemUpdate(
        true,
        betanetConfigFiles,
        nodeConfig,
        peerDatabase,
        Map.of("Shard1", ledger),
        ledger,
        List.of("Shard1"),
        createTestSender(),
        Map.of("Shard1", new BlockchainResource(ledger)),
        rootDirectory.createSub("byoc"),
        new AdditionalStatusProvidersImpl(),
        shutdownHook);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(20);
  }

  /**
   * Starting up the BYOC processes with a BYOC orchestrator contract present should use the bridges
   * in the BYOC orchestrator contract.
   */
  @Test
  public void runByocWithOrchestrator() {
    final ShutdownHook shutdownHook = new ShutdownHook();
    final CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.accountKey = BigInteger.TWO.toString(16);
    bpConfig.finalizationKey = BigInteger.TWO.toString(16);
    bpConfig.ethereumUrl = "ethUrl";
    bpConfig.polygonUrl = "polygonUrl";
    nodeConfig.producerConfig = bpConfig;
    nodeConfig.networkKey = BigInteger.ONE.toString(16);
    nodeConfig.overrides =
        Map.of("genesisPath", "src/test/resources/com/partisiablockchain/server/");
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    final BetanetConfigFiles betanetConfigFiles =
        BetanetConfigFiles.createFromArgs(
            new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});

    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            ShardedMainTest::stateWithByocOrchestrator,
            "Shard1");
    ShardedMain.runByocAndSystemUpdate(
        true,
        betanetConfigFiles,
        nodeConfig,
        peerDatabase,
        Map.of("Shard1", ledger),
        ledger,
        List.of("Shard1"),
        createTestSender(),
        Map.of("Shard1", new BlockchainResource(ledger)),
        rootDirectory.createSub("byoc"),
        new AdditionalStatusProvidersImpl(),
        shutdownHook);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(14);

    // Ensure the number of Ethereum, withdrawal and deposit processes, that have been registered,
    // match the expected
    List<ByocProcessRunner> byocProcessRunners =
        shutdownHook.closeables().stream()
            .filter(autoCloseable -> autoCloseable instanceof ByocProcessRunner)
            .map(autoCloseable -> (ByocProcessRunner) autoCloseable)
            .toList();

    int numberOfEthProcesses =
        byocProcessRunners.stream()
            .filter(byocProcessRunner -> byocProcessRunner.getProcess().name().equals("Ethereum"))
            .toList()
            .size();
    // Two Ethereum processes is expected. One for the BYOC bridge and one for the token bridge
    Assertions.assertThat(numberOfEthProcesses).isEqualTo(2);
    // The same applies for withdrawal and deposit processes
    Assertions.assertThat(
            byocProcessRunners.stream()
                .filter(
                    byocProcessRunner ->
                        byocProcessRunner.getProcess().name().equals("WithdrawalProcess"))
                .toList()
                .size())
        .isEqualTo(2);
    Assertions.assertThat(
            byocProcessRunners.stream()
                .filter(
                    byocProcessRunner ->
                        byocProcessRunner.getProcess().name().equals("DepositProcessor"))
                .toList()
                .size())
        .isEqualTo(2);
  }

  @Test
  void determineAddresses() {
    List<BlockchainAddress> addresses =
        IntStream.range(0, 5)
            .mapToObj(
                i ->
                    BetanetAddresses.createAddress(
                        BlockchainAddress.Type.CONTRACT_GOV, "my-contract-" + i))
            .toList();
    BetanetAddresses.createAddress(BlockchainAddress.Type.CONTRACT_GOV, "my-priceOracle");
    List<ByocConfiguration> configuration =
        List.of(
            new ByocConfiguration(
                "Ethereum", "ETH", addresses.get(0), addresses.get(1), addresses.get(4)),
            new ByocConfiguration("Ethereum", "DOGE", addresses.get(2), addresses.get(3), null));

    Assertions.assertThat(ShardedMain.determinePriceOracleAddresses(configuration))
        .containsExactly(addresses.get(4));
    Assertions.assertThat(ShardedMain.determineActiveContracts(configuration))
        .hasSize(7)
        .contains(
            BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION, BetanetAddresses.LARGE_ORACLE_CONTRACT)
        .containsAll(addresses);
  }

  private CompositeNodeConfigDto setupConfig() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.ethereumUrl = "someUrl";
    bpConfig.polygonUrl = "polyUrl";
    bpConfig.host = "someHost";
    bpConfig.finalizationKey = producer.getPrivateKey().toString(16);
    bpConfig.accountKey = producer.getPrivateKey().toString(16);
    config.producerConfig = bpConfig;
    config.networkKey = producer.getPrivateKey().toString(16);
    config.floodingPort = BlockchainLedgerHelper.getFreshPort();
    config.periodicThreadDumpsConfig = new ThreadDumpModule.Config();
    config.overrides = Map.of("genesisPath", "src/test/resources/com/partisiablockchain/server/");
    return config;
  }

  @Test
  public void main() throws Exception {
    CompositeNodeConfigDto config = setupConfig();
    final Main.CollectingServerConfig serverConfig = getServerConfig();
    config.knownPeers =
        List.of(
            Hex.toHexString(new KeyPair(BigInteger.TWO).getPublic().asBytes()) + ":1.1.1.1:1111",
            Hex.toHexString(new KeyPair(BigInteger.TWO).getPublic().asBytes()) + ":2.2.2.2:2222");

    BetanetConfigFiles.mapper.writeValue(Path.of(genesisConfigTmp.toString()).toFile(), config);
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    final int peers = peerDatabase.getPeers().size();
    BetanetConfigFiles betanetConfigFiles =
        BetanetConfigFiles.createFromArgs(
            new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});
    ThreadDumpModule threadDumpModule =
        new ThreadDumpModule(betanetConfigFiles.getDataDir(), config.periodicThreadDumpsConfig);
    config.periodicThreadDumpsConfig.enabled = true;
    AdditionalStatusProviders additionalStatusProviders = new AdditionalStatusProvidersImpl();
    ShardedMain.runShardedMain(
        config,
        rootDirectory,
        serverConfig,
        betanetConfigFiles,
        peerDatabase,
        true,
        threadDumpModule,
        new ShutdownHook(),
        additionalStatusProviders,
        ShardedMain.TransactionSenderProvider.REST_SENDER,
        new SystemTimeProviderImpl());

    AdditionalStatusProvider nodeStatus = additionalStatusProviders.getProviders().get(0);
    Assertions.assertThat(nodeStatus.getName()).isEqualTo("NodeStatus");
    Assertions.assertThat(nodeStatus.getStatus())
        .isEqualTo(Map.of("version", VersionDto.NODE_VERSION));
    Assertions.assertThat(nodeStatus.getStatus().get("version")).isNotEqualTo("");
    AdditionalStatusProvider externalChainBlockProvider =
        additionalStatusProviders.getProviders().stream()
            .filter(provider -> provider instanceof LatestExternalBlockProvider)
            .findFirst()
            .orElse(null);
    Assertions.assertThat(externalChainBlockProvider).isNotNull();
    String chain = "byoc.ethereum.eth";
    Assertions.assertThat(externalChainBlockProvider.getName())
        .isEqualTo("LatestExternalBlockProvider-%s".formatted(chain));
    Assertions.assertThat(externalChainBlockProvider.getStatus())
        .contains(Map.entry("latestBlockTime", "-1"));
    // We add two peers to the database in updatePeerDatabaseIfProducer
    Assertions.assertThat(peers + 2).isEqualTo(peerDatabase.getPeers().size());
    List<AutoCloseable> closeable = serverConfig.closeable;
    Assertions.assertThat(serverConfig.alive.size()).isEqualTo(3);
    Assertions.assertThat(serverConfig.components.size()).isEqualTo(10);
    Assertions.assertThat(serverConfig.componentClasses.size()).isEqualTo(5);
    Assertions.assertThat(closeable.size()).isEqualTo(17);
    Assertions.assertThatThrownBy(
            () ->
                ShardedMain.main(
                    new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()}))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to bind");

    for (AutoCloseable close : closeable) {
      close.close();
    }

    final Path genesis2 = Files.createTempDirectory("genesis");
    final Path genesisConfigTmp2 = Files.createTempFile("genesis_config", "tmp");
    config.floodingPort = BlockchainLedgerHelper.getFreshPort();
    config.producerConfig = null;
    BetanetConfigFiles.mapper.writeValue(Path.of(genesisConfigTmp2.toString()).toFile(), config);
    ShardedMain.main(new String[] {genesisConfigTmp2.toString(), "ignored", genesis2.toString()});
  }

  @Test
  void runRosetta() {
    Main.CollectingServerConfig serverConfig = getServerConfig();
    Map<String, BlockchainLedger> ledgers = new HashMap<>();
    ledgers.put(
        null,
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1"));
    ShardedMain.runRosetta(serverConfig, ledgers);
    Assertions.assertThat(serverConfig.components.size()).isEqualTo(2);
  }

  @Test
  void runSynaps() {
    Main.CollectingServerConfig serverConfig = getServerConfig();
    Map<String, BlockchainLedger> ledgers = new HashMap<>();
    ledgers.put(
        null,
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1"));
    ShardedMain.runSynaps(serverConfig, ledgers);
    Assertions.assertThat(serverConfig.componentClasses).contains(SynapsResource.class);
    Assertions.assertThat(serverConfig.components).hasSize(1);
    Assertions.assertThat(serverConfig.components.get(0)).isInstanceOf(AbstractBinder.class);
  }

  @Test
  void runChainController() {
    Main.CollectingServerConfig serverConfig = getServerConfig();
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1");
    ShardedMain.runChainController(serverConfig, Map.of("null", ledger));
    Assertions.assertThat(serverConfig.componentClasses).contains(ChainController.class);
    Assertions.assertThat(serverConfig.components).hasSize(1);
    Assertions.assertThat(serverConfig.components.get(0)).isInstanceOf(AbstractBinder.class);
  }

  @Test
  void runShardController() {
    Main.CollectingServerConfig serverConfig = getServerConfig();
    BlockchainLedger shard0 =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard0");
    BlockchainLedger shard1 =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1");
    ShardedMain.runShardController(serverConfig, Map.of("Shard0", shard0, "Shard1", shard1));
    Assertions.assertThat(serverConfig.componentClasses).contains(ShardController.class);
    Assertions.assertThat(serverConfig.components).hasSize(1);
    Assertions.assertThat(serverConfig.components.get(0)).isInstanceOf(AbstractBinder.class);
  }

  @Test
  void blockchainHelperClass() {
    Assertions.assertThat(new BlockchainHelper().getClass()).isEqualTo(BlockchainHelper.class);
  }

  @Test
  void runShardedMainOffline() throws Exception {
    final Path genesis = Files.createTempDirectory("genesis");
    final Path genesisConfigTmp = Files.createTempFile("genesis_config", "tmp");
    CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    nodeConfig.overrides =
        Map.of(
            "rosetta_online",
            "false",
            "genesisPath",
            "src/test/resources/com/partisiablockchain/server/");
    nodeConfig.floodingPort = BlockchainLedgerHelper.getFreshPort();
    nodeConfig.producerConfig = null;
    nodeConfig.restPort = BlockchainLedgerHelper.getFreshPort();

    BetanetConfigFiles.mapper.writeValue(Path.of(genesisConfigTmp.toString()).toFile(), nodeConfig);

    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    ShutdownHook shutdownHook = new ShutdownHook();
    AdditionalStatusProviders additionalStatusProviders = new AdditionalStatusProvidersImpl();
    ShardedMain.runShardedMainOffline(
        rootDirectory, serverConfig, nodeConfig, shutdownHook, additionalStatusProviders);
    Assertions.assertThat(additionalStatusProviders.getProviders().get(0).getName())
        .isEqualTo("NodeStatus");
    ArrayList<AutoCloseable> closeable = new ArrayList<>(serverConfig.closeable);
    closeable.addAll(shutdownHook.closeables());
    Assertions.assertThat(serverConfig.components.size()).isEqualTo(1);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(1);
    for (AutoCloseable close : closeable) {
      close.close();
    }

    ShardedMain.main(new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});
    // Let rest server come online before we query it.
    Thread.sleep(1000);
    // Makes a call to the offline service, to remove pitest void mutation.
    JerseyWebClient client = createRawClient();
    NetworkListResponseDto networkList =
        ExceptionConverter.call(
            () ->
                client.post(
                    "http://localhost:" + nodeConfig.restPort + "/rosetta/network/list",
                    null,
                    NetworkListResponseDto.class),
            "Unable to get network list");
    Assertions.assertThat(networkList).isNotNull();
    Assertions.assertThat(networkList.networkIdentifiers().size()).isEqualTo(2);
    Assertions.assertThat(networkList.networkIdentifiers().get(0).network()).isNotNull();
  }

  @Test
  void largeOracleDatabaseKey() {
    BigInteger key = BigInteger.valueOf(123);
    Hash hash = ShardedMain.computeLargeOracleDatabaseKey(key);
    Assertions.assertThat(hash)
        .isEqualTo(
            Hash.create(
                s -> {
                  s.writeString("large-oracle-database-key");
                  s.write(key.toByteArray());
                }));
  }

  @Test
  void createEventMap() {
    Map<String, String> eventMap = ShardedMain.createEventMap("base", List.of("shard1", "shard2"));
    Assertions.assertThat(eventMap)
        .containsAllEntriesOf(
            Map.of(
                "shard1", "base/shards/shard1",
                "shard2", "base/shards/shard2"));
  }

  @Test
  void isProducer() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    Assertions.assertThat(ShardedMain.isProducer(config)).isTrue();
    config.producerConfig = null;
    Assertions.assertThat(ShardedMain.isProducer(config)).isFalse();
  }

  @Test
  public void producerIsFalse() throws Exception {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.networkKey = producer.getPrivateKey().toString(16);
    config.floodingPort = BlockchainLedgerHelper.getFreshPort();
    config.overrides = Map.of("genesisPath", "src/test/resources/com/partisiablockchain/server/");
    BetanetConfigFiles.mapper.writeValue(Path.of(genesisConfigTmp.toString()).toFile(), config);
    Assertions.assertThatThrownBy(
            () ->
                ShardedMain.main(
                    new String[] {
                      genesisConfigTmp.toString(), "ignored", genesis.toString(),
                    }))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void getTcpReaderAddresses() {
    CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    nodeConfig.floodingPort = 1000;
    Assertions.assertThat(ShardedMain.getTcpReaderAddresses(nodeConfig, List.of("1", "2")))
        .containsExactly(
            new Address("localhost", 1050),
            new Address("localhost", 1051),
            new Address("localhost", 1052));
  }

  /**
   * If the latest block production time in nodes ledger is earlier than 15 min the node is
   * considered alive.
   */
  @Test
  public void isBlockProductionUpToDate() {
    long currentSystemTime = System.currentTimeMillis();
    BlockchainLedger ledgerUnder15Min =
        BlockchainLedgerHelper.createLedgerWithInitialProductionTime(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1",
            currentSystemTime - (Duration.ofMinutes(15).toMillis() - 1));
    Assertions.assertThat(ShardedMain.isStillAlive(ledgerUnder15Min, currentSystemTime)).isTrue();
  }

  /**
   * If the latest block production time in nodes ledger is at or later than 15 min the node is
   * considered not alive.
   */
  @Test
  public void isBlockProductionOutOfDate() {
    long currentSystemTime = System.currentTimeMillis();
    BlockchainLedger ledgerAt15Min =
        BlockchainLedgerHelper.createLedgerWithInitialProductionTime(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1",
            currentSystemTime - Duration.ofMinutes(15).toMillis());
    Assertions.assertThat(ShardedMain.isStillAlive(ledgerAt15Min, currentSystemTime)).isFalse();
  }

  /** If an out-of-date node catches up on block production, it will become up-to-date. */
  @Test
  public void canBecomeUpToDateFromOutOfDateBlockProduction() {
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedgerWithInitialProductionTime(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1",
            1);
    Assertions.assertThat(ShardedMain.isStillAlive(ledger, System.currentTimeMillis())).isFalse();
    BlockchainLedgerHelper.appendBlock(ledger);
    BlockchainLedgerHelper.appendBlock(ledger); // blockchain is one block behind
    Assertions.assertThat(ShardedMain.isStillAlive(ledger, System.currentTimeMillis())).isTrue();
  }

  /** Out-of-date block production results in node being considered not alive. */
  @Test
  public void addRestAliveCheck() {
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    long latestBlockProductionTime = System.currentTimeMillis();
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedgerWithInitialProductionTime(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard0",
            latestBlockProductionTime);
    ShardedMain.addRestAliveCheck(serverConfig, ledger, () -> latestBlockProductionTime);
    Assertions.assertThat(serverConfig.getRestAlive().check()).isTrue();

    ShardedMain.addRestAliveCheck(
        serverConfig, ledger, () -> latestBlockProductionTime + Duration.ofMinutes(15).toMillis());
    Assertions.assertThat(serverConfig.getRestAlive().check()).isFalse();
  }

  @Test
  public void logIfTrue() {
    AtomicReference<String> stringToBeSet = new AtomicReference<>("toBeSet");
    Consumer<String> trueStatement = stringToBeSet::set;
    ShardedMain.logIfTrue(trueStatement, true, "abc");
    Assertions.assertThat("abc").isEqualTo(stringToBeSet.get());
    Consumer<String> falseStatement = message -> Assertions.fail("This should not happened");
    ShardedMain.logIfTrue(falseStatement, false, "abc");
  }

  @Test
  public void runSystemUpdateTest() throws IOException, InterruptedException {
    KeyPair addressKeyPair = new KeyPair(BigInteger.TEN);
    Poll poll =
        poll(
            new KeyPair(BigInteger.TEN).getPublic().createAddress(),
            PollUpdateType.DEPLOY_CONTRACT,
            new byte[110]);
    Poll poll2 = poll(randomAccount(), PollUpdateType.DEPLOY_CONTRACT, new byte[110]);
    Hash pollHash2 = ShardedMain.determinePollHash(StateAccessor.create(poll2));
    Map<Hash, Poll> polls =
        Map.of(ShardedMain.determinePollHash(StateAccessor.create(poll)), poll, pollHash2, poll2);

    Consumer<MutableChainState> stateBuilder = stateWithVotingContract(polls, addressKeyPair);
    Writer writer =
        new FileWriter(
            ExceptionConverter.call(
                () -> ShardedMainTest.class.getResource("update_votes.txt").toURI().getPath()),
            StandardCharsets.UTF_8);
    writer.write(pollHash2.toString());
    writer.flush();
    writer.close();

    final BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory, BlockchainLedgerHelper.createNetworkConfig(), stateBuilder, "Shard0");
    CompositeNodeConfigDto nodeConfigDto = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfigDto =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfigDto.accountKey = addressKeyPair.getPrivateKey().toString(16);
    nodeConfigDto.producerConfig = bpConfigDto;
    final DummyPbcClient dummyClient = new DummyPbcClient();
    String shard = "Shard1";
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(producer),
            10,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);

    SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(new Main.CollectingServerConfig(), client);
    ShutdownHook shutdownHook = new ShutdownHook();
    ShardedMain.runSystemUpdate(
        nodeConfigDto,
        ledger,
        List.of(shard),
        sender,
        Map.of("Shard1", new BlockchainResource(ledger)),
        "update_votes.txt",
        shutdownHook);

    Thread.sleep(30200);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(1);
    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
    Assertions.assertThat(dummyClient.getAllSentTransactions().get(0).getInner().getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(2);
                  pollHash2.write(s);
                  s.writeBoolean(true);
                }));
  }

  @Test
  public void registerFastTrackAndTcpReaders() {
    Main.CollectingServerConfig config = getServerConfig();
    ShardedMain.registerFastTrackAndTcpReadersIfProducer(
        false, null, null, null, 0, null, null, config);
    Assertions.assertThat(config.closeable.size()).isEqualTo(0);
    final BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard0");
    CompositeNodeConfigDto compositeNodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto blockProducerConfigDto =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    blockProducerConfigDto.finalizationKey = BigInteger.TWO.toString(16);
    compositeNodeConfig.producerConfig = blockProducerConfigDto;
    ShardedMain.registerFastTrackAndTcpReadersIfProducer(
        true,
        createTestSender(),
        ShardedMain.createFastTrackConfigDtoIfProducer(
            true, compositeNodeConfig, Optional.of(producer), new AdditionalStatusProvidersImpl()),
        List.of(Address.parseAddress("SomeAddress:2123")),
        0,
        rootDirectory,
        ledger,
        config);
  }

  @Test
  public void createFastTrackConfigDto() {
    AdditionalStatusProvidersImpl additionalStatusProviders = new AdditionalStatusProvidersImpl();
    Assertions.assertThat(
            ShardedMain.createFastTrackConfigDtoIfProducer(
                true, null, Optional.of(producer), additionalStatusProviders))
        .isEqualTo(null);
    Assertions.assertThat(
            ShardedMain.createFastTrackConfigDtoIfProducer(
                false, null, Optional.of(producer), additionalStatusProviders))
        .isEqualTo(null);
    CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto blockProducerConfigDto =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    String finalizationKey = BigInteger.TWO.toString(16);
    blockProducerConfigDto.finalizationKey = finalizationKey;
    nodeConfig.producerConfig = blockProducerConfigDto;
    KeyPair networkKey = new KeyPair(BigInteger.TEN);
    FastTrackProducerConfigDto fastTrackProducerConfig =
        ShardedMain.createFastTrackConfigDtoIfProducer(
            true, nodeConfig, Optional.of(networkKey), additionalStatusProviders);
    Assertions.assertThat(fastTrackProducerConfig.producerKey)
        .isEqualTo(networkKey.getPrivateKey().toString(16));
    Assertions.assertThat(fastTrackProducerConfig.producerBlsKey).isEqualTo(finalizationKey);
    Assertions.assertThat(fastTrackProducerConfig.timeout).isEqualTo(30_000L);
    Assertions.assertThat(fastTrackProducerConfig.networkDelay).isEqualTo(15_000L);
    nodeConfig.overrides =
        Map.of("fasttrack.producer.timeout", "200", "fasttrack.producer.network_delay", "300");
    fastTrackProducerConfig =
        ShardedMain.createFastTrackConfigDtoIfProducer(
            true, nodeConfig, Optional.of(networkKey), additionalStatusProviders);
    Assertions.assertThat(fastTrackProducerConfig.timeout).isEqualTo(200L);
    Assertions.assertThat(fastTrackProducerConfig.networkDelay).isEqualTo(300L);
  }

  @Test
  public void updatePeerDatabase() {
    Assertions.assertThatThrownBy(
            () -> ShardedMain.updatePeerDatabaseIfProducer(true, Optional.empty(), null, null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to register fast track modules without network key");
    CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto producerConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    nodeConfig.producerConfig = producerConfig;
    Assertions.assertThatThrownBy(
            () ->
                ShardedMain.updatePeerDatabaseIfProducer(
                    true, Optional.of(producer), nodeConfig, null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to start producer node without producerConfig.host");
    producerConfig.host = "";
    nodeConfig.producerConfig = producerConfig;
    Assertions.assertThatThrownBy(
            () ->
                ShardedMain.updatePeerDatabaseIfProducer(
                    true, Optional.of(producer), nodeConfig, null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to start producer node without producerConfig.host");
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    producerConfig.host = "someHost";
    nodeConfig.producerConfig = producerConfig;
    nodeConfig.floodingPort = 10001;
    ShardedMain.updatePeerDatabaseIfProducer(true, Optional.of(producer), nodeConfig, peerDatabase);
    Assertions.assertThat(peerDatabase.getPeers())
        .contains(new com.partisiablockchain.demo.byoc.Address("someHost", 10001));
    Assertions.assertThat(peerDatabase.getPeers().size()).isEqualTo(4); // 3 from peerFile
    nodeConfig.floodingPort = 10002;
    ShardedMain.updatePeerDatabaseIfProducer(
        false, Optional.of(new KeyPair(BigInteger.valueOf(420))), nodeConfig, peerDatabase);
    Assertions.assertThat(peerDatabase.getPeers().size()).isEqualTo(4); // 3 from peerFile
  }

  @Test
  public void runByocAndSystemUpdate() {
    final BetanetConfigFiles betanetConfigFiles =
        BetanetConfigFiles.createFromArgs(
            new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});
    final CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.addChainEndpoint("Ethereum", null);
    bpConfig.accountKey = BigInteger.TWO.toString(16);
    bpConfig.finalizationKey = BigInteger.TWO.toString(16);
    nodeConfig.producerConfig = bpConfig;
    nodeConfig.networkKey = BigInteger.ONE.toString(16);
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    HashMap<String, BlockchainLedger> ledgers = new HashMap<>();
    final BlockchainLedger ledger =
        ShardedMain.createLedger(
            Optional.empty(),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            ExceptionConverter.call(
                () -> ShardedMainTest.class.getResource("Shard0.zip").toURI().getPath()),
            ledgers,
            i -> {});
    ShutdownHook shutdownHook = new ShutdownHook();
    ShardedMain.runByocAndSystemUpdate(
        true,
        betanetConfigFiles,
        nodeConfig,
        peerDatabase,
        Map.of("Shard1", ledger),
        ledger,
        List.of("Shard1"),
        createTestSender(),
        Map.of("Shard1", new BlockchainResource(ledger)),
        rootDirectory.createSub("byoc"),
        new AdditionalStatusProvidersImpl(),
        shutdownHook);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(9);
    shutdownHook = new ShutdownHook();
    ShardedMain.runByocAndSystemUpdate(
        false,
        betanetConfigFiles,
        nodeConfig,
        peerDatabase,
        Map.of("Shard1", ledger),
        ledger,
        List.of("Shard1"),
        createTestSender(),
        Map.of("Shard1", new BlockchainResource(ledger)),
        rootDirectory.createSub("byoc"),
        null,
        shutdownHook);
    Assertions.assertThat(shutdownHook.closeables().size()).isEqualTo(0);
  }

  @Test
  public void findExecutableEvent() throws IOException {
    ExecutableEvent event = BlockchainLedgerHelper.createEvent("Shard0", "Shard0", 1L);
    HashMap<String, BlockchainLedger> ledgers = new HashMap<>();
    ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> data = new ConcurrentHashMap<>();
    rootDirectory =
        MemoryStorage.createRootDirectory(Files.createTempDirectory("myDirectory").toFile(), data);
    final BlockchainLedger ledger =
        ShardedMain.createLedger(
            Optional.empty(),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            BlockchainLedgerHelper.setUpGenesis(
                "Shard0", "Shard0", BlockchainLedgerHelper.createChainState(), 0),
            ledgers,
            i -> {});
    ledgers.put("Shard0", ledger);
    BlockchainLedgerHelper.writeToStorage(data, "eventTransactions", event);
    List<FloodableEvent> floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent(
            "Shard0", new KeyPair(BigInteger.ONE), "Shard0", event);
    Assertions.assertThat(ledger.addPendingEvent(floodableEvent.get(0).compress())).isTrue();

    ExecutableEvent event1 = BlockchainLedgerHelper.createEvent("Shard1", "Shard1", 1L);
    final BlockchainLedger ledger1 =
        ShardedMain.createLedger(
            Optional.of(new KeyPair(BigInteger.ONE)),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            BlockchainLedgerHelper.setUpGenesis(
                "Shard1", "Shard1", BlockchainLedgerHelper.createChainState(), 0),
            ledgers,
            i -> {});
    ledgers.put("Shard1", ledger1);
    BlockchainLedgerHelper.writeToStorage(data, "eventTransactions", event1);
    List<FloodableEvent> floodableEvent1 =
        BlockchainLedgerHelper.createFloodableEvent(
            "Shard1", new KeyPair(BigInteger.ONE), "Shard1", event1);
    Assertions.assertThat(ledger1.addPendingEvent(floodableEvent1.get(0).compress())).isFalse();
  }

  @Test
  public void registerCombinedRealNode() {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(producer),
            10,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    final SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(serverConfig, client);

    final BlockchainLedger ledger =
        ShardedMain.createLedger(
            Optional.empty(),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            ExceptionConverter.call(
                () -> ShardedMainTest.class.getResource("Shard0.zip").toURI().getPath()),
            new HashMap<>(),
            i -> {});
    final CompositeNodeConfigDto nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.accountKey = producer.getPrivateKey().toString(16);
    nodeConfig.producerConfig = bpConfig;
    nodeConfig.floodingPort = BlockchainLedgerHelper.getFreshPort();
    List<Address> tcpReaders = List.of(Address.parseAddress("someAddress:20000"));
    PersistentPeerDatabase peerDatabase = new PersistentPeerDatabase(Map.of(), new File(peerPath));
    ShardedMain.registerCombinedRealNodeIfProducer(
        true,
        nodeConfig,
        serverConfig,
        peerDatabase,
        Optional.of(producer),
        rootDirectory,
        ledger,
        sender,
        tcpReaders);
    Assertions.assertThat(serverConfig.closeable.size()).isEqualTo(4);

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    sender.sendAndWaitForEvents(interact, 5000);
    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
    Assertions.assertThat(dummyClient.getAllSentTransactions().get(0).getInner())
        .isEqualTo(interact);
    ShardedMain.registerCombinedRealNodeIfProducer(
        false,
        nodeConfig,
        serverConfig,
        peerDatabase,
        Optional.of(producer),
        rootDirectory,
        ledger,
        sender,
        tcpReaders);
    Assertions.assertThat(serverConfig.closeable.size()).isEqualTo(4);
  }

  /**
   * When a node starts it will send an UPDATE_NODE_VERSION transaction to the BpOrchestration
   * contract, if the on-chain information about which version of node software it is running needs
   * to be updated.
   */
  @Test
  public void reportNodeVersion() {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    SingleThreadedTransactionSender transactionSenderSpy = createTestTransactionSender(dummyClient);

    ShutdownHook hook = new ShutdownHook();
    startShardedMain(
        true,
        (conf, client) -> transactionSenderSpy,
        new AdditionalStatusProvidersImpl(),
        hook,
        GENESIS_BLOCK_PRODUCTION_TIME);

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    transactionSenderSpy.sendAndWaitForEvents(interact, 5000);

    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION);
    Transaction updateVersion =
        dummyClient.findTransaction(createUpdateNodeVersionRpc(nodeVersion));
    Assertions.assertThat(updateVersion).isNotNull();

    Assertions.assertThat(updateVersion.getAddress())
        .isEqualTo(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION);
  }

  private void startShardedMain(
      boolean isProducer,
      ShardedMain.TransactionSenderProvider transactionSender,
      AdditionalStatusProviders additionalStatusProviders,
      ShutdownHook hook,
      long systemTime) {
    final CompositeNodeConfigDto config = setupConfig();
    final Main.CollectingServerConfig serverConfig = getServerConfig();
    final BetanetConfigFiles betanetConfigFiles =
        BetanetConfigFiles.createFromArgs(
            new String[] {genesisConfigTmp.toString(), "ignored", genesis.toString()});
    final PersistentPeerDatabase peerDatabase =
        new PersistentPeerDatabase(Map.of(), new File(peerPath));
    ThreadDumpModule threadDumpModule =
        new ThreadDumpModule(betanetConfigFiles.getDataDir(), config.periodicThreadDumpsConfig);
    ShardedMain.runShardedMain(
        config,
        rootDirectory,
        serverConfig,
        betanetConfigFiles,
        peerDatabase,
        isProducer,
        threadDumpModule,
        hook,
        additionalStatusProviders,
        transactionSender,
        () -> systemTime);
  }

  /**
   * A node will not send an UPDATE_NODE_VERSION transaction if it is out-of-date on block
   * production time. A node is considered out-of-date on block production if latest block
   * production time is greater than or equal to {@link ShardedMain#TIMEOUT_FOR_BLOCK_PRODUCTION}.
   */
  @Test
  public void nodeVersionIsNotSentIfBlockProductionTimeIsOutdated() {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    SingleThreadedTransactionSender transactionSenderSpy = createTestTransactionSender(dummyClient);

    ShutdownHook hook = new ShutdownHook();
    startShardedMain(
        true,
        (conf, client) -> transactionSenderSpy,
        new AdditionalStatusProvidersImpl(),
        hook,
        GENESIS_BLOCK_PRODUCTION_TIME + Duration.ofMinutes(15).toMillis());

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    transactionSenderSpy.sendAndWaitForEvents(interact, 5000);

    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION);
    Transaction updateVersion =
        dummyClient.findTransaction(createUpdateNodeVersionRpc(nodeVersion));
    Assertions.assertThat(updateVersion).isNull();
  }

  /**
   * A node will not send an UPDATE_NODE_VERSION transaction if it is not configured as producer.
   */
  @Test
  public void versionNotSentIfNodeIsNotConfiguredAsProducer() {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    SingleThreadedTransactionSender transactionSenderSpy = createTestTransactionSender(dummyClient);

    ShutdownHook hook = new ShutdownHook();
    startShardedMain(
        false,
        (conf, client) -> transactionSenderSpy,
        new AdditionalStatusProvidersImpl(),
        hook,
        GENESIS_BLOCK_PRODUCTION_TIME);

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    transactionSenderSpy.sendAndWaitForEvents(interact, 5000);

    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION);
    Transaction updateVersion =
        dummyClient.findTransaction(createUpdateNodeVersionRpc(nodeVersion));
    Assertions.assertThat(updateVersion).isNull();
  }

  /**
   * A node will not send an UPDATE_NODE_VERSION transaction, if the on-chain information about
   * which version of node software it is running is already up-to-date.
   */
  @Test
  public void nodeVersionIsNotReportedIfAlreadyUpToDate() {
    final DummyPbcClient dummyClient = new DummyPbcClient();
    SingleThreadedTransactionSender transactionSenderSpy = createTestTransactionSender(dummyClient);
    BlockchainLedger ledger =
        getBpoLedgerWithOneProducerWithVersion(
            producer.getPublic().createAddress(),
            SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION));
    Map<String, BlockchainResource> resources = Map.of("Shard1", new BlockchainResource(ledger));

    final SingleThreadedTransactionSender sender = createTestTransactionSender(dummyClient);

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION);
    ShardedMain.reportNodeVersionIfOutOfDate(
        true,
        sender,
        ledger,
        List.of("Shard1"),
        resources,
        setupConfig(),
        nodeVersion,
        () -> ledger.getLatestBlock().getProductionTime());
    transactionSenderSpy.sendAndWaitForEvents(interact, 5000);

    Transaction updateVersion =
        dummyClient.findTransaction(createUpdateNodeVersionRpc(nodeVersion));
    Assertions.assertThat(updateVersion).isNull();
  }

  private static BlockchainLedger getBpoLedgerWithOneProducerWithVersion(
      BlockchainAddress producerAddress, SemanticVersion producerNodeVersion) {
    BpOrchestrationContractState.BlockProducerDto blockProducer =
        new BpOrchestrationContractState.BlockProducerDto(producerNodeVersion);
    return BlockchainLedgerHelper.createLedger(
        rootDirectory,
        BlockchainLedgerHelper.createNetworkConfig(),
        stateWithBpoContract(Map.of(producerAddress, blockProducer)),
        "Shard1");
  }

  /** A node will not send an UPDATE_NODE_VERSION transaction if node version is null. */
  @Test
  public void versionNotSentIfNodeHasNullVersion() {
    BlockchainLedger ledger =
        getBpoLedgerWithOneProducerWithVersion(producer.getPublic().createAddress(), null);
    Map<String, BlockchainResource> resources = Map.of("Shard1", new BlockchainResource(ledger));

    final DummyPbcClient dummyClient = new DummyPbcClient();
    final SingleThreadedTransactionSender sender = createTestTransactionSender(dummyClient);

    ShardedMain.reportNodeVersionIfOutOfDate(
        true,
        sender,
        ledger,
        List.of("Shard1"),
        resources,
        setupConfig(),
        null,
        () -> ledger.getLatestBlock().getProductionTime());

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    sender.sendAndWaitForEvents(interact, 5000);

    Assertions.assertThat(dummyClient.getAllSentTransactions()).hasSize(1);
  }

  /**
   * A node will not send an UPDATE_NODE_VERSION transaction if it is not registered in the
   * BpOrchestration contract.
   */
  @Test
  public void nodeVersionNotSentIfNotFoundInBpo() {
    Map<BlockchainAddress, BpOrchestrationContractState.BlockProducerDto> withoutProducers =
        Map.of();
    Consumer<MutableChainState> blockchainWithBpoContract = stateWithBpoContract(withoutProducers);
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            blockchainWithBpoContract,
            "Shard1");
    Map<String, BlockchainResource> resources = Map.of("Shard1", new BlockchainResource(ledger));

    final DummyPbcClient dummyClient = new DummyPbcClient();
    final SingleThreadedTransactionSender sender = createTestTransactionSender(dummyClient);
    SemanticVersion nodeVersion = SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION);

    ShardedMain.reportNodeVersionIfOutOfDate(
        true,
        sender,
        ledger,
        List.of("Shard1"),
        resources,
        setupConfig(),
        nodeVersion,
        () -> ledger.getLatestBlock().getProductionTime());

    Transaction interact = Transaction.create(producer.getPublic().createAddress(), new byte[5]);
    dummyClient.addDefaultExecutedTransactions();
    sender.sendAndWaitForEvents(interact, 5000);

    Transaction updateVersion =
        dummyClient.findTransaction(createUpdateNodeVersionRpc(nodeVersion));
    Assertions.assertThat(updateVersion).isNull();
  }

  private static byte[] createUpdateNodeVersionRpc(SemanticVersion nodeVersion) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(UPDATE_NODE_VERSION_INVOCATION);
          stream.writeInt(nodeVersion.major());
          stream.writeInt(nodeVersion.minor());
          stream.writeInt(nodeVersion.patch());
        });
  }

  private static SingleThreadedTransactionSender createTestTransactionSender(
      DummyPbcClient dummyClient) {
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    final BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            dummyClient,
            new SenderAuthenticationKeyPair(producer),
            10,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    final SingleThreadedTransactionSender sender =
        new SingleThreadedTransactionSender(serverConfig, client);
    return sender;
  }

  /** Registering rest components adds 4 resources: Blockchain, Metrics, Shard, and Open Api. */
  @Test
  public void registerRestComponents() {
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    BlockchainLedger ledger =
        ShardedMain.createLedger(
            Optional.empty(),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            ExceptionConverter.call(
                () -> ShardedMainTest.class.getResource("Shard0.zip").toURI().getPath()),
            new HashMap<>(),
            i -> {});
    ShardedMain.registerRestComponents(serverConfig, ledger, Map.of(), Map.of());
    Assertions.assertThat(serverConfig.components.size()).isEqualTo(3);
    Object[] componentClasses = serverConfig.components.stream().map(Object::getClass).toArray();
    Assertions.assertThat(componentClasses).contains(BlockchainResource.class);
    Assertions.assertThat(componentClasses).contains(MetricsResource.class);
    Assertions.assertThat(componentClasses).contains(ShardResource.class);
    Assertions.assertThat(serverConfig.componentClasses.size()).isEqualTo(1);
    Assertions.assertThat(serverConfig.componentClasses).contains(OpenApiResource.class);
  }

  @Test
  public void registerTcpReader() {
    Main.CollectingServerConfig serverConfig = new Main.CollectingServerConfig();
    BlockchainLedger ledger =
        ShardedMain.createLedger(
            Optional.empty(),
            BlockchainLedgerHelper.createNetworkConfig(),
            rootDirectory,
            ExceptionConverter.call(
                () -> ShardedMainTest.class.getResource("Shard0.zip").toURI().getPath()),
            new HashMap<>(),
            i -> {});
    ShardedMain.registerTcpReader(
        serverConfig, rootDirectory, ledger, BlockchainLedgerHelper.getFreshPort());
    Assertions.assertThat(serverConfig.closeable.size()).isEqualTo(1);
  }

  @Test
  void getEthereumByocContract() {
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            ShardedMainTest::stateWithByocOutgoing,
            "Shard1");
    String ethereumByocContract =
        ShardedMain.getEthereumByocContract(
            ledger,
            List.of("Shard1"),
            Map.of("Shard1", new BlockchainResource(ledger)),
            BetanetAddresses.BYOC_OUTGOING);
    Assertions.assertThat(ethereumByocContract)
        .isEqualTo("0x0000000000000000000000000000000000000003");
  }

  @Test
  void getEthereumByocContractIgnoringMisconfiguredContract() {
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            ShardedMainTest::stateWithByocOutgoing,
            "Shard1");
    String ethereumByocContract =
        ShardedMain.getEthereumByocContract(
            ledger,
            List.of("Shard1"),
            Map.of("Shard1", new BlockchainResource(ledger)),
            MISCONFIGURED_BYOC);
    Assertions.assertThat(ethereumByocContract).isNull();
  }

  /**
   * Getting EVM config with an empty producer config returns an empty URL for known chains, and
   * null for unknown chains.
   */
  @Test
  void getEmptyEvmConfig() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig = bpConfig;
    ShardedMain.EvmConfig evmConfig = ShardedMain.getEvmConfig(config, "Ethereum");
    Assertions.assertThat(evmConfig.url()).isEqualTo(null);
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);

    evmConfig = ShardedMain.getEvmConfig(config, "UnknownChain");
    Assertions.assertThat(evmConfig).isNull();
  }

  /**
   * If the chain endpoints exist in the new map style for chain configs, it is possible to get
   * EvmConfigs with these endpoints.
   */
  @Test
  void getNewEvmConfigs() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.addChainEndpoint("Ethereum", "exampleEthUrl");
    bpConfig.addChainEndpoint("Polygon", "examplePolygonUrl");
    bpConfig.addChainEndpoint("Unknown chain", "someUrl");
    config.producerConfig = bpConfig;
    ShardedMain.EvmConfig evmConfig = ShardedMain.getEvmConfig(config, "Ethereum");
    Assertions.assertThat(evmConfig.url()).isEqualTo("exampleEthUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);

    evmConfig = ShardedMain.getEvmConfig(config, "Polygon");
    Assertions.assertThat(evmConfig.url()).isEqualTo("examplePolygonUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(20);

    evmConfig = ShardedMain.getEvmConfig(config, "Unknown chain");
    Assertions.assertThat(evmConfig).isNull();
  }

  /**
   * If the chain endpoints exist in the old record field chain configs, it is still possible to
   * retrieve EvmConfigs with these endpoints.
   */
  @Test
  void getOldEvmConfigs() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.ethereumUrl = "exampleEthUrl";
    bpConfig.polygonUrl = "examplePolygonUrl";
    bpConfig.bnbSmartChainUrl = "exampleBnbUrl";
    config.producerConfig = bpConfig;
    ShardedMain.EvmConfig evmConfig = ShardedMain.getEvmConfig(config, "Ethereum");
    Assertions.assertThat(evmConfig.url()).isEqualTo("exampleEthUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);

    evmConfig = ShardedMain.getEvmConfig(config, "Polygon");
    Assertions.assertThat(evmConfig.url()).isEqualTo("examplePolygonUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(20);

    evmConfig = ShardedMain.getEvmConfig(config, "BnbSmartChain");
    Assertions.assertThat(evmConfig.url()).isEqualTo("exampleBnbUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(20);

    // Sepolia is a valid chain, but no config exists for it
    evmConfig = ShardedMain.getEvmConfig(config, "Sepolia");
    Assertions.assertThat(evmConfig.url()).isNull();
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);
  }

  /**
   * If endpoints are found in both the old chain config, i.e. the record fields, and new chain
   * config, the config map, the map is prioritized.
   */
  @Test
  void newEvmConfigsArePrioritizedOverOldConfigs() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.ethereumUrl = "oldEthUrl";
    bpConfig.polygonUrl = "oldPolygonUrl";
    bpConfig.bnbSmartChainUrl = "oldBnbUrl";
    bpConfig.addChainEndpoint("Ethereum", "newEthUrl");
    bpConfig.addChainEndpoint("Polygon", "newPolygonUrl");
    bpConfig.addChainEndpoint("BnbSmartChain", "newBnbUrl");
    config.producerConfig = bpConfig;

    ShardedMain.EvmConfig evmConfig = ShardedMain.getEvmConfig(config, "Ethereum");
    Assertions.assertThat(evmConfig.url()).isEqualTo("newEthUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);

    evmConfig = ShardedMain.getEvmConfig(config, "Polygon");
    Assertions.assertThat(evmConfig.url()).isEqualTo("newPolygonUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(20);

    evmConfig = ShardedMain.getEvmConfig(config, "BnbSmartChain");
    Assertions.assertThat(evmConfig.url()).isEqualTo("newBnbUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(20);
  }

  /** Getting an EvmConfig for the Sepolia 'chain' is supported. */
  @Test
  void sepoliaConfigIsHandled() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.addChainEndpoint("Sepolia", "someSepoliaUrl");
    config.producerConfig = bpConfig;
    ShardedMain.EvmConfig evmConfig = ShardedMain.getEvmConfig(config, "Sepolia");
    Assertions.assertThat(evmConfig.url()).isEqualTo("someSepoliaUrl");
    Assertions.assertThat(evmConfig.blockValidity()).isEqualTo(10);
  }

  @Test
  void startAppAndByocSystemUpdate() {
    ShutdownHook hook = new ShutdownHook();
    AdditionalStatusProviders additionalStatusProviders = new AdditionalStatusProvidersImpl();
    startShardedMain(
        true,
        ShardedMain.TransactionSenderProvider.REST_SENDER,
        additionalStatusProviders,
        hook,
        System.currentTimeMillis());
    Assertions.assertThat(additionalStatusProviders.getProviders().get(0).getName())
        .isEqualTo("NodeStatus");
    Assertions.assertThat(hook.closeables()).hasSize(28);
  }

  @Test
  void createEventPropagationModule() {
    final CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    BlockchainClient client = ShardedMain.createEventPropagationModule(config, List.of("Shard1"));
    Assertions.assertThat(client).isNotNull();
  }

  @Test
  public void createNetworkConfig() throws IOException {
    CompositeNodeConfigDto compositeNodeConfig = new CompositeNodeConfigDto();
    compositeNodeConfig.floodingPort = 1234;
    compositeNodeConfig.knownPeers =
        List.of(Hex.toHexString(new KeyPair(BigInteger.ONE).getPublic().asBytes()) + ":hosty:1111");
    PersistentPeerDatabase peerDatabase = new PersistentPeerDatabase(Map.of(), new File(peerPath));
    NetworkConfig networkConfig =
        ShardedMain.createNetworkConfig(peerDatabase, compositeNodeConfig, 10, false);
    Assertions.assertThat(NetworkConfigHelper.getNodeAddress(networkConfig))
        .isEqualTo(new Address("localhost", 1244));
    peerDatabase =
        new PersistentPeerDatabase(Map.of(), Files.createTempFile("tmpPeers", "").toFile());
    networkConfig = ShardedMain.createNetworkConfig(peerDatabase, compositeNodeConfig, 10, false);
    Assertions.assertThat(NetworkConfigHelper.getKnownNodes(networkConfig).get())
        .isEqualTo(new Address("hosty", 1121));
    BlockProducerConfigDto producerConfig = new BlockProducerConfigDto();
    producerConfig.host = "some-host";
    compositeNodeConfig.producerConfig = producerConfig;
    NetworkConfig networkConfigProducer =
        ShardedMain.createNetworkConfig(peerDatabase, compositeNodeConfig, 10, true);
    Assertions.assertThat(NetworkConfigHelper.getNodeAddress(networkConfigProducer))
        .isEqualTo(new Address("some-host", 1244));
  }

  @Test
  public void updatePeerDatabaseFromNodeConfig() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();
    String addressA = "1.1.1.1:1111";
    String addressB = "2.2.2.2:2222";
    String addressC = "3.3.3.3:3333";
    dto.knownPeers =
        List.of(
            Hex.toHexString(new KeyPair(BigInteger.ONE).getPublic().asBytes()) + ":" + addressA,
            Hex.toHexString(new KeyPair(BigInteger.TWO).getPublic().asBytes()) + ":" + addressB,
            Hex.toHexString(new KeyPair(BigInteger.TEN).getPublic().asBytes()) + ":" + addressC);
    PersistentPeerDatabase peerDatabase = new PersistentPeerDatabase(Map.of(), new File(peerPath));
    ShardedMain.updatePeerDatabaseFromNodeConfig(dto, peerDatabase);
    Assertions.assertThat(peerDatabase.getPeers())
        .contains(
            com.partisiablockchain.demo.byoc.Address.parseAddress(addressA),
            com.partisiablockchain.demo.byoc.Address.parseAddress(addressB),
            com.partisiablockchain.demo.byoc.Address.parseAddress(addressC));
  }

  @Test
  public void runByocOracleServicesTest() throws Exception {
    KeyPair addressKeyPair = new KeyPair(BigInteger.TEN);
    final CompositeNodeConfigDto nodeConfigDto = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfigDto =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfigDto.accountKey = addressKeyPair.getPrivateKey().toString(16);
    bpConfigDto.ethereumUrl = "ethereumUrl";
    bpConfigDto.host = "someHost";
    bpConfigDto.finalizationKey = BigInteger.valueOf(420).toString(16);
    nodeConfigDto.floodingPort = 9010;
    nodeConfigDto.networkKey = BigInteger.valueOf(42).toString(16);
    nodeConfigDto.producerConfig = bpConfigDto;

    ShardedMain.ByocConfig ethereumByocConfig =
        new ShardedMain.ByocConfig(
            "byoc",
            nodeConfigDto.producerConfig.ethereumUrl,
            "Some ethereum address",
            BigInteger.valueOf(10),
            BetanetAddresses.BYOC_INCOMING,
            BetanetAddresses.BYOC_OUTGOING);
    runAndAssertByocServiceShutdownHooks(ethereumByocConfig, nodeConfigDto, 3);

    nodeConfigDto.overrides =
        Map.of(
            "byoc.dispute.enabled",
            Boolean.toString(true),
            "byoc.deposit.enabled",
            Boolean.toString(false));
    runAndAssertByocServiceShutdownHooks(ethereumByocConfig, nodeConfigDto, 3);

    nodeConfigDto.overrides = Map.of("byoc.withdrawal.enabled", Boolean.toString(false));
    bpConfigDto.ethereumUrl = "";
    ethereumByocConfig =
        new ShardedMain.ByocConfig(
            "byoc",
            nodeConfigDto.producerConfig.ethereumUrl,
            "ethereum address",
            BigInteger.valueOf(420),
            BetanetAddresses.BYOC_INCOMING,
            BetanetAddresses.BYOC_OUTGOING);
    runAndAssertByocServiceShutdownHooks(ethereumByocConfig, nodeConfigDto, 0);
  }

  @Test
  public void getPeersFromFile() {
    Map<BlockchainPublicKey, Address> peers = ShardedMain.getPeersFromFile("does not exist");
    Assertions.assertThat(peers).isEqualTo(Map.of());
    Assertions.assertThat(peers.keySet().getClass().getSimpleName()).isEqualTo("");
    peers =
        ExceptionConverter.call(
            () ->
                ShardedMain.getPeersFromFile(
                    ShardedMainTest.class.getResource("peers.txt").toURI().getPath()));
    Assertions.assertThat(peers)
        .isEqualTo(
            Map.of(
                BlockchainPublicKey.fromEncodedEcPoint(
                    Hex.decode(
                        "03b35511d67e63fa6552db740b48aba6d230c21799e65a6647a5cbfc789ef0184b")),
                new Address("FT-BP1", 9000),
                BlockchainPublicKey.fromEncodedEcPoint(
                    Hex.decode(
                        "039e2158f0d7c0d5f26c3791efefa79597654e7a2b2464f52b1ee6c1347769ef57")),
                new Address("FT-BP2", 9100)));
  }

  @Test
  void serializeVotingContractState() {
    String votingStateString =
        "{\"activeCommitteeVotes\":{\"activeCommitteeVotes\":"
            + "[{\"key\":\"000000000000000000000000000000000000000003\",\"value\":\"1\"},"
            + "{\"key\":\"000000000000000000000000000000000000000004\",\"value\":\"1\"},"
            + "{\"key\":\"000000000000000000000000000000000000000005\",\"value\":\"2\"},"
            + "{\"key\":\"000000000000000000000000000000000000000006\",\"value\":\"1\"}]},"
            + "\"bpOrchestrationContract\":\"048e480a033e8aeb67e6ecb657ce9bcdfdb21aa744\","
            + "\"polls\":"
            + "[{\"key\":\"0000000000000000000000000000000000000000000000000000000000000001\","
            + "\"value\":{\"description\":\"A"
            + " description\",\"owner\":\"000000000000000000000000000000000000000001\","
            + "\"pendingUpdate\":"
            + "{\"identifier\":\"0000000000000000000000000000000000000000000000000000000000000002\""
            + ",\"pollUpdateType\":\"ENABLE_FEATURE\","
            + "\"updateRpc\":{\"data\":\"AAAAC0ZFQVRVUkVfS0VZAAAADUZFQVRVUkVfVkFMVUU=\"}}"
            + ",\"result\":\"NO_RESULT\",\"timeoutTimestamp\":\"0\",\"votes\":"
            + "[{\"key\":\"000000000000000000000000000000000000000003\","
            + "\"value\":{\"answer\":true}}]}}]}\n";
    JsonNode votingState =
        ExceptionConverter.call(
            () -> StateObjectMapper.createObjectMapper().readTree(votingStateString),
            "Unable to read JSON string");
    VotingState state = ShardedMain.deserializeVotingContractState(votingState);
    Assertions.assertThat(state.bpOrchestrationContract())
        .isEqualTo(BlockchainAddress.fromString("048e480a033e8aeb67e6ecb657ce9bcdfdb21aa744"));
    Assertions.assertThat(state.polls().values().size()).isEqualTo(1);
    Poll poll = state.polls().values().get(0);
    Assertions.assertThat(poll.votes().values().get(0)).isEqualTo(new Vote(true));
    Assertions.assertThat(poll.pendingUpdate().pollUpdateType())
        .isEqualTo(PollUpdateType.ENABLE_FEATURE);
    Assertions.assertThat(poll.pendingUpdate().updateRpc().getData())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                rpc -> {
                  rpc.writeString("FEATURE_KEY");
                  rpc.writeString("FEATURE_VALUE");
                }));
    Assertions.assertThat(state.activeCommitteeVotes().activeCommitteeVotes())
        .usingRecursiveComparison()
        .isEqualTo(
            AvlTree.create(
                Map.of(
                    BlockchainAddress.fromString("000000000000000000000000000000000000000003"),
                    1L,
                    BlockchainAddress.fromString("000000000000000000000000000000000000000004"),
                    1L,
                    BlockchainAddress.fromString("000000000000000000000000000000000000000005"),
                    2L,
                    BlockchainAddress.fromString("000000000000000000000000000000000000000006"),
                    1L)));
  }

  @Test
  public void validateConfig() throws IOException, URISyntaxException {
    Path genesisPath =
        Path.of("src/test/resources/com/partisiablockchain/server/genesis/genesis_config");
    Assertions.assertThatThrownBy(
            () ->
                ShardedMain.main(
                    new String[] {
                      genesisPath.toString(),
                      new File(ShardedMainTest.class.getResource("genesisIncorrect").toURI())
                          .getAbsolutePath()
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Config in storage does not match config of current image");

    byte[] genesisBytes =
        Files.readAllBytes(
            Path.of(ShardedMainTest.class.getResource("genesisCorrect/genesis_config").toURI()));

    File tmp = Files.createTempDirectory("tmpGenesis").toFile();

    ShardedMain.validateConfig(
        tmp,
        Path.of(ShardedMainTest.class.getResource("genesisCorrect/genesis_config").toURI()),
        (a, b) -> {});
    Assertions.assertThat(genesisBytes).isEqualTo(Files.readAllBytes(tmp.listFiles()[0].toPath()));

    File dataDirectory = new File("src/test/resources/com/partisiablockchain/server/genesis");
    AtomicBoolean atLeastOneThingConsumed = new AtomicBoolean(false);
    ShardedMain.validateConfig(
        dataDirectory, genesisPath, (a, b) -> atLeastOneThingConsumed.set(true));
    Assertions.assertThat(atLeastOneThingConsumed.get()).isTrue();
  }

  @Test
  void createClient() {
    BlockchainTransactionClient client =
        ShardedMain.createClient(new KeyPair(), "http://test.local", List.of());
    Assertions.assertThat(client)
        .hasFieldOrPropertyWithValue("spawnedEventTimeout", Duration.ofDays(7).toMillis());
  }

  @Test
  public void buildBinaryRealEngineConfigTest() {
    CompositeNodeConfigDto dto = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpDto =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    String hostname = "someHostname";
    int port = 10042;
    dto.floodingPort = port;
    bpDto.host = hostname;
    bpDto.accountKey = producer.getPrivateKey().toString(16);
    dto.producerConfig = bpDto;
    Assertions.assertThat(
            ShardedMain.buildBinaryRealEngineConfig(
                    dto, blockchainPublicKey -> null, producer, List.of())
                .myConnectionAddress())
        .isEqualTo(new ConnectionAddress(hostname, port + PersistentPeerDatabase.REAL_PORT_OFFSET));
  }

  @Test
  public void safeRead() {
    Assertions.assertThat(ShardedMain.safeRead(new File("Some file"))).isEqualTo(null);
  }

  @Test
  public void toAddress() {
    Assertions.assertThat(ShardedMain.toAddress("ThisHasLengthOne"))
        .isEqualTo(Address.parseAddress("ThisHasLengthOne:9888"));
    Assertions.assertThat(ShardedMain.toAddress("ThisHasLengthTwo:2000"))
        .isEqualTo(Address.parseAddress("ThisHasLengthTwo:2000"));
  }

  @Test
  public void readPrivateKey() {
    Assertions.assertThat(ShardedMain.readPrivateKey(() -> "")).isEqualTo(Optional.empty());
    Assertions.assertThat(ShardedMain.readPrivateKey(() -> null)).isEqualTo(Optional.empty());
  }

  @Test
  void readAutomaticVotes_malformed() {
    Map<Hash, Boolean> hashes =
        ShardedMain.readAutomaticVotes(
            "/ShardedMainTest_malformedVotes.txt", new CompositeNodeConfigDto());
    Assertions.assertThat(hashes).isEmpty();
    Assertions.assertThat(hashes.keySet().getClass().getSimpleName()).isEqualTo("");
  }

  @Test
  public void readAutomaticVotesNoFile() {
    Map<Hash, Boolean> hashes =
        ShardedMain.readAutomaticVotes("no file", new CompositeNodeConfigDto());
    Assertions.assertThat(hashes).isEmpty();
    Assertions.assertThat(hashes.keySet().getClass().getSimpleName()).isEqualTo("");
  }

  @Test
  void readAutomaticVotes() {
    Map<Hash, Boolean> hashes =
        ShardedMain.readAutomaticVotes("/ShardedMainTest_votes.txt", new CompositeNodeConfigDto());
    Assertions.assertThat(hashes)
        .containsOnlyKeys(
            Hash.fromString("981dee91405a4f60fcfdfd41e88b04a1e5e9023d4c49449228a9f9207bb4ba39"),
            Hash.fromString("f38987b9ceed396fbe0a33041e4f5c41ad0d781d2bea641b4b31d31d4501b5e5"),
            Hash.fromString("fc840cb1012bb77e9a775deaa85749d7753fd17d8a7019782b29e4de0fa53ef6"),
            Hash.fromString("5e5f24e60ebfc597df7355993124ba02bd59965f97691d5209477597cd285f0a"));
    Assertions.assertThat(hashes).containsValue(true).doesNotContainValue(false);

    String voteOverride = "f38987b9ceed396fbe0a33041e4f5c41ad0d781d2bea641b4b31d31d4501b5e5";
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.overrides = Map.of("systemupdate.vote." + voteOverride, "false");
    Map<Hash, Boolean> suppresses =
        ShardedMain.readAutomaticVotes("/ShardedMainTest_votes.txt", config);

    Assertions.assertThat(suppresses)
        .containsEntry(
            Hash.fromString("981dee91405a4f60fcfdfd41e88b04a1e5e9023d4c49449228a9f9207bb4ba39"),
            true)
        .containsEntry(Hash.fromString(voteOverride), false)
        .containsEntry(
            Hash.fromString("fc840cb1012bb77e9a775deaa85749d7753fd17d8a7019782b29e4de0fa53ef6"),
            true)
        .containsEntry(
            Hash.fromString("5e5f24e60ebfc597df7355993124ba02bd59965f97691d5209477597cd285f0a"),
            true);
  }

  @Test
  void determineVotesToCast() {
    BlockchainAddress account =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeInt(998)));
    CommitteeVotes committee =
        createCommittee(List.of(account, randomAccount(), randomAccount(), randomAccount()));
    Hash shouldVoteTrue = randomHash();
    Hash shouldVoteFalse = randomHash();
    Hash alreadyVoted = randomHash();
    Poll voteTruePoll = poll(randomAccount(), PollUpdateType.DEPLOY_CONTRACT, new byte[110]);
    Poll voteFalsePoll = poll(account, PollUpdateType.UPDATE_CONTRACT, new byte[110]);
    Poll alreadyVotedPoll = poll(account, PollUpdateType.DEPLOY_CONTRACT, new byte[111]);
    alreadyVotedPoll =
        new Poll(
            alreadyVotedPoll.owner(),
            alreadyVotedPoll.votes().set(account, new Vote(true)),
            alreadyVotedPoll.pendingUpdate());
    Assertions.assertThat(alreadyVotedPoll.votes().getValue(account).answer()).isTrue();
    Map<Hash, Poll> polls =
        Map.of(
            shouldVoteTrue,
            voteTruePoll,
            shouldVoteFalse,
            voteFalsePoll,
            alreadyVoted,
            alreadyVotedPoll,
            randomHash(),
            poll(account, PollUpdateType.DEPLOY_CONTRACT, new byte[112]));

    StateSerializable votingState =
        new VotingState(
            BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION, AvlTree.create(polls), committee);

    Map<Hash, Boolean> votesToCast =
        ShardedMain.determineVotesToCast(
            Map.of(
                pollHash(voteTruePoll), true,
                pollHash(voteFalsePoll), false,
                pollHash(alreadyVotedPoll), true),
            account,
            StateAccessor.create(votingState));
    Assertions.assertThat(votesToCast)
        .containsExactlyInAnyOrderEntriesOf(Map.of(shouldVoteTrue, true, shouldVoteFalse, false));
    VotingState contractState =
        new VotingState(
            BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
            AvlTree.create(),
            createCommittee(
                List.of(
                    new KeyPair(BigInteger.valueOf(7)).getPublic().createAddress(),
                    new KeyPair(BigInteger.valueOf(9)).getPublic().createAddress(),
                    new KeyPair(BigInteger.valueOf(8)).getPublic().createAddress())));
    Assertions.assertThat(Map.of())
        .isEqualTo(
            ShardedMain.determineVotesToCast(
                Map.of(),
                new KeyPair(BigInteger.TEN).getPublic().createAddress(),
                StateAccessor.create(contractState)));
  }

  @Test
  void votingState() {
    BlockchainAddress address = new KeyPair(BigInteger.valueOf(7)).getPublic().createAddress();
    CommitteeVotes committee =
        createCommittee(
            List.of(
                address,
                new KeyPair(BigInteger.valueOf(9)).getPublic().createAddress(),
                new KeyPair(BigInteger.valueOf(8)).getPublic().createAddress()));
    VotingState contractState =
        new VotingState(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION, AvlTree.create(), committee);
    Assertions.assertThat(contractState.bpOrchestrationContract())
        .isEqualTo(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION);
    Assertions.assertThat(contractState.polls().size()).isEqualTo(0);
    Assertions.assertThat(contractState.activeCommitteeVotes()).isEqualTo(committee);
    Assertions.assertThat(
            contractState.activeCommitteeVotes().activeCommitteeVotes().getValue(address))
        .isEqualTo(1L);
    Poll poll = poll(address, PollUpdateType.DEPLOY_CONTRACT, new byte[69]);
    Assertions.assertThat(poll.owner()).isEqualTo(address);
    Assertions.assertThat(poll.pendingUpdate().pollUpdateType())
        .isEqualTo(PollUpdateType.DEPLOY_CONTRACT);
    Assertions.assertThat(poll.pendingUpdate().updateRpc().getData()).isEqualTo(new byte[69]);
    Assertions.assertThat(poll.votes().size()).isEqualTo(0);
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  void determinePollHash() {
    BlockchainAddress proposer = randomAccount();
    byte[] rpc = new byte[33];
    PollUpdateType type = PollUpdateType.UPDATE_CONTRACT;
    Assertions.assertThat(
            ShardedMain.determinePollHash(StateAccessor.create(poll(proposer, type, rpc))))
        .isEqualTo(
            Hash.create(
                s -> {
                  proposer.write(s);
                  s.writeByte(type.ordinal());
                  s.write(rpc);
                }));
  }

  private Hash pollHash(Poll poll) {
    return ShardedMain.determinePollHash(StateAccessor.create(poll));
  }

  private Poll poll(BlockchainAddress proposer, PollUpdateType type, byte[] rpc) {
    return new Poll(proposer, AvlTree.create(), new PendingUpdate(type, new LargeByteArray(rpc)));
  }

  private BlockchainAddress randomAccount() {
    return BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, randomHash());
  }

  private Hash randomHash() {
    return Hash.create(s -> s.writeLong(random.nextLong()));
  }

  @Test
  void buildEvmOracleConfig() {
    CompositeNodeConfigDto nodeConfig = setupConfig();
    List<EvmOracleChainConfig> oracleConfigs = ShardedMain.buildEvmOracleChainConfigs(nodeConfig);
    Assertions.assertThat(oracleConfigs).hasSize(2);
    Assertions.assertThat(oracleConfigs.get(0))
        .isEqualTo(
            new EvmOracleChainConfig(
                "Ethereum", "someUrl", BigInteger.valueOf(10), BigInteger.valueOf(5000), 900_000L));
    Assertions.assertThat(oracleConfigs.get(1))
        .isEqualTo(
            new EvmOracleChainConfig(
                "Polygon", "polyUrl", BigInteger.valueOf(20), BigInteger.valueOf(5000), 900_000L));

    nodeConfig = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto bpConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    bpConfig.bnbSmartChainUrl = "bnbScUrl";
    bpConfig.ethereumUrl = ""; // Empty url is ignored
    bpConfig.accountKey = producer.getPrivateKey().toString(16);
    nodeConfig.producerConfig = bpConfig;
    nodeConfig.overrides =
        Map.of(
            "evm.oracle.bnbsmartchain.block_validity",
            "75",
            "evm.oracle.bnbsmartchain.max_block_range",
            "250",
            "evm.oracle.bnbsmartchain.check_interval",
            "180000");
    oracleConfigs = ShardedMain.buildEvmOracleChainConfigs(nodeConfig);
    Assertions.assertThat(oracleConfigs).hasSize(1);
    Assertions.assertThat(oracleConfigs.get(0))
        .isEqualTo(
            new EvmOracleChainConfig(
                "BnbSmartChain",
                "bnbScUrl",
                BigInteger.valueOf(75),
                BigInteger.valueOf(250),
                180_000L));
  }

  @Test
  void createEvmPriceOracleConfigs() {
    CompositeNodeConfigDto nodeConfig = setupConfig();
    List<EvmPriceOracleChainConfig> evmPriceOracleConfigs =
        ShardedMain.createEvmPriceOracleConfigs(nodeConfig);
    Assertions.assertThat(evmPriceOracleConfigs).hasSize(2);
    Assertions.assertThat(evmPriceOracleConfigs.get(0))
        .usingRecursiveComparison()
        .isEqualTo(new EvmPriceOracleChainConfig("Ethereum", "someUrl", BigInteger.valueOf(10)));
    Assertions.assertThat(evmPriceOracleConfigs.get(1))
        .usingRecursiveComparison()
        .isEqualTo(new EvmPriceOracleChainConfig("Polygon", "polyUrl", BigInteger.valueOf(20)));
  }

  /** Dummy contract for testing ledger access. */
  public static final class DummyByocOutgoingContract
      extends SysContract<ByocOutgoingContractState> {}

  /** Dummy voting contract. */
  public static final class DummyVotingContract extends SysContract<VotingState> {}

  /** Dummy BpOrchestration contract. */
  public static final class DummyBpoContract extends SysContract<BpOrchestrationContractState> {}

  /** Mock implementation of BYOC orchestrator. */
  public static final class MockByocOrchestrator extends SysContract<MockByocOrchestratorState> {}

  /** Mock implementation of BYOC orchestrator state. */
  @Immutable
  public record MockByocOrchestratorState(AvlTree<String, MockBridge> byocBridges)
      implements StateSerializable {}

  /**
   * Mock implementation of a token bridge embedded in a bridge within the BYOC orchestrator state.
   */
  @Immutable
  public record TokenBridgeMock(
      String chain, BlockchainAddress incoming, BlockchainAddress outgoing)
      implements StateSerializable {}

  /** Mock implementation of a bridge within the BYOC orchestrator state. */
  @Immutable
  public record MockBridge(
      String chain,
      BlockchainAddress incoming,
      BlockchainAddress outgoing,
      BlockchainAddress priceOracle,
      FixedList<TokenBridgeMock> tokenBridges)
      implements StateSerializable {}
}
