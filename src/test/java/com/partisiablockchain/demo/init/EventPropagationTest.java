package com.partisiablockchain.demo.init;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.rosetta.TestValues;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.server.ShardedMainTest;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class EventPropagationTest extends CloseableTest {
  private final KeyPair producer = new KeyPair(BigInteger.ONE);
  private ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> data;
  private RootDirectory rootDirectory;
  private BlockchainClient client;
  private final ServerModule.ServerConfig config = ShardedMainTest.getServerConfig();
  private EventPropagation eventPropagation;

  /**
   * Before each test.
   *
   * @throws IOException exception
   */
  @BeforeEach
  public void setup() throws IOException {
    File root = Files.createTempDirectory("rootDir").toFile();
    data = new ConcurrentHashMap<>();
    rootDirectory = MemoryStorage.createRootDirectory(root, data);
    client = Mockito.mock(BlockchainClient.class);

    eventPropagation = new EventPropagation(client);
  }

  @Test
  public void registerClosable() {
    final ServerModule.ServerConfig config = Mockito.mock(ServerModule.ServerConfig.class);
    AtomicInteger counter = new AtomicInteger(0);
    Mockito.doAnswer(answer -> counter.getAndIncrement())
        .when(config)
        .registerCloseable(Mockito.any(AutoCloseable.class));
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState("Shard1", "Shard1"),
                "Shard1"));
    eventPropagation.create(ledger, config);
    Assertions.assertThat(counter.get()).isEqualTo(1);
  }

  @Test
  public void registerTest() throws InterruptedException {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                null));

    BlockchainLedgerHelper.spawnEventToShard0(ledger);

    eventPropagation.create(ledger, config);
    BlockchainLedgerHelper.appendBlock(ledger);
    Thread.sleep(100);
    Mockito.verify(client).putEvent(Mockito.eq(new ShardId("Shard1")), Mockito.any());
  }

  @Test
  public void shouldNotPropagateEventsForOwnShard() throws InterruptedException {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                "Shard1"));

    SignedTransaction interactWithContractTransaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            ledger,
            TestValues.CONTRACT_ONE,
            TestValues.KEY_PAIR_ONE,
            FunctionUtility.noOpConsumer());
    BlockchainLedgerHelper.appendBlock(ledger, List.of(interactWithContractTransaction), List.of());
    BlockchainLedgerHelper.appendBlock(ledger, List.of(), List.of());
    ExecutedState executedState = ledger.getChainState().getExecutedState();
    Assertions.assertThat(executedState.getExecutionStatus().size()).isEqualTo(2);

    eventPropagation.create(ledger, config);
    BlockchainLedgerHelper.appendBlock(ledger);
    Thread.sleep(300);

    Mockito.verifyNoInteractions(client);
  }

  /** Propagation retries the sending on initial failure. */
  @Test
  void newFinalBlock() throws InterruptedException {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                null));

    BlockchainLedgerHelper.spawnEventToShard0(ledger);

    Mockito.doThrow(RuntimeException.class)
        .when(client)
        .putEvent(Mockito.eq(new ShardId("Shard1")), Mockito.any());

    eventPropagation.create(ledger, config);
    BlockchainLedgerHelper.appendBlock(ledger);
    Thread.sleep(300);

    Mockito.verify(client, Mockito.times(2))
        .putEvent(
            Mockito.any(),
            Mockito.argThat(
                argument ->
                    SafeDataInputStream.readFully(
                                argument.transactionPayload(), FloodableEvent::read)
                            .getExecutableEvent()
                            .getEvent()
                            .getNonce()
                        == 1));
  }

  @Test
  public void scheduleCheck() throws InterruptedException {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState("Shard1", "Shard1"),
                "Shard1"));
    eventPropagation.create(ledger, config);
    FloodableEvent event = createFloodableEvent(ledger);
    final byte[] eventToFail = SafeDataOutputStream.serialize(event::write);
    Mockito.doThrow(RuntimeException.class)
        .doNothing()
        .when(client)
        .putEvent(
            Mockito.any(),
            Mockito.argThat(argument -> Arrays.equals(argument.transactionPayload(), eventToFail)));
    eventPropagation.scheduleCheck(event, 0);
    Thread.sleep(200L); // First sleep
    Thread.sleep(1200L); // Second
    Mockito.verify(client, Mockito.times(2))
        .putEvent(
            Mockito.any(),
            Mockito.argThat(argument -> Arrays.equals(argument.transactionPayload(), eventToFail)));
    Mockito.verify(client, Mockito.times(2)).putEvent(Mockito.any(), Mockito.any());
  }

  @Test
  public void scheduleCheckIsExecutedFalse() throws InterruptedException {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState("Shard1", "Shard1"),
                "Shard1"));
    eventPropagation.create(ledger, config);
    FloodableEvent event = createFloodableEvent(ledger);
    eventPropagation.scheduleCheck(event, 0);
    ExecutedTransaction executedTransaction =
        new ExecutedTransaction(
            null, null, 0, 0, null, null, false, List.of(), false, false, null, null);
    Mockito.when(client.getTransaction(Mockito.any(), Mockito.any()))
        .thenReturn(executedTransaction);
    Thread.sleep(500L); // First sleep
    Mockito.verify(client).getTransaction(Mockito.any(), Mockito.any());
  }

  @Test
  public void pushEventToDestinationTest() {
    ExecutableEvent event = BlockchainLedgerHelper.createEvent("Shard1", "Shard1", 1L);
    FloodableEvent floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent("Shard1", producer, "Shard1", event).get(0);

    assertThat(EventPropagation.pushEvent(client, floodableEvent)).isFalse();
  }

  @Test
  public void pushEventFailureReturnsTrue() {
    ExecutableEvent event = BlockchainLedgerHelper.createEvent("Shard1", "Shard1", 1L);
    FloodableEvent floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent("Shard1", producer, "Shard1", event).get(0);

    Mockito.doThrow(RuntimeException.class).when(client).putEvent(Mockito.any(), Mockito.any());

    assertThat(EventPropagation.pushEvent(client, floodableEvent)).isTrue();
  }

  @Test
  public void isExecuted() {
    BlockchainLedger ledger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                StateHelper::initial,
                "Shard0"));
    ExecutableEvent executableEvent = BlockchainLedgerHelper.createEvent("ShardId", "Shard1", 27);

    StateStorageRaw stateStorage = ledger.getStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            ledger.getChainId(),
            "ShardId",
            executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    Block block =
        new Block(10L, 1L, 0L, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of());
    FloodableEvent event =
        FloodableEvent.create(
            executableEvent,
            new FinalBlock(
                block, SafeDataOutputStream.serialize(producer.sign(block.identifier()))),
            ledger.getStateStorage());
    assertThat(EventPropagation.isExecuted(client, event)).isEqualTo(false);
    ExecutedTransaction executedTransactionDto =
        new ExecutedTransaction(
            null,
            null,
            0,
            0,
            null,
            executableEvent.identifier().toString(),
            false,
            List.of(),
            false,
            false,
            null,
            null);
    Mockito.when(client.getTransaction(Mockito.any(), Mockito.any()))
        .thenReturn(executedTransactionDto);
    assertThat(EventPropagation.isExecuted(client, event)).isEqualTo(true);
    assertThat(EventPropagation.isExecuted(client, null)).isEqualTo(false);
  }

  private FloodableEvent createFloodableEvent(BlockchainLedger ledger) {
    ExecutableEvent executableEvent = BlockchainLedgerHelper.createEvent("Shard1", "Shard1", 2L);
    StateStorageRaw stateStorage = ledger.getStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            ledger.getChainId(),
            "Shard1",
            executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    Block block =
        new Block(10L, 1L, 0L, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of());
    return FloodableEvent.create(
        executableEvent,
        new FinalBlock(block, SafeDataOutputStream.serialize(producer.sign(block.identifier()))),
        ledger.getStateStorage());
  }
}
