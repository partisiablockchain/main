package com.partisiablockchain.demo.init;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.server.ConditionalLogger;
import java.util.function.Consumer;

/** Test. */
public final class ConditionalLoggerCounter implements ConditionalLogger {
  private int count = 0;
  private int timesCalled = 0;

  public int getTimesCalled() {
    return timesCalled;
  }

  public int getCount() {
    return count;
  }

  @Override
  public void handle(Consumer<String> logger, boolean condition, String message) {
    timesCalled++;
    if (condition) {
      count++;
    }
  }
}
