package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/** Tests. */
public final class MemoryStorage implements Storage {

  private final String path;
  private final Map<PathAndHash, byte[]> data;
  private final boolean immutable;

  private static final Map<String, String> paths = new HashMap<>();

  MemoryStorage(Map<PathAndHash, byte[]> data, String path, boolean immutable) {
    String[] split = path.split(Pattern.quote(File.separator), -1);
    paths.put(split[split.length - 1], path);
    this.data = data;
    this.path = path;
    this.immutable = immutable;
  }

  /**
   * Create a root directory using MemoryStorage as underlying storage format.
   *
   * @param root path to directory root
   * @param data memory map to store data in
   * @return a new RootDirectory
   */
  public static RootDirectory createRootDirectory(File root, Map<PathAndHash, byte[]> data) {
    return new RootDirectory(root, (r, i) -> new MemoryStorage(data, r.getPath(), i));
  }

  @Override
  public <T> T read(Hash hash, Function<SafeDataInputStream, T> reader) {
    PathAndHash key = new PathAndHash(path, hash);
    if (data.containsKey(key)) {
      return SafeDataInputStream.deserialize(reader, data.get(key));
    } else {
      return null;
    }
  }

  /**
   * Get path of a file.
   *
   * @param whereInStorage file
   * @return the path
   */
  public static String getPath(String whereInStorage) {
    return paths.get(whereInStorage);
  }

  @Override
  public <T> Stream<T> stream(Function<SafeDataInputStream, T> reader) {
    return data.entrySet().stream()
        .filter(e -> e.getKey().getDataPath().equals(path))
        .map(Map.Entry::getValue)
        .map(bytes -> SafeDataInputStream.deserialize(reader, bytes));
  }

  @Override
  public byte[] readRaw(Hash hash) {
    PathAndHash key = new PathAndHash(path, hash);
    return data.get(key);
  }

  @Override
  public boolean has(Hash hash) {
    PathAndHash key = new PathAndHash(path, hash);
    return data.containsKey(key);
  }

  @Override
  public boolean isEmpty() {
    return data.keySet().stream().noneMatch(key -> key.getDataPath().equals(path));
  }

  @Override
  public void remove(Hash hash) {
    if (immutable) {
      throw new IllegalStateException("Unable to remove files from immutable storage");
    }
    PathAndHash key = new PathAndHash(path, hash);
    data.remove(key);
  }

  @Override
  public void write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    PathAndHash key = new PathAndHash(path, hash);
    if (!immutable || !data.containsKey(key)) {
      byte[] bytes = SafeDataOutputStream.serialize(writer);
      data.put(key, bytes);
    }
  }

  @Override
  public void clear() {
    List<PathAndHash> keys =
        data.keySet().stream().filter(key -> key.getDataPath().equals(path)).toList();
    for (PathAndHash key : keys) {
      data.remove(key);
    }
  }

  /** Tests. */
  public static final class PathAndHash {
    private final String dataPath;
    private final Hash identifier;

    /**
     * Construct path and hash.
     *
     * @param dataPath the path
     * @param identifier the identifier
     */
    public PathAndHash(String dataPath, Hash identifier) {
      this.dataPath = dataPath;
      this.identifier = identifier;
    }

    /**
     * Get the path to which data is bound.
     *
     * @return path to data
     */
    public String getDataPath() {
      return dataPath;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      PathAndHash that = (PathAndHash) o;
      return Objects.equals(dataPath, that.dataPath) && Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
      return Objects.hash(dataPath, identifier);
    }
  }
}
