package com.partisiablockchain.synaps;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.rosetta.TestValues;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.server.BetanetAddresses;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import jakarta.ws.rs.NotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class SynapsResourceTest {

  private StateSerializable state;
  private final SynapsResource resource = new SynapsResource(() -> state);

  @Test
  void getKyc() {
    @Immutable
    record State(AvlTree<BlockchainAddress, String> kycRegistrations)
        implements StateSerializable {}

    state = new State(AvlTree.create(Map.of(TestValues.ACCOUNT_ONE, "Registration")));
    JsonNode registeredKyc = resource.getRegisteredKyc(TestValues.ACCOUNT_ONE.writeAsString());
    Assertions.assertThat(registeredKyc.textValue()).isEqualTo("Registration");

    Assertions.assertThatThrownBy(
            () -> resource.getRegisteredKyc(TestValues.ACCOUNT_TWO.writeAsString()))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("User not registered");
  }

  @Test
  void getKycWithInvalidState() {
    state = null;
    Assertions.assertThatThrownBy(
            () -> resource.getRegisteredKyc(TestValues.ACCOUNT_TWO.writeAsString()))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Synaps contract does not exist");

    state = new StateString("Invalid state type");
    Assertions.assertThatThrownBy(
            () -> resource.getRegisteredKyc(TestValues.ACCOUNT_TWO.writeAsString()))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Synaps contract does not support kyc registrations");
  }

  @Test
  void accessFromLedger() throws IOException {
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(
            Files.createTempDirectory("state").toFile(), new ConcurrentHashMap<>());

    StateString state = new StateString("Value");
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            s ->
                BlockchainLedgerHelper.createContract(
                    BetanetAddresses.SYNAPS_KYC,
                    () -> JarBuilder.buildJar(DummySynapsContract.class),
                    () -> state,
                    s),
            "Shard1");

    SynapsResource.SynapsContractAccess synapsContractAccess =
        SynapsResource.SynapsContractAccess.fromBlockchainLedger(
            ledger, BetanetAddresses.SYNAPS_KYC);
    StateSerializable stateSerializable = synapsContractAccess.get();
    Assertions.assertThat(stateSerializable)
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(state);
  }

  /** Dummy contract for testing ledger access. */
  public static final class DummySynapsContract extends SysContract<StateString> {}
}
