package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.configgenerator.RegisterNodeCli.KybUserInput;
import com.partisiablockchain.configgenerator.Synaps.SynapsResult;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto;
import com.secata.stream.SafeDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import org.bouncycastle.util.encoders.Hex;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class KycTransactionsTest {
  private static final String ACCOUNT_SECRET_KEY = "b".repeat(64);
  private static final BlockchainPublicKey ACCOUNT_PUBLIC_KEY =
      publicKeyFromPrivateKey(ACCOUNT_SECRET_KEY);

  private static final String NETWORK_SECRET_KEY = "a".repeat(64);
  private static final BlockchainPublicKey NETWORK_PUBLIC_KEY =
      publicKeyFromPrivateKey(NETWORK_SECRET_KEY);

  private static final String FINALIZATION_SECRET_KEY = "c".repeat(64);
  private static final BlsPublicKey FINALIZATION_PUBLIC_KEY =
      new BlsKeyPair(new BigInteger(FINALIZATION_SECRET_KEY, 16)).getPublicKey();

  private static final KybUserInput KYC_USER_INPUT = new KybUserInput("website", 208);

  private CompositeNodeConfigDto config;
  private BlockchainContractClientStub client;
  private SynapsResult synapsResult;
  private ByteArrayOutputStream outputStream;
  private SafeDataOutputStream safeOutput;
  private BlockchainAddress address;
  private BlockchainPublicKey publicKey;
  private BlsKeyPair blsKeyPair;

  @BeforeEach
  void init() throws IOException {
    config = new CompositeNodeConfigDto();
    config.networkKey = NETWORK_SECRET_KEY;
    config.producerConfig = new BlockProducerConfigDto();
    var producerConfig = config.producerConfig;
    producerConfig.accountKey = ACCOUNT_SECRET_KEY;
    producerConfig.finalizationKey = FINALIZATION_SECRET_KEY;

    KeyPair keyPair = new KeyPair(new BigInteger(config.producerConfig.accountKey, 16));
    address = keyPair.getPublic().createAddress();
    publicKey = new KeyPair(new BigInteger(config.networkKey, 16)).getPublic();
    blsKeyPair = new BlsKeyPair(new BigInteger(config.producerConfig.finalizationKey, 16));

    client = new BlockchainContractClientStub(true);

    synapsResult = readSynapsResult("synaps_kyc_approved_response.json");

    outputStream = new ByteArrayOutputStream();
    safeOutput = new SafeDataOutputStream(outputStream);
  }

  @Test
  void kycSubmitHasExpectedLength() {
    Transaction result =
        KycTransactions.createKycSubmit(synapsResult, client, address, publicKey, blsKeyPair);
    result.write(safeOutput);
    assertThat(outputStream.size()).isEqualTo(447);
  }

  @Test
  void kycSubmitSendsSubmitInvocation() {
    Transaction result =
        KycTransactions.createKycSubmit(synapsResult, client, address, publicKey, blsKeyPair);
    assertThat(result.getRpc()[0]).isEqualTo((byte) Synaps.Invocation.KYC_SUBMIT);
  }

  @Test
  void kycSubmitSendsNetworkAndFinalizationKeysInTheCorrectOrder() {
    Transaction result =
        KycTransactions.createKycSubmit(synapsResult, client, address, publicKey, blsKeyPair);
    String rpcAsHex = Hex.toHexString(result.getRpc());

    String networkKeyHex = Hex.toHexString(NETWORK_PUBLIC_KEY.asBytes());
    String finalizationKeyHex =
        Hex.toHexString(FINALIZATION_PUBLIC_KEY.getPublicKeyValue().serialize(true));

    assertThat(rpcAsHex).contains(networkKeyHex);
    assertThat(rpcAsHex).contains(finalizationKeyHex);
    assertThat(rpcAsHex.indexOf(networkKeyHex)).isLessThan(rpcAsHex.indexOf(finalizationKeyHex));
  }

  @Test
  void kycSubmitDoesNotSendAccountKey() {
    Transaction result =
        KycTransactions.createKycSubmit(synapsResult, client, address, publicKey, blsKeyPair);
    String rpcAsHex = Hex.toHexString(result.getRpc());

    String accountKeyHex = Hex.toHexString(ACCOUNT_PUBLIC_KEY.asBytes());
    assertThat(rpcAsHex).doesNotContain(accountKeyHex);
  }

  @Test
  void kybApproveSendsWebsiteAndJurisdiction() {
    Transaction result =
        KycTransactions.createKybApprove(
            synapsResult, KYC_USER_INPUT, client, address, publicKey, blsKeyPair);

    String website = KYC_USER_INPUT.website();
    assertThat(result.getRpc())
        .contains(website.getBytes(StandardCharsets.UTF_8))
        .contains((byte) 0, (byte) 0, (byte) 0, (byte) KYC_USER_INPUT.serverJurisdiction())
        .hasSize(437);
  }

  @Test
  void kybApproveSendsNetworkAndFinalizationKeysInTheCorrectOrder() {
    Transaction result =
        KycTransactions.createKybApprove(
            synapsResult, KYC_USER_INPUT, client, address, publicKey, blsKeyPair);
    String rpcAsHex = Hex.toHexString(result.getRpc());

    String networkKeyHex = Hex.toHexString(NETWORK_PUBLIC_KEY.asBytes());
    String finalizationKeyHex =
        Hex.toHexString(FINALIZATION_PUBLIC_KEY.getPublicKeyValue().serialize(true));

    assertThat(rpcAsHex).contains(networkKeyHex);
    assertThat(rpcAsHex).contains(finalizationKeyHex);
    assertThat(rpcAsHex.indexOf(networkKeyHex)).isLessThan(rpcAsHex.indexOf(finalizationKeyHex));
  }

  @Test
  void kybSubmitSendsApproveInvocation() {
    Transaction result =
        KycTransactions.createKybApprove(
            synapsResult, KYC_USER_INPUT, client, address, publicKey, blsKeyPair);
    assertThat(result.getRpc()[0]).isEqualTo((byte) Synaps.Invocation.KYB_APPROVE);
  }

  @NotNull
  private SynapsResult readSynapsResult(String filename) throws IOException {
    byte[] responseBytes = getClass().getResourceAsStream(filename).readAllBytes();
    String responseString = new String(responseBytes, StandardCharsets.UTF_8);
    return Synaps.parseResponse(responseString, RegisterNodeCli.AuthenticationType.KYC);
  }

  private static BlockchainPublicKey publicKeyFromPrivateKey(String secretKey) {
    return new KeyPair(new BigInteger(secretKey, 16)).getPublic();
  }
}
