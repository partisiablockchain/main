package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import java.math.BigInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class ProofOfPossessionTest {
  private static final String ACCOUNT_SECRET_KEY = "b".repeat(64);

  private static final String NETWORK_SECRET_KEY = "a".repeat(64);

  private static final String FINALIZATION_SECRET_KEY = "c".repeat(64);

  private BlockchainContractClientStub client;

  @BeforeEach
  void init() {
    client = new BlockchainContractClientStub(true);
  }

  /** getBpOrchestrationSessionId returns the session id of the client. */
  @Test
  void session_id_matches_reference() {
    int result = ProofOfPossession.getBpOrchestrationSessionId(client);
    assertThat(result).isEqualTo(5354);
  }

  /**
   * createProofOfPossessionHash hashes blockchain client, node blockchain address, node network
   * public key and node BLS public key.
   */
  @Test
  void proof_of_possession_matches_reference() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.networkKey = NETWORK_SECRET_KEY;
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    var producerConfig = config.producerConfig;
    producerConfig.accountKey = ACCOUNT_SECRET_KEY;
    producerConfig.finalizationKey = FINALIZATION_SECRET_KEY;

    String accountKey = config.producerConfig.accountKey;
    BlockchainAddress address =
        new KeyPair(new BigInteger(accountKey, 16)).getPublic().createAddress();
    BlockchainPublicKey publicKey = new KeyPair(new BigInteger(config.networkKey, 16)).getPublic();
    BlsKeyPair blsKeyPair =
        new BlsKeyPair(new BigInteger(config.producerConfig.finalizationKey, 16));
    Hash result =
        ProofOfPossession.createProofOfPossessionHash(
            client, address, publicKey, blsKeyPair.getPublicKey());
    assertThat(result.toString())
        .isEqualTo("fb0cbcacf3e9e7af19d46f1c71814f88ceb48ca9138fb085c2fbe807bc1a427b");
  }
}
