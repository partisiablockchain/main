package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

final class RegisterNodeCliTest {

  private @TempDir Path temporaryOutputDir;
  private Path configLocation;
  private String[] args;
  private InputStream inputStream;
  private ByteArrayOutputStream outputStream;
  private PrintStream printStream;
  private BlockchainContractClientStub blockchainClient;
  private Client client;
  private Web3Validator.Web3ServiceProvider web3ServiceProvider;

  @BeforeEach
  void setup() throws IOException {
    inputStream = InputStream.nullInputStream();
    outputStream = new ByteArrayOutputStream();
    printStream = new PrintStream(outputStream);
    blockchainClient = new BlockchainContractClientStub(true);
    configLocation = temporaryOutputDir.resolve("config.json");
    createTestConfig(configLocation);
    String[] tmpArgs = {configLocation.toString()};
    args = tmpArgs;

    client = createMockClient(dummySynapsResponse());
    web3ServiceProvider = maybeUri -> Web3ValidatorTest.createService(System.currentTimeMillis());
  }

  private void run() throws IOException {
    RegisterNodeCli.run(
        args, inputStream, printStream, client, blockchainClient, web3ServiceProvider);
  }

  private void run(String[] args) throws IOException {
    this.args = args;
    run();
  }

  static void createTestConfig(Path configLocation) throws IOException {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CompositeNodeConfigDto.BlockProducerConfigDto producerConfig =
        new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.networkKey = "7720603dca34895d0dc22e15779ae32913501b04609159bf7a697b0b0615142b";
    config.producerConfig = producerConfig;
    producerConfig.accountKey = "7720603dca34895d0dc22e15779ae32913501b04609159bf7a697b0b0615142b";
    producerConfig.finalizationKey =
        "7720603dca34895d0dc22e15779ae32913501b04609159bf7a697b0b0615142b";
    CliConfigGeneratorMain.writeConfig(config, configLocation);
  }

  private static Client createMockClient(String response) {
    Client client = Mockito.mock(Client.class);

    Invocation.Builder builder = Mockito.mock(Invocation.Builder.class);
    when(builder.get(String.class)).thenReturn(response);
    when(builder.header(any(String.class), any(Object.class))).thenReturn(builder);

    WebTarget webTarget = Mockito.mock(WebTarget.class);
    when(webTarget.request()).thenReturn(builder);

    when(client.target(any(String.class))).thenReturn(webTarget);

    return client;
  }

  static String dummySynapsResponse() {
    InputStream response =
        RegisterNodeCliTest.class.getResourceAsStream("synaps_kyc_approved_response.json");
    try {
      return new String(response.readAllBytes(), StandardCharsets.UTF_8);
    } catch (IOException e) {
      return "";
    }
  }

  static String dummySynapsResponseWithWrongAddress() {
    String response = dummySynapsResponse();
    return response.replace(
        "00ab145259f96c2c8b980434a3cd4046c323598845", "0040abcddd8fad1e8e4d185f1952c58e63ca50abe9");
  }

  @Test
  void mainMethod() {
    String[] args = {temporaryOutputDir.toString()};

    assertThatThrownBy(() -> RegisterNodeCli.main(args)).isInstanceOf(FileNotFoundException.class);
  }

  @Test
  void mainMethodWithoutArgs() throws IOException {
    String[] args = {};
    RegisterNodeCli.main(args);
  }

  @Test
  void noArguments() throws IOException {
    String[] args = {};

    run(args);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains(RegisterNodeCli.HELP_MESSAGE);
  }

  @Test
  void helpMessage() throws IOException {
    String[] args = {"--help"};

    run(args);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains(RegisterNodeCli.HELP_MESSAGE);
  }

  @Test
  void missingConfigFile() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("missingConfigFile.json");
    String[] args = {configLocation.toString()};

    run(args);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Configuration file does not exist.");
  }

  @Test
  void missingProducerConfig() throws IOException {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CliConfigGeneratorMain.writeConfig(config, configLocation);

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("missing producer configuration.");
  }

  @Test
  void authenticationMessages() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYC", "01234567-89abcdef-01234567-89abcdef", "yes");

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Your KYC information is:")
        .contains("Name:    Company name")
        .contains("City:    Company city")
        .contains("Address: Company address")
        .contains("Country: Denmark");
  }

  @Test
  void errorInKyc() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYB", "01234567-89abcdef-01234567-89abcdef", "yes");
    client = createMockClient(SynapsTest.createJsonWithError("MESSAGE"));

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(10)
        .contains("KYB service")
        .contains("MESSAGE");
  }

  @Test
  void incompatibleBlockchainAddresses() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYB", "01234567-89abcdef-01234567-89abcdef", "yes");
    client = createMockClient(dummySynapsResponseWithWrongAddress());

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("0040abcddd8fad1e8e4d185f1952c58e63ca50abe9")
        .contains("00ab145259f96c2c8b980434a3cd4046c323598845");
  }

  @Test
  void declineSendTransaction() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYB", "01234567-89abcdef-01234567-89abcdef", "no");

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(20)
        .contains("Registration process has been stopped");
  }

  @Test
  void failedTransactionMain() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYC", "01234567-89abcdef-01234567-89abcdef", "yes");
    blockchainClient.eventsSucceed = false;

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Your KYC transaction failed")
        .contains("See transaction details at:")
        .contains("0000000000000000000000000000000000000000100000000000000000000000");
  }

  @Test
  void failedTransactionEvents() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYC", "01234567-89abcdef-01234567-89abcdef", "yes");
    blockchainClient.eventsSucceed = false;

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("failed")
        .contains("0000000000000000000000000000000000000000100000000000000000000000");
  }

  @Test
  void sendSubmitTransaction() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYC", "01234567-89abcdef-01234567-89abcdef", "yes");

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("succeeded")
        .contains("0000000000000000000000000000000000000000100000000000000000000000");
  }

  @Test
  void sendKyc() throws IOException {
    inputStream =
        CliUserInputTest.fromLines(
            "KYB", "01234567-89ab-cdef-01234-56789abcdef", "yes", "https://example.com", "Denmark");

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Waiting")
        .contains("succeeded")
        .contains("0000000000000000000000000000000000000000100000000000000000000000");
  }

  @Test
  void runPrintsIntroMessage() throws IOException {
    inputStream = CliUserInputTest.fromLines("KYC", "01234567-89abcdef-01234567-89abcdef", "yes");

    run();

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Partisia Blockchain Node Registration Utility");
  }

  @Test
  void printIntroMessage() {
    TerminalPrinter terminalPrinter = new TerminalPrinter(80, printStream);

    RegisterNodeCli.printIntroMessage(terminalPrinter);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(7)
        .contains("Partisia Blockchain Node Registration Utility");
  }

  @Test
  void printExitMessageFail() {
    TerminalPrinter terminalPrinter = new TerminalPrinter(80, printStream);

    String identifier = "IDENTIFIER FAIL";
    RegisterNodeCli.printExitMessage(
        terminalPrinter,
        new Transactions.TransactionResult(identifier, false),
        RegisterNodeCli.AuthenticationType.KYC);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(5)
        .contains(identifier)
        .contains("KYC")
        .contains("failed");
  }

  @Test
  void printExitMessageSuccess() {
    TerminalPrinter terminalPrinter = new TerminalPrinter(80, printStream);

    String identifier = "IDENTIFIER SUCCESS";
    RegisterNodeCli.printExitMessage(
        terminalPrinter,
        new Transactions.TransactionResult(identifier, true),
        RegisterNodeCli.AuthenticationType.KYC);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(5)
        .contains(identifier)
        .contains("KYC")
        .contains("succeeded");
  }

  @Test
  void printAuthenticatedMessage() throws IOException {
    Synaps.AuthenticationData authInfo = new Synaps.AuthenticationData();
    authInfo.name = "My Name";
    authInfo.city = "Block City";
    authInfo.address = "2nd street";
    authInfo.countryNumeric = 204;

    TerminalPrinter terminalPrinter = new TerminalPrinter(80, printStream);

    RegisterNodeCli.AuthenticationType authType = RegisterNodeCli.AuthenticationType.KYC;
    RegisterNodeCli.printAuthenticatedMessage(terminalPrinter, authInfo, authType);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(7)
        .contains(authInfo.name)
        .contains(authInfo.city)
        .contains(authInfo.address)
        .contains("Benin");
  }

  @Test
  void printMismatchedBlockchainAddresses() {
    Synaps.AuthenticationData authInfo = new Synaps.AuthenticationData();
    authInfo.blockchainAddress =
        new KeyPair(BigInteger.valueOf(123)).getPublic().createAddress().writeAsString();

    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig.accountKey = "aa";

    TerminalPrinter terminalPrinter = new TerminalPrinter(80, printStream);

    BlockchainPublicKey publicKey =
        new KeyPair(new BigInteger(config.producerConfig.accountKey, 16)).getPublic();
    RegisterNodeCli.printMismatchedBlockchainAddresses(
        publicKey.createAddress().writeAsString(),
        authInfo,
        terminalPrinter,
        RegisterNodeCli.AuthenticationType.KYC);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(4)
        .contains("000e0b42a53540a8fab1edb9c4211aedeffa7aeed2")
        .contains("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed");
  }

  @Test
  void authenticationType() {
    assertThat(RegisterNodeCli.AuthenticationType.KYB.getUrl()).endsWith("kyb");
    assertThat(RegisterNodeCli.AuthenticationType.KYC.getUrl()).endsWith("kyc");
  }
}
