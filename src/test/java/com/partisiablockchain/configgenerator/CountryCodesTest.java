package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.configgenerator.CountryCodes.Country;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class CountryCodesTest {

  private CountryCodes countryCodes;

  @BeforeEach
  void setup() throws IOException {
    countryCodes = new CountryCodes();
  }

  @Test
  void enterUnknownCountryCode() {
    String result = countryCodes.nameFromCode(0);
    assertThat(result).isEqualTo("Unknown country");
  }

  @Test
  void testFindNearestMatch() {
    Country result = countryCodes.findNearestMatch("Danmark");
    assertThat(result.name()).isEqualTo("Denmark");
  }

  @Test
  void levenshteinDistance() {
    CountryCodes.LevenshteinDistance distance = new CountryCodes.LevenshteinDistance();
    assertThat(distance.compute("", "1234")).isEqualTo(4);
    assertThat(distance.compute("", "")).isEqualTo(0);

    assertThat(distance.compute("1234", "1")).isEqualTo(3);
    assertThat(distance.compute("1234", "12")).isEqualTo(2);
    assertThat(distance.compute("1234", "123")).isEqualTo(1);
    assertThat(distance.compute("1234", "")).isEqualTo(4);

    assertThat(distance.compute("Something", "somethin")).isEqualTo(2);
  }
}
