package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.web3j.protocol.Service;
import org.web3j.protocol.Web3jService;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;

final class Web3ValidatorTest {

  @Test
  void valid() {
    var service = createService(System.currentTimeMillis() / 1000 - 10);
    assertThat(Web3Validator.validate(maybeUri -> service, "")).isTrue();
  }

  @Test
  void expired() {
    var service = createService(System.currentTimeMillis() / 1000 - 3661);
    assertThat(Web3Validator.validate(maybeUri -> service, "")).isFalse();
  }

  @Test
  void almostExpired() {
    var service = createService(System.currentTimeMillis() / 1000 - 3601);
    assertThat(Web3Validator.validate(maybeUri -> service, "")).isTrue();
  }

  @Test
  void exception() {
    Web3ServiceStub service = new Web3ServiceStub();
    assertThat(Web3Validator.validate(maybeUri -> service, "")).isFalse();
  }

  static Web3jService createService(long timestamp) {
    var service = new Web3ServiceStub();

    var blockNumber = new EthBlockNumber();
    blockNumber.setResult(String.valueOf(32432543543L));
    service.addResponse(EthBlockNumber.class, blockNumber);

    EthBlock block = new EthBlock();
    var ethBlock = new EthBlock.Block();
    ethBlock.setTimestamp(String.valueOf(timestamp));
    block.setResult(ethBlock);
    service.addResponse(EthBlock.class, block);
    return service;
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private static final class Web3ServiceStub extends Service {

    private final Map<Class<?>, ?> responses = new HashMap<>();

    public Web3ServiceStub() {
      super(false);
    }

    private <T> void addResponse(Class<T> type, T object) {
      ((Map) responses).put(type, object);
    }

    @Override
    protected InputStream performIO(String payload) {
      return null;
    }

    @Override
    public <T extends Response> T send(Request request, Class<T> responseType) {
      return (T) responses.get(responseType);
    }

    @Override
    public void close() {}
  }
}
