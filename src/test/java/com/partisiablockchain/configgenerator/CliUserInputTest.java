package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.server.CompositeNodeConfigDto;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class CliUserInputTest {

  private ByteArrayOutputStream outputStream;
  private PrintStream printStream;
  private Web3Validator.Web3ServiceProvider web3ServiceProvider;

  @BeforeEach
  void setup() {
    outputStream = new ByteArrayOutputStream();
    printStream = new PrintStream(outputStream);
    web3ServiceProvider = maybeUri -> Web3ValidatorTest.createService(System.currentTimeMillis());
  }

  static ByteArrayInputStream fromLines(String... lines) {
    byte[] streamContent = (String.join("\n", lines) + "\n").getBytes(StandardCharsets.UTF_8);
    return new ByteArrayInputStream(streamContent);
  }

  CliUserInput createUserInput(String... lines) {
    ByteArrayInputStream inputStream = fromLines(lines);
    return new CliUserInput(inputStream, printStream, web3ServiceProvider);
  }

  String getOutput() {
    return outputStream.toString(StandardCharsets.UTF_8);
  }

  @Test
  void testAskForString() {
    CliUserInput userInput = createUserInput("inputString");
    String returnedValue = userInput.askForString("question", null);
    assertThat(returnedValue).isEqualTo("inputString");
  }

  @Test
  void testAskForValidatedList() {
    CliUserInput userInput = createUserInput("b,a", "a,a");
    Pattern regex = Pattern.compile("a+");
    List<String> result =
        userInput.askForValidatedList(
            "q",
            null,
            input -> {
              Matcher matcher = regex.matcher(input);
              return matcher.find();
            },
            "some error message");

    String[] expected = {"a", "a"};

    assertThat(result.toArray()).isEqualTo(expected);
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("some error message");
  }

  @Test
  void testForItemNotInList() {
    CliUserInput userInput = createUserInput("c", "a");
    String[] items = {"a", "b"};

    String result = userInput.askForItemInList("q", items);

    assertThat(result).isEqualTo("a");
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("Please enter one of: a, b");
  }

  @Test
  void askForHexEncodedKeyPrintsOutput() {
    CliUserInput userInput = createUserInput("b", "a".repeat(64), "yes");

    userInput.askForHexEncodedPrivateKey("question", null);

    assertThat(getOutput())
        .hasLineCount(4)
        .contains("Please enter a hex encoded key with length 64")
        .contains("The key corresponds to this address");
  }

  @Test
  void askForStringReturnsInput() {
    CliUserInput userInput = createUserInput("not empty");

    String result = userInput.askForString("q");

    assertThat(result).isEqualTo("not empty");
  }

  @Test
  void testAskForCountryWithName() throws IOException {
    CliUserInput userInput = createUserInput("Denmark");

    int countryCode = userInput.askForCountry("q");

    assertThat(countryCode).isEqualTo(208);
  }

  @Test
  void testAskForCountryWithCode() throws IOException {
    CliUserInput userInput = createUserInput("840");

    int countryCode = userInput.askForCountry("q");

    assertThat(countryCode).isEqualTo(840);
  }

  @Test
  void testAskForCountryWithValidation() throws IOException {
    CliUserInput userInput = createUserInput("Danmark", "y");

    int countryCode = userInput.askForCountry("q");

    assertThat(countryCode).isEqualTo(208);
  }

  @Test
  void testAskForCountryCodeWithInvalidCode() throws IOException {
    CliUserInput userInput = createUserInput("9999", "n", "USA");

    int countryCode = userInput.askForCountry("q");

    assertThat(countryCode).isEqualTo(840);
  }

  @Test
  void testAskForCountryOutput() throws IOException {
    CliUserInput userInput = createUserInput("Danmark", "n", "United Kingdom");

    userInput.askForCountry("q");

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Could not find matching country name.")
        .contains("The nearest was Denmark (208)")
        .contains("Is that correct")
        .contains("Please enter a valid country code or name");
  }

  @Test
  void testAskForValidatedString() {
    CliUserInput userInput = createUserInput("227", "abcdef");
    Pattern validationPattern = Pattern.compile("abcdef");

    String output =
        userInput.askForValidatedString(
            "q",
            null,
            input -> {
              Matcher matcher = validationPattern.matcher(input);
              return matcher.find();
            },
            "Enter abcdef");

    assertThat(output).isEqualTo("abcdef");
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("q: Enter abcdef");
  }

  @Test
  void testAskWithDefaultValue() {
    CliUserInput userInput = createUserInput("\n");
    String value = userInput.askForString("question", "something_else");
    assertThat(value).isEqualTo("something_else");
  }

  @Test
  void testBlankInput() {
    CliUserInput userInput = createUserInput("", "something");

    String value = userInput.askForString("q", null);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("q:")
        .contains("can't be blank");
    assertThat(value).isEqualTo("something");
  }

  @Test
  void testPrintQuestion() {
    ByteArrayInputStream input = fromLines("input");

    CliUserInput userInput = new CliUserInput(input, printStream, web3ServiceProvider);
    userInput.askForString("question");

    assertThat(outputStream.toString(StandardCharsets.UTF_8)).isEqualTo("question: ");
  }

  @Test
  void testCreateNormalConfig() {
    String peer =
        "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe169eb:example.com:80";
    ByteArrayInputStream input = fromLines("n", peer);

    CompositeNodeConfigDto config =
        CliUserInput.createConfigFromInput(input, printStream, web3ServiceProvider);

    assertThat(config.knownPeers).containsExactly(peer);
    assertThat(config.restPort).isEqualTo(8080);
    assertThat(config.floodingPort).isEqualTo(9888);
  }

  @Test
  void promptInputTrueDefaultPrintsYes() {
    CliUserInput userInput = createUserInput();

    userInput.promptInput("question", Optional.of(true));

    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("question [current: yes]: ");
  }

  @Test
  void promptInputFalseDefaultPrintsNo() {
    CliUserInput userInput = createUserInput();

    userInput.promptInput("question", Optional.of(false));

    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("question [current: no]: ");
  }

  @Test
  void promptPrintsDefaultValue() {
    CliUserInput userInput = createUserInput();

    userInput.promptInput("question", Optional.of("some default value"));

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("question [current: some default value]: ");
  }

  @Test
  void createNewBlsKeyReturnsValidBlsKey() {
    String privateKey = CliUserInput.createNewBlsKey(null);
    if (privateKey.length() == 63) {
      privateKey = "0" + privateKey;
    }

    boolean isValid = CliInputValidation.validateHexKey(privateKey);
    assertThat(isValid).isTrue();
  }

  @Test
  void backupNagScreenHasOutput() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.networkKey = "networkKey";
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig.accountKey = "accountKey";
    config.producerConfig.finalizationKey = "finalizationKey";
    CliUserInput userInput = createUserInput("yes");
    TerminalPrinter terminal = new TerminalPrinter(80, printStream);

    CliUserInput.printBackupNagScreen(terminal, config, userInput);

    assertThat(getOutput())
        .hasLineCount(19)
        .contains("IMPORTANT")
        .contains("Please confirm that you have a secure, offline backup of the above");
  }

  @Test
  void printSummaryHasOutput() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.floodingPort = 9888;
    config.networkKey = "c".repeat(64);
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    String[] knownPeers = {"peer1", "peer2"};
    config.knownPeers = List.of(knownPeers);
    var producerConfig = config.producerConfig;
    producerConfig.host = "localhost";
    producerConfig.accountKey = "a".repeat(64);
    producerConfig.finalizationKey = "b".repeat(64);
    producerConfig.ethereumUrl = "https://example.com/eth";
    producerConfig.polygonUrl = "https://example.com/eth";
    producerConfig.bnbSmartChainUrl = "https://example.com/eth";
    producerConfig.addChainEndpoint("Ethereum", "newEthUrl");
    producerConfig.addChainEndpoint("Polygon", "https://example.com/eth");
    producerConfig.addChainEndpoint("BnbSmartChain", "https://example.com/eth");
    TerminalPrinter terminal = new TerminalPrinter(80, printStream);

    CliUserInput.printSummary(terminal, config);

    assertThat(getOutput())
        .hasLineCount(14)
        .contains("Congratulations! You have configured a block producing node.");
  }

  @Test
  void summaryShowsReaderNode() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    String[] knownPeers = {"peer1"};
    config.floodingPort = 9888;
    config.networkKey = "c".repeat(64);
    config.knownPeers = List.of(knownPeers);
    TerminalPrinter terminal = new TerminalPrinter(80, printStream);

    CliUserInput.printSummary(terminal, config);

    assertThat(getOutput()).contains("Congratulations! You have configured a reader node.");
  }

  @Test
  void askForSessionIdReturnsInput() {
    CliUserInput userInput = createUserInput("unique session id");

    String result = userInput.askForSessionId();

    assertThat(result).isEqualTo("unique session id");
  }

  @Test
  void askForSendTransactionOutput() {
    CliUserInput userInput = createUserInput("yes");

    userInput.sendTransactionConfirmation("account_key_address");

    assertThat(getOutput()).hasLineCount(5).contains("account_key_address");
  }

  @Test
  void setDefaultPortsDontChangePorts() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.restPort = 999;
    config.floodingPort = 888;

    CliUserInput.setDefaultPorts(config);

    assertThat(config.restPort).isEqualTo(999);
    assertThat(config.floodingPort).isEqualTo(888);
  }

  @Test
  void validUrl() {
    String expected = "https://example.com";
    CliUserInput io = createUserInput(expected);
    String actual = io.askForUrl("URL", null);
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void invalidUrl() {
    CliUserInput io = createUserInput("not_url");
    assertThatThrownBy(() -> io.askForUrl("URL", null)).isInstanceOf(NoSuchElementException.class);
  }

  @Test
  void validEthUrl() {
    String expected = "https://example.com";
    CliUserInput io = createUserInput(expected);
    String actual = io.askForWeb3Url("URL", null);
    assertThat(actual).isEqualTo(expected);
  }

  @Test
  void invalidEthUrl() {
    this.web3ServiceProvider = maybeUri -> Web3ValidatorTest.createService(0);
    CliUserInput io = createUserInput("https://example.com");
    assertThatThrownBy(() -> io.askForWeb3Url("URL", null))
        .isInstanceOf(NoSuchElementException.class);
  }
}
