package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainContractClient;
import java.lang.reflect.Field;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class LocalHostBlockchainClientProviderTest {
  /** Localhost client provider provides a blockchain client on localhost with specified port. */
  @Test
  void createBlockchainContractClientIsBlockchainClient()
      throws IllegalAccessException, NoSuchFieldException {
    int port = 8080;
    BlockchainContractClient client =
        RegisterAsActiveCli.BlockchainClientProvider.localhostBlockchainClientProvider.createClient(
            port);
    // client.getBaseUrl
    Field baseUrl = client.getClass().getDeclaredField("baseUrl");
    baseUrl.setAccessible(true);
    Assertions.assertThat(baseUrl.get(client)).isEqualTo("http://localhost:8080");
  }
}
