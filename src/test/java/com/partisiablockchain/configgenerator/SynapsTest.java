package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

final class SynapsTest {

  @Test
  void approvedJsonParsesAndHasExpectedData() throws IOException {
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(
            "synaps_kyc_approved_response.json", RegisterNodeCli.AuthenticationType.KYC);

    Synaps.AuthenticationData authenticationData = synapsResult.authenticationData();
    assertThat(authenticationData.name).isEqualTo("Company name");
    assertThat(authenticationData.city).isEqualTo("Company city");
    assertThat(authenticationData.address).isEqualTo("Company address");
    assertThat(authenticationData.countryNumeric).isEqualTo(208);
    assertThat(authenticationData.blockchainAddress)
        .isEqualTo("00ab145259f96c2c8b980434a3cd4046c323598845");
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "synaps_kyc_rejected_response.json",
        "synaps_kyb_approved_response.json",
        "synaps_kyc_invalid_signature.json"
      })
  void jsonWithoutApprovedStatusParsesAsAnError(String filename) throws IOException {
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYC);

    assertThat(synapsResult.hasError()).isTrue();
    assertThat(synapsResult.error())
        .isEqualTo(
            "The KYC was not approved, check that the on-chain information is consistent with the"
                + " information sent to the KYC provider.");
  }

  @Test
  void erroneousJsonHasExpectedErrorMessage() throws IOException {
    var message = "Yes";

    Synaps.SynapsResult synapsResult =
        Synaps.parseResponse(createJsonWithError(message), RegisterNodeCli.AuthenticationType.KYC);
    assertThat(synapsResult.error()).isEqualTo(message);
  }

  @Test
  void jsonWithoutApprovedStatusParsesAsApprovedWhenUsingKybAuthentication() throws IOException {
    String filename = "synaps_kyb_approved_response.json";
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYB);

    assertThat(synapsResult.hasError()).isFalse();
  }

  @Test
  void jsonWithApprovedStatusParsesAsApprovedWhenUsingKycAuthentication() throws IOException {
    String filename = "synaps_kyc_approved_response.json";
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYC);

    assertThat(synapsResult.hasError()).isFalse();
  }

  @Test
  void jsonWithErrorParsesAsAnErrorWhenUsingKybAuthentication() throws IOException {
    String filename = "synaps_kyb_rejected_response.json";
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYB);

    assertThat(synapsResult.hasError()).isTrue();
    assertThat(synapsResult.error()).isEqualTo("Corporate not verified");
  }

  @Test
  void jsonWithStatusRejectedParsesAsRejectedWhenUsingKycAuthentication() throws IOException {
    String filename = "synaps_kyc_rejected_response.json";
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYC);

    assertThat(synapsResult.hasError()).isTrue();
    assertThat(synapsResult.error())
        .isEqualTo(
            "The KYC was not approved, check that the on-chain information is consistent with the"
                + " information sent to the KYC provider.");
  }

  @Test
  void jsonWithInvalidSignatureParsesAsAnErrorWhenUsingKybAuthentication() throws IOException {
    String filename = "synaps_kyb_invalid_signature.json";
    Synaps.SynapsResult synapsResult =
        readSynapsResultFromFile(filename, RegisterNodeCli.AuthenticationType.KYB);

    assertThat(synapsResult.hasError()).isTrue();
    assertThat(synapsResult.error())
        .isEqualTo(
            "The KYB was not approved, check the information submitted to the KYB provider.");
  }

  static String createJsonWithError(String error) {
    return "{\"error\": \"%s\"}".formatted(error);
  }

  static Synaps.SynapsResult readSynapsResultFromFile(
      String filename, RegisterNodeCli.AuthenticationType authType) throws IOException {
    String json = readResourceAsString(filename);
    return Synaps.parseResponse(json, authType);
  }

  private static String readResourceAsString(String filename) throws IOException {
    byte[] bytes = SynapsTest.class.getResourceAsStream(filename).readAllBytes();
    return new String(bytes, StandardCharsets.UTF_8);
  }
}
