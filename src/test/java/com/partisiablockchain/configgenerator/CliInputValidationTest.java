package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

final class CliInputValidationTest {

  @Test
  void invalidHost() {
    assertThat(CliInputValidation.validateHost("__a!-aSDSA2CV./<")).isFalse();
  }

  @Test
  void validHost() {
    assertThat(CliInputValidation.validateHost("localhost")).isTrue();
  }

  @Test
  void invalidUrl() {
    assertThat(CliInputValidation.validateUrl("http://")).isFalse();
    assertThat(CliInputValidation.validateUrl(null)).isFalse();
    assertThat(CliInputValidation.validateUrl("")).isFalse();
  }

  @Test
  void validUrl() {
    assertThat(CliInputValidation.validateUrl("http://localhost")).isTrue();
  }

  @Test
  void invalidKey() {
    assertThat(CliInputValidation.validateHexKey("")).isFalse();
    assertThat(CliInputValidation.validateHexKey("1".repeat(63))).isFalse();
    assertThat(CliInputValidation.validateHexKey("1".repeat(65))).isFalse();
    assertThat(CliInputValidation.validateHexKey("x".repeat(64))).isFalse();

    assertThatThrownBy(() -> CliInputValidation.validateHexKey(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  void validKey() {
    assertThat(CliInputValidation.validateHexKey("f".repeat(64))).isTrue();
  }

  @Test
  void alwaysValid() {
    var validator = CliInputValidation.alwaysValid();
    assertThat(validator.validate("")).isTrue();
    assertThat(validator.validate(null)).isTrue();
  }
}
