package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

final class TextFunctionsTest {

  @Test
  void alignmentLengths() {
    List<String> strings = List.of("text", "notmod2");
    for (String textToAlign : strings) {
      for (int i = 0; i < 25; i++) {
        int size = i + textToAlign.length();

        for (TextAlignment alignment : TextAlignment.values()) {
          String aligned = TextFunctions.alignText(textToAlign, alignment, size);
          assertThat(aligned).hasSize(size);
        }
      }
    }
  }

  @Test
  void splitIntoLinesTextSmallerThanMax() {
    List<String> lines = TextFunctions.splitIntoLines("long text", 42);
    assertThat(lines).containsExactly("long text");
  }

  @Test
  void splitIntoLines() {
    List<String> lines = TextFunctions.splitIntoLines("long text", 4);
    assertThat(lines).containsExactly("long", "text");
  }

  @Test
  void splitIntoLinesBoundary1() {
    assertThat(TextFunctions.splitIntoLines("aaa", 4)).containsExactly("aaa");
    assertThat(TextFunctions.splitIntoLines("aaaa", 4)).containsExactly("aaaa");
    assertThat(TextFunctions.splitIntoLines("a a a a a a a a a", 4))
        .containsExactly("a a a", "a a a", "a a a");
  }

  @Test
  void alignTable() {
    List<String> strings =
        TextFunctions.alignTable(
            List.of(List.of("Left:", "Right"), List.of("Very long text:", "More text")),
            List.of(TextAlignment.RIGHT, TextAlignment.LEFT));
    assertThat(strings).containsExactly("          Left: Right    ", "Very long text: More text");
  }
}
