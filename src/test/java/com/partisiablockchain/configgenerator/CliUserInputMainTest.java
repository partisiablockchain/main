package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.IntConsumer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class CliUserInputMainTest {

  private @TempDir Path temporaryOutputDir;
  private Path outputLocation;
  private CompositeNodeConfigDto config;
  private ByteArrayOutputStream outputStream;
  private PrintStream printStream;
  private TerminalPrinter terminalPrinter;
  private Integer lastExitCode;
  private IntConsumer exitHook;

  private static final String PEER_1 =
      "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe169eb:localhost:8111";
  private static final String PEER_2 =
      "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe169eb:example.com:8222";
  private static final String PEER_1_AND_2 = PEER_1 + "," + PEER_2;
  private Web3Validator.Web3ServiceProvider validWeb3jProvider;

  @BeforeEach
  void setup() {
    outputStream = new ByteArrayOutputStream();
    printStream = new PrintStream(outputStream);
    outputLocation = temporaryOutputDir.resolve("temporaryConfig");
    config = new CompositeNodeConfigDto();
    terminalPrinter = new TerminalPrinter(80, printStream);
    validWeb3jProvider = maybeUri -> Web3ValidatorTest.createService(System.currentTimeMillis());
    exitHook = value -> lastExitCode = value;
  }

  @Test
  void testMalformedUrl() {
    assertThat(CliInputValidation.validateUrl("example.com")).isFalse();
  }

  @Test
  void testMissingParentDirFromOutputLocation() throws IOException {
    Path outputFilePath = temporaryOutputDir.resolve("subdir/temporaryConfig");
    Path subdir = temporaryOutputDir.resolve("subdir");

    CliConfigGeneratorMain.writeConfig(config, outputFilePath);

    assertThat(subdir.toFile().exists()).isTrue();
  }

  @Test
  void testWriteConfigFile() throws IOException {
    CliConfigGeneratorMain.writeConfig(config, outputLocation);

    String writtenConfig = Files.readString(outputLocation);
    ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
    CompositeNodeConfigDto readConfig =
        mapper.readValue(writtenConfig, CompositeNodeConfigDto.class);

    assertThat(readConfig).usingRecursiveComparison().isEqualTo(config);
  }

  @Test
  void testLoadConfig() throws IOException {
    config.restPort = 555;
    CliConfigGeneratorMain.writeConfig(config, outputLocation);

    CompositeNodeConfigDto loadedConfig = CliConfigGeneratorMain.loadConfig(outputLocation);

    assertThat(loadedConfig.restPort).isEqualTo(555);
  }

  @Test
  void testRun() throws Exception {
    String[] args = {outputLocation.toString()};

    ByteArrayInputStream inputStream = CliUserInputTest.fromLines("n", PEER_1);

    CliConfigGeneratorMain.runWithArgs(
        args, inputStream, printStream, validWeb3jProvider, exitHook);
    assertThat(outputLocation.toFile()).exists();

    String json = Files.readString(outputLocation);
    assertThat(json).contains("\n  ");
  }

  @Test
  void runWithExistingConfig() throws IOException {
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig.host = "fromOldConfig";
    config.producerConfig.ethereumUrl = "oldEthUrl";
    CliConfigGeneratorMain.writeConfig(config, outputLocation);

    String[] args = {outputLocation.toString()};
    ByteArrayInputStream inputStream =
        CliUserInputTest.fromLines(
            "y",
            "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe16a",
            "y",
            "",
            "https://example.com/eth",
            "https://example.com/eth",
            "https://example.com/eth",
            PEER_1,
            "y",
            "y");

    CliConfigGeneratorMain.runWithArgs(
        args, inputStream, printStream, validWeb3jProvider, exitHook);

    CompositeNodeConfigDto loadedConfig = CliConfigGeneratorMain.loadConfig(outputLocation);

    assertThat(loadedConfig.producerConfig.host).isEqualTo("fromOldConfig");
    assertThat(loadedConfig.producerConfig.ethereumUrl).isNull();
    assertThat(loadedConfig.producerConfig.getChainEndpoint("Ethereum"))
        .isEqualTo("https://example.com/eth");
  }

  /**
   * If the producer config has endpoints for a chain in both the old and new style, and the user
   * does not supply a new endpoint, then the endpoint in the new style should be used, and the
   * endpoint in the old style removed.
   */
  @Test
  void useNewChainConfigAsDefault() throws IOException {
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig.host = "host";
    config.producerConfig.ethereumUrl = "oldEthUrl";
    config.producerConfig.addChainEndpoint("Ethereum", "newEthUrl");
    CliConfigGeneratorMain.writeConfig(config, outputLocation);

    String[] args = {outputLocation.toString()};
    ByteArrayInputStream inputStream =
        CliUserInputTest.fromLines(
            "y",
            "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe16a",
            "y",
            "",
            "",
            "https://example.com/polygon",
            "https://example.com/bnb",
            PEER_1,
            "y",
            "y");

    CliConfigGeneratorMain.runWithArgs(
        args, inputStream, printStream, validWeb3jProvider, exitHook);

    CompositeNodeConfigDto loadedConfig = CliConfigGeneratorMain.loadConfig(outputLocation);
    // Old endpoint is cleared
    assertThat(loadedConfig.producerConfig.ethereumUrl).isNull();
    // New endpoint is preserved
    assertThat(loadedConfig.producerConfig.getChainEndpoint("Ethereum")).isEqualTo("newEthUrl");
  }

  @Test
  void helpMessage() throws IOException {
    for (String arg : List.of("--help", "-h")) {
      String[] args = {arg};
      CliConfigGeneratorMain.runWithArgs(
          args, InputStream.nullInputStream(), printStream, validWeb3jProvider, exitHook);

      assertThat(outputStream.toString(StandardCharsets.UTF_8))
          .contains(CliConfigGeneratorMain.HELP_MESSAGE);

      assertThat(lastExitCode).isNull();
    }
  }

  @Test
  void successfulMain() throws IOException {
    String[] args = {outputLocation.toString()};

    ByteArrayInputStream input =
        CliUserInputTest.fromLines(
            "y",
            "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe16a",
            "n",
            "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe16a",
            "y",
            "localhost",
            "https://example.com/eth",
            "https://example.com/eth",
            "https://example.com/eth",
            PEER_1_AND_2,
            "n",
            "y");

    CliConfigGeneratorMain.runWithArgs(args, input, printStream, validWeb3jProvider, exitHook);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(59)
        .contains("Partisia Blockchain Node Configuration Utility")
        .contains("Welcome to the node configuration utility.")
        .contains("==================")
        .contains("Your blockchain address is")
        .contains("Your node IP")
        .contains("Your flooding port: 9888")
        .contains("base64 or hex")
        .contains("known peer")
        .contains(":8111")
        .contains(":8222")
        .contains("configuration file has been written")
        .contains("BACK THIS UP")
        .contains("Please confirm that you have a secure, offline backup")
        .contains(outputLocation.toString());
  }

  @Test
  void existingConfig() throws IOException {
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    config.producerConfig.accountKey = "a".repeat(64);
    config.producerConfig.finalizationKey = "b".repeat(64);
    config.networkKey = "c".repeat(64);
    CliConfigGeneratorMain.writeConfig(config, outputLocation);

    String[] args = {outputLocation.toString()};

    ByteArrayInputStream input =
        CliUserInputTest.fromLines(
            "",
            "y",
            "localhost",
            "https://example.com/eth",
            "https://example.com/eth",
            "https://example.com/eth",
            PEER_1_AND_2,
            "y");

    CliConfigGeneratorMain.runWithArgs(args, input, printStream, validWeb3jProvider, exitHook);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(58)
        .contains("NOTE: Found existing configuration file, loading previous answers.")
        .contains("==================")
        .contains("Your blockchain address is")
        .contains("Your node IP")
        .contains("Your flooding port: 9888")
        .contains("base64 or hex")
        .contains("known peer")
        .contains(":8111")
        .contains(":8222")
        .contains("configuration file has been written")
        .contains("BACK THIS UP")
        .contains("Please confirm that you have a secure, offline backup")
        .contains(outputLocation.toString());

    assertThat(lastExitCode).isEqualTo(0);
  }

  @Test
  void noArgs() throws IOException {
    String[] args = {};
    CliConfigGeneratorMain.main(args);
  }

  @Test
  void fileNotFound() {
    String[] args = {temporaryOutputDir.toString()};
    assertThatThrownBy(() -> CliConfigGeneratorMain.main(args))
        .isInstanceOf(FileNotFoundException.class);
  }

  @Test
  void printIntroMessage() {
    CliConfigGeneratorMain.printIntroMessage(terminalPrinter);
    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Welcome")
        .contains("Following")
        .contains("Trailing")
        .contains("If")
        .hasLineCount(7);
  }

  @Test
  void printHeaderMessage() {
    CliConfigGeneratorMain.printHeader(terminalPrinter);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(3)
        .contains("Partisia Blockchain Node Configuration Utility");
  }

  @Test
  void printExitMessage() {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    String[] knownPeers = {"peer1"};
    config.floodingPort = 9888;
    config.networkKey = "c".repeat(64);
    config.knownPeers = List.of(knownPeers);

    CliConfigGeneratorMain.printExitMessage(terminalPrinter, outputLocation, config);

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(13)
        .contains("The configuration file has been written to")
        .contains("You are now ready to start your node.");
  }
}
