package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.crypto.KeyPair;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class KnownPeersValidatorTest {

  private KnownPeersValidator validator;
  private String hexKey;
  private String base64key;

  @BeforeEach
  void setUp() {
    validator = new KnownPeersValidator();
    byte[] bytes = new KeyPair().getPublic().asBytes();
    hexKey = Hex.toHexString(bytes);
    base64key = Base64.toBase64String(bytes);
  }

  @Test
  void notEnoughParts() {
    assertThat(validator.validate("localhost")).isFalse();
  }

  @Test
  void moreThanThreeParts() {
    assertThat(validator.validate("a:b:c:d")).isFalse();
  }

  @Test
  void emptyHost() {
    assertThat(validator.validate(hexKey + ":" + ":" + "8080")).isFalse();
  }

  @Test
  void invalidHost() {
    assertThat(validator.validate(hexKey + ":invalidhost_-!:8080")).isFalse();
  }

  @Test
  void invalidPort() {
    assertThat(validator.validate(hexKey + ":localhost:xyz")).isFalse();
  }

  @Test
  void validPublicKeyInvalidHost() {
    assertThat(validator.validate(hexKey + ":!_!_!_!:8999")).isFalse();
  }

  @Test
  void invalidPublicKeyValidHost() {
    assertThat(validator.validate("not valid:localhost:8999")).isFalse();
  }

  @Test
  void validHostAndPort() {
    assertThat(validator.validate(hexKey + ":localhost:8999")).isTrue();
    assertThat(validator.validate(hexKey + ":127.0.0.1:8999")).isTrue();
    assertThat(validator.validate(base64key + ":localhost:8999")).isTrue();
    assertThat(validator.validate(base64key + ":127.0.0.1:8999")).isTrue();
  }

  @Test
  void validateKey() {
    assertThat(KnownPeersValidator.parseBase64key(hexKey)).isNull();
    assertThat(KnownPeersValidator.parseBase64key(base64key)).isNotNull();

    assertThat(KnownPeersValidator.parseHexKey(hexKey)).isNotNull();
    assertThat(KnownPeersValidator.parseHexKey(base64key)).isNull();
  }

  @Test
  void transformValid() {
    String withHex = hexKey + ":localhost:8999";
    String withBase64 = base64key + ":localhost:8999";

    assertThat(validator.toSerializedForm(withBase64)).isEqualTo(withHex);
    assertThat(validator.toSerializedForm(withHex)).isEqualTo(withHex);
  }
}
