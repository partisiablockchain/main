package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.server.CompositeNodeConfigDto;
import java.io.IOException;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class NodeConfigReaderTest {
  private static final String ACCOUNT_SECRET_KEY = "b".repeat(64);

  private static final String NETWORK_SECRET_KEY = "a".repeat(64);

  private static final String FINALIZATION_SECRET_KEY = "c".repeat(64);

  private static final int REST_PORT = 8888;

  private CompositeNodeConfigDto config;
  private @TempDir Path temporaryOutputDir;
  private Path configLocation;
  private NodeConfigReader nodeConfigReader;

  @BeforeEach
  void setUp() throws IOException {
    config = new CompositeNodeConfigDto();
    config.networkKey = NETWORK_SECRET_KEY;
    config.restPort = REST_PORT;
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    var producerConfig = config.producerConfig;
    producerConfig.accountKey = ACCOUNT_SECRET_KEY;
    producerConfig.finalizationKey = FINALIZATION_SECRET_KEY;

    configLocation = temporaryOutputDir.resolve("config.json");
    CliConfigGeneratorMain.writeConfig(config, configLocation);
    nodeConfigReader = new NodeConfigReader(configLocation.toString());
  }

  /** getRestPort returns the rest port specified in the config file. */
  @Test
  void getRestPort() {
    assertThat(nodeConfigReader.getRestPort()).isEqualTo(REST_PORT);
  }
}
