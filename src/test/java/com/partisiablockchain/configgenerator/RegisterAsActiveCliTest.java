package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.bls.BlsVerifier;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import com.secata.stream.SafeDataInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class RegisterAsActiveCliTest {
  private static final String ACCOUNT_SECRET_KEY = "b".repeat(64);

  private static final String NETWORK_SECRET_KEY = "a".repeat(64);

  private static final String FINALIZATION_SECRET_KEY = "c".repeat(64);

  private @TempDir Path temporaryOutputDir;

  private static final int BPO_INVOKE_MARK_AS_ACTIVE = 14;

  /**
   * Sends transaction consisting of mark-as-active invocation and valid proof-of-possession
   * signature.
   */
  @Test
  void markAsActiveInvocationContainsInvocationBytesAndValidProofOfPossession() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("config.json");
    saveDefaultConfig(configLocation);
    BlockchainContractClientSpy client =
        new BlockchainContractClientSpy(new BlockchainContractClientStub(true));
    run(new String[] {configLocation.toString()}, client);
    SafeDataInputStream transactionRpc =
        SafeDataInputStream.createFromBytes(client.getTransactions().get(0).getRpc());

    byte[] invocationBytes = transactionRpc.readBytes(1);
    assertThat(invocationBytes[0]).isEqualTo((byte) BPO_INVOKE_MARK_AS_ACTIVE);

    NodeConfigReader configReader = new NodeConfigReader(configLocation.toString());
    Hash proofOfPossessionHash =
        ProofOfPossession.createProofOfPossessionHash(
            client,
            configReader.getBlockchainAddress(),
            configReader.getNetworkPublicKey(),
            configReader.getFinalizationKey().getPublicKey());
    BlsSignature blsSignature = BlsSignature.read(transactionRpc);
    assertThat(
            BlsVerifier.verify(
                proofOfPossessionHash,
                blsSignature,
                configReader.getFinalizationKey().getPublicKey()))
        .isTrue();
  }

  /** Registering as active announce if transaction was successful. */
  @Test
  void markAsActiveReportsSuccess() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("config.json");
    saveDefaultConfig(configLocation);
    String[] args = {configLocation.toString()};
    ByteArrayOutputStream stdout = run(args, new BlockchainContractClientStub(true));

    assertThat(stdout.toString(StandardCharsets.UTF_8))
        .contains("Your 'Register as active' transaction has now succeeded");
  }

  /**
   * Registering as active announce if transaction failed and directs user to the transaction in
   * browser.
   */
  @Test
  void transactionFailureDirectsToBrowser() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("config.json");
    saveDefaultConfig(configLocation);
    boolean shouldTransactionSucceed = false;
    BlockchainContractClientSpy client =
        new BlockchainContractClientSpy(new BlockchainContractClientStub(shouldTransactionSucceed));
    ByteArrayOutputStream stdout = run(new String[] {configLocation.toString()}, client);

    assertThat(stdout.toString(StandardCharsets.UTF_8))
        .hasLineCount(5)
        .contains("Your 'Register as active' transaction failed")
        .contains("https://browser.partisiablockchain.com/transactions/");
  }

  /** Help message is displayed when <code>--help</code> or no argument is used. */
  @Test
  void helpMessage() throws IOException {
    String[] args = {"--help"};
    ByteArrayOutputStream outputStream = run(args, new BlockchainContractClientStub(true));

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(2)
        .contains("This tool sends a transaction to mark this node as active");

    args = new String[] {};
    outputStream = run(args, new BlockchainContractClientStub(true));

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .hasLineCount(2)
        .contains("This tool sends a transaction to mark this node as active");
  }

  /** Registering as active requires node to have a valid config file. */
  @Test
  void nodeIsNotConfigured() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("missingConfigFile.json");
    String[] args = {configLocation.toString()};

    ByteArrayOutputStream outputStream = run(args, new BlockchainContractClientStub(true));

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Configuration file does not exist.");
  }

  /** Registering as active requires node to be configured as a producer. */
  @Test
  void missingProducerConfig() throws IOException {
    Path configLocation = temporaryOutputDir.resolve("notProducerConfigured.json");
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    CliConfigGeneratorMain.writeConfig(config, configLocation);
    String[] args = {configLocation.toString()};

    ByteArrayOutputStream outputStream = run(args, new BlockchainContractClientStub(true));

    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("missing producer configuration.");
  }

  /** Mutation coverage of main. */
  @Test
  void mutationCoverageOfMain() {
    String[] args = {temporaryOutputDir.toString()};
    assertThatThrownBy(() -> RegisterAsActiveCli.main(args))
        .isInstanceOf(FileNotFoundException.class);
  }

  /** Line coverage of main. */
  @Test
  void coverageOfMain() {
    String[] args = {};
    assertThatNoException().isThrownBy(() -> RegisterAsActiveCli.main(args));
  }

  /**
   * Runs RegisterAsActiveCli with given arguments and returns stdout stream.
   *
   * @param args arguments for cli
   * @param client blockchain client for sending transactions
   * @return stdout stream
   */
  private ByteArrayOutputStream run(String[] args, BlockchainContractClient client)
      throws IOException {
    ByteArrayOutputStream stdout = new ByteArrayOutputStream();
    PrintStream printStream = new PrintStream(stdout);
    RegisterAsActiveCli.run(args, printStream, (port) -> client);
    return stdout;
  }

  /**
   * Writes a config file with to the location with test values.
   *
   * @param location location of the config file
   */
  private void saveDefaultConfig(Path location) throws IOException {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    config.networkKey = NETWORK_SECRET_KEY;
    config.producerConfig = new CompositeNodeConfigDto.BlockProducerConfigDto();
    var producerConfig = config.producerConfig;
    producerConfig.accountKey = ACCOUNT_SECRET_KEY;
    producerConfig.finalizationKey = FINALIZATION_SECRET_KEY;

    CliConfigGeneratorMain.writeConfig(config, location);
  }
}
