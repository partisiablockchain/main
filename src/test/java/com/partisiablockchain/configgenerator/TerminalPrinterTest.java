package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class TerminalPrinterTest {
  private TerminalPrinter printer;
  private ByteArrayOutputStream rawOut;

  @BeforeEach
  void setUp() {
    rawOut = new ByteArrayOutputStream();
    printer = new TerminalPrinter(80, new PrintStream(rawOut));

    assertThat(getOutput()).isEmpty();
  }

  @Test
  void printHorizontalRule() {
    printer.printHorizontalRule();
    assertThat(getOutput()).contains("====");
  }

  @Test
  void newLine() {
    printer.newLine();
    assertThat(getOutput()).isEqualTo(System.lineSeparator());
  }

  @Test
  void printLine() {
    String text = "this is a text";
    printer.println(text);
    assertThat(getOutput()).contains(text);
  }

  @Test
  void printLineSplit() {
    String horizontalLine = "-".repeat(76);
    String text = "this " + horizontalLine + " word";

    printer.println(text);
    assertThat(getOutput()).contains("this " + horizontalLine).hasLineCount(2).contains("word");
  }

  @Test
  void printnlnf() {
    printer.printlnf("%d %s %d %s", 20, "cakes", 3, "minis");
    assertThat(getOutput()).contains("20 cakes 3 minis");
  }

  @Test
  void printAligned() {
    String text = "test";
    printer.printAligned(text, TextAlignment.CENTER);
    assertThat(getOutput()).contains("  test  ");
  }

  private String getOutput() {
    return rawOut.toString(StandardCharsets.UTF_8);
  }
}
