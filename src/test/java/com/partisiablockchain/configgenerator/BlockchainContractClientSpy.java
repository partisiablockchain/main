package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import java.util.ArrayList;
import java.util.List;

/** Saves all transactions it receives. */
final class BlockchainContractClientSpy implements BlockchainContractClient {
  private final BlockchainContractClientStub clientStub;
  private final List<Transaction> transactions;

  BlockchainContractClientSpy(BlockchainContractClientStub clientStub) {
    this.clientStub = clientStub;
    transactions = new ArrayList<>();
  }

  /**
   * Get all transactions this client has received.
   *
   * @return all received transactions
   */
  List<Transaction> getTransactions() {
    return transactions;
  }

  @Override
  public AccountState getAccountState(BlockchainAddress address) {
    return clientStub.getAccountState(address);
  }

  @Override
  public ContractState getContractState(BlockchainAddress address) {
    return clientStub.getContractState(address);
  }

  @Override
  public BlockState getLatestBlock(ShardId shardId) {
    return null;
  }

  @Override
  public ChainId getChainId() {
    return new ChainId("mock-chain-id");
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash transactionId) {
    return clientStub.getTransaction(shardId, transactionId);
  }

  @Override
  public TransactionPointer putTransaction(SignedTransaction signedTransaction) {
    transactions.add(signedTransaction.getInner());
    return clientStub.putTransaction(signedTransaction);
  }
}
