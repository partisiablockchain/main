package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.InputStream;
import java.util.ArrayList;

final class BlockchainContractClientStub implements BlockchainContractClient {
  BlockchainContractClientStub(boolean shouldTransactionSucceed) {
    this.mainSucceed = shouldTransactionSucceed;
  }

  boolean mainSucceed;
  boolean eventsSucceed = true;

  static final Hash mainIdentifier =
      Hash.fromString("0000000000000000000000000000000000000000100000000000000000000000");
  static final Hash eventIdentifier =
      Hash.fromString("00000000000a0000000000000000000000000000100000000000000000000000");

  @Override
  public ContractState getContractState(BlockchainAddress address) {
    try {
      if (address.equals(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION)) {
        ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
        InputStream response = getClass().getResourceAsStream("blockProducerContractState.json");
        JsonNode serializedContract = mapper.readValue(response, JsonNode.class);
        return new ContractState(null, null, null, 0, serializedContract, null);
      }
    } catch (Exception e) {
      throw new RuntimeException(e.toString());
    }
    throw new UnsupportedOperationException("Unimplemented method 'getContractState'");
  }

  @Override
  public AccountState getAccountState(BlockchainAddress address) {
    return new AccountState(100000000);
  }

  @Override
  public ChainId getChainId() {
    return new ChainId("stub-chain-id");
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash transactionId) {
    if (transactionId.equals(mainIdentifier)) {
      ArrayList<Event> events = new ArrayList<Event>();
      for (int i = 0; i < 5; i++) {
        events.add(new Event(eventIdentifier.toString(), ""));
      }
      return new ExecutedTransaction(
          null, "", 0, 0, "", "", mainSucceed, events, true, false, null, null);
    }
    if (transactionId.equals(eventIdentifier)) {
      ArrayList<Event> events = new ArrayList<Event>();
      return new ExecutedTransaction(
          null, "", 0, 0, "", "", eventsSucceed, events, true, false, null, null);
    }
    throw new UnsupportedOperationException("Unimplemented method 'getTransaction'");
  }

  @Override
  public TransactionPointer putTransaction(SignedTransaction signedTransaction) {
    return new TransactionPointer(mainIdentifier, "Shard0");
  }

  @Override
  public BlockState getLatestBlock(ShardId shardId) {
    return null;
  }
}
