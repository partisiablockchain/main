package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.tree.AvlTree;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Test client. */
public final class RewardsClientMock implements RewardsClient {
  private static int counter = 0;
  private static final List<BlockchainAddress> PRODUCERS =
      List.of(
          TestnetAddresses.PRODUCER_1,
          TestnetAddresses.PRODUCER_2,
          TestnetAddresses.PRODUCER_3,
          TestnetAddresses.PRODUCER_4);

  @Override
  public long getBlockTimeAtTimestamp(long timestamp, String shard) {
    if (counter == PRODUCERS.size()) {
      counter = 0;
    }
    return counter++;
  }

  @Override
  public JsonNode getAccountPluginAtBlockTime(long blockTime, String shard) {
    int stakedTokens = 1;
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers = AvlTree.create();
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withDelegatedStakesFromOthers(delegatedStakesFromOthers)
            .build();
    return StateObjectMapper.createObjectMapper()
        .valueToTree(List.of(new Account(PRODUCERS.get((int) blockTime).writeAsString(), balance)));
  }

  record Account(String key, Balance value) {}

  @Override
  public BlockState getBlock(long blockTime, String shard) {
    return new BlockState("", 0, blockTime, 0, List.of(), List.of(), "", "", (short) blockTime);
  }

  @Override
  public Committees getAllCommittees() {
    List<Producer> activeCommittee = new ArrayList<>();
    for (BlockchainAddress producer : PRODUCERS) {
      activeCommittee.add(new Producer(producer));
    }
    JsonNode committeesJson =
        StateObjectMapper.createObjectMapper().valueToTree(List.of(new Committee(activeCommittee)));
    return RewardsClientImpl.convertGlobalConsensusCommitteeJson(committeesJson);
  }

  @Override
  public Map<CommitteeProducer, Long> getProducedBlocks(long from, long to, String shard) {
    Map<CommitteeProducer, Long> blocksProduced = new HashMap<>();
    // First producer did not produce anything but resulted in a reset block instead
    blocksProduced.put(new CommitteeProducer(0, (short) -1), 1L);
    for (int i = 1; i < PRODUCERS.size(); i++) {
      blocksProduced.put(new CommitteeProducer(0, (short) i), 1L);
    }
    return blocksProduced;
  }

  record Committee(List<Producer> activeCommittee) {}
}
