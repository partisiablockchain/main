package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class RewardsCalculatorTest {

  private static final BlockchainAddress FIRST_ACCOUNT =
      BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  private static final BlockchainAddress SECOND_ACCOUNT =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private static final BlockchainAddress THIRD_ACCOUNT =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");
  private static final BlockchainAddress FOURTH_ACCOUNT =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  /**
   * The rewards for each week are distributed relative to the amount of rewardable tokens after
   * performance adjustments.
   */
  @Test
  void rewardsAreDistributedAccordingToAmountOfRewardableTokens() {
    List<Stake> stakes =
        List.of(
            createStake(FIRST_ACCOUNT, THIRD_ACCOUNT, 1002, 300),
            createStake(FOURTH_ACCOUNT, SECOND_ACCOUNT, 498, 700));

    int weeklyRewardInQuarter = 1000;
    List<Reward> rewards = RewardsCalculator.buildRewards(stakes, weeklyRewardInQuarter);

    int sumOfRewardable = 2500;
    Assertions.assertThat(rewards)
        .containsExactlyInAnyOrder(
            new Reward(FIRST_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 1002, 1002d / 2.5d),
            new Reward(SECOND_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 700, 700d / 2.5d),
            new Reward(THIRD_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 300, 300d / 2.5d),
            new Reward(FOURTH_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 498, 498d / 2.5d));
  }

  /**
   * A BP that delegates to his own account will receive rewards for both token holder and BP
   * rewardable tokens.
   */
  @Test
  void bpCanStakeToSelfAndGetAllRewards() {
    List<Stake> stakes = List.of(createStake(FIRST_ACCOUNT, FIRST_ACCOUNT, 100, 50));

    int weeklyRewardInQuarter = 1000;
    List<Reward> rewards = RewardsCalculator.buildRewards(stakes, weeklyRewardInQuarter);

    int sumOfRewardable = 150;
    Assertions.assertThat(rewards)
        .containsExactlyInAnyOrder(
            new Reward(FIRST_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 100 + 50, 1000));
  }

  /** Rewardable tokens and rewards are summed if a token holder delegates to multiple BPs. */
  @Test
  void userDelegateToMultipleBps() {
    List<Stake> stakes =
        List.of(
            createStake(FIRST_ACCOUNT, THIRD_ACCOUNT, 100, 50),
            createStake(FIRST_ACCOUNT, SECOND_ACCOUNT, 50, 50));

    int weeklyRewardInQuarter = 1000;
    List<Reward> rewards = RewardsCalculator.buildRewards(stakes, weeklyRewardInQuarter);

    int sumOfRewardable = 250;
    Assertions.assertThat(rewards)
        .containsExactlyInAnyOrder(
            new Reward(FIRST_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 100 + 50, 600),
            new Reward(SECOND_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 50, 200),
            new Reward(THIRD_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 50, 200));
  }

  /** Rewardable tokens and rewards are summed if a BP receive delegations from multiple users. */
  @Test
  void bpReceiveMultipleDelegations() {
    List<Stake> stakes =
        List.of(
            createStake(FIRST_ACCOUNT, THIRD_ACCOUNT, 100, 60),
            createStake(SECOND_ACCOUNT, THIRD_ACCOUNT, 800, 40));

    int weeklyRewardInQuarter = 3000;
    List<Reward> rewards = RewardsCalculator.buildRewards(stakes, weeklyRewardInQuarter);

    int sumOfRewardable = 1000;
    Assertions.assertThat(rewards)
        .containsExactlyInAnyOrder(
            new Reward(FIRST_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 100, 300),
            new Reward(SECOND_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 800, 2400),
            new Reward(THIRD_ACCOUNT, weeklyRewardInQuarter, sumOfRewardable, 100, 300));
  }

  /**
   * Create a rewardable stake relation with only the field relevant for rewards calculation
   * populated.
   *
   * @param from the token holder performing the staking
   * @param to the receiving block producer of the stake
   * @param rewardableTokenHolder the amount of tokens rewardable to the holder
   * @param rewardableBp the amount of tokens rewardable to the block producer
   * @return the stake relation
   */
  private Stake createStake(
      BlockchainAddress from,
      BlockchainAddress to,
      double rewardableTokenHolder,
      double rewardableBp) {
    return new Stake(
        from,
        to,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        rewardableTokenHolder + rewardableBp,
        rewardableTokenHolder,
        rewardableBp);
  }
}
