package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.rest.testing.client.ClientTestFilter;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

final class ExportBalanceTest {
  private static final boolean REGENERATE_REF_FILES = false;
  private static final LocalDate day = LocalDate.of(2023, 8, 31);
  private static final String url = "https://reader.partisiablockchain.com";

  /** Balances can be created from on-chain information. */
  @Test
  public void exportBalancesOnChain() {
    Client replayClient =
        ClientTestFilter.recordReplay(
            ClientBuilder.newClient(),
            Path.of("src/test/resources/replayClient/exportBalance/exportBalancesOnChain"),
            REGENERATE_REF_FILES);
    ExportBalance balanceExporter =
        new ExportBalance(day, new RewardsClientImpl(url, replayClient), "Shard0");
    Map<BlockchainAddress, Balance> balances = balanceExporter.exportBalances();

    assertThat(balances.size()).isEqualTo(1977);

    Balance balances1 =
        balances.get(BlockchainAddress.fromString("0087e83fc9df70ba11b1ffdf8e5d5bcbc2599029da"));
    assertThat(balances1.accountCoins().get(0).balance()).isEqualTo("7583222500000000");
    assertThat(balances1.mpcTokens()).isEqualTo(-1670002667);
    assertThat(balances1.stakedTokens()).isEqualTo(1670002667);
    assertThat(balances1.vestingAccounts().get(0).tokens()).isEqualTo(1670002667);
    assertThat(balances1.stakeable()).isEqualTo(true);
    assertThat(balances1.tokensInCustody()).isEqualTo(0);

    Balance balances2 =
        balances.get(BlockchainAddress.fromString("00dae0d98dbbe72eb56a8efbcc957d08dfa817dfbe"));
    assertThat(balances2.storedPendingStakeDelegations().values().get(0).amount())
        .isEqualTo(150000);
    assertThat(balances2.delegatedStakesFromOthers().values().get(0).acceptedDelegatedStakes())
        .isEqualTo(448000000);
    assertThat(balances2.delegatedStakesToOthers().values().get(0)).isEqualTo(16408087);

    Balance balances3 =
        balances.get(BlockchainAddress.fromString("00c057f909500cdc283ac51c65fdeef8b25664fde0"));
    assertThat(balances3.storedPendingTransfers().values().get(0).amount())
        .isEqualTo(Unsigned256.create(10000000000000000L));

    Balance balances4 =
        balances.get(BlockchainAddress.fromString("001c55bce7420948369cce8f014b7ab2b37139f99d"));
    assertThat(balances4.pendingRetractedDelegatedStakes().values().get(0)).isEqualTo(12500000);

    Balance balances5 =
        balances.get(BlockchainAddress.fromString("00a561d0208cffc300ff3e8e084a0e903ac73631ce"));
    StakesFromOthers stakesFromOthers = balances5.delegatedStakesFromOthers().values().get(0);
    assertThat(stakesFromOthers.acceptedDelegatedStakes()).isEqualTo(48096500L);
    assertThat(stakesFromOthers.pendingDelegatedStakes()).isEqualTo(0L);
    assertThat(stakesFromOthers.expirationTimestamp()).isNull();
  }

  /** New unknown fields in balances on-chain is ignored when exporting a {@link Balance}. */
  @Test
  public void exporterIgnoreUnknownFields() {
    Balance balance =
        new Balance(
            FixedList.create(List.of()),
            123L,
            456L,
            AvlTree.create(),
            FixedList.create(),
            AvlTree.create(),
            AvlTree.create(),
            AvlTree.create(),
            AvlTree.create(),
            AvlTree.create(),
            false,
            AvlTree.create(),
            789L);
    ObjectMapper objectMapper = StateObjectMapper.createObjectMapper();
    BlockchainAddress address =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    AvlTree<BlockchainAddress, Balance> balanceAvlTree = AvlTree.create(Map.of(address, balance));

    JsonNode balanceJson = objectMapper.valueToTree(balanceAvlTree);
    String jsonAsString =
        ExceptionConverter.call(() -> objectMapper.writeValueAsString(balanceJson));
    String stringWithNewField =
        jsonAsString.replaceFirst("\"stakeable\":false,", "\"stakeable\":false,\"newField\":true,");
    JsonNode jsonWithNewField =
        ExceptionConverter.call(() -> objectMapper.readTree(stringWithNewField));
    Map<BlockchainAddress, Balance> exporterResult =
        ExportBalance.convertJsonAccountsToBalances(jsonWithNewField);

    assertThat(exporterResult.get(address).mpcTokens()).isEqualTo(123L);
    assertThat(exporterResult.get(address).stakedTokens()).isEqualTo(456L);
    assertThat(exporterResult.get(address).tokensInCustody()).isEqualTo(789L);
  }
}
