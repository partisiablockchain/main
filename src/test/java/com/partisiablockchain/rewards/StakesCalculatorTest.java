package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.tree.AvlTree;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** StakeCalculator builds {@link Stake} for each individual stake from a {@link TokenHolder}. */
final class StakesCalculatorTest {
  private static final List<BlockchainAddress> producers = TestnetAddresses.PRODUCERS;
  private static final LocalDate FUTURE_DATE = LocalDate.parse("2050-02-02");

  private final BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
  private final BlockchainAddress delegator1 = TestnetAddresses.PRODUCER_2;
  private final BlockchainAddress delegator2 = TestnetAddresses.PRODUCER_3;
  private final TokenHolder tokenHolder = new TokenHolderBuilder(stakeHolder).build();

  /** Stakes can be calculated from producers, tokenHolders, BpScores and delegations. */
  @Test
  void buildStakesFromProducersAndTokenHolders() {
    Map<BlockchainAddress, AvlTree<BlockchainAddress, StakesFromOthers>> delegations =
        new HashMap<>();
    int delegatedStakes = 10;
    for (BlockchainAddress delegator : producers) {
      delegations.put(
          delegator,
          AvlTree.create(
              Map.of(producers.get(0), new StakesFromOthers(delegatedStakes, 0L, null))));
    }
    double unlockRatio = 0.9;
    double stakeRatio = 0.8;
    double stakeCapRatio = 0.7;
    int stakedTokens = 100;
    List<TokenHolder> tokenHolders =
        producers.stream()
            .map(p -> buildTokenHolder(p, stakedTokens, unlockRatio, stakeRatio, stakeCapRatio))
            .toList();

    Map<BlockchainAddress, TokenHolder> tokenHolderMap = new HashMap<>();
    for (TokenHolder tokenHolder : tokenHolders) {
      tokenHolderMap.put(tokenHolder.address(), tokenHolder);
    }

    Map<BlockchainAddress, Double> bpScoresMap = Map.of(producers.get(0), 0.25d);

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            producers, tokenHolderMap, bpScoresMap, delegations, FUTURE_DATE);
    assertThat(stakes.size()).isEqualTo(8);

    double rewardRatios =
        unlockRatio * stakeRatio * stakeCapRatio * ((double) delegatedStakes / stakedTokens);
    assertThat(stakes.get(0).rewardable())
        .isEqualTo(100 * bpScoresMap.get(producers.get(0)) * rewardRatios);
    assertThat(stakes.get(0).rewardableBeforePerformance()).isEqualTo(100 * rewardRatios);
    assertThat(stakes.get(0).rewardableTokenHolder()).isEqualTo(stakes.get(0).rewardable() * 0.98);
    assertThat(stakes.get(0).rewardableBp()).isEqualTo(stakes.get(0).rewardable() * 0.02);
  }

  private static TokenHolder buildTokenHolder(
      BlockchainAddress address,
      int stakedTokens,
      double unlockRatio,
      double stakeRatio,
      double stakeCapRatio) {
    return new TokenHolder(
        address,
        0,
        0,
        stakedTokens,
        0,
        0,
        0,
        stakedTokens,
        0,
        0,
        0,
        stakedTokens,
        0,
        stakedTokens,
        unlockRatio,
        stakeRatio,
        stakeCapRatio);
  }

  /** Only producers that are a {@link TokenHolder} are added to stakes list. */
  @Test
  void stakedTokensAreNeededToBeStakeHolder() {
    BlockchainAddress producer = TestnetAddresses.PRODUCER_1;
    TokenHolder notReallyTokenHolder =
        new TokenHolder(producer, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(producer),
            Map.of(producer, notReallyTokenHolder),
            Map.of(producer, 0d),
            Map.of(producer, AvlTree.create()),
            FUTURE_DATE);
    assertThat(stakes.size()).isEqualTo(0);
  }

  /** Producers without a bpScore will receive a score of 0 when stakes are calculated. */
  @Test
  void producerThatHasNotBeenScoredGetsBpScoreOf0() {
    BlockchainAddress producer = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolder(producer, 0, 0, 123, 0, 0, 0, 0, 0, 0, 0, 123L, 0, 123L, 1, 1, 1);

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(producer),
            Map.of(producer, tokenHolder),
            Map.of(),
            Map.of(producer, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).bpScore()).isEqualTo(0d);
  }

  /** Amount is the amount of accepted stakes from others. */
  @Test
  void amount() {
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder, delegator1, tokenHolder, delegator2, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(
                    Map.of(
                        delegator1,
                        new StakesFromOthers(100L, 0L, null),
                        delegator2,
                        new StakesFromOthers(200L, 0L, null)))),
            FUTURE_DATE);

    assertThat(stakes.get(0).amount()).isEqualTo(100L);
    assertThat(stakes.get(1).amount()).isEqualTo(200L);
  }

  @Test
  @DisplayName("Accepted delegated stakes not expired, gives rewards")
  void notExpiredStakesGetsPayedOut() {
    long payoutDate = FUTURE_DATE.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder, delegator1, tokenHolder, delegator2, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(
                    Map.of(
                        delegator1,
                        new StakesFromOthers(100L, 0L, payoutDate + 100L),
                        delegator2,
                        new StakesFromOthers(200L, 0L, payoutDate)))),
            FUTURE_DATE);

    assertThat(stakes.get(0).amount()).isEqualTo(100L);
    assertThat(stakes.get(1).amount()).isEqualTo(200L);
  }

  @Test
  @DisplayName("Accepted delegated stakes that has expired, does not give rewards")
  void expiredStakesDoesNotGetPayedOut() {
    long payoutDate = FUTURE_DATE.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder, delegator1, tokenHolder, delegator2, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(
                    Map.of(
                        delegator1,
                        new StakesFromOthers(100L, 0L, payoutDate - 100L),
                        delegator2,
                        new StakesFromOthers(200L, 0L, payoutDate - 100L)))),
            FUTURE_DATE);

    assertThat(stakes).hasSize(0);
  }

  /** Pending amount is the amount of pending stakes from others. */
  @Test
  void pendingAmount() {
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder, delegator1, tokenHolder, delegator2, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(
                    Map.of(
                        delegator1,
                        new StakesFromOthers(0L, 100L, null),
                        delegator2,
                        new StakesFromOthers(0L, 200L, null)))),
            FUTURE_DATE);

    assertThat(stakes.get(0).pendingAmount()).isEqualTo(100L);
    assertThat(stakes.get(1).pendingAmount()).isEqualTo(200L);
  }

  /** Staked is the amount of staked MPC in the TokenHolder. */
  @Test
  void staked() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withStaked(100).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).staked()).isEqualTo(tokenHolder.staked());
  }

  /** Unlocked is the total amount of unlocked MPC tokens in the TokenHolder. */
  @Test
  void unlocked() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withUnlocked(100).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).unlocked()).isEqualTo(tokenHolder.unlocked());
  }

  /** Total is the total amount of MPC tokens in the TokenHolder. */
  @Test
  void total() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withTotal(100).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).total()).isEqualTo(tokenHolder.total());
  }

  /** UnlockedRatio is the ratio of unlocked MPC tokens in the TokenHolder. */
  @Test
  void unlockedRatio() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withUnlockedRatio(0.5d).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).unlockedRatio()).isEqualTo(tokenHolder.unlockedRatio());
  }

  /** StakeRatio is the ratio of staked MPC tokens in the TokenHolder. */
  @Test
  void stakeRatio() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withStakeRatio(0.5d).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).stakeRatio()).isEqualTo(tokenHolder.stakeRatio());
  }

  /** StakeCapRatio is the ratio of staked MPC that is under the staking cap in the TokenHolder. */
  @Test
  void stakeCapRatio() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withStakeCapRatio(0.5d).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).stakeCapRatio()).isEqualTo(tokenHolder.stakeCapRatio());
  }

  /** BpScore is the BpScore of the block producer. */
  @Test
  void bpScore() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder = new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).build();

    double bpScore = 0.5d;
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(stakeHolder, bpScore),
            Map.of(stakeHolder, AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).bpScore()).isEqualTo(bpScore);
  }

  /**
   * DelegationRatio is calculated from the amount of tokens which comes from delegators compared to
   * the amount of staked tokens in the TokenHolder.
   */
  @Test
  void delegationRatio() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder).withStakedToSelf(1).withStaked(200L).build();

    long acceptedDelegatedStakes = 100L;
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(
                    Map.of(stakeHolder, new StakesFromOthers(acceptedDelegatedStakes, 0L, null)))),
            FUTURE_DATE);

    assertThat(stakes.get(0).delegationRatio())
        .isEqualTo((double) acceptedDelegatedStakes / tokenHolder.staked());
  }

  /**
   * RewardableBeforePerformance is calculated from the total amount of staked tokens that is
   * unlocked, staked, under the stake cap and is from current stakeholder out of the total.
   */
  @Test
  void rewardableBeforePerformance() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder)
            .withStakedToSelf(1)
            .withTotal(1000)
            .withStaked(100)
            .withUnlockedRatio(0.1d)
            .withStakeRatio(0.1d)
            .withStakeCapRatio(0.1d)
            .build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(),
            Map.of(
                stakeHolder,
                AvlTree.create(Map.of(stakeHolder, new StakesFromOthers(10L, 0L, null)))),
            FUTURE_DATE);

    assertThat(stakes.get(0).rewardableBeforePerformance())
        .isEqualTo(1000 * (10d / 100) * 0.1d * 0.1d * 0.1d);
  }

  /** Rewardable is calculated from the rewardableBeforePerformance times the bpScore. */
  @Test
  void rewardable() {
    BlockchainAddress stakeHolder = TestnetAddresses.PRODUCER_1;
    TokenHolder tokenHolder =
        new TokenHolderBuilder(stakeHolder)
            .withStakedToSelf(1)
            .withStaked(100)
            .withTotal(100)
            .build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(stakeHolder),
            Map.of(stakeHolder, tokenHolder),
            Map.of(stakeHolder, 0.5d),
            Map.of(
                stakeHolder,
                AvlTree.create(Map.of(stakeHolder, new StakesFromOthers(100L, 0L, null)))),
            FUTURE_DATE);

    double rewardableBeforePerformance = stakes.get(0).rewardableBeforePerformance();
    assertThat(rewardableBeforePerformance).isEqualTo(100);
    assertThat(stakes.get(0).rewardable()).isEqualTo(rewardableBeforePerformance * 0.5d);
  }

  /**
   * RewardableTokenHolder and Rewardablebp is calculated from the rewardable tokens times the
   * tokenHolder split of reward {@code TOKEN_HOLDER_SHARE} or block producers split of {@code
   * BLOCK_PRODUCER_SHARE}.
   */
  @Test
  void rewardableTokenHolder() {
    BlockchainAddress blockProducer = TestnetAddresses.PRODUCER_1;
    TokenHolder blockProducerAccount =
        new TokenHolderBuilder(blockProducer)
            .withStakedToSelf(1)
            .withStaked(100)
            .withTotal(100)
            .build();

    BlockchainAddress delegator = TestnetAddresses.PRODUCER_2;
    TokenHolder delegatorAccount =
        new TokenHolderBuilder(blockProducer).withStaked(100).withTotal(100).build();

    List<Stake> stakes =
        StakesCalculator.buildStakes(
            List.of(blockProducer, delegator),
            Map.of(blockProducer, blockProducerAccount, delegator, delegatorAccount),
            Map.of(blockProducer, 1d),
            Map.of(
                blockProducer,
                AvlTree.create(Map.of(delegator, new StakesFromOthers(100L, 0L, null))),
                delegator,
                AvlTree.create()),
            FUTURE_DATE);

    assertThat(stakes.get(0).rewardable()).isEqualTo(100);
    assertThat(stakes.get(0).rewardableTokenHolder()).isEqualTo(100 * 0.98d);
    assertThat(stakes.get(0).rewardableBp()).isEqualTo(100 * 0.02d);
  }
}
