package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** Builder for {@link TokenHolder} for testing export of {@link Stake}. */
public final class TokenHolderBuilder {
  private final BlockchainAddress address;
  private long transferable;
  private long inTransit;
  private long stakedToSelf;
  private long pendingUnstakes;
  private long delegatedToOthers;
  private long acceptedFromOthers;
  private long stakeForJobs;
  private long pendingRetractedDelegated;
  private long inSchedule;
  private long releaseable;
  private long staked;
  private long unlocked;
  private long total;
  private double unlockedRatio;
  private double stakeRatio;
  private double stakeCapRatio;

  TokenHolderBuilder(BlockchainAddress address) {
    this.address = address;
    this.transferable = 0;
    this.inTransit = 0;
    this.stakedToSelf = 0;
    this.pendingUnstakes = 0;
    this.delegatedToOthers = 0;
    this.acceptedFromOthers = 0;
    this.stakeForJobs = 0;
    this.pendingRetractedDelegated = 0;
    this.inSchedule = 0;
    this.releaseable = 0;
    this.staked = 0;
    this.unlocked = 0;
    this.total = 0;
    this.unlockedRatio = 1;
    this.stakeRatio = 1;
    this.stakeCapRatio = 1;
  }

  TokenHolderBuilder withTransferable(long amount) {
    this.transferable = amount;
    return this;
  }

  TokenHolderBuilder withInTransit(long amount) {
    this.inTransit = amount;
    return this;
  }

  TokenHolderBuilder withStakedToSelf(long amount) {
    this.stakedToSelf = amount;
    return this;
  }

  TokenHolderBuilder withPendingUnstakes(long amount) {
    this.pendingUnstakes = amount;
    return this;
  }

  TokenHolderBuilder withDelegatedToOthers(long amount) {
    this.delegatedToOthers = amount;
    return this;
  }

  TokenHolderBuilder withAcceptedFromOthers(long amount) {
    this.acceptedFromOthers = amount;
    return this;
  }

  TokenHolderBuilder withStakeForJobs(long amount) {
    this.stakeForJobs = amount;
    return this;
  }

  TokenHolderBuilder withPendingRetractedDelegated(long amount) {
    this.pendingRetractedDelegated = amount;
    return this;
  }

  TokenHolderBuilder withInSchedule(long amount) {
    this.inSchedule = amount;
    return this;
  }

  TokenHolderBuilder withReleaseable(long amount) {
    this.releaseable = amount;
    return this;
  }

  TokenHolderBuilder withStaked(long amount) {
    this.staked = amount;
    return this;
  }

  TokenHolderBuilder withUnlocked(long amount) {
    this.unlocked = amount;
    return this;
  }

  TokenHolderBuilder withTotal(long amount) {
    this.total = amount;
    return this;
  }

  TokenHolderBuilder withUnlockedRatio(double amount) {
    this.unlockedRatio = amount;
    return this;
  }

  TokenHolderBuilder withStakeRatio(double amount) {
    this.stakeRatio = amount;
    return this;
  }

  TokenHolderBuilder withStakeCapRatio(double amount) {
    this.stakeCapRatio = amount;
    return this;
  }

  TokenHolder build() {
    return new TokenHolder(
        address,
        transferable,
        inTransit,
        stakedToSelf,
        pendingUnstakes,
        delegatedToOthers,
        acceptedFromOthers,
        stakeForJobs,
        pendingRetractedDelegated,
        inSchedule,
        releaseable,
        staked,
        unlocked,
        total,
        unlockedRatio,
        stakeRatio,
        stakeCapRatio);
  }
}
