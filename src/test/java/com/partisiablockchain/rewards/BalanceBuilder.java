package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.rewards.accountplugin.AccountCoin;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.IceStake;
import com.partisiablockchain.rewards.accountplugin.InitialVestingAccount;
import com.partisiablockchain.rewards.accountplugin.StakeDelegation;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.rewards.accountplugin.TransferInformation;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** Builder for {@link Balance} for testing export of {@link TokenHolder}. */
final class BalanceBuilder {
  private FixedList<AccountCoin> accountCoins;
  private long mpcTokens;
  private long stakedTokens;
  private AvlTree<Long, Long> pendingUnstakes;
  private FixedList<InitialVestingAccount> vestingAccounts;
  private AvlTree<Hash, TransferInformation> storedPendingTransfers;
  private AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations;
  private AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers;
  private AvlTree<BlockchainAddress, Long> delegatedStakesToOthers;
  private AvlTree<Long, Long> pendingRetractedDelegatedStakes;
  private boolean stakeable;
  private AvlTree<Hash, IceStake> storedPendingIceStakes;
  private long tokensInCustody;

  BalanceBuilder() {
    this.accountCoins = FixedList.create();
    this.mpcTokens = 0L;
    this.stakedTokens = 0L;
    this.pendingUnstakes = AvlTree.create();
    this.vestingAccounts = FixedList.create();
    this.storedPendingTransfers = AvlTree.create();
    this.storedPendingStakeDelegations = AvlTree.create();
    this.delegatedStakesFromOthers = AvlTree.create();
    this.delegatedStakesToOthers = AvlTree.create();
    this.pendingRetractedDelegatedStakes = AvlTree.create();
    this.stakeable = false;
    this.storedPendingIceStakes = AvlTree.create();
    this.tokensInCustody = 0;
  }

  Balance build() {
    return new Balance(
        accountCoins,
        mpcTokens,
        stakedTokens,
        pendingUnstakes,
        vestingAccounts,
        storedPendingTransfers,
        storedPendingStakeDelegations,
        delegatedStakesFromOthers,
        delegatedStakesToOthers,
        pendingRetractedDelegatedStakes,
        stakeable,
        storedPendingIceStakes,
        tokensInCustody);
  }

  BalanceBuilder withAccountCoins(FixedList<AccountCoin> accountCoins) {
    this.accountCoins = accountCoins;
    return this;
  }

  BalanceBuilder withMpcTokens(long mpcTokens) {
    this.mpcTokens = mpcTokens;
    return this;
  }

  BalanceBuilder withStakedTokens(long stakedTokens) {
    this.stakedTokens = stakedTokens;
    return this;
  }

  BalanceBuilder withPendingUnstakes(AvlTree<Long, Long> pendingUnstakes) {
    this.pendingUnstakes = pendingUnstakes;
    return this;
  }

  BalanceBuilder withVestingAccounts(FixedList<InitialVestingAccount> vestingAccounts) {
    this.vestingAccounts = vestingAccounts;
    return this;
  }

  BalanceBuilder withStoredPendingTransfers(
      AvlTree<Hash, TransferInformation> storedPendingTransfers) {
    this.storedPendingTransfers = storedPendingTransfers;
    return this;
  }

  BalanceBuilder withStoredPendingStakeDelegations(
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations) {
    this.storedPendingStakeDelegations = storedPendingStakeDelegations;
    return this;
  }

  BalanceBuilder withDelegatedStakesFromOthers(
      AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers) {
    this.delegatedStakesFromOthers = delegatedStakesFromOthers;
    return this;
  }

  BalanceBuilder withDelegatedStakesToOthers(
      AvlTree<BlockchainAddress, Long> delegatedStakesToOthers) {
    this.delegatedStakesToOthers = delegatedStakesToOthers;
    return this;
  }

  BalanceBuilder withPendingRetractedDelegatedStakes(
      AvlTree<Long, Long> pendingRetractedDelegatedStakes) {
    this.pendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes;
    return this;
  }

  BalanceBuilder withStakeable(boolean stakeable) {
    this.stakeable = stakeable;
    return this;
  }

  BalanceBuilder withStoredPendingIceStakes(AvlTree<Hash, IceStake> storedPendingIceStakes) {
    this.storedPendingIceStakes = storedPendingIceStakes;
    return this;
  }

  BalanceBuilder withTokensInCustody(long tokensInCustody) {
    this.tokensInCustody = tokensInCustody;
    return this;
  }
}
