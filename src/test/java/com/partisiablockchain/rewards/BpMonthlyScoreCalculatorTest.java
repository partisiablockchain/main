package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.dto.BlockState;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class BpMonthlyScoreCalculatorTest {

  /**
   * The block producer monthly scores are calculated based on the number of blocks produced by each
   * block producer in the month. It uses the blocks produced in the previous months to determine
   * which producers are new.
   */
  @Test
  void computeMonthlyScore() {
    BpMonthlyScoreCalculator bpMonthlyScoreCalculator =
        new BpMonthlyScoreCalculator(
            List.of("Gov"),
            new Committees(Map.of(0L, TestnetAddresses.PRODUCERS)),
            new RewardsClientStub());

    Assertions.assertThat(bpMonthlyScoreCalculator.getComputedScores()).isEmpty();

    Map<BlockchainAddress, BpScore> scores =
        bpMonthlyScoreCalculator.computeMonthlyScore(LocalDate.of(2023, Month.SEPTEMBER, 7));
    Map<BlockchainAddress, BpScore> expectedScores =
        Map.of(
            // The most productive incumbent producer scores 1.0
            TestnetAddresses.PRODUCER_1,
            new BpScore(TestnetAddresses.PRODUCER_1, 3, 1),
            TestnetAddresses.PRODUCER_2,
            // Other incumbent producers scores relative to the most productive
            new BpScore(TestnetAddresses.PRODUCER_2, 1, 1d / 3),
            TestnetAddresses.PRODUCER_3,
            // New producer is given the average score of incumbent producers
            new BpScore(TestnetAddresses.PRODUCER_3, 5, (1d + 1d / 3) / 2));
    Assertions.assertThat(scores).containsExactlyInAnyOrderEntriesOf(expectedScores);
    Map<YearMonth, Collection<BpScore>> computedScores =
        bpMonthlyScoreCalculator.getComputedScores();
    YearMonth september = YearMonth.of(2023, Month.SEPTEMBER);
    Assertions.assertThat(computedScores).containsKey(september);
    Assertions.assertThat(computedScores.get(september))
        .containsExactlyInAnyOrderElementsOf(expectedScores.values());
  }

  private static final class RewardsClientStub implements RewardsClient {

    @Override
    public long getBlockTimeAtTimestamp(long timestamp, String shard) {
      if (Instant.ofEpochMilli(timestamp).equals(Instant.parse("2023-08-01T00:00:00Z"))) {
        return 0;
      } else if (Instant.ofEpochMilli(timestamp).equals(Instant.parse("2023-09-01T00:00:00Z"))) {
        return 10;
      } else if (Instant.ofEpochMilli(timestamp).equals(Instant.parse("2023-10-01T00:00:00Z"))) {
        return 19;
      } else {
        throw new UnsupportedOperationException("Unexpected call");
      }
    }

    @Override
    public JsonNode getAccountPluginAtBlockTime(long blockTime, String shard) {
      throw new UnsupportedOperationException("");
    }

    @Override
    public BlockState getBlock(long blockTime, String shard) {
      throw new UnsupportedOperationException("");
    }

    @Override
    public Map<CommitteeProducer, Long> getProducedBlocks(
        long fromBlockTime, long toBlockTime, String shard) {
      if (fromBlockTime == 1 && toBlockTime == 10) {
        return Map.of(
            new CommitteeProducer(0, (short) 0), 1L, new CommitteeProducer(0, (short) 1), 9L);
      } else if (fromBlockTime == 11 && toBlockTime == 19) {
        return Map.of(
            new CommitteeProducer(0, (short) 0),
            3L,
            new CommitteeProducer(0, (short) 1),
            1L,
            new CommitteeProducer(0, (short) 2),
            5L);
      } else {
        throw new UnsupportedOperationException("Unexpected call");
      }
    }

    @Override
    public Committees getAllCommittees() {
      throw new UnsupportedOperationException("");
    }
  }
}
