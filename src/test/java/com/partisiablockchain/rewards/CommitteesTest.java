package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

final class CommitteesTest {

  private static final BlockchainAddress FIRST_PRODUCER =
      BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  private static final BlockchainAddress SECOND_PRODUCER =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private static final BlockchainAddress THIRD_PRODUCER =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");
  private static final BlockchainAddress FOURTH_PRODUCER =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  /** Contains two different committees where two producers are in both committees. */
  private static final Committees DEFAULT_COMMITTEES =
      new Committees(
          Map.of(
              0L, List.of(FIRST_PRODUCER, SECOND_PRODUCER, THIRD_PRODUCER),
              1L, List.of(SECOND_PRODUCER, FOURTH_PRODUCER, THIRD_PRODUCER)));

  /** Produced blocks are attributed to the identity according to index into committees. */
  @Test
  void attributeProductionToCorrectProducer() {
    Map<BlockchainAddress, Long> producedBlocks =
        DEFAULT_COMMITTEES.determineProducerIdentities(
            Map.of(
                new RewardsClient.CommitteeProducer(0L, (short) 1),
                33L,
                new RewardsClient.CommitteeProducer(0L, (short) 2),
                45L));
    assertThat(producedBlocks)
        .containsExactlyInAnyOrderEntriesOf(Map.of(SECOND_PRODUCER, 33L, THIRD_PRODUCER, 45L));
  }

  /**
   * A producer who produces in multiple committees gets summed across the committees when his index
   * is unchanged.
   */
  @Test
  void aggregateAcrossCommitteesSameIndex() {
    Map<BlockchainAddress, Long> producedBlocks =
        DEFAULT_COMMITTEES.determineProducerIdentities(
            Map.of(
                new RewardsClient.CommitteeProducer(0L, (short) 2),
                33L,
                new RewardsClient.CommitteeProducer(1L, (short) 2),
                45L));
    assertThat(producedBlocks)
        .containsExactlyInAnyOrderEntriesOf(Map.of(THIRD_PRODUCER, 33L + 45L));
  }

  /**
   * A producer who produces in multiple committees gets summed across the committees when his index
   * changes.
   */
  @Test
  void aggregateAcrossCommitteesChangedIndex() {
    Map<BlockchainAddress, Long> producedBlocks =
        DEFAULT_COMMITTEES.determineProducerIdentities(
            Map.of(
                new RewardsClient.CommitteeProducer(0L, (short) 1),
                33L,
                new RewardsClient.CommitteeProducer(1L, (short) 0),
                45L));
    assertThat(producedBlocks)
        .containsExactlyInAnyOrderEntriesOf(Map.of(SECOND_PRODUCER, 33L + 45L));
  }

  /** Reset blocks are not attributed to a producer. */
  @Test
  void resetBlocksAreNotAttributed() {
    Map<BlockchainAddress, Long> producedBlocks =
        DEFAULT_COMMITTEES.determineProducerIdentities(
            Map.of(
                new RewardsClient.CommitteeProducer(0L, (short) -1),
                33L,
                new RewardsClient.CommitteeProducer(1L, (short) 1),
                45L,
                new RewardsClient.CommitteeProducer(1L, (short) -1),
                10L));
    assertThat(producedBlocks).containsExactlyInAnyOrderEntriesOf(Map.of(FOURTH_PRODUCER, 45L));
  }
}
