package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;

/** Testnet addresses. */
public final class TestnetAddresses {
  /** Testnet url. */
  public static final String READER_URL = "https://node1.testnet.partisiablockchain.com";

  /** Testnet producer 1. */
  public static final BlockchainAddress PRODUCER_1 =
      BlockchainAddress.fromString("00dd4aa71c78bfea151a0fdb149e4c012720ee0250");

  /** Testnet producer 2. */
  public static final BlockchainAddress PRODUCER_2 =
      BlockchainAddress.fromString("00a44d16e47871a20cdc8b2beb6bb2fe16d1ba8ff1");

  /** Testnet producer 3. */
  public static final BlockchainAddress PRODUCER_3 =
      BlockchainAddress.fromString("006df142b1c3f820ffc07df2ab7358cad2202da18f");

  /** Testnet producer 4. */
  public static final BlockchainAddress PRODUCER_4 =
      BlockchainAddress.fromString("00c4b91b81531fb571f0a815346b808cfd266b4fbf");

  /** All testnet producers. */
  public static final List<BlockchainAddress> PRODUCERS =
      List.of(PRODUCER_1, PRODUCER_2, PRODUCER_3, PRODUCER_4);
}
