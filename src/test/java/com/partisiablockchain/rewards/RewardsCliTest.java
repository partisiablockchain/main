package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.tree.AvlTree;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class RewardsCliTest {
  @TempDir private static File tempDir;
  private static ByteArrayOutputStream outputStream;
  private static File rewardsDir;

  private static final List<BlockchainAddress> producers =
      List.of(
          TestnetAddresses.PRODUCER_1,
          TestnetAddresses.PRODUCER_2,
          TestnetAddresses.PRODUCER_3,
          TestnetAddresses.PRODUCER_4);

  /** Reset output stream before each test. Instantiates test folders with test data. */
  @BeforeEach
  void setUp() {
    outputStream = new ByteArrayOutputStream();
    rewardsDir = new File(tempDir, "rewards");
  }

  /** Rewards can be calculated based on producers, tokenHolders and bpScores. */
  @Test
  void computingRewards() {
    YearMonth month = YearMonth.of(2023, 8);

    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
        AvlTree.create(Map.of(producers.get(0), new StakesFromOthers(100, 0, null)));
    Map<BlockchainAddress, Balance> balances =
        producers.stream()
            .collect(
                Collectors.toMap(
                    p -> p,
                    p ->
                        new BalanceBuilder()
                            .withDelegatedStakesFromOthers(delegatedStakesFromOthers)
                            .build()));
    List<BpScore> scoresForMonth = producers.stream().map(p -> new BpScore(p, 1, 1)).toList();
    TokenHolder tokenholder =
        new TokenHolder(null, 0, 0, 100L, 0, 0, 0, 0, 0, 0, 0, 100L, 0, 100L, 1, 1, 1);
    Map<BlockchainAddress, TokenHolder> tokenHoldersMap =
        producers.stream().collect(Collectors.toMap(p -> p, p -> tokenholder));
    RewardsCli rewardsCli = new RewardsCli(null, null, tempDir);
    List<Reward> rewards =
        rewardsCli.computeRewards(
            month.atDay(1), producers, tokenHoldersMap, scoresForMonth, balances, 800);
    assertThat(rewards).hasSize(producers.size());
    // Convert rewards to long since equality of doubles are complicated
    Map<BlockchainAddress, Long> rewardsAsLong =
        rewards.stream()
            .collect(Collectors.toMap(Reward::identity, reward -> (long) reward.rewards()));
    assertThat(rewardsAsLong)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                producers.get(0), 100L + 2 + 98L * 4,
                producers.get(1), 100L + 2,
                producers.get(2), 100L + 2,
                producers.get(3), 100L + 2L));
  }

  /** Rewards can be calculated for a quarter index. */
  @Test
  void reportsWhenFinished() throws IOException {
    RewardsCli.run(
        new String[] {"6"},
        new PrintStream(outputStream),
        new RewardsClientMock(),
        rewardsDir.getAbsoluteFile());
    assertThat(new File(rewardsDir, "Rewards-2023-W35.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W36.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W37.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W38.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W39.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W40.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W41.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W42.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W43.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W44.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W45.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W46.csv").exists()).isTrue();
    File rewardsFor23W48 = new File(rewardsDir, "Rewards-2023-W47.csv");
    assertThat(rewardsFor23W48.exists()).isTrue();
    List<String> rewardFileContent = Files.readAllLines(rewardsFor23W48.toPath());
    assertThat(rewardFileContent.get(0))
        .contains("Identity;WeeklyRewardInQuarter;SumOfRewardable;RewardableForHolder;Rewards");
    assertThat(rewardFileContent.get(1))
        .contains(producers.get(1).writeAsString() + ";1625603076;3.0;1.0;5.41867692E8");
    assertThat(rewardFileContent.get(2))
        .contains(producers.get(3).writeAsString() + ";1625603076;3.0;1.0;5.41867692E8");
    assertThat(rewardFileContent.get(3))
        .contains(producers.get(0).writeAsString() + ";1625603076;3.0;0.0;0.0");
    assertThat(rewardFileContent.get(4))
        .contains(producers.get(2).writeAsString() + ";1625603076;3.0;1.0;5.41867692E8");
    String output = outputStream.toString(StandardCharsets.UTF_8);
    assertThat(output).contains("Done");

    assertThat(Files.readAllLines(new File(rewardsDir, "Payout-Q6.csv").toPath()))
        .containsExactly(
            "Identity;Rewards",
            producers.get(1).writeAsString() + ";7044279996",
            producers.get(3).writeAsString() + ";7044279996",
            producers.get(2).writeAsString() + ";7044279996");

    String expectedVoteHash = "8e43078bcb6df754a33a7aaae893a41d1d4e1e9939f48ae2de1a844f7e1b4f97";
    assertThat(Files.readAllLines(new File(rewardsDir, "Payout-vote-Q6.txt").toPath()))
        .containsExactly(expectedVoteHash);
    assertThat(output).contains(expectedVoteHash);
  }

  /** Rewards cannot be calculated for first three quarters indices. */
  @Test
  void invalidQuarterIndex() {
    String[] args = {"3"};
    RewardsCli.run(
        args, new PrintStream(outputStream), new RewardsClientImpl("", null), rewardsDir);
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains("Invalid quarter index");
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).doesNotContain("Done");
  }

  /** Calculating rewards writes data to file. */
  @Test
  void writesDataToFile() {
    RewardsCli.run(
        new String[] {"6"}, new PrintStream(outputStream), new RewardsClientMock(), rewardsDir);
    assertThat(new File(rewardsDir, "Producers-2023-W47.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "TokenHolders-2023-W47.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "BpScores-2023-11.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Stakes-2023-W47.csv").exists()).isTrue();
    assertThat(new File(rewardsDir, "Rewards-2023-W47.csv").exists()).isTrue();
  }

  /**
   * Help message is displayed when rewards calculator is called with <code>--help</code> argument.
   */
  @Test
  void helpMessage() {
    String[] args = {"--help"};
    RewardsCli.run(
        args, new PrintStream(outputStream), new RewardsClientImpl("", null), rewardsDir);

    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains(RewardsCli.HELP_MESSAGE);
  }

  /** Help message is displayed when rewards calculator is called with no argument. */
  @Test
  void helpMessageByIncorrectArgumentNumber() {
    String[] args = {"", ""};
    RewardsCli.run(
        args, new PrintStream(outputStream), new RewardsClientImpl("", null), rewardsDir);

    assertThat(outputStream.toString(StandardCharsets.UTF_8)).contains(RewardsCli.HELP_MESSAGE);
  }

  /** Calculating rewards output error message if invalid quarter index input. */
  @Test
  void notNumberQuarterIndex() {
    String[] args = {"abc"};
    RewardsCli.run(
        args, new PrintStream(outputStream), new RewardsClientImpl("", null), rewardsDir);
    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("Quarter index needs to be a number.");
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).doesNotContain("Done");
  }

  /** Rewards calculator outputs message if given quarter has not yet ended. */
  @Test
  void notYetEndedQuarter() {
    RewardsCli cli =
        new RewardsCli(new PrintStream(outputStream), new RewardsClientImpl("", null), rewardsDir);
    cli.export(4, LocalDate.of(2023, 1, 1));
    assertThat(outputStream.toString(StandardCharsets.UTF_8))
        .contains("The quarter has not yet ended");
    assertThat(outputStream.toString(StandardCharsets.UTF_8)).doesNotContain("Done");
  }

  /** Coverage of main. */
  @Test
  void mainWithStringAsArgument() {
    String[] args = {"6"};
    Assertions.assertThatThrownBy(() -> RewardsCli.main(args));
  }

  /**
   * main method returns gracefully when called with '--help'. Mainly here for coverage of the
   * static main method.
   */
  @Test
  void mainCoverage() {
    String[] args = {"--help"};
    RewardsCli.main(args);
  }

  /**
   * The weekly formatter is configured with a locale that uses monday as the first day of the week.
   */
  @Test
  void weeklyFormatPlaceSundayInCorrectWeek() {
    assertThat(RewardsCli.formatWeekly.format(LocalDate.of(2023, Month.SEPTEMBER, 3)))
        .isEqualTo("2023-W35");
    assertThat(RewardsCli.formatWeekly.format(LocalDate.of(2023, Month.SEPTEMBER, 4)))
        .isEqualTo("2023-W36");
  }
}
