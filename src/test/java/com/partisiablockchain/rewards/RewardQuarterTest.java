package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.time.LocalDate;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class RewardQuarterTest {

  /** The sundays for a quarter can be determined. */
  @Test
  void determineSundays_q1() {
    RewardQuarter rewardQuarter = new RewardQuarter(1);
    List<LocalDate> actualSundays = rewardQuarter.determineSundays();
    List<LocalDate> expectedSundays =
        List.of(
            // June 2022
            LocalDate.of(2022, 6, 5),
            LocalDate.of(2022, 6, 12),
            LocalDate.of(2022, 6, 19),
            LocalDate.of(2022, 6, 26),
            // July 2022
            LocalDate.of(2022, 7, 3),
            LocalDate.of(2022, 7, 10),
            LocalDate.of(2022, 7, 17),
            LocalDate.of(2022, 7, 24),
            LocalDate.of(2022, 7, 31),
            // August 2022
            LocalDate.of(2022, 8, 7),
            LocalDate.of(2022, 8, 14),
            LocalDate.of(2022, 8, 21),
            LocalDate.of(2022, 8, 28));
    Assertions.assertThat(actualSundays).isEqualTo(expectedSundays);
  }

  /** The sundays for a quarter can be determined. */
  @Test
  void determineSundays_q2() {
    RewardQuarter rewardQuarter = new RewardQuarter(2);
    List<LocalDate> actualSundays = rewardQuarter.determineSundays();
    List<LocalDate> expectedSundays =
        List.of(
            // September 2022
            LocalDate.of(2022, 9, 4),
            LocalDate.of(2022, 9, 11),
            LocalDate.of(2022, 9, 18),
            LocalDate.of(2022, 9, 25),
            // October 2022
            LocalDate.of(2022, 10, 2),
            LocalDate.of(2022, 10, 9),
            LocalDate.of(2022, 10, 16),
            LocalDate.of(2022, 10, 23),
            LocalDate.of(2022, 10, 30),
            // November 2022
            LocalDate.of(2022, 11, 6),
            LocalDate.of(2022, 11, 13),
            LocalDate.of(2022, 11, 20),
            LocalDate.of(2022, 11, 27));
    Assertions.assertThat(actualSundays).isEqualTo(expectedSundays);
  }

  /** The sundays for a quarter can be determined. */
  @Test
  void determineSundays_q6() {
    RewardQuarter rewardQuarter = new RewardQuarter(6);
    List<LocalDate> actualSundays = rewardQuarter.determineSundays();
    List<LocalDate> expectedSundays =
        List.of(
            // September 2023
            LocalDate.of(2023, 9, 3),
            LocalDate.of(2023, 9, 10),
            LocalDate.of(2023, 9, 17),
            LocalDate.of(2023, 9, 24),
            // October 2023
            LocalDate.of(2023, 10, 1),
            LocalDate.of(2023, 10, 8),
            LocalDate.of(2023, 10, 15),
            LocalDate.of(2023, 10, 22),
            LocalDate.of(2023, 10, 29),
            // November 2023
            LocalDate.of(2023, 11, 5),
            LocalDate.of(2023, 11, 12),
            LocalDate.of(2023, 11, 19),
            LocalDate.of(2023, 11, 26));
    Assertions.assertThat(actualSundays).isEqualTo(expectedSundays);
  }

  @Test
  void determineSundays_q13() {
    RewardQuarter rewardQuarter = new RewardQuarter(13);
    List<LocalDate> actualSundays = rewardQuarter.determineSundays();
    List<LocalDate> expectedSundays =
        List.of(
            // June 2025
            LocalDate.of(2025, 6, 1),
            LocalDate.of(2025, 6, 8),
            LocalDate.of(2025, 6, 15),
            LocalDate.of(2025, 6, 22),
            LocalDate.of(2025, 6, 29),
            // July 2025
            LocalDate.of(2025, 7, 6),
            LocalDate.of(2025, 7, 13),
            LocalDate.of(2025, 7, 20),
            LocalDate.of(2025, 7, 27),
            // August 2025
            LocalDate.of(2025, 8, 3),
            LocalDate.of(2025, 8, 10),
            LocalDate.of(2025, 8, 17),
            LocalDate.of(2025, 8, 24),
            LocalDate.of(2025, 8, 31));
    Assertions.assertThat(actualSundays).isEqualTo(expectedSundays);
  }

  /** A quarter is considered ended if the last day of the quarter is before the current date. */
  @Test
  void determineQuarterEnd() {
    RewardQuarter q1 = new RewardQuarter(1);
    LocalDate q1LastDay = LocalDate.of(2022, 8, 31);
    LocalDate q2firstDay = LocalDate.of(2022, 9, 1);
    Assertions.assertThat(q1.hasEnded(q1LastDay)).isFalse();
    Assertions.assertThat(q1.hasEnded(q2firstDay)).isTrue();

    RewardQuarter q7 = new RewardQuarter(7);
    LocalDate q7LastDay = LocalDate.of(2024, 2, 29);
    LocalDate q8firstDay = LocalDate.of(2024, 3, 1);
    Assertions.assertThat(q7.hasEnded(q7LastDay)).isFalse();
    Assertions.assertThat(q7.hasEnded(q8firstDay)).isTrue();

    Assertions.assertThat(q1.hasEnded(q7LastDay)).isTrue();
    Assertions.assertThat(q1.hasEnded(q8firstDay)).isTrue();
    Assertions.assertThat(q7.hasEnded(q1LastDay)).isFalse();
    Assertions.assertThat(q7.hasEnded(q2firstDay)).isFalse();
  }

  /** The number of tokens available for rewards in a quarter can be determined. */
  @Test
  void determineNumberOfRewards() {
    Assertions.assertThat(new RewardQuarter(1).determineRewards()).isEqualTo(500000);
    Assertions.assertThat(new RewardQuarter(7).determineRewards()).isEqualTo(2476504);
    Assertions.assertThat(new RewardQuarter(40).determineRewards()).isEqualTo(1323406);
  }
}
