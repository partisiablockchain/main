package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.IceStake;
import com.partisiablockchain.rewards.accountplugin.InitialVestingAccount;
import com.partisiablockchain.rewards.accountplugin.StakeDelegation;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.rewards.accountplugin.TransferInformation;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/**
 * ExportTokenHolder builds {@link TokenHolder} for each accounts from a {@link Balance} if the
 * account owns or is delegated MPC tokens.
 */
final class ExportTokenHolderTest {
  private static final LocalDate day = LocalDate.of(2023, 8, 31);
  private static final BlockchainAddress accountAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  /** Stakes delegated to users are added to unlocked stakes. */
  @Test
  void accountWithTransferableMpc() {
    int stakedTokens = 1;
    Balance balance =
        new BalanceBuilder().withStakedTokens(stakedTokens).withMpcTokens(123L).build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.transferable()).isEqualTo(123L);
  }

  /** All pending unstakes are added to total stakes. */
  @Test
  void accountWithPendingUnstakes() {
    int stakedTokens = 1;
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withPendingUnstakes(AvlTree.create(Map.of(1L, 123L, 2L, 456L)))
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.total()).isEqualTo(stakedTokens + 123L + 456L);
  }

  /** Vesting account mpc are added to unlocked mpc. */
  @Test
  void accountWithVestingAccount() {
    int stakedTokens = 1;
    int mpcTokens = 25;
    InitialVestingAccount initialVestingAccount = new InitialVestingAccount(100, 25, 1, 1, 1);
    Balance balance =
        new BalanceBuilder()
            .withMpcTokens(mpcTokens)
            .withStakedTokens(stakedTokens)
            .withVestingAccounts(FixedList.create(List.of(initialVestingAccount)))
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.unlocked()).isEqualTo(100 + stakedTokens);
    assertThat(tokenHolder.unlockedRatio()).isEqualTo(1.0d);
  }

  /** Vesting account mpc are added to unlocked mpc. */
  @Test
  void accountWithVestingAccount2() {
    int stakedTokens = 100;
    Balance balance =
        new BalanceBuilder().withMpcTokens(100).withStakedTokens(stakedTokens).build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.stakeRatio()).isEqualTo(0.5d);
  }

  /** Stakes delegated to users are added to unlocked stakes. */
  @Test
  void accountWithStakesDelegatedToOthers() {
    Balance balance =
        new BalanceBuilder()
            .withDelegatedStakesToOthers(AvlTree.create(Map.of(accountAddress, 123L)))
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.unlocked()).isEqualTo(123L);
  }

  /**
   * Accepted stakes delegated from users are added to stakes. Pending delegated stakes from others
   * are not added.
   */
  @Test
  void accountWithStakesDelegatedFromOthers() {
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
        AvlTree.create(Map.of(accountAddress, new StakesFromOthers(123L, 0L, null)));
    Balance balance =
        new BalanceBuilder().withDelegatedStakesFromOthers(delegatedStakesFromOthers).build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.acceptedFromOthers()).isEqualTo(123);
    assertThat(tokenHolder.stakeForJobs()).isEqualTo(123);
  }

  /** Stakes pending retraction are added to unlocked stakes. */
  @Test
  void accountWithPendingRetractedDelegatedStakes() {
    int stakedTokens = 1;
    AvlTree<Long, Long> pendingRetractedDelegatedStakes = AvlTree.create(Map.of(0L, 123L));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withPendingRetractedDelegatedStakes(pendingRetractedDelegatedStakes)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.unlocked()).isEqualTo(124L);
  }

  /** {@code stakeCapRatio} is calculated based on staked tokens over STAKING_REWARD_CAP. */
  @Test
  void accountWithStakeOverCap() {
    long stakedTokens = (long) (Balance.STAKING_REWARD_CAP + 10000000000L);
    Balance balance = new BalanceBuilder().withStakedTokens(stakedTokens).build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.stakeCapRatio()).isEqualTo(0.8333333333333334);
  }

  /** Delegated stakes is added to in-transit stakes. */
  @Test
  void accountWithDelegatedStakes() {
    int stakedTokens = 1;
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new StakeDelegation(null, 123L, StakeDelegation.DelegationType.DELEGATE_STAKES)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingStakeDelegations(storedPendingStakeDelegations)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(123);
  }

  /** Retracted delegated stakes is added to in-transit stakes. */
  @Test
  void accountWithRetractedDelegatedStakes() {
    int stakedTokens = 1;
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new StakeDelegation(
                    null, 123L, StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingStakeDelegations(storedPendingStakeDelegations)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(123);
  }

  /** Account with irrelevant in-transit delegated stakes is not added as a token-holder. */
  @Test
  void accountWithWrongDelegatedStakes() {
    int stakedTokens = 1;
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new StakeDelegation(
                    null, 123L, StakeDelegation.DelegationType.RETURN_DELEGATED_STAKES)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingStakeDelegations(storedPendingStakeDelegations)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(0);
  }

  /** In transit transfer MPC is added to unlocked and in-transit stakes. */
  @Test
  void accountWithPendingMpcTokens() {
    int stakedTokens = 1;
    AvlTree<Hash, TransferInformation> storedPendingTransfers =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new TransferInformation(Unsigned256.create(123L), false, -1)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingTransfers(storedPendingTransfers)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(123);
    assertThat(tokenHolder.unlocked()).isEqualTo(124L);
  }

  /** In transit transfer MPC that is deposit is not added to in-transit stakes. */
  @Test
  void accountWithPendingMpcTokensAdded() {
    int stakedTokens = 1;
    AvlTree<Hash, TransferInformation> storedPendingTransfers =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new TransferInformation(Unsigned256.create(123L), true, -1)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingTransfers(storedPendingTransfers)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(0);
    assertThat(tokenHolder.unlocked()).isEqualTo(1L);
  }

  /** In transit transfer Byoc is not added to in-transit stakes. */
  @Test
  void accountWithPendingNonMpcTokens() {
    int stakedTokens = 1;
    AvlTree<Hash, TransferInformation> storedPendingTransfers =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new TransferInformation(Unsigned256.create(123L), false, 0)));
    Balance balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingTransfers(storedPendingTransfers)
            .build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    TokenHolder tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(0);

    storedPendingTransfers =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new TransferInformation(Unsigned256.create(123L), true, 0)));
    balance =
        new BalanceBuilder()
            .withStakedTokens(stakedTokens)
            .withStoredPendingTransfers(storedPendingTransfers)
            .build();
    tokenHolderExporter = new ExportTokenHolder(day, Map.of(accountAddress, balance));
    tokenHolder = tokenHolderExporter.exportTokenHolders().get(0);
    assertThat(tokenHolder.inTransit()).isEqualTo(0);
  }

  /** Balances with pending ice stakes is not included in list of token holders. */
  @Test
  void accountWithPendingIceStakes() {
    AvlTree<Hash, IceStake> storedPendingIceStakes =
        AvlTree.create(
            Map.of(
                Hash.create(s -> s.writeString("TransferHash")),
                new IceStake(accountAddress, 123L, IceStake.IceStakeType.ASSOCIATE),
                Hash.create(s -> s.writeString("TransferHash2")),
                new IceStake(accountAddress, 123L, IceStake.IceStakeType.DISASSOCIATE)));
    Balance balance =
        new BalanceBuilder().withStoredPendingIceStakes(storedPendingIceStakes).build();

    ExportTokenHolder tokenHolderExporter =
        new ExportTokenHolder(day, Map.of(accountAddress, balance));
    List<TokenHolder> tokenHolders = tokenHolderExporter.exportTokenHolders();
    assertThat(tokenHolders.size()).isEqualTo(0);
  }
}
