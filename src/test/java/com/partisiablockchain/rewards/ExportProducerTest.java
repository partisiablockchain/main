package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.rest.testing.client.ClientTestFilter;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;

final class ExportProducerTest {
  private static final boolean REGENERATE_REF_FILES = false;

  @Test
  void recordShouldBeDisabled() {
    assertThat(REGENERATE_REF_FILES).isFalse();
  }

  /** Producers can be created from on-chain information. */
  @Test
  public void getProducersFromOnChain() {
    LocalDate lastDayOfLastMonthOfQ1 = LocalDate.of(2022, 8, 31);

    Client replayClient =
        ClientTestFilter.recordReplay(
            ClientBuilder.newClient(),
            Path.of("src/test/resources/replayClient/exportProducer/getProducersFromOnChain"),
            REGENERATE_REF_FILES);
    RewardsClientImpl rewardsClient =
        new RewardsClientImpl(TestnetAddresses.READER_URL, replayClient);
    ExportProducer producerExporter =
        new ExportProducer(rewardsClient, lastDayOfLastMonthOfQ1, rewardsClient.getAllCommittees());
    List<BlockchainAddress> producers = producerExporter.exportProducers();
    assertThat(producers.size()).isEqualTo(4);
    assertThat(producers.get(0)).isEqualTo(TestnetAddresses.PRODUCER_4);
    assertThat(producers.get(3)).isEqualTo(TestnetAddresses.PRODUCER_3);
  }
}
