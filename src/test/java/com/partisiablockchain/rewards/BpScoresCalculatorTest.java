package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/** Test the block producer performance score calculator. */
final class BpScoresCalculatorTest {

  private static final List<BlockchainAddress> PRODUCERS = TestnetAddresses.PRODUCERS;

  /**
   * For incumbent producers the performance score is calculated as the number of blocks produced by
   * the producer in the period divided by the maximum number of blocks produced by any producer in
   * the period.
   */
  @Test
  void incumbentProducersScoreRelativeToTheMostProductive() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(
            PRODUCERS.get(0),
            1611L,
            PRODUCERS.get(1),
            927L,
            PRODUCERS.get(2),
            1831L,
            PRODUCERS.get(3),
            1555L);
    Map<BlockchainAddress, BpScore> bpScores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, PRODUCERS);
    assertThat(bpScores.get(PRODUCERS.get(0)).score()).isEqualTo((double) 1611 / 1831);
    assertThat(bpScores.get(PRODUCERS.get(1)).score()).isEqualTo((double) 927 / 1831);
    assertThat(bpScores.get(PRODUCERS.get(2)).score()).isEqualTo(1.0);
    assertThat(bpScores.get(PRODUCERS.get(3)).score()).isEqualTo((double) 1555 / 1831);
  }

  /**
   * An incumbent producer that has produced the same number of blocks as the most productive
   * producer in the period receives a performance score of 1.0.
   */
  @Test
  void incumbentProducerWithMaxBlocksScoresOne() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(
            PRODUCERS.get(0),
            1831L,
            PRODUCERS.get(1),
            927L,
            PRODUCERS.get(2),
            1831L,
            PRODUCERS.get(3),
            1555L);
    Map<BlockchainAddress, BpScore> bpScores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, PRODUCERS);
    assertThat(bpScores.get(PRODUCERS.get(0)).score()).isEqualTo(1.0);
    assertThat(bpScores.get(PRODUCERS.get(2)).score()).isEqualTo(1.0);
  }

  /**
   * An incumbent producer that has produced no blocks in the period receives a performance score of
   * 0.0.
   */
  @Test
  void incumbentProducerWithZeroBlocksScoresZero() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(
            PRODUCERS.get(0),
            1831L,
            PRODUCERS.get(1),
            0L,
            PRODUCERS.get(2),
            1831L,
            PRODUCERS.get(3),
            1555L);
    Map<BlockchainAddress, BpScore> bpScores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, PRODUCERS);
    assertThat(bpScores.get(PRODUCERS.get(1)).score()).isEqualTo(0.0);
  }

  /**
   * A producer that was not part of the committee in the period will not receive a performance
   * score, even if they did produce blocks in the previous period.
   */
  @Test
  void producerNotInCommitteeGetsNoScore() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(PRODUCERS.get(0), 1831L, PRODUCERS.get(2), 1831L, PRODUCERS.get(3), 1555L);
    Map<BlockchainAddress, BpScore> bpScores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, PRODUCERS);
    assertThat(bpScores.get(PRODUCERS.get(1))).isNull();
  }

  /**
   * Any producer that produced no blocks in the previous period will be regarded as a <i>new
   * producer</i>, and will receive a performance score that is the average of all incumbent
   * producers.
   */
  @Test
  void scoresForMixedProducers() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(
            PRODUCERS.get(0),
            1611L,
            PRODUCERS.get(1),
            927L,
            PRODUCERS.get(2),
            1831L,
            PRODUCERS.get(3),
            1555L);
    List<BlockchainAddress> producersInPreviousPeriod =
        List.of(PRODUCERS.get(0), PRODUCERS.get(1), PRODUCERS.get(3));
    Map<BlockchainAddress, BpScore> scores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, producersInPreviousPeriod);
    double averageIncumbentScore = (1611d / 1611d + 927d / 1611d + 1555d / 1611d) / 3;
    assertThat(scores.get(PRODUCERS.get(2)).score()).isEqualTo(averageIncumbentScore);
  }

  /** If the previous period had zero producers all performance scores are NaN. */
  @Test
  void scoresForOnlyNewProducers() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod =
        Map.of(
            PRODUCERS.get(0),
            1611L,
            PRODUCERS.get(1),
            927L,
            PRODUCERS.get(2),
            1831L,
            PRODUCERS.get(3),
            1555L);
    List<BlockchainAddress> noProducers = List.of();
    Map<BlockchainAddress, BpScore> bpScores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, noProducers);
    assertThat(bpScores.get(PRODUCERS.get(0)).score()).isNaN();
    assertThat(bpScores.get(PRODUCERS.get(1)).score()).isNaN();
    assertThat(bpScores.get(PRODUCERS.get(2)).score()).isNaN();
    assertThat(bpScores.get(PRODUCERS.get(3)).score()).isNaN();
  }

  /** The produced score for new nodes are dependent on the computation order. */
  @Test
  void ensureOrdering() {
    Map<BlockchainAddress, Long> blocksProducedInPeriod = new LinkedHashMap<>();
    blocksProducedInPeriod.put(PRODUCERS.get(0), 3333L);
    blocksProducedInPeriod.put(PRODUCERS.get(1), 1555L);
    blocksProducedInPeriod.put(PRODUCERS.get(2), 1831L);
    blocksProducedInPeriod.put(PRODUCERS.get(3), 927L);

    List<BlockchainAddress> producersInPreviousPeriod =
        List.of(PRODUCERS.get(0), PRODUCERS.get(1), PRODUCERS.get(3));
    Map<BlockchainAddress, BpScore> scores =
        BpScoresCalculator.calculateScores(blocksProducedInPeriod, producersInPreviousPeriod);
    double averageIncumbentScore = (1d + 927d / 3333d + 1555d / 3333d) / 3;
    assertThat(scores.get(PRODUCERS.get(2)).score()).isEqualTo(averageIncumbentScore);

    double averageIncumbentScoreUnsorted = (1d + 1555d / 3333d + 927d / 3333d) / 3;
    assertThat(scores.get(PRODUCERS.get(2)).score()).isNotEqualTo(averageIncumbentScoreUnsorted);
  }

  /** The score for new producers should be stable. Verified using an old score result. */
  @Test
  void consistentOperationOrderOnDoubles() {
    List<String> lines =
        new BufferedReader(
                new InputStreamReader(
                    Objects.requireNonNull(
                        BpScoresCalculatorTest.class.getResourceAsStream("m25_score.csv")),
                    StandardCharsets.UTF_8))
            .lines()
            .toList();
    Map<BlockchainAddress, Long> producedBlocks = new HashMap<>();
    Map<BlockchainAddress, Double> expectedScore = new HashMap<>();
    for (int i = 1; i < lines.size(); i++) {
      String[] split = lines.get(i).split(";", -1);
      producedBlocks.put(BlockchainAddress.fromString(split[0]), Long.parseLong(split[1]));
      expectedScore.put(BlockchainAddress.fromString(split[0]), Double.parseDouble(split[2]));
    }

    List<BlockchainAddress> newProducers =
        List.of(
            BlockchainAddress.fromString("009f1b1ae52e1c0287aa656ca0713efc99329dd620"),
            BlockchainAddress.fromString("002c30b8822473502012d2598aa75a60d98fe6b558"),
            BlockchainAddress.fromString("003787378473179e7544bcf447024c267a1d38d416"),
            BlockchainAddress.fromString("004e08af3ecd7ac62b06fe3a844a693189ede8e3ff"),
            BlockchainAddress.fromString("008e191b8888bb763a1f6083efe7d2686eea69a59a"));
    // Overwrite the expected score since iteration order is not the same as in the previous tool
    newProducers.forEach(id -> expectedScore.put(id, 0.8962792264672191));

    ArrayList<BlockchainAddress> incumbentProducers = new ArrayList<>(producedBlocks.keySet());
    incumbentProducers.removeAll(newProducers);
    Map<BlockchainAddress, BpScore> scores =
        BpScoresCalculator.calculateScores(producedBlocks, incumbentProducers);

    Map<BlockchainAddress, Double> actual =
        scores.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().score()));

    assertThat(actual).containsExactlyInAnyOrderEntriesOf(expectedScore);
  }
}
