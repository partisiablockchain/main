package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.rewards.accountplugin.InitialVestingAccount.PAUSE_RELEASE_END_TIME;
import static com.partisiablockchain.rewards.accountplugin.InitialVestingAccount.PAUSE_RELEASE_START_TIME;
import static com.partisiablockchain.rewards.accountplugin.InitialVestingAccount.PUBLIC_SALE_RELEASE_DURATION;
import static com.partisiablockchain.rewards.accountplugin.InitialVestingAccount.PUBLIC_SALE_RELEASE_INTERVAL;

import com.partisiablockchain.rewards.accountplugin.InitialVestingAccount;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Calculate the amount of tokens to be release from a {@link InitialVestingAccount}. */
final class InitialVestingAccountTest {

  /** Tokens yet to be released from vesting account with no released tokens. */
  @Test
  void noReleasedTokens() {
    long tokens = 100000;
    long releasedTokens = 0;
    long tokenGenerationEvent = 1;
    long releaseDuration = PUBLIC_SALE_RELEASE_DURATION;
    long releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL;
    InitialVestingAccount vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    long releaseTime = releaseDuration + 1;
    long result = vestingAccount.getAmountToRelease(releaseTime);

    Assertions.assertThat(result).isEqualTo(tokens);
  }

  /** Tokens yet to be released from vesting account with one interval passed. */
  @Test
  void oneReleasesInterval() {
    long tokens = 100100;
    long releasedTokens = 100;
    long tokenGenerationEvent = 1;
    long releaseDuration = PUBLIC_SALE_RELEASE_DURATION;
    long releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL;
    InitialVestingAccount vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    long releaseTime = releaseInterval + 1;
    long result = vestingAccount.getAmountToRelease(releaseTime);

    Assertions.assertThat(result)
        .isEqualTo((long) (tokens * (double) releaseInterval / releaseDuration) - releasedTokens);
  }

  /** Tokens yet to be released from vesting account with some released tokens. */
  @Test
  void alreadyReleasedTokens() {
    long tokens = 100000;
    long releasedTokens = 25000;
    long tokenGenerationEvent = 1;
    long releaseDuration = PUBLIC_SALE_RELEASE_DURATION;
    long releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL;
    InitialVestingAccount vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    long releaseTime = releaseDuration + 1;
    long result = vestingAccount.getAmountToRelease(releaseTime);

    Assertions.assertThat(result).isEqualTo(tokens - releasedTokens);
  }

  /** Release pause prevents tokens from being released. */
  @Test
  void releasePauseStopsVestingAccountsFromLeasing() {
    long tokens = 10_0123;
    long releasedTokens = 0;
    long tokenGenerationEvent = PAUSE_RELEASE_START_TIME;
    long releaseDuration = PUBLIC_SALE_RELEASE_DURATION - 1;
    long releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL;
    InitialVestingAccount vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    long result = vestingAccount.getAmountToRelease(PAUSE_RELEASE_START_TIME);

    Assertions.assertThat(result).isEqualTo(0);

    releaseDuration = PUBLIC_SALE_RELEASE_DURATION;
    releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL - 1;
    vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    result = vestingAccount.getAmountToRelease(PAUSE_RELEASE_END_TIME);

    Assertions.assertThat(result).isEqualTo(0);
  }

  /** Release pause does not pause public sale tokens from releasing. */
  @Test
  void releasePauseDoesNotAffectPublicSale() {
    long tokens = 10_0123;
    long releasedTokens = 123;
    long releaseDuration = PUBLIC_SALE_RELEASE_DURATION;
    long releaseInterval = PUBLIC_SALE_RELEASE_INTERVAL;
    long tokenGenerationEvent = PAUSE_RELEASE_END_TIME - releaseDuration;
    InitialVestingAccount vestingAccount =
        new InitialVestingAccount(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    long result = vestingAccount.getAmountToRelease(PAUSE_RELEASE_END_TIME);

    Assertions.assertThat(result).isEqualTo(tokens - releasedTokens);
  }
}
