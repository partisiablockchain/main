package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.rest.testing.client.ClientTestFilter;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.nio.file.Path;
import java.util.Map;
import org.junit.jupiter.api.Test;

final class RewardsClientImplTest {
  private static final boolean REGENERATE_REF_FILES = false;
  private static final String url = "https://node1.testnet.partisiablockchain.com";

  @Test
  void recordShouldBeDisabled() {
    assertThat(REGENERATE_REF_FILES).isFalse();
  }

  /** Committees can be created from on-chain information. */
  @Test
  void getCommittees() {
    Client replayClient =
        ClientTestFilter.recordReplay(
            ClientBuilder.newClient(),
            Path.of("src/test/resources/replayClient/rewardsClientImpl/getCommittees/"),
            REGENERATE_REF_FILES);
    Committees committees = new RewardsClientImpl(url, replayClient).getAllCommittees();
    assertThat(committees.allCommittees().size()).isEqualTo(2);
    assertThat(committees.getCommittee(0L).size()).isEqualTo(4);
    assertThat(committees.getCommittee(0L).get(0)).isEqualTo(TestnetAddresses.PRODUCER_4);
  }

  /** Number of blocks per committee can be created from on-chain information. */
  @Test
  void blocksFromOnChain() {
    Map<RewardsClient.CommitteeProducer, Long> blocksProduced =
        requestBlocks(
            "src/test/resources/replayClient/rewardsClientImpl/blocksFromOnChain/",
            "https://reader.partisiablockchain.com",
            400L);
    assertThat(blocksProduced)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                new RewardsClient.CommitteeProducer(1, (short) 10), 27L,
                new RewardsClient.CommitteeProducer(1, (short) 11), 100L,
                new RewardsClient.CommitteeProducer(1, (short) 12), 100L,
                new RewardsClient.CommitteeProducer(1, (short) 13), 100L,
                new RewardsClient.CommitteeProducer(1, (short) 14), 74L));
  }

  /**
   * Client can get number of blocks produced when requesting the exact number allowed by the API.
   */
  @Test
  void blocksFromOnChainExactInterval() {
    Map<RewardsClient.CommitteeProducer, Long> blocksProduced =
        requestBlocks(
            "src/test/resources/replayClient/rewardsClientImpl/blocksFromOnChainOverMaxInterval/",
            TestnetAddresses.READER_URL,
            999L);

    assertThat(blocksProduced)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                new RewardsClient.CommitteeProducer(1, (short) 0), 200L,
                new RewardsClient.CommitteeProducer(1, (short) 1), 227L,
                new RewardsClient.CommitteeProducer(1, (short) 2), 300L,
                new RewardsClient.CommitteeProducer(1, (short) 3), 273L));
  }

  /**
   * Number of blocks per committee can be created from on-chain information, when getting more
   * blocks than allowed by the API.
   */
  @Test
  void blocksFromOnChainOverMaxInterval() {
    Map<RewardsClient.CommitteeProducer, Long> blocksProduced =
        requestBlocks(
            "src/test/resources/replayClient/rewardsClientImpl/blocksFromOnChainOverMaxInterval/",
            TestnetAddresses.READER_URL,
            1000L);

    assertThat(blocksProduced)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                new RewardsClient.CommitteeProducer(1, (short) 0), 200L,
                new RewardsClient.CommitteeProducer(1, (short) 1), 227L,
                new RewardsClient.CommitteeProducer(1, (short) 2), 300L,
                new RewardsClient.CommitteeProducer(1, (short) 3), 273L + 1L));
  }

  /** The client is able to get blocks from governance shard. */
  @Test
  void canGetBlocksFromGovernance() {
    Client replayClient =
        ClientTestFilter.recordReplay(
            ClientBuilder.newClient(),
            Path.of(
                "src/test/resources/replayClient/rewardsClientImpl/canGetBlocksFromGovernance/"),
            REGENERATE_REF_FILES);
    RewardsClientImpl rewardsClient =
        new RewardsClientImpl(TestnetAddresses.READER_URL, replayClient);

    long expectedFirstBlock = 100L;
    long expectedLastBlock = expectedFirstBlock + 1;
    Map<RewardsClient.CommitteeProducer, Long> producedBlocks =
        rewardsClient.getProducedBlocks(expectedFirstBlock, expectedLastBlock, "Gov");
    assertThat(producedBlocks)
        .containsExactlyInAnyOrderEntriesOf(
            Map.of(
                new RewardsClient.CommitteeProducer(0, (short) -1), 1L,
                new RewardsClient.CommitteeProducer(0, (short) 0), 1L));
  }

  private static Map<RewardsClient.CommitteeProducer, Long> requestBlocks(
      String recordingDirectory, String readerUrl, long numberOfBlocksToFetch) {
    Client replayClient =
        ClientTestFilter.recordReplay(
            ClientBuilder.newClient(), Path.of(recordingDirectory), REGENERATE_REF_FILES);
    RewardsClientImpl rewardsClient = new RewardsClientImpl(readerUrl, replayClient);

    long expectedFirstBlock = 330374L;
    long expectedLastBlock = expectedFirstBlock + numberOfBlocksToFetch;
    return rewardsClient.getProducedBlocks(expectedFirstBlock, expectedLastBlock, "Shard0");
  }
}
