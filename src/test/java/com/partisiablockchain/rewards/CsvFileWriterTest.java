package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.nio.charset.StandardCharsets.UTF_8;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test the CSV file writer. */
final class CsvFileWriterTest extends CloseableTest {

  @TempDir private static File tempDir;

  /**
   * The CSV exporter writes a collection of records to a semicolon separated CSV file using the
   * record field names as headers. Field names are capitalized when used as headers.
   */
  @Test
  void writeRecordsAsCsvFile() throws IOException {

    record Person(int id, String name, double score) {}

    final List<Person> entities =
        List.of(new Person(1, "John Doe", 2.5), new Person(2, "Jane Doe", 3.33));

    File file = new File(tempDir, "persons.csv");
    CsvFileWriter.writeRecords(Person.class, entities, Comparator.comparing(Person::id), file);
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
    register(reader);

    List<String> lines = reader.lines().toList();
    Assertions.assertThat(lines.size()).isEqualTo(3);
    Assertions.assertThat(lines.get(0)).isEqualTo("Id;Name;Score");
    Assertions.assertThat(lines.get(1)).isEqualTo("1;John Doe;2.5");
    Assertions.assertThat(lines.get(2)).isEqualTo("2;Jane Doe;3.33");
  }

  /** The lines of the files are sorted using a customizable comparator. */
  @Test
  void sortLinesByCustomComparator() throws IOException {

    record City(String name, int population) {}

    final List<City> cities =
        List.of(
            new City("New York", 5000000), new City("Aarhus", 250000), new City("Tokyo", 20000000));

    File file = new File(tempDir, "sorted.csv");
    CsvFileWriter.writeRecords(City.class, cities, Comparator.comparing(City::population), file);
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
    register(reader);

    List<String> lines = reader.lines().toList();
    Assertions.assertThat(lines.get(0)).isEqualTo("Name;Population");
    Assertions.assertThat(lines.size()).isEqualTo(4);
    Assertions.assertThat(lines.get(1)).isEqualTo("Aarhus;250000");
    Assertions.assertThat(lines.get(2)).isEqualTo("New York;5000000");
    Assertions.assertThat(lines.get(3)).isEqualTo("Tokyo;20000000");
  }

  /** Blockchain addresses are printed using the normal human-readable form. */
  @Test
  void blockchainAddressesAreHumanReadable() throws IOException {

    record NodeOperator(BlockchainAddress address, long tokens) {}

    final List<NodeOperator> operators =
        List.of(
            new NodeOperator(TestnetAddresses.PRODUCER_1, 12131213L),
            new NodeOperator(TestnetAddresses.PRODUCER_2, 898922L));

    File file = new File(tempDir, "sort.csv");
    CsvFileWriter.writeRecords(
        NodeOperator.class, operators, Comparator.comparing(NodeOperator::tokens), file);
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
    register(reader);

    List<String> lines = reader.lines().toList();
    Assertions.assertThat(lines.get(0)).isEqualTo("Address;Tokens");
    Assertions.assertThat(lines.size()).isEqualTo(3);
    Assertions.assertThat(lines.get(1))
        .isEqualTo("00a44d16e47871a20cdc8b2beb6bb2fe16d1ba8ff1;898922");
    Assertions.assertThat(lines.get(2))
        .isEqualTo("00dd4aa71c78bfea151a0fdb149e4c012720ee0250;12131213");
  }
}
