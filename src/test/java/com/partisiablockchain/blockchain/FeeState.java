package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** Test. */
@Immutable
public final class FeeState implements StateSerializable {
  public final FixedList<AccountCoin> accountCoins;
  public final long mpcTokens;
  public final long stakedTokens;
  public final AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations;
  public final AvlTree<BlockchainAddress, Long> delegatedStakesToOthers;

  public final AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers;
  public final AvlTree<Long, Long> pendingRetractedDelegatedStakes;
  public final FixedList<VestingAccount> vestingAccounts;
  public final AvlTree<Hash, TransferInformation> storedPendingTransfers;
  public final AvlTree<Long, Long> pendingUnstakes;

  @SuppressWarnings("unused")
  FeeState() {
    accountCoins = FixedList.create();
    mpcTokens = 0;
    stakedTokens = 0;
    vestingAccounts = FixedList.create();
    storedPendingTransfers = AvlTree.create();
    storedPendingStakeDelegations = AvlTree.create();
    delegatedStakesToOthers = AvlTree.create();
    delegatedStakesFromOthers = AvlTree.create();
    pendingRetractedDelegatedStakes = AvlTree.create();
    pendingUnstakes = AvlTree.create();
  }

  /**
   * Create a new fee state.
   *
   * @param mpcTokens the amount of tokens
   * @param stakedTokens the amount of staked tokens
   * @param vestingAccounts vesting amounts for different accounts
   */
  public FeeState(
      FixedList<AccountCoin> accountCoins,
      long mpcTokens,
      long stakedTokens,
      FixedList<VestingAccount> vestingAccounts,
      AvlTree<Hash, TransferInformation> storedPendingTransfers,
      AvlTree<Long, Long> pendingUnstakes,
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations,
      AvlTree<BlockchainAddress, Long> delegatedStakesToOthers,
      AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers,
      AvlTree<Long, Long> pendingRetractedDelegatedStakes) {
    this.accountCoins = accountCoins;
    this.mpcTokens = mpcTokens;
    this.stakedTokens = stakedTokens;
    this.vestingAccounts = vestingAccounts;
    this.storedPendingTransfers = storedPendingTransfers;
    this.pendingUnstakes = pendingUnstakes;
    this.storedPendingStakeDelegations = storedPendingStakeDelegations;
    this.delegatedStakesToOthers = delegatedStakesToOthers;
    this.delegatedStakesFromOthers = delegatedStakesFromOthers;
    this.pendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes;
  }

  FeeState addMpcTokens(long tokens) {
    return new FeeState(
        accountCoins,
        mpcTokens + tokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState stakeTokens(long stake) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens + stake,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setVestingAccounts(FixedList<VestingAccount> vestingAccounts) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setStoredPendingTransfers(AvlTree<Hash, TransferInformation> storedPendingTransfers) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setPendingUnstakes(AvlTree<Long, Long> pendingUnstakes) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setStoredPendingStakeDelegations(
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setDelegatedStakesToOthers(AvlTree<BlockchainAddress, Long> delegatedStakesToOthers) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setDelegatedStakesFromOthers(
      AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setPendingRetractedDelegatedStakes(AvlTree<Long, Long> pendingRetractedDelegatedStakes) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }

  FeeState setAccountCoins(FixedList<AccountCoin> accountCoins) {
    return new FeeState(
        accountCoins,
        mpcTokens,
        stakedTokens,
        vestingAccounts,
        storedPendingTransfers,
        pendingUnstakes,
        storedPendingStakeDelegations,
        delegatedStakesToOthers,
        delegatedStakesFromOthers,
        pendingRetractedDelegatedStakes);
  }
}
