package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Test. */
@Immutable
public final class StakesFromOthers implements StateSerializable {

  public final long acceptedDelegatedStakes;
  public final long pendingDelegatedStakes;

  @SuppressWarnings("unused")
  StakesFromOthers() {
    acceptedDelegatedStakes = 0;
    pendingDelegatedStakes = 0;
  }

  private StakesFromOthers(long acceptedDelegatedStakes, long pendingDelegatedStakes) {
    this.acceptedDelegatedStakes = acceptedDelegatedStakes;
    this.pendingDelegatedStakes = pendingDelegatedStakes;
  }

  /**
   * Create stakes from others.
   *
   * @param acceptedDelegatedStakes accepted stakes
   * @param pendingDelegatedStakes pending stakes
   * @return the created object
   */
  public static StakesFromOthers create(long acceptedDelegatedStakes, long pendingDelegatedStakes) {
    return new StakesFromOthers(acceptedDelegatedStakes, pendingDelegatedStakes);
  }
}
