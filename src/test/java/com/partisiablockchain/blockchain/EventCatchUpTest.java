package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainLedgerHelper.createEvent;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.init.ConditionalLoggerCounter;
import com.partisiablockchain.rosetta.TestValues;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.thread.ShutdownHook;
import java.io.File;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/** Test. */
public final class EventCatchUpTest extends CloseableTest {
  private static final KeyPair producer = new KeyPair(BigInteger.ONE);

  @TempDir File root;
  @TempDir File rootTo;

  private static ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> data;
  private static RootDirectory rootDirectory;
  private static RootDirectory rootDirectoryTo;
  private static ConditionalLoggerCounter counter;

  @BeforeEach
  void setup() {
    data = new ConcurrentHashMap<>();
    ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> toData = new ConcurrentHashMap<>();
    rootDirectory = MemoryStorage.createRootDirectory(root, data);
    rootDirectoryTo = MemoryStorage.createRootDirectory(rootTo, toData);
    counter = new ConditionalLoggerCounter();
  }

  @Test
  public void classDefinition() {
    Assertions.assertThat(new EventCatchUp()).isInstanceOf(EventCatchUp.class);
  }

  /** The run method are able to successfully add a pending event. */
  @Test
  public void run() throws Exception {
    final BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                null));
    final BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                "Shard1"));

    BlockchainLedgerHelper.spawnEventToShard0(fromLedger);
    BlockchainLedgerHelper.appendBlock(fromLedger);

    ShutdownHook shutdownHook = new ShutdownHook();
    Map<String, BlockchainLedger> ledgers = new java.util.HashMap<>();
    ledgers.put(null, fromLedger);
    ledgers.put("Shard1", toLedger);
    EventCatchUp.run(shutdownHook, ledgers, counter, 1, 10);
    Thread.sleep(1100);
    shutdownHook.closeables().get(0).close();

    Assertions.assertThat(toLedger.getPendingEvents()).hasSize(1);
    Assertions.assertThat(shutdownHook.closeables()).hasSize(1);
    Assertions.assertThat(counter.getTimesCalled()).isEqualTo(2);
    Assertions.assertThat(counter.getCount()).isEqualTo(0);
  }

  @Test
  public void getAlreadyKnownEventsTest() {
    ExecutableEvent event = createEvent("Shard1", "Shard1", 1L);
    ExecutableEvent event1 = createEvent("Shard1", "Shard1", 2L);
    BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                "Shard1"));
    BlockchainLedgerHelper.addPendingEvent(toLedger, producer, true, event);
    BlockchainLedgerHelper.addPendingEvent(toLedger, producer, true, event1);
    Assertions.assertThat(EventCatchUp.getAlreadyKnownEvents(toLedger, "Shard0"))
        .isEqualTo(Set.of());
    Assertions.assertThat(EventCatchUp.getAlreadyKnownEvents(toLedger, "Shard1"))
        .isEqualTo(Set.of(1L, 2L));
  }

  @Test
  public void checkUpStateNull() {
    BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState("Shard1"),
                "Shard0"));
    BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                "Shard1"));
    Assertions.assertThatThrownBy(() -> EventCatchUp.check(fromLedger, toLedger, counter))
        .isInstanceOf(NullPointerException.class)
        .hasMessage(
            "Cannot invoke \"com.partisiablockchain.blockchain."
                + "ImmutableChainState.getShardNonces()\" because \"chainState\" is null");
    Assertions.assertThat(counter.getCount()).isEqualTo(1);
  }

  @Test
  public void shouldAddEventToQueue() {
    HashSet<Long> missingEvents = new HashSet<>(Set.of(1L, 2L));
    ExecutableEvent event = createEvent("Shard1", "Shard0", 1L);
    Assertions.assertThat(EventCatchUp.shouldAddEventToQueue(missingEvents, event, "Shard0"))
        .isTrue();
    event = createEvent("Shard1", "Shard1", 2L);
    Assertions.assertThat(EventCatchUp.shouldAddEventToQueue(missingEvents, event, "Shard0"))
        .isFalse();
    event = createEvent("Shard1", "Shard0", 3L);
    Assertions.assertThat(EventCatchUp.shouldAddEventToQueue(missingEvents, event, "Shard0"))
        .isFalse();
    event = createEvent("Shard1", "Shard1", 2L);
    Assertions.assertThat(EventCatchUp.shouldAddEventToQueue(missingEvents, event, "Shard0"))
        .isFalse();
  }

  /** EventCatchUp should handle all pending events. */
  @Test
  public void checkup() {
    Consumer<MutableChainState> stateBuilder = BlockchainLedgerHelper.createChainState();
    BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory, BlockchainLedgerHelper.createNetworkConfig(), stateBuilder, null));

    BlockchainLedgerHelper.spawnEventToShard0(fromLedger);
    BlockchainLedgerHelper.spawnEventToShard0(fromLedger);
    BlockchainLedgerHelper.spawnEventToShard0(fromLedger);

    BlockchainLedgerHelper.appendBlock(fromLedger);

    BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                stateBuilder,
                "Shard1"));

    ConditionalLoggerCounter counter = new ConditionalLoggerCounter();
    EventCatchUp.check(fromLedger, toLedger, counter);
    Assertions.assertThat(counter.getCount()).isEqualTo(0);
    int eventsAdded = 3;
    int blocksVisited = 5;
    Assertions.assertThat(counter.getTimesCalled()).isEqualTo(eventsAdded + blocksVisited);
  }

  @Test
  public void checkupTooNewNonce() {
    Consumer<MutableChainState> stateBuilder =
        BlockchainLedgerHelper.createChainState("Shard1", "Shard1");
    BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                stateBuilder,
                "Shard1"));
    BlockchainLedgerHelper.addPendingEvent(
        fromLedger, producer, true, createEvent("Shard1", "Shard1", 1L));
    fromLedger.appendBlock(
        BlockchainLedgerHelper.createBlock(1, producer, fromLedger.latest(), 2L, List.of(), false));

    Assertions.assertThatThrownBy(
            () ->
                EventCatchUp.checkMissingEvents(
                    fromLedger,
                    fromLedger,
                    (logger, condition, message) -> {},
                    new HashSet<>(Set.of(3L))))
        .hasMessageContaining("Unable to find remaining events")
        .hasMessageContaining("3")
        .hasMessageContaining("back to blocktime=1");
  }

  @Test
  public void checkMissingEventsShouldIgnoreIfShardDoesNotMatch() {
    final BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState("Shard0"),
                "Shard1"));
    final BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                "Shard0"));

    SignedTransaction interactWithContractTransaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            fromLedger,
            TestValues.CONTRACT_ONE,
            TestValues.KEY_PAIR_ONE,
            FunctionUtility.noOpConsumer());

    BlockchainLedgerHelper.appendBlock(
        fromLedger, List.of(interactWithContractTransaction), List.of());
    BlockchainLedgerHelper.appendBlock(fromLedger, List.of(), List.of());
    ExecutedState executedState = fromLedger.getChainState().getExecutedState();
    Assertions.assertThat(executedState.getExecutionStatus().size()).isEqualTo(2);
    BlockchainLedgerHelper.appendBlock(fromLedger);

    ConditionalLoggerCounter counter = new ConditionalLoggerCounter();
    Assertions.assertThatThrownBy(
            () ->
                EventCatchUp.checkMissingEvents(
                    fromLedger, toLedger, counter, new HashSet<>(Set.of(1L))))
        .isInstanceOf(NullPointerException.class);

    // Searches 3 blocks and fails to find a state for the 4th being block time -1.
    Assertions.assertThat(counter.getTimesCalled()).isEqualTo(4);
    Assertions.assertThat(counter.getCount()).isEqualTo(1);
  }

  /** When EventCatchup are unable to add a pending event to a ledger it is logged. */
  @Test
  public void checkupNotAdded() {
    BlockchainLedger fromLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectory,
                BlockchainLedgerHelper.createNetworkConfig(),
                BlockchainLedgerHelper.createChainState(),
                null));

    BlockchainLedgerHelper.spawnEventToShard0(fromLedger);
    BlockchainLedgerHelper.appendBlock(fromLedger, List.of(), List.of());

    Consumer<MutableChainState> toStateBuilder =
        mutable ->
            mutable.setPlugin(
                FunctionUtility.noOpBiConsumer(),
                null,
                ChainPluginType.CONSENSUS,
                ConsensusPluginHelper.createJar(),
                SafeDataOutputStream.serialize(TestValues.ACCOUNT_GOV));
    BlockchainLedger toLedger =
        register(
            BlockchainLedgerHelper.createLedger(
                rootDirectoryTo,
                BlockchainLedgerHelper.createNetworkConfig(),
                toStateBuilder,
                "Shard1"));
    EventCatchUp.check(fromLedger, toLedger, counter);

    Assertions.assertThat(toLedger.getPendingEvents()).isEmpty();
    Assertions.assertThat(counter.getCount()).isEqualTo(1);
    Assertions.assertThat(counter.getTimesCalled()).isEqualTo(2);
  }
}
