package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Test. */
@Immutable
public final class TransferInformation implements StateSerializable {
  public final long amount;
  public final boolean addTokensOrCoinsIfTransferSuccessful;
  public final int coinIndex;

  @SuppressWarnings("unused")
  TransferInformation() {
    amount = 0;
    addTokensOrCoinsIfTransferSuccessful = false;
    coinIndex = -1;
  }

  /**
   * Creates a transfer information.
   *
   * @param amount amount
   * @param addTokensOrCoinsIfTransferSuccessful transfer successful
   * @param coinIndex coinIndex, -1 if mpc token
   */
  public TransferInformation(
      long amount, boolean addTokensOrCoinsIfTransferSuccessful, int coinIndex) {
    this.amount = amount;
    this.addTokensOrCoinsIfTransferSuccessful = addTokensOrCoinsIfTransferSuccessful;
    this.coinIndex = coinIndex;
  }
}
