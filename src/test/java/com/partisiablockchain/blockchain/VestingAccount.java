package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Test. */
@Immutable
public final class VestingAccount implements StateSerializable {
  public final long tokens;
  public final long releasedTokens;

  @SuppressWarnings("unused")
  VestingAccount() {
    this.tokens = 0;
    this.releasedTokens = 0;
  }

  /**
   * Create a new vesting account with the supplied tokens and zero released.
   *
   * @param tokens number of tokens
   */
  public VestingAccount(long tokens) {
    this(tokens, 0);
  }

  /**
   * Create a new vesting account.
   *
   * @param tokens vested tokens
   * @param releasedTokens released tokens
   */
  public VestingAccount(long tokens, long releasedTokens) {
    this.tokens = tokens;
    this.releasedTokens = releasedTokens;
  }
}
