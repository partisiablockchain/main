package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import java.util.Arrays;

/** Test. */
public final class DummyConsensusPlugin
    extends BlockchainConsensusPlugin<DummyConsensusGlobal, DummyConsensusLocal> {

  @Override
  public BlockValidation validateLocalBlock(
      DummyConsensusGlobal globalState, DummyConsensusLocal local, FinalBlock block) {
    if (Arrays.equals(block.getFinalizationData(), new byte[] {123})) {
      return BlockValidation.createAccepted(false);
    }
    return BlockValidation.createAccepted(true);
  }

  @Override
  public boolean validateExternalBlock(DummyConsensusGlobal globalState, FinalBlock block) {
    Signature signature =
        SafeDataInputStream.readFully(block.getFinalizationData(), Signature::read);
    BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
    return globalState.getExternalSigner().equals(signer);
  }

  @Override
  public Class<DummyConsensusGlobal> getGlobalStateClass() {
    return DummyConsensusGlobal.class;
  }

  @Override
  public Class<DummyConsensusLocal> getLocalStateClass() {
    return DummyConsensusLocal.class;
  }

  @Override
  public InvokeResult<DummyConsensusGlobal> invokeGlobal(
      PluginContext pluginContext, DummyConsensusGlobal state, byte[] rpc) {
    return null;
  }

  @Override
  public DummyConsensusGlobal migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
    return new DummyConsensusGlobal(BlockchainAddress.read(rpc));
  }

  @Override
  public DummyConsensusLocal migrateLocal(StateAccessor currentLocal) {
    return null;
  }

  @Override
  public DummyConsensusLocal updateForBlock(
      PluginContext pluginContext,
      DummyConsensusGlobal globalState,
      DummyConsensusLocal localState,
      Block block) {
    return null;
  }
}
