package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.RosettaTestHelper;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Test version of account plugin. */
@Immutable
public final class AccountHolderAccountPlugin
    extends BlockchainAccountPlugin<
        GasAndCoinFeePluginGlobal, AccumulatedFees, FeeState, ContractStorage> {

  public static final List<Coin> TEST_COINS =
      List.of(
          new Coin("BYOC_IT_COIN", new Fraction(1, 10)),
          new Coin("BYOC_IT_COIN2", new Fraction(1, 1)));

  @Override
  public long convertNetworkFee(GasAndCoinFeePluginGlobal globalState, long bytes) {
    return bytes;
  }

  @Override
  public PayCostResult<AccumulatedFees, FeeState> payCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    return new PayCostResult<>(localState, 1000000);
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> contractCreated(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      BlockchainAddress address) {
    ContractStorage contractStorage = new ContractStorage();
    return new ContractState<>(contextFreeState, contractStorage);
  }

  @Override
  public AccumulatedFees removeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState) {
    return null;
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateContractGasBalance(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState,
      BlockchainAddress contract,
      long gas) {
    return localState;
  }

  @Override
  public AccumulatedFees registerBlockchainUsage(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      long gas) {
    return null;
  }

  @Override
  public PayServiceFeesResult<ContractState<AccumulatedFees, ContractStorage>> payServiceFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> contractState,
      List<PendingFee> pendingFees) {
    return null;
  }

  @Override
  public AccumulatedFees payInfrastructureFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFree,
      List<PendingFee> pendingFees) {
    return null;
  }

  @Override
  public boolean canCoverCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    return true;
  }

  @Override
  public AccumulatedFees updateForBlock(
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      BlockContext<ContractStorage> context) {
    return null;
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateActiveContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> currentState,
      BlockchainAddress contract,
      long size) {
    return currentState;
  }

  @Override
  public GasAndCoinFeePluginGlobal migrateGlobal(
      StateAccessor currentGlobal, SafeDataInputStream rpc) {
    ExternalCoins externalCoins = new ExternalCoins(FixedList.create(TEST_COINS));
    return new GasAndCoinFeePluginGlobal(externalCoins);
  }

  @Override
  protected AccumulatedFees migrateContextFree(StateAccessor current) {
    return new AccumulatedFees();
  }

  @Override
  protected ContractStorage migrateContract(BlockchainAddress address, StateAccessor current) {
    return new ContractStorage();
  }

  @Override
  protected FeeState migrateAccount(StateAccessor current) {
    return new FeeState();
  }

  @Override
  protected InvokeResult<ContractState<AccumulatedFees, ContractStorage>> invokeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      ContractState<AccumulatedFees, ContractStorage> current,
      BlockchainAddress address,
      byte[] invocationBytes) {
    return null;
  }

  AvlTree<Hash, StakeDelegation> createStoredPendingStakeDelegations(
      SafeDataInputStream fromBytes) {
    int numberOfStoredPendingStakes = fromBytes.readInt();
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations = AvlTree.create();
    for (int i = 0; i < numberOfStoredPendingStakes; i++) {
      long value = fromBytes.readLong();
      int type = fromBytes.readInt();
      StakeDelegation.DelegationType delegationType = StakeDelegation.DelegationType.values()[type];
      int key = i;
      storedPendingStakeDelegations =
          storedPendingStakeDelegations.set(
              Hash.create(h -> h.writeInt(key)), StakeDelegation.create(value, delegationType));
    }
    return storedPendingStakeDelegations;
  }

  AvlTree<BlockchainAddress, Long> createDelegatedStakesToOthers(SafeDataInputStream fromBytes) {
    int numberOfDelegatedStakesToOthers = fromBytes.readInt();
    AvlTree<BlockchainAddress, Long> delegatedStakesToOthers = AvlTree.create();
    for (int i = 0; i < numberOfDelegatedStakesToOthers; i++) {
      long stakeAmount = fromBytes.readLong();
      delegatedStakesToOthers =
          delegatedStakesToOthers.set(
              BlockchainAddress.fromHash(
                  BlockchainAddress.Type.ACCOUNT,
                  Hash.create(h -> h.writeString("someAddress" + stakeAmount))),
              stakeAmount);
    }
    return delegatedStakesToOthers;
  }

  AvlTree<BlockchainAddress, StakesFromOthers> createDelegatedStakesFromOthers(
      SafeDataInputStream rpc) {
    int numberOfDelegatedStakesFromOthers = rpc.readInt();
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers = AvlTree.create();
    for (int i = 0; i < numberOfDelegatedStakesFromOthers; i++) {
      delegatedStakesFromOthers =
          delegatedStakesFromOthers.set(
              BlockchainAddress.read(rpc), StakesFromOthers.create(rpc.readLong(), rpc.readLong()));
    }
    return delegatedStakesFromOthers;
  }

  AvlTree<Long, Long> createPendingRetractedDelegatedStakes(SafeDataInputStream fromBytes) {
    int n = fromBytes.readInt();
    AvlTree<Long, Long> pendingRetractedDelegatedStakes = AvlTree.create();
    for (int i = 0; i < n; i++) {
      long j = fromBytes.readLong();
      pendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes.set((long) i, j);
    }
    return pendingRetractedDelegatedStakes;
  }

  Map<Hash, TransferInformation> createStoredPendingTransfers(SafeDataInputStream rpc) {
    int numberOfPendingTransfers = rpc.readInt();
    Map<Hash, TransferInformation> storedPendingTransfers = new HashMap<>();
    for (int i = 0; i < numberOfPendingTransfers; i++) {
      storedPendingTransfers.put(
          Hash.read(rpc),
          new TransferInformation(rpc.readLong(), rpc.readBoolean(), rpc.readInt()));
    }
    return storedPendingTransfers;
  }

  Map<Long, Long> createPendingUnstakes(SafeDataInputStream fromBytes) {
    int n = fromBytes.readInt();
    Map<Long, Long> pendingUnstakes = new HashMap<>();
    for (int i = 0; i < n; i++) {
      pendingUnstakes.put(fromBytes.readLong(), fromBytes.readLong());
    }
    return pendingUnstakes;
  }

  List<AccountCoin> createAccountCoins(SafeDataInputStream rpc) {
    int numberOfCoins = rpc.readInt();
    List<AccountCoin> accountCoins = new ArrayList<>();
    for (int i = 0; i < numberOfCoins; i++) {
      accountCoins.add(new AccountCoin(Unsigned256.read(rpc)));
    }
    return accountCoins;
  }

  List<VestingAccount> createVestingAccounts(SafeDataInputStream rpc) {
    int numberOfVestingAccounts = rpc.readInt();
    List<VestingAccount> vestingAccounts = new ArrayList<>();
    for (int i = 0; i < numberOfVestingAccounts; i++) {
      vestingAccounts.add(new VestingAccount(rpc.readLong()));
    }
    return vestingAccounts;
  }

  @Override
  protected InvokeResult<AccountState<AccumulatedFees, FeeState>> invokeAccount(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccountState<AccumulatedFees, FeeState> current,
      byte[] rpcBytes) {
    SafeDataInputStream rpc = SafeDataInputStream.createFromBytes(rpcBytes);
    int invocationByte = rpc.readUnsignedByte();

    if (invocationByte == 3) {
      final long tokens = rpc.readLong();
      if (tokens < 0) {
        throw new RuntimeException("Unable to deduct negative amount of tokens");
      }
    }

    if (invocationByte == RosettaTestHelper.DummyInvocations.MINT) {
      final long tokens = rpc.readLong();
      final long stakeTokens = rpc.readLong();

      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          createStoredPendingStakeDelegations(rpc);
      AvlTree<BlockchainAddress, Long> delegatedStakesToOthers = createDelegatedStakesToOthers(rpc);
      AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
          createDelegatedStakesFromOthers(rpc);
      AvlTree<Long, Long> pendingRetractedDelegatedStakes =
          createPendingRetractedDelegatedStakes(rpc);
      List<VestingAccount> vestingAccounts = createVestingAccounts(rpc);
      Map<Hash, TransferInformation> storedPendingTransfers = createStoredPendingTransfers(rpc);
      Map<Long, Long> pendingUnstakes = createPendingUnstakes(rpc);
      List<AccountCoin> accountCoins = createAccountCoins(rpc);

      FeeState account = current.account == null ? new FeeState() : current.account;
      return new InvokeResult<>(
          new AccountState<>(
              current.local,
              account
                  .addMpcTokens(tokens)
                  .stakeTokens(stakeTokens)
                  .setVestingAccounts(FixedList.create(vestingAccounts))
                  .setStoredPendingTransfers(AvlTree.create(storedPendingTransfers))
                  .setPendingUnstakes(AvlTree.create(pendingUnstakes))
                  .setStoredPendingStakeDelegations(storedPendingStakeDelegations)
                  .setDelegatedStakesToOthers(delegatedStakesToOthers)
                  .setDelegatedStakesFromOthers(delegatedStakesFromOthers)
                  .setPendingRetractedDelegatedStakes(pendingRetractedDelegatedStakes)
                  .setAccountCoins(FixedList.create(accountCoins))),
          new byte[0]);
    } else {
      return new InvokeResult<>(current, new byte[0]);
    }
  }

  @Override
  protected InvokeResult<AccumulatedFees> invokeContextFree(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccumulatedFees contextFree,
      byte[] rpc) {
    return null;
  }

  @Override
  protected AccumulatedFees payByocFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      Unsigned256 amount,
      String symbol,
      FixedList<BlockchainAddress> nodes) {
    return null;
  }

  @Override
  public InvokeResult<GasAndCoinFeePluginGlobal> invokeGlobal(
      PluginContext pluginContext, GasAndCoinFeePluginGlobal state, byte[] invocationBytes) {
    return null;
  }

  @Override
  public List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of(AccumulatedFees.class, FeeState.class, ContractStorage.class);
  }

  @Override
  public Class<GasAndCoinFeePluginGlobal> getGlobalStateClass() {
    return GasAndCoinFeePluginGlobal.class;
  }
}
