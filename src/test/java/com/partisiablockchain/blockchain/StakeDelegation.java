package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializableInline;

/** Stores information of pending delegation of stakes. */
@Immutable
public final class StakeDelegation implements StateSerializableInline {
  final long amount;
  final DelegationType delegationType;

  @SuppressWarnings("unused")
  StakeDelegation() {
    this.amount = 0;
    this.delegationType = null;
  }

  private StakeDelegation(long amount, DelegationType delegationType) {
    this.amount = amount;
    this.delegationType = delegationType;
  }

  /**
   * Create a pending stake delegation.
   *
   * @param amount stakes tokens
   * @param delegationType type of delegation
   * @return the created delegation
   */
  public static StakeDelegation create(long amount, DelegationType delegationType) {
    return new StakeDelegation(amount, delegationType);
  }

  public long getAmount() {
    return amount;
  }

  public DelegationType getDelegationType() {
    return delegationType;
  }

  /** The different types of delegation of stakes. */
  public enum DelegationType {
    /** Delegate stakes to another account. */
    DELEGATE_STAKES,
    /** Retract delegated stakes from another account. */
    RETRACT_DELEGATED_STAKES,
    /** Receive delegated stakes from another account. */
    RECEIVE_DELEGATED_STAKES,
    /** Return delegated stakes to the account that delegated them. */
    RETURN_DELEGATED_STAKES
  }
}
