package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/** Test. */
public final class StateHelper {

  /** Add the supplied events to the executed state. */
  public static ImmutableChainState withEvent(
      MutableChainState state, String chainId, String subChainId, Hash... events) {
    AvlTree<Hash, Hash> spawnedEvents =
        AvlTree.create(
            Arrays.stream(events).collect(Collectors.toMap(Function.identity(), Hash::create)));
    return state.asImmutable(chainId, subChainId, spawnedEvents, AvlTree.create());
  }

  /** Create a new mutable chain state with the given storage and populate function. */
  public static MutableChainState mutableFromPopulate(
      StateStorage storage, Consumer<MutableChainState> populate) {
    ChainStateCache context = new ChainStateCache(storage);
    MutableChainState empty = MutableChainState.emptyMaster(context);
    populate.accept(empty);
    return empty;
  }

  /** Create the initial chain state. */
  public static void initial(MutableChainState state) {
    byte[] rpc = new byte[0];
    state.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        FeePluginHelper.createJar(),
        rpc);

    state.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        ConsensusPluginHelper.createJar(),
        SafeDataOutputStream.serialize(new KeyPair(BigInteger.ONE).getPublic().createAddress()));
    state.createAccount(new KeyPair(BigInteger.ONE).getPublic().createAddress());
  }
}
