package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.Block.GENESIS_PARENT;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.rosetta.RosettaTestHelper;
import com.partisiablockchain.rosetta.TestValues;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.server.BetanetAddresses;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.jarutil.PomDependencies;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;

/** Helper class to create BlockchainLedger, block events etc. */
public final class BlockchainLedgerHelper {

  private static final PomDependencies pom = PomDependencies.readFromPom();
  private static final AtomicInteger port = new AtomicInteger(10000);

  /**
   * Get an unused port.
   *
   * @return a port.
   */
  public static int getFreshPort() {
    return port.getAndAdd(100);
  }

  /**
   * Creates a ledger with an initial production time.
   *
   * @param rootDataDirectory its root directory
   * @param network network
   * @param stateBuilder genesis state
   * @param subChain ledgers sub chain
   * @param productionTime production time
   * @return blockchain ledger
   */
  public static BlockchainLedger createLedgerWithInitialProductionTime(
      RootDirectory rootDataDirectory,
      NetworkConfig network,
      Consumer<MutableChainState> stateBuilder,
      String subChain,
      long productionTime) {

    return BlockchainLedger.createBlockChain(
        network,
        rootDataDirectory,
        setUpGenesis("ChainId", subChain, stateBuilder, productionTime));
  }

  /**
   * Returns a blockchain ledger.
   *
   * @param rootDataDirectory the ledgers root directory
   * @param network network
   * @param stateBuilder state builder for genesis state
   * @param subChain ledgers sub chain
   * @return blockchain ledger
   */
  public static BlockchainLedger createLedger(
      RootDirectory rootDataDirectory,
      NetworkConfig network,
      Consumer<MutableChainState> stateBuilder,
      String subChain) {

    String genesis = setUpGenesis("ChainId", subChain, stateBuilder, 0);
    return BlockchainLedger.createBlockChain(network, rootDataDirectory, genesis);
  }

  /**
   * Creates a genesis file.
   *
   * @param chainId chain id
   * @param subChain sub chain
   * @param stateBuilder state builder
   * @param productionTime start production time for genesis
   * @return path to genesis file
   */
  public static String setUpGenesis(
      String chainId,
      String subChain,
      Consumer<MutableChainState> stateBuilder,
      long productionTime) {
    File genesis = ExceptionConverter.call(() -> File.createTempFile("genesis", ".zip"), "");

    GenesisStorage storage = new GenesisStorage();
    ChainStateCache context = new ChainStateCache(storage);

    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    stateBuilder.accept(genesisState);
    Hash stateHash =
        genesisState
            .asImmutable(chainId, subChain, AvlTree.create(), AvlTree.create())
            .saveExecutedState(
                s -> {
                  StateSerializer stateSerializer = new StateSerializer(storage, true);
                  return stateSerializer.write(s, ExecutedState.class).hash();
                });
    Block block =
        new Block(productionTime, 0L, 2L, GENESIS_PARENT, stateHash, List.of(), List.of());
    storage.persistGenesis(genesis, new FinalBlock(block, new byte[0]));
    return genesis.getAbsolutePath();
  }

  private static Block createBlock(ImmutableChainState state) {
    return new Block(
        System.currentTimeMillis(),
        3L,
        2L,
        Hash.create(s -> {}),
        state.getHash(),
        List.of(),
        List.of());
  }

  private static Block createBlock(
      long blockTime, long committeeId, Hash identifier, Hash state, List<Hash> events) {
    return new Block(
        System.currentTimeMillis(), blockTime, committeeId, identifier, state, events, List.of());
  }

  /**
   * Creates a final block.
   *
   * @param blockTime block time
   * @param initialProducer initial producers
   * @param block the block in the final block
   * @param committeeId committee id
   * @param events list of events.
   * @return final block
   */
  public static FinalBlock createBlock(
      long blockTime, KeyPair initialProducer, Block block, long committeeId, List<Hash> events) {
    Block returnBlock =
        createBlock(blockTime, committeeId, block.identifier(), block.getState(), events);
    return new FinalBlock(
        returnBlock,
        SafeDataOutputStream.serialize(initialProducer.sign(returnBlock.identifier())));
  }

  /**
   * Creates a final block.
   *
   * @param blockTime block time
   * @param initialProducer initial producers
   * @param latest latest block and state
   * @param committeeId committee id
   * @param events list of events.
   * @param canRollback can the block roll back
   * @return final block
   */
  public static FinalBlock createBlock(
      long blockTime,
      KeyPair initialProducer,
      BlockchainLedger.BlockAndState latest,
      long committeeId,
      List<Hash> events,
      boolean canRollback) {
    Block block =
        createBlock(
            blockTime,
            committeeId,
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            events);
    if (canRollback) {
      return new FinalBlock(
          block, SafeDataOutputStream.serialize(initialProducer.sign(block.identifier())));
    }
    return new FinalBlock(block, new byte[] {123});
  }

  /**
   * Creates a final block.
   *
   * @param initialProducer initial producers
   * @param latest latest block and state
   * @param committeeId committe id
   * @param events events
   * @return final block
   */
  public static FinalBlock createBlock(
      KeyPair initialProducer,
      BlockchainLedger.BlockAndState latest,
      long committeeId,
      List<Hash> events) {
    Block block =
        createBlock(
            latest.getBlock().getBlockTime() + 1,
            committeeId,
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            events);
    return new FinalBlock(
        block, SafeDataOutputStream.serialize(initialProducer.sign(block.identifier())));
  }

  /**
   * Creates a list of floodable events.
   *
   * @param chainId chain id
   * @param producer producer
   * @param sendingShard sending shard
   * @param events events to create floodable events from
   * @return list of floodable events.
   */
  public static List<FloodableEvent> createFloodableEvent(
      String chainId, KeyPair producer, String sendingShard, ExecutableEvent... events) {
    StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState state =
        StateHelper.withEvent(
            MutableChainState.emptyMaster(new ChainStateCache(storage)),
            chainId,
            sendingShard,
            Arrays.stream(events).map(ExecutableEvent::identifier).toArray(Hash[]::new));
    state.saveExecutedState(
        s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());

    Block newBlock = createBlock(state);
    FinalBlock finalBlock =
        new FinalBlock(
            newBlock, SafeDataOutputStream.serialize(producer.sign(newBlock.identifier())));
    List<FloodableEvent> floodableEvents = new ArrayList<>();
    for (ExecutableEvent event : events) {
      floodableEvents.add(FloodableEvent.create(event, finalBlock, storage));
    }
    return floodableEvents;
  }

  /**
   * Adds a pending event to the ledger.
   *
   * @param ledger the ledger
   * @param producer producer to sign
   * @param result expected result
   * @param events events to add.
   */
  public static void addPendingEvent(
      BlockchainLedger ledger, KeyPair producer, boolean result, ExecutableEvent... events) {
    addPendingEvent(ledger.getChainId(), ledger, producer, result, "Shard1", events);
  }

  private static void addPendingEvent(
      String chainId,
      BlockchainLedger ledger,
      KeyPair producer,
      boolean result,
      String sendingShard,
      ExecutableEvent... events) {
    List<FloodableEvent> floodableEvents =
        createFloodableEvent(chainId, producer, sendingShard, events);
    for (FloodableEvent fe : floodableEvents) {
      Assertions.assertThat(ledger.addPendingEvent(fe)).isEqualTo(result);
    }
  }

  /**
   * Returns a network config.
   *
   * @return network config.
   */
  public static NetworkConfig createNetworkConfig() {
    Address address = Address.parseAddress("someAddress:" + getFreshPort());
    return new NetworkConfig(address, () -> address, address);
  }

  /**
   * Returns a consumer of mutable chain state, with dummy routing, consensus and account plugins.
   *
   * @param routeToShard Increase the outbound count for given shard.
   * @return mutable chain state
   */
  public static Consumer<MutableChainState> createChainState(String... routeToShard) {
    return createChainState(List.of(), routeToShard);
  }

  /**
   * Returns a consumer of mutable chain state, with dummy routing, consensus and account plugins
   * and enabled features.
   *
   * @param features to enable
   * @param routeToShard Increase the outbound count for given shard
   * @return mutable chain state
   */
  public static Consumer<MutableChainState> createChainState(
      List<String> features, String... routeToShard) {
    return mutable -> {
      addRouting(mutable, routeToShard);
      addPlugins(mutable);
      createAccountsAndContracts(mutable);
      clearSync(mutable);
      addFeatures(mutable, features);
    };
  }

  private static void addFeatures(MutableChainState mutable, List<String> features) {
    for (String feature : features) {
      mutable.setFeature(feature, "OK");
    }
  }

  /**
   * Add routing to a chain state.
   *
   * @param mutable the state
   * @param routeToShard the routing
   */
  public static void addRouting(MutableChainState mutable, String... routeToShard) {
    for (String shard : routeToShard) {
      mutable.routeToShard(shard);
    }
    mutable.addActiveShard(FunctionUtility.noOpBiConsumer(), "Shard1", "Shard1");
  }

  /**
   * Add some default plugins to a chain state.
   *
   * @param mutable the state
   */
  public static void addPlugins(MutableChainState mutable) {
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        ConsensusPluginHelper.createJar(),
        SafeDataOutputStream.serialize(TestValues.KEY_PAIR_ONE.getPublic().createAddress()));
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        FeePluginHelper.createJar(),
        new byte[0]);
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ROUTING,
        RoutingPluginHelper.createJar(),
        new byte[0]);
  }

  /**
   * Add some default accounts and contracts to a chain state.
   *
   * @param mutable the state
   */
  public static void createAccountsAndContracts(MutableChainState mutable) {
    mutable.createAccount(TestValues.KEY_PAIR_ONE.getPublic().createAddress());
    mutable.createAccount(TestValues.ACCOUNT_ONE);
    mutable.createAccount(TestValues.ACCOUNT_TWO);
    mutable.createAccount(TestValues.ACCOUNT_THREE);
    mutable.createAccount(TestValues.ACCOUNT_GOV);

    byte[] mintJar = JarBuilder.buildJar(RosettaTestHelper.TestTokenMinterContract.class);
    createContract(TestValues.CONTRACT_MINT, () -> mintJar, () -> null, mutable);
    byte[] mpcTokenJar = JarBuilder.buildJar(RosettaTestHelper.TestMpcTokenContract.class);
    createContract(BetanetAddresses.MPC_TOKEN_CONTRACT, () -> mpcTokenJar, () -> null, mutable);
    byte[] mpc20TokenJar = JarBuilder.buildJar(RosettaTestHelper.TestMpcTokenMpc20Contract.class);
    createContract(
        BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT, () -> mpc20TokenJar, () -> null, mutable);
  }

  private static void clearSync(MutableChainState builder) {
    for (String activeShard : builder.getActiveShards()) {
      clearSync(builder, activeShard);
    }
    clearSync(builder, null);
  }

  private static void clearSync(MutableChainState builder, String activeShard) {
    ShardNonces shardNonce = builder.getShardNonce(activeShard);
    if (shardNonce.isMissingSync()) {
      builder.incomingSync(activeShard, new SyncEvent(List.of(), List.of(), List.of()));
    }
  }

  /**
   * Creates a contract on the blockchain ledgers mutable chain state.
   *
   * @param address address
   * @param contractJar contract jar
   * @param initialState initial state of the contract
   * @param state blockchain ledgers mutable chain state.
   */
  public static void createContract(
      BlockchainAddress address,
      Supplier<byte[]> contractJar,
      Supplier<StateSerializable> initialState,
      MutableChainState state) {
    byte[] jar = contractJar.get();
    Hash contractIdentifier = state.saveJar(jar);
    Hash abiIdentifier = state.saveJar(new byte[0]);
    CoreContractState contractState =
        CoreContractState.create(
            state.saveJar(getBinder(address.getType())),
            contractIdentifier,
            abiIdentifier,
            jar.length);
    state.createContract(address, contractState);
    ContractState stateContract = state.getContract(address);
    SerializationResult write = state.serializer.write(initialState.get());
    state.setContract(address, stateContract.withHashAndSize(write.hash(), write.totalByteCount()));
    state.contractCreated(address, 0);
  }

  private static byte[] getBinder(BlockchainAddress.Type type) {
    byte[] sysBinder =
        pom.lookup("com.partisiablockchain.governance", "sys-binder").readFromRepository();
    if (type == BlockchainAddress.Type.CONTRACT_SYSTEM) {
      return sysBinder;
    } else if (type == BlockchainAddress.Type.CONTRACT_GOV) {
      return sysBinder;
    } else {
      throw new IllegalArgumentException("Unable to handle contract type: " + type);
    }
  }

  /**
   * Writes an event to a given storage.
   *
   * @param storage the storage
   * @param storageString where to find storage
   * @param event the event to write
   */
  public static void writeToStorage(
      ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> storage,
      String storageString,
      ExecutableEvent event) {
    storage.put(
        new MemoryStorage.PathAndHash(MemoryStorage.getPath(storageString), event.identifier()),
        SafeDataOutputStream.serialize(event::write));
  }

  /**
   * Creates an event.
   *
   * @param originShard origin shard
   * @param destination destination shard.
   * @param nonce nonce
   * @return event.
   */
  public static ExecutableEvent createEvent(String originShard, String destination, long nonce) {
    return createEvent(
        originShard,
        destination,
        nonce,
        BlockchainAddress.fromString("000000000000000000000000000000000000000004"),
        BlockchainAddress.fromString("02A000000000000000000000000000000000000002"));
  }

  private static ExecutableEvent createEvent(
      String originShard,
      String destination,
      long nonce,
      BlockchainAddress sender,
      BlockchainAddress recipient) {
    return new ExecutableEvent(
        originShard,
        EventTransaction.createStandalone(
            Hash.create(s -> {}),
            sender,
            0L,
            InteractWithContractTransaction.create(recipient, new byte[0]),
            new ShardRoute(destination, nonce),
            0L,
            0L));
  }

  static FinalBlock createFinalBlock(Block block) {
    byte[] signature =
        SafeDataOutputStream.serialize(TestValues.KEY_PAIR_ONE.sign(block.identifier())::write);
    return new FinalBlock(block, signature);
  }

  /**
   * Appends a block to the ledger.
   *
   * @param ledger to append block to
   * @param transactions list of transactions in the block
   * @param events list of events in the block
   */
  public static void appendBlock(
      BlockchainLedger ledger, List<SignedTransaction> transactions, List<FloodableEvent> events) {
    Hash stateHash;
    Block latestBlock;
    if (ledger.getProposals().size() > 0) {
      BlockchainLedger.BlockAndState blockAndState = ledger.getProposals().get(0);
      stateHash = blockAndState.getState().getHash();
      latestBlock = blockAndState.getBlock();
    } else {
      stateHash = ledger.getChainState().getHash();
      latestBlock = ledger.getLatestBlock();
    }
    Block block =
        new Block(
            Math.max(System.currentTimeMillis(), latestBlock.getProductionTime()) + 1,
            latestBlock.getBlockTime() + 1,
            2,
            latestBlock.identifier(),
            stateHash,
            events.stream()
                .map(event -> event.getExecutableEvent().identifier())
                .collect(Collectors.toList()),
            transactions.stream().map(SignedTransaction::identifier).collect(Collectors.toList()));
    for (SignedTransaction signedTransaction : transactions) {
      ledger.addPendingTransaction(signedTransaction);
    }
    for (FloodableEvent event : events) {
      ledger.addPendingEvent(event);
    }
    ledger.appendBlock(createFinalBlock(block));
    Assertions.assertThat(
            ledger.getPossibleHeads().stream().map(BlockAndStateWithParent::currentBlock).toList())
        .describedAs("Block was not appended to the ledger")
        .contains(block);
  }

  /**
   * Appends block to the ledger, with empty transactions and events.
   *
   * @param ledger to append block to
   */
  public static void appendBlock(BlockchainLedger ledger) {
    appendBlock(ledger, List.of(), List.of());
  }

  /**
   * Creates a signed transaction to the contract, on the given ledger by the sender.
   *
   * @param ledger transaction being sent on
   * @param contract to send transaction to
   * @param sender of transaction
   * @param rpc transaction rpc
   * @return signed transaction
   */
  public static SignedTransaction createInteractWithContractTransaction(
      BlockchainLedger ledger,
      BlockchainAddress contract,
      KeyPair sender,
      Consumer<SafeDataOutputStream> rpc) {
    CoreTransactionPart core =
        CoreTransactionPart.create(
            ledger.getAccountState(sender.getPublic().createAddress()).getNonce(),
            Long.MAX_VALUE / 10,
            100_000);

    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contract, SafeDataOutputStream.serialize(rpc));
    return SignedTransaction.create(core, interact).sign(sender, ledger.getChainId());
  }

  /**
   * Append a block on the governance shard with a transaction that targets a contract on shard1.
   * Appends an additional empty block to finalize the transaction.
   *
   * @param fromLedger the governance ledger
   */
  public static void spawnEventToShard0(BlockchainLedger fromLedger) {
    SignedTransaction interactWithContractTransaction =
        createInteractWithContractTransaction(
            fromLedger,
            TestValues.CONTRACT_ONE,
            TestValues.KEY_ACCOUNT_GOV,
            FunctionUtility.noOpConsumer());
    appendBlock(fromLedger, List.of(interactWithContractTransaction), List.of());
    appendBlock(fromLedger, List.of(), List.of());
    ExecutedState executedState = fromLedger.getChainState().getExecutedState();
    Assertions.assertThat(executedState.getExecutionStatus().size()).isEqualTo(1);
  }
}
