package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.configgenerator.RegisterNodeCli.AuthenticationType;
import com.partisiablockchain.crypto.Signature;
import com.secata.tools.rest.ObjectMapperProvider;
import jakarta.ws.rs.client.Client;
import java.io.IOException;
import java.math.BigInteger;
import org.bouncycastle.util.encoders.Hex;

/** Download and parse information from Synaps. */
final class Synaps {

  private static final String SESSION_ID_HEADER = "Session-Id";
  private static final String STATUS_APPROVED = "APPROVED";

  static final class Invocation {

    static final int KYB_APPROVE = 1;
    static final int KYC_SUBMIT = 3;

    private Invocation() {}
  }

  /** Response from Synaps server. */
  @JsonIgnoreProperties(ignoreUnknown = true)
  private static final class SynapsResponse {

    public Inner result;
    public String error;
    public String status;

    boolean isApproved() {
      return STATUS_APPROVED.equals(status);
    }

    boolean hasError() {
      return error != null;
    }

    /** Inner content of response from Synaps server. */
    private static final class Inner {

      public String message;
      public String signature;
    }
  }

  /** KYC/KYB information from Synaps. */
  @JsonIgnoreProperties(ignoreUnknown = true)
  static final class AuthenticationData {

    public String name;
    public String address;
    public String city;

    @JsonProperty("country_numeric")
    public int countryNumeric;

    @JsonProperty("partisia_blockchain_address")
    public String blockchainAddress;

    /**
     * Return authenticated entity's country name.
     *
     * @return the country code of the authenticated entity
     */
    String getCountryName() throws IOException {
      CountryCodes countryCodes = new CountryCodes();
      return countryCodes.nameFromCode(countryNumeric);
    }
  }

  record SynapsResult(
      Signature signature, AuthenticationData authenticationData, String synapsJson, String error) {
    boolean hasError() {
      return error != null;
    }
  }

  /**
   * Deserialize a signature from a hex-encoded string.
   *
   * @param signatureInHex the signature string
   * @return matching signature
   */
  static Signature readSignatureFromHexString(String signatureInHex) {
    if (signatureInHex.length() == 65 * 2) {
      byte[] bytes = Hex.decode(signatureInHex);

      byte recoveryId = bytes[64];
      BigInteger valueR = new BigInteger(1, bytes, 0, 32);
      BigInteger valueS = new BigInteger(1, bytes, 32, 32);

      return new Signature(recoveryId, valueR, valueS);
    } else {
      return null;
    }
  }

  /**
   * Parse response from Synaps.
   *
   * @param responseJson the string response from Synaps
   * @return parsed response
   */
  static SynapsResult parseResponse(String responseJson, AuthenticationType authType)
      throws IOException {
    ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
    SynapsResponse response = mapper.readValue(responseJson, SynapsResponse.class);

    if (response.hasError()) {
      return new SynapsResult(null, null, null, response.error);
    } else if (authType == AuthenticationType.KYB) {
      Signature signature = readSignatureFromHexString(response.result.signature);
      if (signature == null) {
        return createInvalidKybResponse();
      } else {
        AuthenticationData authenticationData =
            mapper.readValue(response.result.message, AuthenticationData.class);
        return new SynapsResult(signature, authenticationData, response.result.message, null);
      }
    } else { // authType == AuthenticationType.KYC
      if (response.isApproved()) {
        Signature signature = readSignatureFromHexString(response.result.signature);
        if (signature == null) {
          return createInvalidKycResponse();
        } else {
          AuthenticationData authenticationData =
              mapper.readValue(response.result.message, AuthenticationData.class);
          return new SynapsResult(signature, authenticationData, response.result.message, null);
        }
      } else {
        return createInvalidKycResponse();
      }
    }
  }

  private static SynapsResult createInvalidKycResponse() {
    return new SynapsResult(
        null,
        null,
        null,
        "The KYC was not approved, check that the on-chain information is consistent with the"
            + " information sent to the KYC provider.");
  }

  private static SynapsResult createInvalidKybResponse() {
    return new SynapsResult(
        null,
        null,
        null,
        "The KYB was not approved, check the information submitted to the KYB provider.");
  }

  /**
   * Retrieve KYC/KYB info from Synaps.
   *
   * @param client the http used to retrieve data
   * @param sessionId the session id
   * @param authType the authentication type; either KYB or KYC
   * @return result from Synaps
   */
  static SynapsResult callSynaps(Client client, String sessionId, AuthenticationType authType)
      throws IOException {

    String responseString =
        client
            .target(authType.getUrl())
            .request()
            .header(SESSION_ID_HEADER, sessionId)
            .get(String.class);

    return parseResponse(responseString, authType);
  }

  private Synaps() {}
}
