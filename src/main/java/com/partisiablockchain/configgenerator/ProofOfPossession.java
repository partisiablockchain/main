package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.server.BetanetAddresses;

/** Create BLS proof-of-possession to show ownership BLS private key. */
public final class ProofOfPossession {

  /**
   * Get sessionId from BpOrchestrationContract.
   *
   * @param client the blockchain client
   * @return session BpOrchestrationContract sessionId
   */
  static int getBpOrchestrationSessionId(BlockchainContractClient client) {
    ContractState state = client.getContractState(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION);

    return state.serializedContract().get("sessionId").asInt();
  }

  /**
   * Create a proof-of-possession for the BLS private key.
   *
   * @param client the blockchain client
   * @param address node operators blockchain address
   * @param publicKey node operators public key
   * @param keyPair node operators BLS key pair
   * @return new signature that proves possession of BLS private key
   */
  static BlsSignature createProofOfPossession(
      BlockchainContractClient client,
      BlockchainAddress address,
      BlockchainPublicKey publicKey,
      BlsKeyPair keyPair) {
    return keyPair.sign(
        createProofOfPossessionHash(client, address, publicKey, keyPair.getPublicKey()));
  }

  /**
   * Create hash of address, node public key, BLS public key and BpOrchestration session id.
   *
   * @param client the blockchain client
   * @param address node operators blockchain address
   * @param publicKey node operators public key
   * @param blsPublicKey node operators BLS public key
   * @return hash of address, node public key, BLS public key and BpOrchestration session id
   */
  static Hash createProofOfPossessionHash(
      BlockchainContractClient client,
      BlockchainAddress address,
      BlockchainPublicKey publicKey,
      BlsPublicKey blsPublicKey) {
    return Hash.create(
        h -> {
          address.write(h);
          publicKey.write(h);
          blsPublicKey.write(h);
          h.writeLong(getBpOrchestrationSessionId(client));
        });
  }

  private ProofOfPossession() {}
}
