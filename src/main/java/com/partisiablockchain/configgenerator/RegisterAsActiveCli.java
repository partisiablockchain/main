package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Sends a transaction to mark this node as active if the node have been marked as inactive with
 * status 'INACTIVE'.
 *
 * <p>A producer is automatically marked with status 'INACTIVE' as a part of the large oracle update
 * protocol, if the producer has status 'CONFIRMED' but do not participate in the protocol. Having
 * status 'INACTIVE' prevents the block producer from joining any new committee.
 *
 * <p>Register-as-active can be used by a node to indicate that any problem with the node has been
 * fixed and that it is ready to participate actively in the network again.
 *
 * <p>If the transaction succeeds the nodes status will again be 'CONFIRMED'.
 */
public final class RegisterAsActiveCli {

  private static final String HELP_MESSAGE =
      "This tool sends a transaction to mark this node as active if the node have been marked as"
          + " inactive with status 'INACTIVE'.A producer is automatically marked with status"
          + " 'INACTIVE' as a part of the large oracle update protocol, if the producer has status"
          + " 'CONFIRMED' but do not participate in the protocol. Having status 'INACTIVE' prevents"
          + " the block producer from joining any new committee. Register-as-active can be used by"
          + " a node to indicate that any problem with the node has been fixed and that it is ready"
          + " to participate actively in the network again.\n"
          + "To use this tool the first and only argument should be the path of your config file."
          + " If the transaction succeeds the nodes status will again be 'CONFIRMED'.";
  private static final int BPO_INVOKE_MARK_AS_ACTIVE = 14;
  private static final int MARK_AS_ACTIVE_TRANSACTION_COST = 4000;

  /**
   * Run register-as-active transaction cli.
   *
   * @param args the first argument is location of config file
   * @throws IOException Unable to read or write to IO
   */
  public static void main(String[] args) throws IOException {
    run(args, System.out, BlockchainClientProvider.localhostBlockchainClientProvider);
  }

  static void run(
      String[] args, PrintStream output, BlockchainClientProvider blockchainContractClientProvider)
      throws IOException {

    if (args.length == 0 || args[0].equals("--help")) {
      output.println(HELP_MESSAGE);
      return;
    }

    TerminalPrinter printer = new TerminalPrinter(80, output);
    NodeConfigReader nodeConfigReader;
    try {
      nodeConfigReader = new NodeConfigReader(args[0]);
    } catch (IllegalArgumentException e) {
      printer.println(e.getMessage());
      return;
    }

    BlockchainContractClient blockchainContractClient =
        blockchainContractClientProvider.createClient(nodeConfigReader.getRestPort());

    Transaction transaction =
        markAsActive(
            blockchainContractClient,
            nodeConfigReader.getBlockchainAddress(),
            nodeConfigReader.getNetworkPublicKey(),
            nodeConfigReader.getFinalizationKey());
    Transactions.TransactionResult result =
        Transactions.sendTransaction(
            transaction,
            MARK_AS_ACTIVE_TRANSACTION_COST,
            blockchainContractClient,
            nodeConfigReader.getAccountKey());
    printExitMessage(printer, result);
  }

  private static void printExitMessage(TerminalPrinter out, Transactions.TransactionResult result) {
    out.newLine();
    if (result.succeeded()) {
      out.println("Your 'Register as active' transaction has now succeeded");
    } else {
      out.println("Your 'Register as active' transaction failed");
    }
    out.newLine();
    out.println("See transaction details at:");
    out.printf("https://browser.partisiablockchain.com/transactions/%s\n", result.identifier());
  }

  private static Transaction markAsActive(
      BlockchainContractClient client,
      BlockchainAddress address,
      BlockchainPublicKey publicKey,
      BlsKeyPair blsKeyPair) {
    return Transaction.create(
        BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(BPO_INVOKE_MARK_AS_ACTIVE);
              ProofOfPossession.createProofOfPossession(client, address, publicKey, blsKeyPair)
                  .write(s);
            }));
  }

  private RegisterAsActiveCli() {}

  interface BlockchainClientProvider {
    BlockchainClientProvider localhostBlockchainClientProvider =
        (port) -> BlockchainClient.create("http://localhost:" + port, 3);

    BlockchainContractClient createClient(int port);
  }
}
