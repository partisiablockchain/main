package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import java.util.function.Function;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.DecoderException;
import org.bouncycastle.util.encoders.Hex;

final class KnownPeersValidator implements StringValidator {

  @Override
  public boolean validate(String input) {
    String[] parts = input.split(":", -1);
    if (parts.length != 3) {
      return false;
    }

    return parsePublicKey(parts[0]) != null && validateHostOrIp(parts[1], parts[2]);
  }

  private static BlockchainPublicKey validate(String input, Function<String, byte[]> parser) {
    try {
      byte[] decoded = parser.apply(input);
      return convertPointToKey(decoded);
    } catch (DecoderException ignored) {
      // Invalid encoding
      return null;
    }
  }

  @Override
  public String toSerializedForm(String input) {
    String[] parts = input.split(":", -1);

    BlockchainPublicKey publicKey = parsePublicKey(parts[0]);
    return Hex.toHexString(publicKey.asBytes()) + ":" + parts[1] + ":" + parts[2];
  }

  private boolean validateHostOrIp(String host, String portString) {
    try {
      Integer.parseInt(portString);
    } catch (NumberFormatException e) {
      return false;
    }

    if (host.trim().isBlank()) {
      return false;
    }

    return CliInputValidation.validateHost(host);
  }

  static BlockchainPublicKey parsePublicKey(String publicKeyString) {
    BlockchainPublicKey key = parseBase64key(publicKeyString);
    if (key != null) {
      return key;
    } else {
      return parseHexKey(publicKeyString);
    }
  }

  static BlockchainPublicKey parseBase64key(String publicKeyString) {
    return validate(publicKeyString, Base64::decode);
  }

  static BlockchainPublicKey parseHexKey(String publicKeyString) {
    return validate(publicKeyString, Hex::decode);
  }

  static BlockchainPublicKey convertPointToKey(byte[] decoded) {
    try {
      return BlockchainPublicKey.fromEncodedEcPoint(decoded);
    } catch (IllegalArgumentException ignored) {
      return null;
    }
  }
}
