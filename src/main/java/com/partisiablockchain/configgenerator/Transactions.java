package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.ExecutedTransactionTree;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.dto.ExecutedTransaction;

/** Sends transactions . */
public final class Transactions {
  /**
   * Send transaction to chain and wait for transaction to complete.
   *
   * @param transaction the transaction to send
   * @param transactionCost gas cost to send transaction
   * @param blockchainClient the client to send the transaction with
   * @param accountKey account key of the sender
   * @return result of transaction
   */
  static TransactionResult sendTransaction(
      Transaction transaction,
      int transactionCost,
      BlockchainContractClient blockchainClient,
      SenderAuthentication accountKey) {

    BlockchainTransactionClient client =
        BlockchainTransactionClient.create(blockchainClient, accountKey);

    SentTransaction sentTransaction = client.signAndSend(transaction, transactionCost);
    ExecutedTransactionTree executedTransactions = client.waitForSpawnedEvents(sentTransaction);

    boolean eventsSucceeded =
        executedTransactions.executedEvents().stream()
            .allMatch(ExecutedTransaction::executionSucceeded);

    return new TransactionResult(
        sentTransaction.transactionPointer().identifier().toString(),
        executedTransactions.executedTransaction().executionSucceeded() && eventsSucceeded);
  }

  /** Result from sending transaction. */
  record TransactionResult(String identifier, boolean succeeded) {}

  private Transactions() {}
}
