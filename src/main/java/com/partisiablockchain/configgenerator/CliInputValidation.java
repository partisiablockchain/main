package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Function for validating user input. */
final class CliInputValidation {

  private static final Pattern HEX_VALIDATION_REGEX = Pattern.compile("\\p{XDigit}{64}");

  /**
   * Checks if hostname or IP address is correctly formatted.
   *
   * @param hostname the hostname or IP to validate
   * @return true if hostname is valid
   */
  static boolean validateHost(String hostname) {
    try {
      InetAddress.getAllByName(hostname);
      return true;
    } catch (UnknownHostException e) {
      return false;
    }
  }

  /**
   * Checks if the input is a valid url.
   *
   * @param input the input from user
   * @return true if input is a valid url
   */
  static boolean validateUrl(String input) {
    try {
      new URL(input).toURI();
      return true;
    } catch (IllegalArgumentException | MalformedURLException | URISyntaxException e) {
      return false;
    }
  }

  /**
   * Checks if the input is a valid hex key.
   *
   * @param input the input from user
   * @return true if input is valid
   */
  static boolean validateHexKey(String input) {
    Matcher matcher = HEX_VALIDATION_REGEX.matcher(input);
    return matcher.matches();
  }

  /**
   * Always returns true.
   *
   * @return true
   */
  static StringValidator alwaysValid() {
    return ignored -> true;
  }

  private CliInputValidation() {}
}
