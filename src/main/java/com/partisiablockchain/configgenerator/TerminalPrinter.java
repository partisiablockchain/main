package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.PrintStream;
import java.util.List;

final class TerminalPrinter {
  private final int maxLength;
  private final PrintStream out;

  TerminalPrinter(int maxLength, PrintStream out) {
    this.maxLength = maxLength;
    this.out = out;
  }

  void printHorizontalRule() {
    out.println("=".repeat(maxLength));
  }

  void newLine() {
    out.println();
  }

  void println(String text) {
    TextFunctions.splitIntoLines(text, maxLength).forEach(out::println);
  }

  void printf(String text, Object... args) {
    out.printf(text, args);
  }

  @SuppressWarnings("AnnotateFormatMethod")
  void printlnf(String text, Object... args) {
    TextFunctions.splitIntoLines(text.formatted(args), maxLength).forEach(out::println);
  }

  void printAligned(String text, TextAlignment alignment) {
    String alignedText = TextFunctions.alignText(text, alignment, maxLength);
    out.println(alignedText);
  }

  void printTable(List<List<String>> table, TextAlignment... alignments) {
    TextFunctions.alignTable(table, List.of(alignments)).forEach(out::println);
  }
}
