package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jService;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;

final class Web3Validator {
  private Web3Validator() {}

  static boolean validate(Web3ServiceProvider provider, String maybeUri) {
    try {
      Web3j web3j = Web3j.build(provider.create(maybeUri));

      EthBlockNumber blockNumberRequest = web3j.ethBlockNumber().send();
      BigInteger blockNumber = blockNumberRequest.getBlockNumber();

      EthBlock blockRequest =
          web3j.ethGetBlockByNumber(DefaultBlockParameter.valueOf(blockNumber), false).send();
      EthBlock.Block block = blockRequest.getBlock();

      long unixTimestamp = block.getTimestamp().longValue();
      Instant instant = Instant.ofEpochSecond(unixTimestamp);

      long minutesSinceLatestBlock = ChronoUnit.MINUTES.between(instant, Instant.now());
      return minutesSinceLatestBlock <= 60;
    } catch (Exception e) {
      return false;
    }
  }

  @FunctionalInterface
  interface Web3ServiceProvider {
    Web3jService create(String maybeUri);
  }
}
