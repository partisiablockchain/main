package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.configgenerator.RegisterNodeCli.AuthenticationType;
import com.partisiablockchain.configgenerator.RegisterNodeCli.KybUserInput;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto.Chains;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import org.bouncycastle.util.encoders.Hex;

/** Handle user input. */
final class CliUserInput {
  private static final String BLANK_LINE_MESSAGE = "Input can't be blank. Please enter a value";

  private final Scanner scanner;
  private final TerminalPrinter terminal;
  private final Web3Validator.Web3ServiceProvider web3Validator;

  /**
   * Storage for cli streams.
   *
   * @param input the user input stream
   * @param output the user output stream
   * @param web3Validator the web3j validator to use for checking URLs
   */
  CliUserInput(
      InputStream input, PrintStream output, Web3Validator.Web3ServiceProvider web3Validator) {
    this.scanner = new Scanner(input, StandardCharsets.UTF_8);
    this.terminal = new TerminalPrinter(80, output);
    this.web3Validator = web3Validator;
  }

  /**
   * Ask the user for a new hostname or ip address.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return user input
   */
  String askForHost(String message, String defaultValue) {
    return askForValidatedString(
        message,
        defaultValue,
        CliInputValidation::validateHost,
        "Please enter a hostname or ip address");
  }

  /**
   * Ask the user for a URL to a Web3 compatible service.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return user input
   */
  String askForWeb3Url(String message, String defaultValue) {
    return askForValidatedString(
        message,
        defaultValue,
        input -> Web3Validator.validate(web3Validator, input),
        "Please enter a url (with https://...)");
  }

  /**
   * Ask the user for a URL.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return user input
   */
  String askForUrl(String message, String defaultValue) {
    return askForValidatedString(
        message,
        defaultValue,
        CliInputValidation::validateUrl,
        "Please enter a url (with https://...)");
  }

  /**
   * Ask the user for new input.
   *
   * @param message Message to print
   * @return User input
   */
  public String askForString(String message) {
    return askForString(message, null);
  }

  /**
   * Ask the user for new input.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return user input
   */
  String askForString(String message, String defaultValue) {
    return askWithParsing(message, Optional.ofNullable(defaultValue), Optional::of);
  }

  /**
   * Ask the user a yes/no question.
   *
   * @param message the message to print
   * @return true if the user entered "y"
   */
  boolean askForBool(String message, boolean defaultValue) {
    return askWithParsing(
        message,
        Optional.of(defaultValue),
        input -> Optional.of(Character.toLowerCase(input.charAt(0)) == 'y'));
  }

  /**
   * Ask the user for an item in a list.
   *
   * @param message the message to print
   * @param options the list of valid options
   * @return user input
   */
  String askForItemInList(String message, String[] options) {
    return askForValidatedString(
        message,
        null,
        input -> {
          for (String option : options) {
            if (option.equals(input)) {
              return true;
            }
          }
          return false;
        },
        "Please enter one of: " + String.join(", ", options));
  }

  /**
   * Ask for a list of hostnames or ips.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return list of hostnames or ips
   */
  List<String> askForHostList(String message, List<String> defaultValue) {
    return askForValidatedList(
        message, defaultValue, new KnownPeersValidator(), "\"%s\" is not a valid peer");
  }

  /**
   * Ask the user for a comma seperated list of arguments.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @param validator the validation function
   * @param errorMessage the message printed when given invalid input
   * @return a list of arguments from the user
   */
  List<String> askForValidatedList(
      String message, List<String> defaultValue, StringValidator validator, String errorMessage) {
    return askWithParsing(
        message,
        Optional.ofNullable(defaultValue),
        input -> {
          String[] elements = input.split(",", -1);
          for (String element : elements) {
            if (!validator.validate(element)) {
              terminal.printlnf(errorMessage, element);
              return Optional.empty();
            }
          }
          return Optional.of(Arrays.stream(elements).map(validator::toSerializedForm).toList());
        });
  }

  /**
   * Ask the user for a country.
   *
   * @param message to print
   * @return ISO_3166-1_numeric for the country
   */
  public int askForCountry(String message) throws IOException {
    CountryCodes countryCodes = new CountryCodes();
    return doAskLoop(
        message,
        Optional.empty(),
        CliInputValidation.alwaysValid(),
        "",
        (input) -> {
          Optional<Integer> maybeCountryCode = countryCodes.getCountryCode(input);
          if (maybeCountryCode.isPresent()) {
            return maybeCountryCode;
          }

          var nearestCountry = countryCodes.findNearestMatch(input);
          String countryName = nearestCountry.name();
          int countryCode = nearestCountry.code();
          boolean isNearestCorrect =
              askForBool(
                  String.format(
                      "Could not find matching country name. The nearest was %s (%d). Is that"
                          + " correct? [y/n]",
                      countryName, countryCode),
                  false);
          if (isNearestCorrect) {
            return Optional.of(countryCode);
          }

          return Optional.empty();
        },
        "Please enter a valid country code or name");
  }

  /**
   * Ask for a string and validate that it is correctly formatted.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @param validator the validation function
   * @param invalidMessage the message to print if invalid input is given
   * @return validated user input
   */
  String askForValidatedString(
      String message, String defaultValue, StringValidator validator, String invalidMessage) {
    return doAskLoop(
        message, Optional.ofNullable(defaultValue), validator, invalidMessage, Optional::of, "");
  }

  /**
   * Ask for input from user and parse result. Will keep trying until parsing succeeds.
   *
   * @param message the message printed to user
   * @param defaultValue the default value; Used if no value is given by user
   * @param parser the function that parses user input
   * @param parseError the error that is printed when user input can't be parsed
   * @param <T> the output type
   * @return input from user
   */
  <T> T askWithParsing(
      String message, Optional<T> defaultValue, InputParser<T> parser, String parseError) {
    return doAskLoop(
        message, defaultValue, CliInputValidation.alwaysValid(), "", parser, parseError);
  }

  /**
   * Ask for input from user and parse result. Will keep trying until parsing succeeds.
   *
   * @param message the message printed to user
   * @param defaultValue the default value; Used if no value is given by user
   * @param parser the function that parses user input
   * @param <T> the output type
   * @return input from user
   */
  <T> T askWithParsing(String message, Optional<T> defaultValue, InputParser<T> parser) {
    return askWithParsing(message, defaultValue, parser, "");
  }

  /**
   * Ask for input from user until a valid answer is given.
   *
   * @param message the message printed to user
   * @param defaultValue the default value; Used if no value is given by user
   * @param validator the function that validates user input
   * @param invalidMessage the message printed if input is invalid
   * @param parser the function that parses user input
   * @param parseError the error that is printed when user input can't be parsed
   * @param <T> the output type
   * @return input from user
   */
  <T> T doAskLoop(
      String message,
      Optional<T> defaultValue,
      StringValidator validator,
      String invalidMessage,
      InputParser<T> parser,
      String parseError) {
    while (true) {
      Optional<T> value = ask(message, defaultValue, validator, invalidMessage, parser, parseError);
      if (value.isPresent()) {
        return value.get();
      }
    }
  }

  /**
   * Ask for input from the user.
   *
   * @param message the message printed to user
   * @param defaultValue the default value; Used if no value is given by user
   * @param validator the function that validates user input
   * @param invalidMessage the message printed if input is invalid
   * @param parser the function that parses user input
   * @param parseError the error that is printed when user input can't be parsed
   * @param <T> the output type
   * @return input from user
   */
  private <T> Optional<T> ask(
      String message,
      Optional<T> defaultValue,
      StringValidator validator,
      String invalidMessage,
      InputParser<T> parser,
      String parseError) {
    promptInput(message, defaultValue);
    String userInput = scanner.nextLine();

    if (userInput.isBlank()) {
      if (defaultValue.isPresent()) {
        return defaultValue;
      } else {
        terminal.println(BLANK_LINE_MESSAGE);
        return Optional.empty();
      }
    }

    if (!validator.validate(userInput)) {
      terminal.println(invalidMessage);
      return Optional.empty();
    }

    String serializedForm = validator.toSerializedForm(userInput);
    Optional<T> parsedInput = parser.parseInput(serializedForm);

    if (parsedInput.isEmpty()) {
      terminal.println(parseError);
    }

    return parsedInput;
  }

  /**
   * Prompts the user for input.
   *
   * @param message the message printed to user
   * @param defaultValue the default value; Used if no value is given by user
   * @param <T> the type of input from user
   */
  <T> void promptInput(String message, Optional<T> defaultValue) {
    if (defaultValue.isPresent()) {
      if (defaultValue.get().getClass() == Boolean.class) {
        String currentValue = (boolean) defaultValue.get() ? "yes" : "no";
        terminal.printf("%s [current: %s]: ", message, currentValue);
      } else {
        terminal.printf("%s [current: %s]: ", message, defaultValue.get());
      }
    } else {
      terminal.printf("%s: ", message);
    }
  }

  /**
   * Ask the user for a new private key.
   *
   * @param message the message to print
   * @param defaultValue the default value; Used if no value is given by user
   * @return user input
   */
  String askForHexEncodedPrivateKey(String message, String defaultValue) {
    while (true) {
      String hexKey =
          askForValidatedString(
              message,
              defaultValue,
              CliInputValidation::validateHexKey,
              "Please enter a hex encoded key with length 64");

      BigInteger secret = new BigInteger(hexKey, 16);
      BlockchainAddress address = new KeyPair(secret).getPublic().createAddress();
      terminal.printlnf("The key corresponds to this address: %s", address.writeAsString());
      terminal.newLine();
      boolean isCorrectKey = askForBool("Is this the address you expected [y/n]?", false);
      terminal.newLine();
      if (isCorrectKey) {
        return hexKey;
      }
    }
  }

  /**
   * Create key and print information about it to the user.
   *
   * @return new private key
   */
  String createNewKey(String previousValue) {
    KeyPair keyPair;
    if (previousValue == null) {
      keyPair = new KeyPair();
    } else {
      keyPair = new KeyPair(new BigInteger(previousValue, 16));
    }

    return keyPair.getPrivateKey().toString(16);
  }

  /**
   * Create BLS key and print information about it to the user.
   *
   * @param previousValue the previous bls key
   * @return new private key
   */
  static String createNewBlsKey(String previousValue) {
    BlsKeyPair keyPair;
    if (previousValue == null) {
      keyPair = new BlsKeyPair(new BigInteger(256, new SecureRandom()));
    } else {
      keyPair = new BlsKeyPair(new BigInteger(previousValue, 16));
    }

    String privateKeyAsHex = keyPair.getPrivateKey().toString(16);
    return padKey(privateKeyAsHex);
  }

  static String padKey(String privateKeyAsHex) {
    int length = privateKeyAsHex.length();
    int padding = 64 - length;
    return "0".repeat(padding) + privateKeyAsHex;
  }

  /**
   * Ask the user for input to create configuration file.
   *
   * @param input the user input
   * @param output the output to user
   * @param web3ServiceProvider the web3j service provider
   * @return new CompositeNodeConfigDto with data from cli questions
   */
  static CompositeNodeConfigDto createConfigFromInput(
      InputStream input,
      PrintStream output,
      Web3Validator.Web3ServiceProvider web3ServiceProvider) {
    CompositeNodeConfigDto config = new CompositeNodeConfigDto();
    updateConfigFromInput(config, input, output, web3ServiceProvider);
    return config;
  }

  /**
   * Update configuration with new input from user.
   *
   * @param config the previous configuration
   * @param input the user input
   * @param output the output to user
   * @param web3ServiceProvider the web3j service provider
   */
  static void updateConfigFromInput(
      CompositeNodeConfigDto config,
      InputStream input,
      PrintStream output,
      Web3Validator.Web3ServiceProvider web3ServiceProvider) {
    setDefaultPorts(config);
    CliUserInput io = new CliUserInput(input, output, web3ServiceProvider);

    boolean wasProducingNode = config.producerConfig != null;
    boolean isProducingNode =
        wasProducingNode || io.askForBool("Is this a block producing node? [y/n]", false);

    if (isProducingNode) {
      if (!wasProducingNode) {
        config.producerConfig = new BlockProducerConfigDto();
      }

      BlockProducerConfigDto producerConfig = config.producerConfig;
      producerConfig.accountKey =
          io.askForHexEncodedPrivateKey("Your account key", config.producerConfig.accountKey);
      producerConfig.host = io.askForHost("The IP address of this node", producerConfig.host);

      updateChainEndpoints(producerConfig, io);

      producerConfig.finalizationKey = createNewBlsKey(producerConfig.finalizationKey);
    }
    config.networkKey = io.createNewKey(config.networkKey);

    config.knownPeers =
        io.askForHostList(
            "List of IPs of known peers on the form public_key:ip:port, separated by comma",
            config.knownPeers);

    io.terminal.println(
        "The known peers public keys can be in either base64 or hex, but are always stored as"
            + " hex.");

    printBackupNagScreen(io.terminal, config, io);
  }

  /**
   * Updates the block producer's chain endpoints, and migrates any old configuration to the newer
   * chain map config.
   *
   * @param producerConfig The block producer's config.
   * @param io Input handler for getting input from the user.
   */
  private static void updateChainEndpoints(BlockProducerConfigDto producerConfig, CliUserInput io) {
    for (String chain : List.of(Chains.ETHEREUM, Chains.POLYGON, Chains.BNB_SMART_CHAIN)) {
      updateChainEndpoint(producerConfig, io, chain);
    }
    clearOldEndpoints(producerConfig);
  }

  /**
   * Updates the chain endpoint in the block producer's config. If there is no existing endpoint URL
   * in the newer chain map config, it checks for an existing endpoint URL in the old chain config.
   * If the user does not supply a new endpoint URL, the existing URL is used. If the existing URL
   * was in the old chain config, it is moved to the newer chain map config.
   *
   * @param producerConfig The block producer's config
   * @param io The input handler for receiving a new endpoint from the user
   * @param chain The chain to update the endpoint for
   */
  private static void updateChainEndpoint(
      BlockProducerConfigDto producerConfig, CliUserInput io, String chain) {
    String existingUrl = producerConfig.getChainEndpoint(chain);
    String updatedUrl = io.askForWeb3Url("URL to %s API".formatted(chain), existingUrl);
    producerConfig.addChainEndpoint(chain, updatedUrl);
  }

  /**
   * Clears the old block producer chain configs. To be called after updating the chain config of
   * the block producer, where the endpoints are migrated to the newer chain map config.
   */
  private static void clearOldEndpoints(BlockProducerConfigDto config) {
    config.ethereumUrl = null;
    config.polygonUrl = null;
    config.bnbSmartChainUrl = null;
  }

  static void printBackupNagScreen(
      TerminalPrinter terminal, CompositeNodeConfigDto config, CliUserInput io) {
    if (config.producerConfig == null) {
      return;
    }

    final List<List<String>> table =
        List.of(
            List.of("", ""),
            List.of("Account key:", config.producerConfig.accountKey),
            List.of("Network key:", config.networkKey),
            List.of("BLS key:", config.producerConfig.finalizationKey));

    terminal.newLine();
    terminal.printHorizontalRule();
    terminal.newLine();

    terminal.printAligned("IMPORTANT", TextAlignment.CENTER);
    terminal.newLine();
    terminal.println("The following keys are needed to run your node.");
    terminal.println("The keys CANNOT be recovered if they are lost.");
    terminal.newLine();
    terminal.printHorizontalRule();
    terminal.newLine();
    terminal.printAligned("BACK THIS UP", TextAlignment.CENTER);

    terminal.printTable(table, TextAlignment.RIGHT, TextAlignment.LEFT);
    terminal.newLine();
    terminal.printHorizontalRule();

    terminal.newLine();

    boolean confirmBackup = false;
    while (!confirmBackup) {
      confirmBackup =
          io.askWithParsing(
              "Please confirm that you have a secure, offline backup of the above (y/N)",
              Optional.empty(),
              input -> Optional.of(Character.toLowerCase(input.charAt(0)) == 'y'));
    }
  }

  static void printSummary(TerminalPrinter terminal, CompositeNodeConfigDto config) {
    if (config.producerConfig != null) {
      terminal.println("Congratulations! You have configured a block producing node.");
    } else {
      terminal.println("Congratulations! You have configured a reader node.");
    }
    terminal.newLine();

    List<List<String>> allNodeTypesInfo = new ArrayList<>();
    allNodeTypesInfo.add(List.of("Your known peers:", config.knownPeers.get(0)));
    int size = config.knownPeers.size();
    for (int i = 1; i < size; i++) {
      allNodeTypesInfo.add(List.of("", config.knownPeers.get(i)));
    }

    String networkKey =
        Hex.toHexString(new KeyPair(new BigInteger(config.networkKey, 16)).getPublic().asBytes());
    if (config.producerConfig != null) {
      BlockchainAddress accountAddress =
          new KeyPair(new BigInteger(config.producerConfig.accountKey, 16))
              .getPublic()
              .createAddress();

      final List<List<String>> producerNodeInfo =
          List.of(
              List.of("", ""),
              List.of("Your blockchain address is:", accountAddress.writeAsString()),
              List.of("Your node IP:", config.producerConfig.host),
              List.of("Your flooding port:", String.valueOf(config.floodingPort)),
              List.of("", ""),
              List.of(
                  "Your node as a 'known peer':",
                  networkKey + ":" + config.producerConfig.host + ":" + config.floodingPort),
              List.of("", ""),
              List.of(
                  "Your configured Ethereum URL:",
                  config.producerConfig.getChainEndpoint(Chains.ETHEREUM)),
              List.of(
                  "Your configured Polygon URL:",
                  config.producerConfig.getChainEndpoint(Chains.POLYGON)),
              List.of(
                  "Your configured BNB URL:",
                  config.producerConfig.getChainEndpoint(Chains.BNB_SMART_CHAIN)));

      List<List<String>> table = new ArrayList<>();
      table.addAll(allNodeTypesInfo);
      table.addAll(producerNodeInfo);

      terminal.printTable(table, TextAlignment.RIGHT, TextAlignment.LEFT);
    } else {
      List<List<String>> table = new ArrayList<>(allNodeTypesInfo);
      table.add(List.of("Your flooding port:", String.valueOf(config.floodingPort)));

      terminal.printTable(table, TextAlignment.RIGHT, TextAlignment.LEFT);
    }
  }

  /**
   * Ask the user for authentication type (either KYB or KYC).
   *
   * @return user input
   */
  AuthenticationType askForAuthenticationType() {
    String[] authenticationOptions = {"KYB", "KYC"};
    String authenticationType =
        askForItemInList("Authenticate using KYB or KYC", authenticationOptions);
    if (authenticationType.equals("KYB")) {
      return AuthenticationType.KYB;
    } else {
      return AuthenticationType.KYC;
    }
  }

  /**
   * Ask the user for session id.
   *
   * @return user input
   */
  String askForSessionId() {
    return askForString(
        "Session id from Synaps (See"
            + " https://partisiablockchain.gitlab.io/documentation/node-operations/register-your-node.html)");
  }

  /**
   * Ask the user for consent to send transaction.
   *
   * @param accountKeyAddress the blockchain address from account key
   * @return true if user says yes; no otherwise
   */
  boolean sendTransactionConfirmation(String accountKeyAddress) {
    terminal.newLine();
    terminal.println("Are you sure you want to send the registration transaction?");
    terminal.printlnf(
        "The transaction will cost %d gas and the gas will be deducted from %s.",
        KycTransactions.TRANSACTION_COST, accountKeyAddress);
    return askForBool("Confirm [y/n]", false);
  }

  /**
   * Ask the user for information required to complete KYB.
   *
   * @return user input
   * @throws IOException when io fails
   */
  KybUserInput askForKybData() throws IOException {
    String website = askForUrl("Website", null);
    int serverJurisdiction = askForCountry("Server jurisdiction");
    return new KybUserInput(website, serverJurisdiction);
  }

  /**
   * Set default ports in configuraiton.
   *
   * @param config the configuration to change
   */
  static void setDefaultPorts(CompositeNodeConfigDto config) {
    if (config.restPort == 0) {
      config.restPort = 8080;
    }

    if (config.floodingPort == 0) {
      config.floodingPort = 9888;
    }
  }
}
