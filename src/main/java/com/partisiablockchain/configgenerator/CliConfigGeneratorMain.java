package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.IntConsumer;
import org.web3j.protocol.http.HttpService;

/** Command line based configuration generator for reader and block producing nodes. */
public final class CliConfigGeneratorMain {

  static final String HELP_MESSAGE =
      "First and only argument should be the path to the output file.";

  /**
   * Serialize and write configuration file.
   *
   * @param config the configuration to serialize and write
   * @param outputLocation the path to output file
   * @throws IOException Unable to read or write to IO
   */
  static void writeConfig(CompositeNodeConfigDto config, Path outputLocation) throws IOException {
    createParentDir(outputLocation);
    ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
    mapper.writerWithDefaultPrettyPrinter().writeValue(outputLocation.toFile(), config);
  }

  /**
   * Read and deserialize configuration file from disk.
   *
   * @param configLocation the location of configuration file
   * @return deserialized configuration
   * @throws IOException Unable to read or write to IO
   */
  static CompositeNodeConfigDto loadConfig(Path configLocation) throws IOException {
    ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
    return mapper.readValue(configLocation.toFile(), CompositeNodeConfigDto.class);
  }

  /**
   * Creates parent directory of file.
   *
   * @param path the file that is potentially missing a parent dir
   * @throws IOException Unable to read or write to IO
   */
  private static void createParentDir(Path path) throws IOException {
    Files.createDirectories(path.getParent());
  }

  /**
   * Print help message to user.
   *
   * @param out the stream to print to
   */
  static void printHelpMessage(PrintStream out) {
    out.println(HELP_MESSAGE);
  }

  /**
   * Print introduction message to the user.
   *
   * @param out the stream to print to
   */
  static void printIntroMessage(TerminalPrinter out) {
    out.newLine();
    out.println("Welcome to the node configuration utility.");
    out.println("Following is a list of questions required to configure this node.");
    out.newLine();
    out.println("Trailing square brackets indicate the default answer to the question.");
    out.println("If you have an existing config the values will be shown as defaults.");
    out.newLine();
  }

  static void printHeader(TerminalPrinter out) {
    out.printHorizontalRule();
    out.printAligned("Partisia Blockchain Node Configuration Utility", TextAlignment.CENTER);
    out.printHorizontalRule();
  }

  /**
   * Print final message to user at exit.
   *
   * @param out the stream to print to
   * @param configLocation the path to the configuration file
   * @param config the generated configuration
   */
  static void printExitMessage(
      TerminalPrinter out, Path configLocation, CompositeNodeConfigDto config) {
    out.newLine();
    out.printHorizontalRule();
    out.newLine();
    CliUserInput.printSummary(out, config);
    out.newLine();

    out.printlnf("The configuration file has been written to %s.", configLocation.toString());
    out.newLine();
    out.println("You are now ready to start your node.");
    out.printHorizontalRule();
  }

  /**
   * Run configuration generator cli.
   *
   * @param args the first argument is output path
   * @throws IOException Unable to read or write to IO
   */
  public static void main(String[] args) throws IOException {
    InputStream in = System.in;
    PrintStream out = System.out;
    runWithArgs(args, in, out, HttpService::new, System::exit);
  }

  /**
   * Run configurator.
   *
   * @param args the cli arguments
   * @param in the input stream
   * @param out the output stream
   * @param web3ServiceProvider the web3 service provider
   * @throws IOException Unable to read or write to IO
   */
  static void runWithArgs(
      String[] args,
      InputStream in,
      PrintStream out,
      Web3Validator.Web3ServiceProvider web3ServiceProvider,
      IntConsumer exitHook)
      throws IOException {
    TerminalPrinter terminalPrinter = new TerminalPrinter(80, out);
    printHeader(terminalPrinter);

    if (args.length == 0 || args[0].equals("--help") || args[0].equals("-h")) {
      printHelpMessage(out);
      return;
    }

    Path outputLocation = Paths.get(args[0]);

    printIntroMessage(terminalPrinter);

    CompositeNodeConfigDto config;
    if (Files.exists(outputLocation)) {
      terminalPrinter.println("NOTE: Found existing configuration file, loading previous answers.");
      terminalPrinter.newLine();

      config = loadConfig(outputLocation);
      CliUserInput.updateConfigFromInput(config, in, out, web3ServiceProvider);
    } else {
      config = CliUserInput.createConfigFromInput(in, out, web3ServiceProvider);
    }

    writeConfig(config, outputLocation);

    printExitMessage(terminalPrinter, outputLocation, config);

    exitHook.accept(0);
  }

  private CliConfigGeneratorMain() {}
}
