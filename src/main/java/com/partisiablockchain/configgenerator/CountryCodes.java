package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/** Convert country names to ISO 3166-1 numeric. */
public final class CountryCodes {

  private final List<Country> countries;

  CountryCodes() throws IOException {
    ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
    InputStream countryCodesFile = getClass().getResourceAsStream("countryCodes.json");
    countries = mapper.readValue(countryCodesFile, new TypeReference<>() {});
  }

  private boolean codeExists(int code) {
    return countries.stream().anyMatch(country -> country.code == code);
  }

  /**
   * Find the country that matches the input the best.
   *
   * @param input Wrongly spelled country name
   * @return Correctly spelled contry name
   */
  Country findNearestMatch(String input) {
    LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
    return countries.stream()
        .min(
            (a, b) -> {
              int distanceA = levenshteinDistance.compute(a.name, input);
              int distanceB = levenshteinDistance.compute(b.name, input);
              return Integer.compare(distanceA, distanceB);
            })
        .get();
  }

  /**
   * Get name of country from numeric country code.
   *
   * @param numericCountryCode the numeric country code
   * @return the matching country name
   */
  String nameFromCode(int numericCountryCode) {
    return countries.stream()
        .filter(country -> country.code == numericCountryCode)
        .map(country -> country.name)
        .findFirst()
        .orElseGet(() -> "Unknown country");
  }

  /**
   * Returns the matching country code if input is a valid coutntry name or country code.
   *
   * @param input Potential country name or code
   * @return An optional with the matching country code
   */
  Optional<Integer> getCountryCode(String input) {
    Optional<Country> maybeCountry =
        countries.stream().filter(country -> country.name.equals(input)).findFirst();
    if (maybeCountry.isPresent()) {
      return maybeCountry.map(country -> country.code);
    }

    try {
      int countryCode = Integer.parseInt(input);
      if (codeExists(countryCode)) {
        return Optional.of(countryCode);
      }
    } catch (NumberFormatException e) {
      return Optional.empty();
    }

    return Optional.empty();
  }

  /** Country with name and ISO 3166-1 numeric code. */
  public record Country(String name, Integer code) {}

  static final class LevenshteinDistance {

    private final HashMap<String, Integer> distances = new HashMap<>();

    int compute(String a, String b) {
      String concat = a + ":" + b;

      if (distances.containsKey(concat)) {
        return distances.get(concat);
      }

      int result = distance(a, b);
      distances.put(concat, result);
      return result;
    }

    private int distance(String a, String b) {
      if (a.isEmpty()) {
        return b.length();
      } else if (b.isEmpty()) {
        return a.length();
      } else {
        String subA = a.substring(1);
        String subB = b.substring(1);

        if (a.charAt(0) == b.charAt(0)) {
          return compute(subA, subB);
        } else {
          int insertedCharacter = compute(a, subB);
          int deletedCharacter = compute(subA, b);
          int replacedCharacter = compute(subA, subB);

          return min(insertedCharacter, deletedCharacter, replacedCharacter) + 1;
        }
      }
    }

    private static int min(int a, int b, int c) {
      return Math.min(Math.min(a, b), c);
    }
  }
}
