package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.CompositeNodeConfigDto;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/** Reads config on given location and gives access to configurations. */
public final class NodeConfigReader {
  static final String MISSING_FILE_MESSAGE = "Configuration file does not exist.";
  static final String NOT_PRODUCER_CONFIG_MESSAGE =
      "The given configuration file is missing producer configuration.";
  CompositeNodeConfigDto config;

  NodeConfigReader(String path) throws IOException {
    Path configLocation = Paths.get(path);

    if (!Files.exists(configLocation)) {
      throw new IllegalArgumentException(MISSING_FILE_MESSAGE);
    }

    CompositeNodeConfigDto config = CliConfigGeneratorMain.loadConfig(configLocation);
    if (config.producerConfig == null) {
      throw new IllegalArgumentException(NOT_PRODUCER_CONFIG_MESSAGE);
    }
    this.config = config;
  }

  int getRestPort() {
    return config.restPort;
  }

  SenderAuthentication getAccountKey() {
    return SenderAuthenticationKeyPair.fromString(config.producerConfig.accountKey);
  }

  BlockchainAddress getBlockchainAddress() {
    KeyPair keyPair = new KeyPair(new BigInteger(config.producerConfig.accountKey, 16));
    BlockchainPublicKey publicKey = keyPair.getPublic();
    return publicKey.createAddress();
  }

  BlockchainPublicKey getNetworkPublicKey() {
    KeyPair keyPair = new KeyPair(new BigInteger(config.networkKey, 16));
    return keyPair.getPublic();
  }

  BlsKeyPair getFinalizationKey() {
    return new BlsKeyPair(new BigInteger(config.producerConfig.finalizationKey, 16));
  }
}
