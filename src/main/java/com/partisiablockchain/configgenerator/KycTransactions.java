package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.configgenerator.RegisterNodeCli.KybUserInput;
import com.partisiablockchain.configgenerator.Synaps.AuthenticationData;
import com.partisiablockchain.configgenerator.Synaps.SynapsResult;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataOutputStream;

/** Create KYC transactions. */
final class KycTransactions {

  static final int TRANSACTION_COST = 25_000;

  /**
   * Checks if blockchain address from configuration and synaps match.
   *
   * @param authenticationData the response from Synaps
   * @return true if they match
   */
  static boolean blockchainAddressesMatch(
      BlockchainAddress addressFromConfig, AuthenticationData authenticationData) {
    return addressFromConfig.writeAsString().equals(authenticationData.blockchainAddress);
  }

  /**
   * Create KYB APPROVE transaction.
   *
   * @return new KYB APPROVE transaction.
   */
  static Transaction createKybApprove(
      SynapsResult synapsResult,
      KybUserInput userInput,
      BlockchainContractClient client,
      BlockchainAddress address,
      BlockchainPublicKey blockchainPublicKey,
      BlsKeyPair blsKeyPair) {
    return Transaction.create(
        BetanetAddresses.SYNAPS_KYC,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(Synaps.Invocation.KYB_APPROVE);
              s.writeString(synapsResult.synapsJson());
              synapsResult.signature().write(s);
              s.writeString(userInput.website());
              s.writeInt(userInput.serverJurisdiction());
              blockchainPublicKey.write(s);
              blsKeyPair.getPublicKey().write(s);
              ProofOfPossession.createProofOfPossession(
                      client, address, blockchainPublicKey, blsKeyPair)
                  .write(s);
            }));
  }

  /**
   * Create KYC_SUBMIT transaction.
   *
   * @param synapsResult the response from Synaps server
   * @param client the blockchain client
   * @return new KYC_SUBMIT transaction
   */
  static Transaction createKycSubmit(
      SynapsResult synapsResult,
      BlockchainContractClient client,
      BlockchainAddress address,
      BlockchainPublicKey blockchainPublicKey,
      BlsKeyPair blsKeyPair) {
    return Transaction.create(
        BetanetAddresses.SYNAPS_KYC,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(Synaps.Invocation.KYC_SUBMIT);
              s.writeString(synapsResult.synapsJson());
              synapsResult.signature().write(s);
              blockchainPublicKey.write(s);
              blsKeyPair.getPublicKey().write(s);
              ProofOfPossession.createProofOfPossession(
                      client, address, blockchainPublicKey, blsKeyPair)
                  .write(s);
            }));
  }

  private KycTransactions() {}
}
