package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.configgenerator.Synaps.AuthenticationData;
import com.partisiablockchain.configgenerator.Synaps.SynapsResult;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import org.web3j.protocol.http.HttpService;

/** Command line based node registration tool. */
public final class RegisterNodeCli {

  static final String HELP_MESSAGE = "First and only argument should be the path your config file.";

  /** Authentication type; either kyb or kyc. */
  enum AuthenticationType {
    KYB("https://synaps.partisiablockchain.com/kyb"),
    KYC("https://synaps.partisiablockchain.com/kyc");

    private final String url;

    AuthenticationType(String url) {
      this.url = url;
    }

    String getUrl() {
      return url;
    }
  }

  /** KYB information from user. */
  record KybUserInput(String website, int serverJurisdiction) {}

  /**
   * Print introduction message to the user.
   *
   * @param out the printer
   */
  static void printIntroMessage(TerminalPrinter out) {
    out.printHorizontalRule();
    out.printAligned("Partisia Blockchain Node Registration Utility", TextAlignment.CENTER);
    out.printHorizontalRule();
    out.newLine();
    out.println("Welcome to the node registration utility.");
    out.println("Following is a list of questions required to register this node.");
    out.newLine();
  }

  /**
   * Print final message to user at exit.
   *
   * @param out the stream to print to
   * @param result the transaction sent to blockchain
   */
  static void printExitMessage(
      TerminalPrinter out,
      Transactions.TransactionResult result,
      AuthenticationType authenticationType) {
    out.newLine();
    if (result.succeeded()) {
      out.printlnf("Your %s transaction has now succeeded", authenticationType.name());
    } else {
      out.printlnf("Your %s transaction failed", authenticationType.name());
    }
    out.newLine();
    out.println("See transaction details at:");
    out.printf("https://browser.partisiablockchain.com/transactions/%s\n", result.identifier());
  }

  /**
   * Print message about who is authenticated.
   *
   * @param out the stream to print to
   * @param authInfo the information about the authenticated entity
   * @param authType the type of authentication, either KYC or KYB
   */
  static void printAuthenticatedMessage(
      TerminalPrinter out, AuthenticationData authInfo, AuthenticationType authType)
      throws IOException {
    out.newLine();
    out.println(String.format("Your %s information is: ", authType.name()));
    out.printTable(
        List.of(
            List.of("Name:", authInfo.name),
            List.of("Address:", authInfo.address),
            List.of("City:", authInfo.city),
            List.of("Country:", authInfo.getCountryName())),
        TextAlignment.LEFT,
        TextAlignment.LEFT);
    out.newLine();
  }

  /** Print message about blockchain address mismatch. */
  static void printMismatchedBlockchainAddresses(
      String configAddress,
      AuthenticationData authenticationData,
      TerminalPrinter out,
      AuthenticationType authtype) {
    String synapsAddress = authenticationData.blockchainAddress;
    out.newLine();
    out.printlnf(
        "Your %s blockchain address does not match with the key in your config.", authtype.name());
    out.printlnf("Your %s blockchain address is set to: %s", authtype.name(), synapsAddress);
    out.printlnf("Your key from your config matches:     %s", configAddress);
  }

  /**
   * Run node registration transaction cli.
   *
   * @param args the cli arguments
   * @param input the user input
   * @param output the output to user
   * @param client the http client
   * @param blockchainContractClient the blockchain client
   * @param web3ServiceProvider the web3j service provider
   * @throws IOException Unable to read or write to IO
   */
  static void run(
      String[] args,
      InputStream input,
      PrintStream output,
      Client client,
      BlockchainContractClient blockchainContractClient,
      Web3Validator.Web3ServiceProvider web3ServiceProvider)
      throws IOException {
    if (args.length == 0 || args[0].equals("--help")) {
      output.println(HELP_MESSAGE);
      return;
    }

    TerminalPrinter printer = new TerminalPrinter(80, output);
    printIntroMessage(printer);
    CliUserInput cliInput = new CliUserInput(input, output, web3ServiceProvider);

    NodeConfigReader nodeConfigReader;
    try {
      nodeConfigReader = new NodeConfigReader(args[0]);
    } catch (IllegalArgumentException e) {
      printer.println(e.getMessage());
      return;
    }

    // Synaps
    AuthenticationType authType = cliInput.askForAuthenticationType();
    String sessionId = cliInput.askForSessionId();
    SynapsResult synapsResponse = Synaps.callSynaps(client, sessionId, authType);

    if (synapsResponse.hasError()) {
      printer.newLine();
      printer.printlnf("The %s service call failed with the following error:", authType.name());
      printer.println(synapsResponse.error());
      return;
    }

    BlockchainAddress addressFromConfig = nodeConfigReader.getBlockchainAddress();
    if (!KycTransactions.blockchainAddressesMatch(
        addressFromConfig, synapsResponse.authenticationData())) {
      printMismatchedBlockchainAddresses(
          addressFromConfig.writeAsString(),
          synapsResponse.authenticationData(),
          printer,
          authType);
      return;
    }

    printAuthenticatedMessage(printer, synapsResponse.authenticationData(), authType);

    // Transaction
    String accountKeyAddress = addressFromConfig.writeAsString();
    if (!cliInput.sendTransactionConfirmation(accountKeyAddress)) {
      printer.newLine();
      printer.println("Registration process has been stopped.");
      return;
    }

    Transaction transaction;
    if (authType == AuthenticationType.KYC) {
      transaction =
          KycTransactions.createKycSubmit(
              synapsResponse,
              blockchainContractClient,
              nodeConfigReader.getBlockchainAddress(),
              nodeConfigReader.getNetworkPublicKey(),
              nodeConfigReader.getFinalizationKey());
    } else {
      KybUserInput userInput = cliInput.askForKybData();
      transaction =
          KycTransactions.createKybApprove(
              synapsResponse,
              userInput,
              blockchainContractClient,
              nodeConfigReader.getBlockchainAddress(),
              nodeConfigReader.getNetworkPublicKey(),
              nodeConfigReader.getFinalizationKey());
    }

    printer.println("Waiting for transaction to complete...");
    Transactions.TransactionResult result =
        Transactions.sendTransaction(
            transaction,
            KycTransactions.TRANSACTION_COST,
            blockchainContractClient,
            nodeConfigReader.getAccountKey());

    printExitMessage(printer, result, authType);
  }

  /**
   * Run node registration transaction cli.
   *
   * @param args the first argument is location of config file
   * @throws IOException Unable to read or write to IO
   */
  public static void main(String[] args) throws IOException {
    run(
        args,
        System.in,
        System.out,
        ClientBuilder.newClient(),
        BlockchainClient.create("http://localhost:8080", 3),
        HttpService::new);
  }

  private RegisterNodeCli() {}
}
