package com.partisiablockchain.configgenerator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;

final class TextFunctions {
  private TextFunctions() {}

  static List<String> alignTable(List<List<String>> rows, List<TextAlignment> alignments) {
    int columnCount = alignments.size();
    List<Integer> sizes = new ArrayList<>(columnCount);
    for (int i = 0; i < columnCount; i++) {
      sizes.add(0);
    }

    for (List<String> row : rows) {
      for (int i = 0; i < sizes.size(); i++) {
        Integer max = sizes.get(i);
        sizes.set(i, Math.max(max, row.get(i).length()));
      }
    }

    List<String> output = new ArrayList<>();
    for (List<String> row : rows) {
      List<String> alignedRow = new ArrayList<>();
      for (int i = 0; i < columnCount; i++) {
        Integer cellSize = sizes.get(i);
        String cellText = row.get(i);

        TextAlignment alignment = alignments.get(i);
        alignedRow.add(alignText(cellText, alignment, cellSize));
      }
      output.add(String.join(" ", alignedRow));
    }
    return output;
  }

  static String alignText(String text, TextAlignment alignment, int maxLength) {
    if (text.length() == maxLength) {
      return text;
    }

    if (alignment == TextAlignment.RIGHT) {
      int spaces = maxLength - text.length();
      return " ".repeat(spaces) + text;
    } else if (alignment == TextAlignment.LEFT) {
      int spaces = maxLength - text.length();
      return text + " ".repeat(spaces);
    } else {
      int spaces = maxLength - text.length();

      int leftHalf = spaces / 2;
      int rightHalf = maxLength - text.length() - leftHalf;

      return " ".repeat(leftHalf) + text + " ".repeat(rightHalf);
    }
  }

  static List<String> splitIntoLines(String text, int maxLength) {
    List<String> lines = new ArrayList<>();

    Counter counter = new Counter(maxLength);

    List<String> currentLine = new ArrayList<>();
    for (String word : text.split("\\s", -1)) {
      if (counter.wordFits(word)) {
        currentLine.add(word);
        counter.addWord(word);
      } else {
        lines.add(String.join(" ", currentLine));

        currentLine = new ArrayList<>();
        currentLine.add(word);

        counter.reset();
        counter.addWord(word);
      }
    }

    lines.add(String.join(" ", currentLine));

    return lines;
  }

  private static final class Counter {
    private final int maxLength;

    private int lengthOfWords;
    private int wordCount;
    private int spaces;

    private Counter(int maxLength) {
      this.maxLength = maxLength;
    }

    private boolean wordFits(String word) {
      return lengthOfWords + spaces + word.length() <= maxLength;
    }

    private void reset() {
      lengthOfWords = 0;
      spaces = 0;
      wordCount = 0;
    }

    private void addWord(String word) {
      lengthOfWords += word.length();
      wordCount++;

      spaces = wordCount - 1;
    }
  }
}
