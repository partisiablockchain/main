package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates monthly performance scores for block producers across all shards. The underlying
 * computation of the performance score from the blocks produced is defined in {@link
 * BpScoresCalculator}.
 */
public final class BpMonthlyScoreCalculator {

  private final Committees committees;
  private final List<String> shards;
  private final RewardsClient client;

  /**
   * Caches the total number of produced blocks per block producer for each month that the
   * calculator has encountered.
   */
  private final Map<YearMonth, Map<BlockchainAddress, Long>> producedBlocks = new HashMap<>();

  /**
   * Holds the block producer performance score for each month that the calculator has computed a
   * monthly score for.
   */
  private final Map<YearMonth, Map<BlockchainAddress, BpScore>> scores = new HashMap<>();

  /**
   * Construct a new calculator.
   *
   * @param shards the shards present on the chain where blocks should be fetched from
   * @param committees all known committees used to identity producers
   * @param client client to communicate with the chain
   */
  public BpMonthlyScoreCalculator(
      List<String> shards, Committees committees, RewardsClient client) {
    this.committees = committees;
    this.shards = shards;
    this.client = client;
  }

  /**
   * Compute BP score for the month of the supplied date.
   *
   * @param date the time to compute score for
   * @return the computed scores
   */
  public Map<BlockchainAddress, BpScore> computeMonthlyScore(LocalDate date) {
    YearMonth month = YearMonth.from(date);

    return scores.computeIfAbsent(month, this::computeScoreForMonth);
  }

  /**
   * Get all the scores that has been computed by this calculator.
   *
   * <p>Does not return scores for months that are only fetched to determine new and incumbent
   * producers.
   *
   * @return map of monthly scores
   */
  public Map<YearMonth, Collection<BpScore>> getComputedScores() {
    Map<YearMonth, Collection<BpScore>> result = new HashMap<>();
    scores.forEach((key, value) -> result.put(key, value.values()));
    return result;
  }

  private Map<BlockchainAddress, BpScore> computeScoreForMonth(YearMonth month) {
    Map<BlockchainAddress, Long> producedBlocksPreviousMonth =
        getProducedBlocks(month.minusMonths(1));
    return BpScoresCalculator.calculateScores(
        getProducedBlocks(month), producedBlocksPreviousMonth.keySet());
  }

  private Map<BlockchainAddress, Long> getProducedBlocks(YearMonth month) {
    return producedBlocks.computeIfAbsent(month, this::computeProducedBlocksForMonth);
  }

  private Map<BlockchainAddress, Long> computeProducedBlocksForMonth(YearMonth month) {
    Map<BlockchainAddress, Long> blocks = new HashMap<>();
    for (String shard : shards) {
      long lastBlockBeforeMonth =
          client.getBlockTimeAtTimestamp(
              month.atDay(1).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli(), shard);
      long lastBlockInMonth =
          client.getBlockTimeAtTimestamp(
              month.plusMonths(1).atDay(1).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli(),
              shard);
      Map<RewardsClient.CommitteeProducer, Long> producedBlocks =
          client.getProducedBlocks(lastBlockBeforeMonth + 1, lastBlockInMonth, shard);
      committees
          .determineProducerIdentities(producedBlocks)
          .forEach((producer, blockCount) -> blocks.merge(producer, blockCount, Long::sum));
    }
    return blocks;
  }
}
