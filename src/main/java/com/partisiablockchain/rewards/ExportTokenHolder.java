package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.rewards.accountplugin.Balance;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Exports all {@link TokenHolder} from all given {@link Balance} on the given date to find accounts
 * with staked MPC tokens. {@link TokenHolder} and {@link Balance} are based on <a
 * href="https://partisiablockchain.gitlab.io/documentation/pbc-fundamentals/mpc-token-model-and-account-elements.html">$MPC
 * token model and Account Elements</a>.
 */
public final class ExportTokenHolder {

  private final long timeToExportUtcMillis;

  /** All account balances on blockchain. */
  private final Map<BlockchainAddress, Balance> balances;

  /**
   * Default constructor. Exporting is done at the end of the supplied dateForExport in UTC.
   *
   * @param dateForExport when to export.
   * @param balances account balances on blockchain
   */
  ExportTokenHolder(LocalDate dateForExport, Map<BlockchainAddress, Balance> balances) {
    this.timeToExportUtcMillis =
        dateForExport.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();
    this.balances = balances;
  }

  /**
   * Exports {@link TokenHolder} from all account {@link Balance} for calculating stake rewards.
   *
   * @return Token holders.
   */
  List<TokenHolder> exportTokenHolders() {
    List<TokenHolder> tokenHolders = new ArrayList<>();
    for (Map.Entry<BlockchainAddress, Balance> account : balances.entrySet()) {
      TokenHolder tokenHolder = buildTokenHolderIfStakedMpc(account.getKey(), account.getValue());
      if (tokenHolder != null) {
        tokenHolders.add(tokenHolder);
      }
    }
    return tokenHolders;
  }

  /**
   * Builds a {@link TokenHolder} from {@link Balance} if the account owns or is delegated MPC. Null
   * is returned otherwise. {@link TokenHolder} is used for calculating stake rewards.
   *
   * @param identity blockchain address of balance
   * @param balance balance of account
   * @return tokenHolder if account owns MPC
   */
  private TokenHolder buildTokenHolderIfStakedMpc(BlockchainAddress identity, Balance balance) {
    if (!balance.hasStakedMpc()) {
      return null;
    }
    return createTokenHolder(identity, balance);
  }

  /**
   * Creates {@link TokenHolder} from a blockchain address and {@link Balance}. Values are
   * calculated according to <a
   * href="https://partisiablockchain.gitlab.io/documentation/pbc-fundamentals/mpc-token-model-and-account-elements.html">$MPC
   * token model and Account Elements</a>
   *
   * @param identity blockchain address of balance
   * @param balance balance of account
   * @return all tokens information associated with address
   */
  private TokenHolder createTokenHolder(BlockchainAddress identity, Balance balance) {
    return new TokenHolder(
        identity,
        balance.getTransferable(),
        balance.getInTransit(),
        balance.getStakedToSelf(),
        balance.getPendingUnstakes(),
        balance.getDelegatedToOthers(),
        balance.getAcceptedFromOthers(),
        balance.getStakeForJobs(),
        balance.getPendingRetractedDelegated(),
        balance.getInSchedule(),
        balance.getReleasable(timeToExportUtcMillis),
        balance.getStaked(),
        balance.getUnlocked(timeToExportUtcMillis),
        balance.getTotal(),
        balance.getUnlockedRatio(timeToExportUtcMillis),
        balance.getStakeRatio(),
        balance.getStakeCapRatio());
  }
}
