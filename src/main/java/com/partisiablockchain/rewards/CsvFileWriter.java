package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.RecordComponent;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;

/** Writes collections of records into semicolon-separated CSV files. */
public final class CsvFileWriter {

  /**
   * Writes a collection of records to a CSV file using the record field names as headers.
   *
   * <p>Field names are capitalized when used as headers. The lines of the CSV-file are sorted using
   * a comparator. Each record field is printed using its toString()-method. Blockchain addresses
   * are printed using the normal human-readable form.
   *
   * @param recordType record type
   * @param entries entries to write
   * @param sortOrder comparator used for sorting the entities
   * @param file file to write to
   * @param <T> type
   * @throws FileNotFoundException if file does not exist
   */
  static <T extends Record> void writeRecords(
      Class<T> recordType, Collection<T> entries, Comparator<T> sortOrder, File file)
      throws IOException {

    RecordComponent[] recordComponents = recordType.getRecordComponents();
    try (PrintWriter printWriter = new PrintWriter(file, StandardCharsets.UTF_8)) {

      StringJoiner header = new StringJoiner(";");
      for (RecordComponent recordComponent : recordComponents) {
        String name = recordComponent.getName();
        header.add(name.substring(0, 1).toUpperCase(Locale.getDefault()) + name.substring(1));
      }
      printWriter.println(header);

      List<T> sortedEntries = entries.stream().sorted(sortOrder).toList();

      for (T entry : sortedEntries) {
        StringJoiner line = new StringJoiner(";");
        for (RecordComponent recordComponent : recordComponents) {
          if (recordComponent.getType() == BlockchainAddress.class) {
            BlockchainAddress value =
                (BlockchainAddress)
                    ExceptionConverter.call(() -> recordComponent.getAccessor().invoke(entry));
            line.add(value.writeAsString());
          } else {
            Object value =
                ExceptionConverter.call(() -> recordComponent.getAccessor().invoke(entry));
            line.add(String.valueOf(value));
          }
        }
        printWriter.println(line);
      }
    }
  }

  private CsvFileWriter() {}
}
