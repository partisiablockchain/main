package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

/** Exports every account {@link Balance} from a shard for a given day. */
public final class ExportBalance {
  private final RewardsClient client;
  private final String shard;
  private final long timeToExportUtcMillis;

  /**
   * Default constructor. Exporting is done at 23:59 of the day in UTC.
   *
   * @param day when to export.
   * @param client to fetch from blockchain.
   * @param shard which local account plugin to fetch
   */
  public ExportBalance(LocalDate day, RewardsClient client, String shard) {
    this.timeToExportUtcMillis = day.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();
    this.client = client;
    this.shard = shard;
  }

  /**
   * Exports {@link Balance} for all Blockchain accounts in local AccountPlugin.
   *
   * @return every account {@link Balance}.
   */
  public Map<BlockchainAddress, Balance> exportBalances() {
    JsonNode accountPlugin =
        client.getAccountPluginAtBlockTime(
            client.getBlockTimeAtTimestamp(timeToExportUtcMillis, shard), shard);
    return convertJsonAccountsToBalances(accountPlugin);
  }

  static Map<BlockchainAddress, Balance> convertJsonAccountsToBalances(JsonNode accountsJson) {
    Map<BlockchainAddress, Balance> result = new HashMap<>();
    for (JsonNode account : accountsJson) {
      JsonNode value = account.get("value");
      Balance balance =
          ExceptionConverter.call(
              () ->
                  StateObjectMapper.createObjectMapper()
                      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                      .treeToValue(value, Balance.class),
              "Could not deserialize account from account plugin");
      result.put(BlockchainAddress.fromString(account.get("key").asText()), balance);
    }
    return result;
  }
}
