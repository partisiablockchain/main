package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Distributes rewards for a week to stakeholders. */
public final class RewardsCalculator {

  /**
   * Distributes weekly rewards to all stakers.
   *
   * <p>A fixed amount of reward tokens are distributed to stakers each week. The rewards for each
   * week are distributed relative to the amount of rewardable tokens after performance adjustments
   * as defined by {@link Stake#rewardable()}.
   *
   * @param stakes all stakes relations that should be rewarded within the given week
   * @param weeklyRewards the total amount of reward tokens to be distributed for the given week
   * @return the distributed rewards
   */
  static List<Reward> buildRewards(List<Stake> stakes, long weeklyRewards) {
    Map<BlockchainAddress, Double> rewardablePerHolder = new HashMap<>();
    double sumOfRewardable = 0d;

    List<Stake> sortedStakes =
        stakes.stream().sorted(Comparator.comparing(Stake::from).thenComparing(Stake::to)).toList();

    for (Stake stake : sortedStakes) {
      rewardablePerHolder.merge(stake.from(), stake.rewardableTokenHolder(), Double::sum);
      rewardablePerHolder.merge(stake.to(), stake.rewardableBp(), Double::sum);
      sumOfRewardable += stake.rewardable();
    }
    List<Reward> result = new ArrayList<>();
    for (Map.Entry<BlockchainAddress, Double> rewardableHolder : rewardablePerHolder.entrySet()) {
      BlockchainAddress identity = rewardableHolder.getKey();
      double rewardableForHolder = rewardableHolder.getValue();

      double rewards = rewardableForHolder / sumOfRewardable * weeklyRewards;
      result.add(
          new Reward(identity, weeklyRewards, sumOfRewardable, rewardableForHolder, rewards));
    }
    return result;
  }

  private RewardsCalculator() {}
}
