package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/**
 * A single staking relation between two accounts. This either represents a delegated stake between
 * a user and a BP or a BP that has self-staked.
 *
 * <p>The stake contains the amount of rewardable tokens for the user and for the BP. A rewardable
 * token is a token that are unlocked and delegated to an active block producer. The computation of
 * rewardable tokens distribute the free tokens equally among all the staking relation the staker
 * takes part in, such that to get full rewards a user has to stake all his tokens.
 *
 * @param from the owner of the tokens that are staked
 * @param to the receiving BP of the stakes
 * @param amount the number of MPC tokens that has been delegated and accepted or self-staked in the
 *     relation
 * @param pendingAmount the number of MPC tokens that has been delegated and are pending accept in
 *     the relation
 * @param staked the total number of MPC tokens that are staked towards running blockchain jobs for
 *     {@link #from}
 * @param unlocked the number of MPC tokens that either have been released or are releasable from an
 *     unlocking schedule for {@link #from}
 * @param total the total number of MPC tokens owned by {@link #from}
 * @param unlockedRatio the ratio of total tokens that are unlocked for {@link #from}
 * @param stakeRatio the ratio of total tokens that are staked for {@link #from}
 * @param stakeCapRatio the ratio of staked token that are under the cap of staked tokens eligible
 *     for rewards for {@link #to}
 * @param bpScore scores for block producer ({@link #to}) based on blocks produced relative to other
 *     producers
 * @param delegationRatio the percentage that this delegation represents in relation to the total
 *     stakes for {@link #from}
 * @param rewardableBeforePerformance the number of tokens that are rewardable prior to performance
 *     adjustment
 * @param rewardable the number of tokens that are rewardable after performance adjustment
 * @param rewardableTokenHolder the number of tokens that are rewardable for {@link #from}
 * @param rewardableBp the number of tokens that are rewardable for {@link #to}
 */
public record Stake(
    BlockchainAddress from,
    BlockchainAddress to,
    long amount,
    long pendingAmount,
    long staked,
    long unlocked,
    long total,
    double unlockedRatio,
    double stakeRatio,
    double stakeCapRatio,
    double bpScore,
    double delegationRatio,
    double rewardableBeforePerformance,
    double rewardable,
    double rewardableTokenHolder,
    double rewardableBp) {}
