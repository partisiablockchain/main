package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/**
 * Rewards that a recipient will receive for a given week.
 *
 * @param identity Blockchain address of the recipient
 * @param weeklyRewardInQuarter the total amount of rewards that are to be paid out in the given
 *     week
 * @param sumOfRewardable the sum of rewardable tokens for all stakers in the given week
 * @param rewardableForHolder the sum of rewardable tokens for this recipient in the given week
 * @param rewards the rewards for this recipient in the given week
 */
public record Reward(
    BlockchainAddress identity,
    long weeklyRewardInQuarter,
    double sumOfRewardable,
    double rewardableForHolder,
    double rewards) {}
