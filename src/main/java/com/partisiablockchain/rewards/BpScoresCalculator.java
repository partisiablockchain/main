package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates performance scores for block producers based on how many blocks they have produced.
 */
public final class BpScoresCalculator {

  /**
   * Calculates the <i>performance score</i> for all block producers in a period. In practice the
   * period is a calendar month.
   *
   * <p>The performance score is defined as the number of blocks produced by the producer in the
   * period divided by the maximum number of blocks produced by any producer in the period. A
   * performance score of 1.0 means the producer has produced the same number of blocks as the most
   * productive producer in the period. A performance score of 0.0 means the producer has produced
   * no blocks in the period.
   *
   * <p>To avoid that newly added producers receive a very low performance score, any producer that
   * produced no blocks in the previous period will be regarded as a <i>new producer</i>, and will
   * receive a performance score that is the average of all non-new producers.
   *
   * @param blocksProducedInPeriod Map containing the number of blocks produced by each producer in
   *     the period. Only producers that have been a part of the committee in the period must be
   *     present in the map.
   * @param producersInPreviousPeriod All producers that produced blocks in previous period. Used to
   *     identify new producers.
   * @return The performance score and number of blocks produced for each producer in the period
   */
  public static Map<BlockchainAddress, BpScore> calculateScores(
      Map<BlockchainAddress, Long> blocksProducedInPeriod,
      Collection<BlockchainAddress> producersInPreviousPeriod) {

    // Identify new and incumbent producers
    List<BlockchainAddress> newProducers = new ArrayList<>();
    List<BlockchainAddress> incumbentProducers = new ArrayList<>();
    for (BlockchainAddress producer : blocksProducedInPeriod.keySet()) {
      if (producersInPreviousPeriod.contains(producer)) {
        incumbentProducers.add(producer);
      } else {
        newProducers.add(producer);
      }
    }

    long maxBlocksProduced =
        incumbentProducers.stream()
            .map(blocksProducedInPeriod::get)
            .mapToLong(Long::longValue)
            .summaryStatistics()
            .getMax();

    double sumOfIncumbentScores = 0.0d;

    // Sort to ensure that the computed results are consistent since the order of operations on
    // doubles can impact the least significant digits in the result
    Collections.sort(incumbentProducers);

    // Calculate performance score for all incumbent producers
    Map<BlockchainAddress, BpScore> producerScores = new HashMap<>();
    for (BlockchainAddress producer : incumbentProducers) {
      Long blocksProduced = blocksProducedInPeriod.get(producer);
      double score = blocksProduced.doubleValue() / maxBlocksProduced;
      BpScore bpScore = new BpScore(producer, blocksProduced, score);
      producerScores.put(producer, bpScore);
      sumOfIncumbentScores += score;
    }

    // Set all new producers performance score to the average incumbent producer score
    double averageIncumbentScore = sumOfIncumbentScores / incumbentProducers.size();
    for (BlockchainAddress producer : newProducers) {
      Long blocksProduced = blocksProducedInPeriod.get(producer);
      BpScore bpScore = new BpScore(producer, blocksProduced, averageIncumbentScore);
      producerScores.put(producer, bpScore);
    }

    return producerScores;
  }

  private BpScoresCalculator() {}
}
