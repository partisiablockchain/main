package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.server.model.BlockProducer;
import com.partisiablockchain.server.model.BlockchainStatistics;
import com.partisiablockchain.server.model.Committee;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Implementation for fetching blockchain information. */
public final class RewardsClientImpl implements RewardsClient {
  private final String url;
  private final Client client;

  /** Metrics endpoint MAX interval size. Endpoint is inclusive on both block times. */
  private static final int BLOCK_INTERVAL_MAX_STEP_SIZE = 1000;

  /**
   * Default constructor.
   *
   * @param url base url for creating requests.
   * @param client client to make requests.
   */
  public RewardsClientImpl(String url, Client client) {
    this.url = url;
    this.client = client;
  }

  @Override
  public long getBlockTimeAtTimestamp(long timestamp, String shard) {
    String baseUrl = url(shard) + "/blockchain";
    return client
        .target(baseUrl + "/blocks/latestBlockTime?utcTime=" + timestamp)
        .request()
        .get(long.class);
  }

  @Override
  public JsonNode getAccountPluginAtBlockTime(long blockTime, String shard) {
    String baseUrl = url(shard) + "/blockchain";
    return client
        .target(baseUrl + "/accountPlugin/local" + "?blockTime=" + blockTime)
        .request(MediaType.APPLICATION_JSON_TYPE)
        .post(
            Entity.entity(
                new TraversePath(List.of(new FieldTraverse("accounts"))),
                MediaType.APPLICATION_JSON_TYPE),
            JsonNode.class);
  }

  @Override
  public BlockState getBlock(long blockTime, String shard) {
    return client
        .target(url(shard) + "/blockchain/blocks/blockTime/" + blockTime)
        .request()
        .get(BlockState.class);
  }

  @Override
  public Map<CommitteeProducer, Long> getProducedBlocks(
      long fromBlockTime, long toBlockTime, String shard) {
    Map<CommitteeProducer, Long> producedCount = new HashMap<>();

    for (long i = fromBlockTime; i <= toBlockTime; i += BLOCK_INTERVAL_MAX_STEP_SIZE) {
      long end = Math.min(i + BLOCK_INTERVAL_MAX_STEP_SIZE - 1, toBlockTime);
      BlockchainStatistics blockStat = fetchBlockMetricInterval(i, end, shard);

      for (Map.Entry<Long, Committee> commEntry : blockStat.committees.entrySet()) {
        Long committeeIndex = commEntry.getKey();
        Committee producerInfo = commEntry.getValue();
        for (Map.Entry<Short, BlockProducer> producerEntry : producerInfo.producers.entrySet()) {
          Short producerIndex = producerEntry.getKey();
          BlockProducer producedBlocks = producerEntry.getValue();

          producedCount.merge(
              new CommitteeProducer(committeeIndex, producerIndex),
              producedBlocks.blocksProduced,
              Long::sum);
        }
      }
    }
    return producedCount;
  }

  /**
   * Fetches blocks produced by producers in the interval {@code blockTimeIntervalStart} to {@code
   * blockTimeIntervalEnd}, inclusive on both block times. The metrics endpoint is limited to a max
   * interval size of {@code BLOCK_INTERVAL_MAX_STEP_SIZE}.
   *
   * @param blockTimeIntervalStart interval start, inclusive
   * @param blockTimeIntervalEnd interval end, inclusive
   * @param shard which shard to fetch from
   * @return BlockchainStatistics for interval on shard
   */
  private BlockchainStatistics fetchBlockMetricInterval(
      long blockTimeIntervalStart, long blockTimeIntervalEnd, String shard) {
    return client
        .target(
            url(shard)
                + "/metrics/blocks/?from="
                + blockTimeIntervalStart
                + "&to="
                + blockTimeIntervalEnd)
        .request()
        .get(BlockchainStatistics.class);
  }

  @Override
  public Committees getAllCommittees() {
    JsonNode committeesJson =
        client
            .target(url("Shard0") + "/blockchain/consensusPlugin/global")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .post(
                Entity.entity(
                    new TraversePath(List.of(new FieldTraverse("committees"))),
                    MediaType.APPLICATION_JSON_TYPE),
                JsonNode.class);
    return convertGlobalConsensusCommitteeJson(committeesJson);
  }

  static Committees convertGlobalConsensusCommitteeJson(JsonNode committeesJson) {
    Map<Long, List<BlockchainAddress>> committeeMembers = new HashMap<>();
    for (int i = 0; i < committeesJson.size(); i++) {
      List<BlockchainAddress> producers = new ArrayList<>();
      JsonNode producerJson = committeesJson.get(i).get("activeCommittee");
      for (int j = 0; j < producerJson.size(); j++) {
        producers.add(
            BlockchainAddress.fromString(producerJson.get(j).get("identity").textValue()));
      }
      committeeMembers.put((long) i, producers);
    }
    return new Committees(committeeMembers);
  }

  private String url(String shard) {
    if ("Gov".equals(shard)) {
      return url;
    } else {
      return url + "/shards/" + shard;
    }
  }
}
