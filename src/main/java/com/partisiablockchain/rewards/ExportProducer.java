package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.dto.BlockState;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

/** Exports a list of committee producers for a given date. */
public final class ExportProducer {
  private final RewardsClient client;
  private final Committees committees;
  private final long timeToExportUtcMillis;
  private static final String shard0 = "Shard0";

  /**
   * Default constructor. Exporting is done at 23:59 of the day in UTC.
   *
   * @param client client to get committee list from Bpo contract
   * @param day which day to fetch committee from
   * @param committees all committees sorted by index
   */
  public ExportProducer(RewardsClient client, LocalDate day, Committees committees) {
    this.timeToExportUtcMillis = day.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();
    this.client = client;
    this.committees = committees;
  }

  /**
   * Fetches the producers at the time given in the constructor.
   *
   * @return list of producers at the time
   */
  public List<BlockchainAddress> exportProducers() {
    long blockTime = client.getBlockTimeAtTimestamp(timeToExportUtcMillis, shard0);
    BlockState block = client.getBlock(blockTime, shard0);
    return committees.getCommittee(block.committeeId());
  }
}
