package com.partisiablockchain.rewards.accountplugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/**
 * A vesting account contains information about the total amount of vested tokens together with the
 * amount of tokens currently released. Each vesting account is for a specific vesting schedule and
 * an account on the blockchain can have multiple vesting accounts.
 *
 * <p>Copied from AccountPlugin.
 *
 * @param tokens Total amount of tokens
 * @param releasedTokens Amount release of total
 * @param tokenGenerationEvent When tokens were generated.
 * @param releaseDuration How long it will take for tokens all to be releasable
 * @param releaseInterval How often tokens should be released during releaseDuration
 */
@Immutable
public record InitialVestingAccount(
    long tokens,
    long releasedTokens,
    long tokenGenerationEvent,
    long releaseDuration,
    long releaseInterval) {
  /** Start time of pausing the release of vested presale tokens - corresponds to 2023-05-20. */
  public static final long PAUSE_RELEASE_START_TIME = 1684540800000L;

  /** End time of pausing the release of vested presale tokens - corresponds to 2024-03-20. */
  public static final long PAUSE_RELEASE_END_TIME = 1710892800000L;

  /** Release duration for public token sale. */
  public static final long PUBLIC_SALE_RELEASE_DURATION = 62899200000L;

  /** Release interval for public token sale. */
  public static final long PUBLIC_SALE_RELEASE_INTERVAL = 7862400000L;

  /**
   * Calculates the amount of vested tokens ready for release.
   *
   * <p>The release of vested presale tokens are paused in the run-up to a potential exchange
   * launch. Token buyers during public sale at TGE are not affected by this.
   *
   * @param blockProductionTime block production time
   * @return amount of vested tokens ready for release
   */
  public long getAmountToRelease(long blockProductionTime) {
    if (!isPublicSale()) {
      blockProductionTime = subtractPause(blockProductionTime);
    }
    if (blockProductionTime >= tokenGenerationEvent + releaseDuration) {
      return tokens - releasedTokens;
    } else {
      long amountToRelease = tokens / (releaseDuration / releaseInterval);
      long batches = Math.max(0, (blockProductionTime - tokenGenerationEvent) / releaseInterval);
      long totalToRelease = amountToRelease * batches;

      return totalToRelease - releasedTokens;
    }
  }

  /**
   * Checks if the vesting schedule is from a public sale.
   *
   * @return true if vesting schedule is from a public sale otherwise false
   */
  private boolean isPublicSale() {
    return releaseDuration == PUBLIC_SALE_RELEASE_DURATION
        && releaseInterval == PUBLIC_SALE_RELEASE_INTERVAL;
  }

  /**
   * Subtracts the pause from the block production time to avoid including it when calculating the
   * amount of vested tokens to release.
   *
   * <p>Depending on if the current block production time is before, during, or after the pause
   * duration the time is adjusted as follows:
   *
   * <ul>
   *   <li>If blockProductionTime &lt; {@link InitialVestingAccount#PAUSE_RELEASE_START_TIME} return
   *       blockProductionTime
   *   <li>If blockProductionTime &lt; {@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} return
   *       {@link InitialVestingAccount#PAUSE_RELEASE_START_TIME}
   *   <li>If blockProductionTime &gt; {@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} return
   *       blockProductionTime - ({@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} - {@link
   *       InitialVestingAccount#PAUSE_RELEASE_START_TIME})
   * </ul>
   *
   * @param blockProductionTime block production time
   * @return block production time adjusted for pause duration
   */
  public static long subtractPause(long blockProductionTime) {
    return Math.max(
        Math.min(PAUSE_RELEASE_START_TIME, blockProductionTime),
        blockProductionTime - (PAUSE_RELEASE_END_TIME - PAUSE_RELEASE_START_TIME));
  }
}
