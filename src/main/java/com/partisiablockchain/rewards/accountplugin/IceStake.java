package com.partisiablockchain.rewards.accountplugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;

/**
 * Used in the two phase ice staking protocol to represent a pending stake to a contract, for the
 * purpose of "icing" it.
 *
 * <p>Copied from AccountPlugin.
 *
 * @param address From the account's point of view, this is the contract to ice stake towards. From
 *     the contract's point of view, this is the account doing the ice staking.
 * @param amount The amount of MPC tokens to ice stake
 * @param type The type of ice staking see {@link IceStakeType}
 */
@Immutable
public record IceStake(BlockchainAddress address, long amount, IceStakeType type) {

  /** Types of ice staking. */
  public enum IceStakeType {
    /** Associate ice stakes. */
    ASSOCIATE,
    /** Disassociate ice stakes. */
    DISASSOCIATE
  }
}
