package com.partisiablockchain.rewards.accountplugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.time.Instant;
import java.util.List;

/**
 * Keeps track of an accounts balance of MPC tokens and BYOC coins. Can calculate values relevant to
 * MPC holdings as according to <a
 * href="https://partisiablockchain.gitlab.io/documentation/pbc-fundamentals/mpc-token-model-and-account-elements.html">$MPC
 * token model and Account Elements</a>.
 *
 * <p>Fields are copied from AccountPlugin.
 *
 * @param accountCoins The amount of each BYOC coin owned by the account. The index in this list
 *     matches the index in the <code>coins</code> list in AccountPlugins AccountStateGlobal.
 * @param mpcTokens The total amount of free MPC tokens.
 * @param stakedTokens The total amount of MPC tokens that are staked. To run a service the staked
 *     MPC tokens must be associated to a specific contract address.
 * @param pendingUnstakes MPC tokens which were staked, and are now being freed. When unstaking MPC
 *     tokens there is a 7 days waiting period during which they are kept here. The keys in the map
 *     are unix timestamps specifying when MPC tokens will be freed, and the values are the amounts
 *     of MPC tokens that will be freed at that time.
 * @param vestingAccounts MPC tokens that are vested which will only become free for transfer after
 *     the vesting period.
 * @param storedPendingTransfers Keeps track of incoming/outgoing transfers of MPC tokens and BYOC
 *     coins while a two-phase commit protocol is being executed. The keys in the map are
 *     transaction event hashes from the first phase of the transfer, and the values are information
 *     about the pending transfer.
 * @param storedPendingStakeDelegations Keeps track of incoming/outgoing delegated MPC tokens while
 *     a two-phase commit protocol is being executed. The keys in the map are transaction event
 *     hashes from the first phase of the delegation, and the values are information about the
 *     pending stake delegation.
 * @param delegatedStakesFromOthers MPC tokens delegated from other users. If they are accepted MPC
 *     tokens delegated from others can be used to run a service by associating to a contract.
 *     Delegated tokens can have an expiration timestamp, at which they can no longer be used to
 *     associate to contract. The keys in the map are addresses of the accounts that have delegated
 *     MPC tokens to this account, and the values are information about the delegated stakes.
 * @param delegatedStakesToOthers MPC tokens delegated to other users for them to use as stake. The
 *     keys in the map are addresses of the accounts that MPC tokens have been delegated to, and the
 *     values are the amount of MPC tokens delegated to the corresponding account.
 * @param pendingRetractedDelegatedStakes MPC tokens, which were delegated to another user and are
 *     now being retracted and freed. Retraction of delegated MPC tokens has a 7 days waiting period
 *     during which they are kept here. The keys in the map are unix timestamps specifying when MPC
 *     tokens will be freed, and the values are the amounts of MPC tokens that will be freed at that
 *     time.
 * @param stakeable Specifies if this account is allowed to stake MPC tokens.
 * @param storedPendingIceStakes Pending ice stakes. I.e., ice stakes that have been locked on the
 *     account, but aren't tracked on the address yet.
 * @param tokensInCustody The total number of MPC tokens for the account. Calculated as {@link
 *     #stakedTokens} + sum of {@link #delegatedStakesFromOthers} + sum of {@link
 *     #pendingRetractedDelegatedStakes} + sum of {@link #pendingUnstakes} + sum of outgoing {@link
 *     #storedPendingTransfers} + sum of {@link #storedPendingStakeDelegations} which are either
 *     DELEGATE_STAKES or RETURN_DELEGATED_STAKES + unreleased vesting schedules from {@link
 *     #vestingAccounts} + {@link #mpcTokens}
 */
@Immutable
public record Balance(
    FixedList<AccountCoin> accountCoins,
    long mpcTokens,
    long stakedTokens,
    AvlTree<Long, Long> pendingUnstakes,
    FixedList<InitialVestingAccount> vestingAccounts,
    AvlTree<Hash, TransferInformation> storedPendingTransfers,
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations,
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers,
    AvlTree<BlockchainAddress, Long> delegatedStakesToOthers,
    AvlTree<Long, Long> pendingRetractedDelegatedStakes,
    boolean stakeable,
    AvlTree<Hash, IceStake> storedPendingIceStakes,
    long tokensInCustody) {

  /** Upper limit at which staked tokens does not contribute to rewards. */
  public static final double STAKING_REWARD_CAP = 5_000_000_0000d;

  /** Initial release time for vesting account tokens in epoch millis UTC. */
  static final long INITIAL_RELEASE = Instant.parse("2022-11-30T23:59:59Z").toEpochMilli();

  /**
   * Determines whether this balance contains staked MPC. This can either be its own MPC or
   * delegated stakes from others.
   *
   * @return true if it has staked MPC
   */
  public boolean hasStakedMpc() {
    return getStakedToSelf() + getDelegatedToOthers() > 0 || delegatedStakesFromOthers.size() > 0;
  }

  /**
   * The total number of MPC tokens that can be transferred to another account.
   *
   * @return The number of MPC tokens
   */
  public long getTransferable() {
    return mpcTokens;
  }

  /**
   * The number of MPC tokens that are in transit in either delegation or token transfer.
   *
   * @return The number of MPC tokens
   */
  public long getInTransit() {
    return computeInTransitTransfer(storedPendingTransfers().values())
        + computeInTransitDelegated(storedPendingStakeDelegations().values());
  }

  /**
   * The number of MPC tokens that are staked to the account itself.
   *
   * @return The number of MPC tokens
   */
  public long getStakedToSelf() {
    return stakedTokens;
  }

  /**
   * The number of MPC tokens staked to the account itself that have been unstaked, but are still
   * pending.
   *
   * @return The number of MPC tokens
   */
  public long getPendingUnstakes() {
    return pendingUnstakes().values().stream().mapToLong(Long::longValue).sum();
  }

  /**
   * The number of MPC tokens that have been delegated to other accounts.
   *
   * @return The number of MPC tokens
   */
  public long getDelegatedToOthers() {
    return delegatedStakesToOthers().values().stream().mapToLong(Long::longValue).sum();
  }

  /**
   * The total number of MPC tokens that have been delegated by others and accepted.
   *
   * @return The total number of MPC tokens
   */
  public long getAcceptedFromOthers() {
    return delegatedStakesFromOthers().values().stream()
        .mapToLong(StakesFromOthers::acceptedDelegatedStakes)
        .sum();
  }

  /**
   * The total number of MPC tokens that have been staked and can be used for running jobs as a
   * blockchain node.
   *
   * @return The total number of MPC tokens
   */
  public long getStakeForJobs() {
    return getAcceptedFromOthers() + getStakedToSelf();
  }

  /**
   * The number of stakes delegated to others accounts that have been retracted, but are still
   * pending.
   *
   * @return The number of MPC tokens
   */
  public long getPendingRetractedDelegated() {
    return pendingRetractedDelegatedStakes().values().stream().mapToLong(Long::longValue).sum();
  }

  /**
   * The number of MPC tokens that are inside an unlocking schedule.
   *
   * @return The number of MPC tokens
   */
  public long getInSchedule() {
    long inSchedule = 0;
    for (InitialVestingAccount vestingAccount : vestingAccounts()) {
      inSchedule += vestingAccount.tokens() - vestingAccount.releasedTokens();
    }
    return inSchedule;
  }

  /**
   * The number of MPC tokens that are ready to be released from an unlocking schedule at the time
   * of export.
   *
   * @param timeOfExportUtcMillis time of export
   * @return The number of MPC tokens
   */
  public long getReleasable(long timeOfExportUtcMillis) {
    long releasable = 0;
    for (InitialVestingAccount vestingAccount : vestingAccounts()) {
      long releaseTime = Math.max(timeOfExportUtcMillis, INITIAL_RELEASE);
      long amountToRelease = vestingAccount.getAmountToRelease(releaseTime);
      releasable += amountToRelease;
    }
    return releasable;
  }

  /**
   * The total number of MPC tokens that are staked towards running blockchain jobs.
   *
   * @return The number of MPC tokens
   */
  public long getStaked() {
    return getStakedToSelf() + getDelegatedToOthers();
  }

  /**
   * The number of MPC tokens that either have been released or are releasable from an unlocking
   * schedule at the time of export.
   *
   * @param timeOfExportUtcMillis time of export
   * @return The number of MPC tokens
   */
  public long getUnlocked(long timeOfExportUtcMillis) {
    return getInUse() + getTransferable() + getReleasable(timeOfExportUtcMillis);
  }

  /**
   * The total number of MPC tokens owned by an account.
   *
   * @return The number of MPC tokens
   */
  public long getTotal() {
    long released = getInUse() + getTransferable();
    return released + getInSchedule();
  }

  /**
   * The number of MPC tokens that are currently in use as stake.
   *
   * @return The number of MPC tokens
   */
  private long getInUse() {
    long pendingUnused = getPendingUnstakes() + getPendingRetractedDelegated();
    return getStaked() + pendingUnused + getInTransit();
  }

  /**
   * The ratio of unlocked MPC tokens compared to the total amount owned by an account at the time
   * of export.
   *
   * @param timeOfExportUtcMillis time of export
   * @return The ratio of unlocked MPC tokens compared to the total amount owned
   */
  public double getUnlockedRatio(long timeOfExportUtcMillis) {
    return ((double) getUnlocked(timeOfExportUtcMillis)) / getTotal();
  }

  /**
   * The ratio of staked MPC tokens compared to the total amount owned by an account at the time of
   * export.
   *
   * @return The ratio of staked MPC tokens compared to the total amount owned
   */
  public double getStakeRatio() {
    return ((double) getStaked()) / getTotal();
  }

  /**
   * The ratio of the cap on rewards for staking {@code STAKING_REWARD_CAP} compared to the amount
   * of MPC tokens staked to a job. If this ratio is above 1 it returns 1 instead.
   *
   * @return The ratio of the cap on rewards for staking {@code STAKING_REWARD_CAP} compared to the
   *     amount of MPC tokens staked to a job. If this ratio is above 1 it returns 1 instead
   */
  public double getStakeCapRatio() {
    return Math.min(STAKING_REWARD_CAP / getStakeForJobs(), 1d);
  }

  /**
   * Compute the number of MPC tokens that are in transit as part of a token transfer to another
   * account.
   *
   * @param storedPendingTransfers All stored pending transfers for MPC tokens and BYOC coins
   * @return The number of MPC tokens
   */
  private long computeInTransitTransfer(List<TransferInformation> storedPendingTransfers) {
    long pendingTransfers = 0;
    for (TransferInformation pendingTransfer : storedPendingTransfers) {
      if (!pendingTransfer.addTokensOrCoinsIfTransferSuccessful()
          && pendingTransfer.coinIndex() == -1) {
        pendingTransfers += pendingTransfer.amount().longValueExact();
      }
    }
    return pendingTransfers;
  }

  /**
   * Compute the number of MPC tokens that are in transit as part of a token delegation to another
   * account.
   *
   * @param stakeDelegations All stored pending stake delegations for the account
   * @return The number of MPC tokens
   */
  private long computeInTransitDelegated(List<StakeDelegation> stakeDelegations) {
    long pendingStakeDelegations = 0;
    for (StakeDelegation pendingDelegation : stakeDelegations) {
      StakeDelegation.DelegationType delegationType = pendingDelegation.delegationType();
      boolean isDelegateStakesOrRetractDelegatedStakes =
          delegationType.equals(StakeDelegation.DelegationType.DELEGATE_STAKES)
              || delegationType.equals(StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES);
      if (isDelegateStakesOrRetractDelegatedStakes) {
        pendingStakeDelegations += pendingDelegation.amount();
      }
    }
    return pendingStakeDelegations;
  }
}
