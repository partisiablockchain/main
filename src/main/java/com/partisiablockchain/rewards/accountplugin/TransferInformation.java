package com.partisiablockchain.rewards.accountplugin;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;

/**
 * Stores information of pending transactions for Two Phase Commit of token or BYOC transfers.
 *
 * <p>Copied from AccountPlugin.
 *
 * @param amount amount being transferred
 * @param addTokensOrCoinsIfTransferSuccessful true if this transfer is a deposit, otherwise false
 * @param coinIndex coin index of the BYOC being transferred or -1 if transfer is of MPC tokens
 */
@Immutable
public record TransferInformation(
    Unsigned256 amount, boolean addTokensOrCoinsIfTransferSuccessful, int coinIndex) {}
