package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/**
 * Keeps track of MPC holdings and stakes for a blockchain account based on <a
 * href="https://partisiablockchain.gitlab.io/documentation/pbc-fundamentals/mpc-token-model-and-account-elements.html">$MPC
 * token model and Account Elements</a>.
 *
 * @param address Blockchain address
 * @param transferable The total number of MPC tokens that can be transferred to another account
 * @param inTransit The number of MPC tokens that are in transit as part of a token transfer to
 *     another account
 * @param stakedToSelf The number of MPC tokens that are staked to the account itself
 * @param pendingUnstakes The number of MPC tokens staked to the account itself that have been
 *     unstaked, but are still pending
 * @param delegatedToOthers The number of MPC tokens that have been delegated to other accounts,
 *     which can use the tokens as stake to run a blockchain node
 * @param acceptedFromOthers The total number of MPC tokens that have been delegated by others and
 *     accepted
 * @param stakeForJobs The total number of MPC tokens that have been staked and can be used for
 *     running jobs as a blockchain node
 * @param pendingRetractedDelegated The number of stakes delegated to others accounts that have been
 *     retracted, but are still pending
 * @param inSchedule The number of MPC tokens that are inside an unlocking schedule
 * @param releaseable The number of MPC tokens that are ready to be released from an unlocking
 *     schedule
 * @param staked The total number of MPC tokens that are staked towards running blockchain jobs
 * @param unlocked The number of MPC tokens that either have been released or are releasable from an
 *     unlocking schedule
 * @param total The total number of MPC tokens owned by an account
 * @param unlockedRatio The ratio of total tokens that are unlocked
 * @param stakeRatio The ratio of total tokens that are staked
 * @param stakeCapRatio The ratio of staked token that are under the cap of staked tokens eligible
 *     for rewards
 */
public record TokenHolder(
    BlockchainAddress address,
    long transferable,
    long inTransit,
    long stakedToSelf,
    long pendingUnstakes,
    long delegatedToOthers,
    long acceptedFromOthers,
    long stakeForJobs,
    long pendingRetractedDelegated,
    long inSchedule,
    long releaseable,
    long staked,
    long unlocked,
    long total,
    double unlockedRatio,
    double stakeRatio,
    double stakeCapRatio) {}
