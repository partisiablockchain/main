package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.WithResource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

/** A three-month period used in the calculation of rewards. */
public final class RewardQuarter {
  /**
   * The first day of the very first rewards quarter. This is the start of the quarter with index 1.
   */
  static final LocalDate START_MONTH = LocalDate.of(2022, Month.JUNE, 1);

  static final int MONTHS_PER_QUARTER = 3;

  private final int quarterIndex;

  /**
   * Create a reward quarter from an index. The quarter is determined by the index starting from
   * June 2022 (see {@link RewardQuarter#START_MONTH}).
   *
   * <p>Quarter indices are one-indexed, i.e. Q1 consists of June, July, and August 2022.
   *
   * @param quarterIndex index of quarter
   */
  public RewardQuarter(int quarterIndex) {
    this.quarterIndex = quarterIndex;
  }

  /**
   * Determine all sundays of the quarter.
   *
   * @return sundays of the quarter
   */
  public List<LocalDate> determineSundays() {
    LocalDate firstDayOfQuarter = getFirstDay();
    LocalDate firstDayOfNextQuarter = getFirstDayOfNextQuarter();
    LocalDate firstSunday = firstDayOfQuarter.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
    return firstSunday.datesUntil(firstDayOfNextQuarter, Period.ofWeeks(1)).toList();
  }

  /**
   * Determine if the quarter has ended.
   *
   * <p>A quarter is considered ended if the last day of the quarter is before the current date.
   *
   * @param now the current date
   * @return true if the quarter has ended otherwise false
   */
  public boolean hasEnded(LocalDate now) {
    LocalDate lastDayOfQuarter = getFirstDayOfNextQuarter().minusDays(1);
    return lastDayOfQuarter.isBefore(now);
  }

  /**
   * Determine the total amount of tokens that are to be paid out as rewards in this quarter. The
   * total reward amounts are loaded from a CSV-file.
   *
   * @return the number of tokens to reward
   */
  public long determineRewards() {
    return WithResource.apply(
        () -> RewardQuarter.class.getResourceAsStream("reward-periods.csv"),
        inputStream -> {
          String rewardQuarter =
              new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                  .lines()
                  .skip(quarterIndex)
                  .findFirst()
                  .orElseThrow();
          String[] split = rewardQuarter.split(";", -1);
          return Long.parseLong(split[1]);
        });
  }

  private LocalDate getFirstDay() {
    return getFirstDayOfQuarter(quarterIndex);
  }

  private LocalDate getFirstDayOfNextQuarter() {
    return getFirstDayOfQuarter(quarterIndex + 1);
  }

  private static LocalDate getFirstDayOfQuarter(int quarterIndex) {
    return START_MONTH.plusMonths((quarterIndex - 1L) * MONTHS_PER_QUARTER);
  }
}
