package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/**
 * The performance score for a block producer for a period.
 *
 * <p>The performance score is defined as the number of blocks produced by the producer in the
 * period divided by the maximum number of blocks produced by any producer in the period. A
 * performance score of 1.0 means the producer has produced the same number of blocks as the most
 * productive producer in the period. A performance score of 0.0 means the producer has produced no
 * blocks in the period.
 *
 * <p>To avoid that newly added producers receive a very low performance score, any producer that
 * produced no blocks in the previous period will be regarded as a <i>new producer</i>, and will
 * receive a performance score that is the average of all non-new producers.
 *
 * @param producer The producer of blocks
 * @param numOfBlocks The number of blocks produced by the producer in the period
 * @param score The performance score of the producer
 */
public record BpScore(BlockchainAddress producer, long numOfBlocks, double score) {}
