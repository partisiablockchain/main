package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.tree.AvlTree;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Find all staking relations where stakes are staked towards an active block producer. */
public final class StakesCalculator {
  private static final Logger logger = LoggerFactory.getLogger(StakesCalculator.class);
  private static final double TOKEN_HOLDER_SHARE = 0.98;
  private static final double BLOCK_PRODUCER_SHARE = 0.02;

  private StakesCalculator() {}

  /**
   * Build a list of all staking relations where tokens are staked to an active block producer.
   *
   * @param producers active block producers
   * @param tokenHolders all token holders
   * @param bpScores block producer scores
   * @param specificSunday the sunday of the week the rewards should be calculated for
   * @return list of stake relations
   */
  static List<Stake> buildStakes(
      List<BlockchainAddress> producers,
      Map<BlockchainAddress, TokenHolder> tokenHolders,
      Map<BlockchainAddress, Double> bpScores,
      Map<BlockchainAddress, AvlTree<BlockchainAddress, StakesFromOthers>> delegations,
      LocalDate specificSunday) {
    List<Stake> result = new ArrayList<>();
    for (BlockchainAddress producer : producers) {
      result.addAll(
          buildStakesForProducer(tokenHolders, delegations, bpScores, producer, specificSunday));
    }
    return result;
  }

  private static List<Stake> buildStakesForProducer(
      Map<BlockchainAddress, TokenHolder> tokenHolders,
      Map<BlockchainAddress, AvlTree<BlockchainAddress, StakesFromOthers>> delegations,
      Map<BlockchainAddress, Double> bpScores,
      BlockchainAddress producer,
      LocalDate payoutDate) {
    final double bpScore = determineBpScore(bpScores, producer);
    AvlTree<BlockchainAddress, StakesFromOthers> receivedDelegation = delegations.get(producer);
    List<Stake> stakesToProducer = new ArrayList<>();
    TokenHolder operatorAccount = tokenHolders.get(producer);
    final double stakeCapRatio = operatorAccount.stakeCapRatio();
    for (BlockchainAddress from : receivedDelegation.keySet()) {
      StakesFromOthers incomingStakes = receivedDelegation.getValue(from);
      TokenHolder holder = tokenHolders.get(from);

      Long expirationTimestamp = incomingStakes.expirationTimestamp();
      long payoutTimestamp =
          payoutDate.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).toEpochMilli();
      boolean notExpired = expirationTimestamp == null || expirationTimestamp >= payoutTimestamp;
      if (notExpired) {
        Stake stake = createStake(producer, stakeCapRatio, bpScore, from, incomingStakes, holder);
        stakesToProducer.add(stake);
      }
    }
    if (operatorAccount.stakedToSelf() > 0) {
      stakesToProducer.add(
          createStake(
              producer,
              stakeCapRatio,
              bpScore,
              producer,
              new StakesFromOthers(operatorAccount.stakedToSelf(), 0, null),
              operatorAccount));
    }
    return stakesToProducer;
  }

  /**
   * Determine the score of a block producer, defaulting to 0 if the producer did not produce any
   * blocks in the current month.
   *
   * @param bpScores scores for all block producers that have produced blocks
   * @param producer the producer to find
   * @return the score
   */
  private static double determineBpScore(
      Map<BlockchainAddress, Double> bpScores, BlockchainAddress producer) {
    Double bpScore = bpScores.get(producer);
    if (bpScore == null) {
      logger.error("No BP score for {}", producer);
      bpScore = 0d;
    }
    return bpScore;
  }

  private static Stake createStake(
      BlockchainAddress operator,
      double stakeCapRatio,
      double bpScore,
      BlockchainAddress from,
      StakesFromOthers incomingStakes,
      TokenHolder holder) {
    long amount = incomingStakes.acceptedDelegatedStakes();
    long totalStakes = holder.staked();
    double delegationRatio = (double) amount / totalStakes;
    long totalTokens = holder.total();
    double rewardableBeforePerformance =
        totalTokens
            * holder.unlockedRatio()
            * holder.stakeRatio()
            * delegationRatio
            * stakeCapRatio;
    double rewardable = rewardableBeforePerformance * bpScore;
    double rewardableTokenHolder = rewardable * TOKEN_HOLDER_SHARE;
    double rewardableBp = rewardable * BLOCK_PRODUCER_SHARE;
    return new Stake(
        from,
        operator,
        amount,
        incomingStakes.pendingDelegatedStakes(),
        totalStakes,
        holder.unlocked(),
        totalTokens,
        holder.unlockedRatio(),
        holder.stakeRatio(),
        stakeCapRatio,
        bpScore,
        delegationRatio,
        rewardableBeforePerformance,
        rewardable,
        rewardableTokenHolder,
        rewardableBp);
  }
}
