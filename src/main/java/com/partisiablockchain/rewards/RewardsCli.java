package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.rewards.accountplugin.Balance;
import com.partisiablockchain.rewards.accountplugin.StakesFromOthers;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Clock;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/** Tools for exporting and calculating quarterly rewards for staking. */
public final class RewardsCli {

  static final String HELP_MESSAGE =
      "First argument should be the index of the quarter you want to calculate rewards for";

  static final DateTimeFormatter formatWeekly =
      DateTimeFormatter.ofPattern("yyyy-'W'ww").withZone(ZoneId.of("UTC")).withLocale(Locale.UK);

  static final DateTimeFormatter formatMonthly =
      DateTimeFormatter.ofPattern("uuuu-MM").withZone(ZoneId.of("UTC")).withLocale(Locale.UK);

  static final List<String> SHARDS = List.of("Gov", "Shard0", "Shard1", "Shard2");
  private final File rewardsDirectory;
  private final RewardsClient client;
  private final PrintStream output;

  /**
   * Create a new CLI.
   *
   * @param output the output stream for writing status and progress
   * @param client the client to use when interacting with the blockchain
   * @param rewardsDirectory directory to write result files
   */
  public RewardsCli(PrintStream output, RewardsClient client, File rewardsDirectory) {
    this.output = output;
    this.client = client;
    this.rewardsDirectory = rewardsDirectory;
  }

  /**
   * Exports producers for each week within the given quarter to a directory named 'rewards'. The
   * quarter is determined by a 'quarter index' starting from June 2022 (see {@link
   * RewardQuarter#RewardQuarter(int)}).
   *
   * @param args quarter index
   */
  public static void main(String[] args) {
    Client client = ClientBuilder.newClient();
    run(
        args,
        System.out,
        new RewardsClientImpl("http://localhost:8080", client),
        new File("/storage/rewards"));
  }

  static void run(String[] args, PrintStream output, RewardsClient client, File rewardsDirectory) {
    if (args.length != 1 || args[0].equals("--help")) {
      output.println(HELP_MESSAGE);
      return;
    }

    int quarterIdx;
    try {
      quarterIdx = Integer.parseInt(args[0]);
    } catch (NumberFormatException e) {
      output.println("Quarter index needs to be a number.");
      return;
    }

    RewardsCli cli = new RewardsCli(output, client, rewardsDirectory);
    cli.export(quarterIdx, LocalDate.now(Clock.systemUTC()));
  }

  /**
   * Export all rewards data for a quarter.
   *
   * @param quarterIndex the index of the quarter to export
   * @param currentDate current date
   */
  public void export(int quarterIndex, LocalDate currentDate) {
    if (quarterIndex <= 3) {
      output.println("Invalid quarter index: " + quarterIndex);
      return;
    }

    RewardQuarter rewardQuarter = new RewardQuarter(quarterIndex);
    if (!rewardQuarter.hasEnded(currentDate)) {
      output.println("The quarter has not yet ended");
      return;
    }

    ExceptionConverter.run(() -> Files.createDirectories(rewardsDirectory.toPath()));

    List<LocalDate> sundayOfWeeks = rewardQuarter.determineSundays();
    Committees committees = client.getAllCommittees();
    long rewardForPeriod = rewardQuarter.determineRewards();
    long weeklyRewardInPeriod = rewardForPeriod * 10_000L / sundayOfWeeks.size();

    BpMonthlyScoreCalculator bpMonthlyScoreCalculator =
        new BpMonthlyScoreCalculator(SHARDS, committees, client);
    Map<BlockchainAddress, Double> totalRewards = new HashMap<>();
    for (LocalDate sunday : sundayOfWeeks) {
      Map<BlockchainAddress, BpScore> bpScores =
          bpMonthlyScoreCalculator.computeMonthlyScore(sunday);
      List<Reward> weeklyRewards =
          calculateRewards(bpScores, committees, weeklyRewardInPeriod, sunday);

      weeklyRewards.stream()
          .sorted(Comparator.comparing(Reward::identity))
          .forEach(
              weeklyReward ->
                  totalRewards.merge(weeklyReward.identity(), weeklyReward.rewards(), Double::sum));
    }
    Map<YearMonth, Collection<BpScore>> computedScores =
        bpMonthlyScoreCalculator.getComputedScores();
    computedScores.forEach(
        (yearMonth, bpScores) ->
            writeToCsvFile(
                BpScore.class,
                bpScores,
                Comparator.comparing(BpScore::producer),
                yearMonth,
                formatMonthly));

    List<Payout> payouts =
        totalRewards.entrySet().stream()
            .sorted(Map.Entry.comparingByKey())
            .map(
                entry -> {
                  double value = entry.getValue();
                  return new Payout(entry.getKey(), (long) value);
                })
            .filter(payout -> payout.rewards() > 0L)
            .toList();

    ExceptionConverter.run(
        () ->
            CsvFileWriter.writeRecords(
                Payout.class,
                payouts,
                Comparator.comparing(Payout::identity),
                new File(rewardsDirectory, "Payout-Q" + quarterIndex + ".csv")));

    Hash hashForVoting =
        Hash.create(
            s -> {
              for (Payout payout : payouts) {
                payout.identity().write(s);
                s.writeLong(payout.rewards());
              }
            });

    WithResource.accept(
        () ->
            new PrintWriter(
                new File(rewardsDirectory, "Payout-vote-Q" + quarterIndex + ".txt"),
                StandardCharsets.UTF_8),
        writer -> writer.println(hashForVoting));

    output.println("Final hash used for voting on-chain: " + hashForVoting);
    output.println("Done");
  }

  private List<Reward> calculateRewards(
      Map<BlockchainAddress, BpScore> scoresForMonth,
      Committees committees,
      long rewardForPeriod,
      LocalDate specificSunday) {
    List<BlockchainAddress> producerAddresses = exportProducers(specificSunday, committees);

    Map<BlockchainAddress, Balance> balances = exportBalances(client, specificSunday);

    Map<BlockchainAddress, TokenHolder> tokenHoldersMap =
        exportTokenHolders(specificSunday, balances);

    return computeRewards(
        specificSunday,
        producerAddresses,
        tokenHoldersMap,
        scoresForMonth.values(),
        balances,
        rewardForPeriod);
  }

  private static Map<BlockchainAddress, Balance> exportBalances(
      RewardsClient client, LocalDate day) {
    Map<BlockchainAddress, Balance> balances = new HashMap<>();
    for (String shard : SHARDS) {
      ExportBalance balanceExporter = new ExportBalance(day, client, shard);
      balances.putAll(balanceExporter.exportBalances());
    }
    return balances;
  }

  private List<BlockchainAddress> exportProducers(LocalDate date, Committees committees) {
    ExportProducer producerExporter = new ExportProducer(client, date, committees);
    List<BlockchainAddress> producerAddresses = producerExporter.exportProducers();
    List<Producer> producers = producerAddresses.stream().map(Producer::new).toList();
    writeToCsvFile(
        Producer.class, producers, Comparator.comparing(Producer::identity), date, formatWeekly);
    return producerAddresses;
  }

  private Map<BlockchainAddress, TokenHolder> exportTokenHolders(
      LocalDate date, Map<BlockchainAddress, Balance> balances) {
    ExportTokenHolder tokenHolderExporter = new ExportTokenHolder(date, balances);
    List<TokenHolder> tokenHolders = tokenHolderExporter.exportTokenHolders();
    writeToCsvFile(
        TokenHolder.class,
        tokenHolders,
        Comparator.comparing(TokenHolder::address),
        date,
        formatWeekly);
    return tokenHolders.stream().collect(Collectors.toMap(TokenHolder::address, s -> s));
  }

  List<Reward> computeRewards(
      LocalDate specificSunday,
      List<BlockchainAddress> producers,
      Map<BlockchainAddress, TokenHolder> tokenHoldersMap,
      Collection<BpScore> scoresForMonth,
      Map<BlockchainAddress, Balance> balances,
      long weeklyRewardForPeriod) {
    Map<BlockchainAddress, Double> bpScores =
        scoresForMonth.stream().collect(Collectors.toMap(BpScore::producer, BpScore::score));
    Map<BlockchainAddress, AvlTree<BlockchainAddress, StakesFromOthers>> delegations =
        balances.entrySet().stream()
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey, entry -> entry.getValue().delegatedStakesFromOthers()));
    List<Stake> stakes =
        StakesCalculator.buildStakes(
            producers, tokenHoldersMap, bpScores, delegations, specificSunday);
    writeToCsvFile(
        Stake.class, stakes, Comparator.comparing(Stake::from), specificSunday, formatWeekly);

    List<Reward> rewards = RewardsCalculator.buildRewards(stakes, weeklyRewardForPeriod);
    writeToCsvFile(
        Reward.class,
        rewards,
        Comparator.comparing(Reward::identity),
        specificSunday,
        formatWeekly);
    return rewards;
  }

  /**
   * Exports sorted records to CSV file using its record fields.
   *
   * <p>The name of the file is <code>ClassName-DateTimeFormat</code>.
   *
   * @param entryClass type of the records to export
   * @param entries the record collection to export to file
   * @param sortOrder comparator defining the order of the lines in the CSV file
   * @param date date for the date part of the file name
   * @param format formatter for the date part of the file name
   * @param <T> record type
   */
  private <T extends Record> void writeToCsvFile(
      Class<T> entryClass,
      Collection<T> entries,
      Comparator<T> sortOrder,
      TemporalAccessor date,
      DateTimeFormatter format) {
    ExceptionConverter.run(
        () ->
            CsvFileWriter.writeRecords(
                entryClass,
                entries,
                sortOrder,
                new File(
                    rewardsDirectory,
                    entryClass.getSimpleName() + "s-" + format.format(date) + ".csv")));
  }
}
