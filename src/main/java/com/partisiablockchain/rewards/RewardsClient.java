package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.dto.BlockState;
import java.util.Map;

/** Client interface for fetching blockchain information. */
public interface RewardsClient {

  /**
   * Gets latest block time before timestamp on shard.
   *
   * @param timestamp timestamp after block time
   * @param shard which shard to search on
   * @return latest block time before timestamp
   */
  long getBlockTimeAtTimestamp(long timestamp, String shard);

  /**
   * Gets account plugin at given block time on shard.
   *
   * @param blockTime block time of account plugin
   * @param shard which shard to search on
   * @return account plugin at given block time
   */
  JsonNode getAccountPluginAtBlockTime(long blockTime, String shard);

  /**
   * Get specific block according to block time on shard.
   *
   * @param blockTime specific blocks block time
   * @param shard which shard to search on
   * @return block
   */
  BlockState getBlock(long blockTime, String shard);

  /**
   * Fetch blocks for inclusive interval between from and to on given shard.
   *
   * @param fromBlockTime interval start, inclusive
   * @param toBlockTime interval end, inclusive
   * @param shard which shard to fetch from
   * @return the number of blocks produced by each committee producer
   */
  Map<CommitteeProducer, Long> getProducedBlocks(
      long fromBlockTime, long toBlockTime, String shard);

  /**
   * Committee- and producer index for block producer.
   *
   * @param committeeIdx index of committee
   * @param producerIdx index of producer within committee
   */
  record CommitteeProducer(long committeeIdx, short producerIdx) {

    /** Producer index -1 is considered a Reset block. */
    private static final int RESET_BLOCK = -1;

    /**
     * Is this key indicating the count of reset blocks.
     *
     * @return true if reset block
     */
    boolean isResetBlocks() {
      return producerIdx() == RESET_BLOCK;
    }
  }

  /**
   * Get all committees of producers from consensus plugin.
   *
   * @return all committee
   */
  Committees getAllCommittees();
}
