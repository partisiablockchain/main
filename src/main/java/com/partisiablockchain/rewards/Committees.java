package com.partisiablockchain.rewards;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Keeps track of all committees by committee index. */
public record Committees(Map<Long, List<BlockchainAddress>> allCommittees) {

  /**
   * Determines the identity of producers and aggregates the number of produced blocks across
   * committees.
   *
   * @param blocksPerProducer number of blocks producer by a producer in a specific committee
   * @return the number of blocks produced per producer
   */
  public Map<BlockchainAddress, Long> determineProducerIdentities(
      Map<RewardsClient.CommitteeProducer, Long> blocksPerProducer) {
    Map<BlockchainAddress, Long> blocksProduced = new HashMap<>();
    for (Map.Entry<RewardsClient.CommitteeProducer, Long> committeeProducerEntry :
        blocksPerProducer.entrySet()) {
      RewardsClient.CommitteeProducer committeeProducer = committeeProducerEntry.getKey();
      if (!committeeProducer.isResetBlocks()) {
        Long producedBlocksInCommittee = committeeProducerEntry.getValue();
        BlockchainAddress producerAddress =
            getProducerAddress(committeeProducer.committeeIdx(), committeeProducer.producerIdx());
        blocksProduced.merge(producerAddress, producedBlocksInCommittee, Long::sum);
      }
    }
    return blocksProduced;
  }

  /**
   * Get committee by committee index.
   *
   * @param committeeIndex index of committee
   * @return committee members
   */
  public List<BlockchainAddress> getCommittee(Long committeeIndex) {
    return allCommittees.get(committeeIndex);
  }

  /**
   * Get producer blockchain address from committee index and producer index.
   *
   * @param committeeIndex index of committee
   * @param producerIndex index of producer in committee
   * @return producer blockchain address
   */
  public BlockchainAddress getProducerAddress(Long committeeIndex, short producerIndex) {
    return getCommittee(committeeIndex).get(producerIndex);
  }
}
