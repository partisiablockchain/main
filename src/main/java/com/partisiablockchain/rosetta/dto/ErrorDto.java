package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.errorprone.annotations.Immutable;
import jakarta.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * DTO encapsulating a Rosetta error. See <a
 * href="https://www.rosetta-api.org/docs/models/Error.html">Error</a>.
 */
@Immutable
public final class ErrorDto {

  static {
    List<ErrorDto> errorDtoAccumulator = new ArrayList<>();
    UNKNOWN_CURVE_TYPE = createErrorDto(0, "Unknown curve type", false, errorDtoAccumulator);
    UNKNOWN_OPERATION_TYPE =
        createErrorDto(1, "Unknown operation type", false, errorDtoAccumulator);
    UNKNOWN_CALL_METHOD = createErrorDto(2, "Unknown call method", false, errorDtoAccumulator);
    UNKNOWN_CURRENCY = createErrorDto(3, "Unknown currency", false, errorDtoAccumulator);
    UNKNOWN_BLOCK = createErrorDto(4, "Unknown block", true, errorDtoAccumulator);
    UNKNOWN_ACCOUNT = createErrorDto(5, "Account does not exist", false, errorDtoAccumulator);
    UNKNOWN_TRANSACTION = createErrorDto(6, "Transaction not in block", false, errorDtoAccumulator);
    UNABLE_TO_FIND_TRANSACTION =
        createErrorDto(7, "Transaction could not be found", true, errorDtoAccumulator);
    INVALID_HEX_BYTES = createErrorDto(100, "Invalid hex string", false, errorDtoAccumulator);
    INVALID_NETWORK_IDENTIFIER =
        createErrorDto(101, "Invalid network identifier", false, errorDtoAccumulator);
    INVALID_BLOCKCHAIN_ADDRESS =
        createErrorDto(102, "Invalid blockchain address in invocation", false, errorDtoAccumulator);
    INVALID_INVOCATION_BYTE =
        createErrorDto(103, "Invalid invocation byte", false, errorDtoAccumulator);
    INVALID_AMOUNT = createErrorDto(104, "Invalid amount", false, errorDtoAccumulator);
    INVALID_SIGNATURE = createErrorDto(105, "Invalid signature", false, errorDtoAccumulator);
    INVALID_CALL_ARGUMENT =
        createErrorDto(106, "Invalid call argument", false, errorDtoAccumulator);
    INVALID_TRANSACTION = createErrorDto(107, "Invalid transaction", false, errorDtoAccumulator);
    INVALID_BLOCK_IDENTIFIER =
        createErrorDto(108, "Invalid block identifier", false, errorDtoAccumulator);
    SUBMIT_FAILURE =
        createErrorDto(
            300, "Could not include submitted transaction in mempool", false, errorDtoAccumulator);
    GENERIC_ERROR = createErrorDto(400, "Generic error", false, errorDtoAccumulator);
    errorDtos = Collections.unmodifiableList(errorDtoAccumulator);
  }

  /** A list of all the error dtos. */
  public static final List<ErrorDto> errorDtos;

  /** Error indicating encounter with an unknown elliptic curve type. */
  public static final ErrorDto UNKNOWN_CURVE_TYPE;

  /** Error indicating an unknown operation type. */
  public static final ErrorDto UNKNOWN_OPERATION_TYPE;

  /** Unknown call method. */
  public static final ErrorDto UNKNOWN_CALL_METHOD;

  /** Unknown currency. */
  public static final ErrorDto UNKNOWN_CURRENCY;

  /** Error indicating that a specified block could not be found. */
  public static final ErrorDto UNKNOWN_BLOCK;

  /** Error indicating encounter with an unknown elliptic curve type. */
  public static final ErrorDto INVALID_HEX_BYTES;

  /** Error indicating an invalid network identifier. */
  public static final ErrorDto INVALID_NETWORK_IDENTIFIER;

  /** Error indicating an incorrect blockchain address in invocation. */
  public static final ErrorDto INVALID_BLOCKCHAIN_ADDRESS;

  /** Error indicating an incorrect invocation byte in invocation. */
  public static final ErrorDto INVALID_INVOCATION_BYTE;

  /** Error indicating an invalid amount was received. */
  public static final ErrorDto INVALID_AMOUNT;

  /** Error indicating an account was not found on a queried shard. */
  public static final ErrorDto UNKNOWN_ACCOUNT;

  /** Invalid signature. */
  public static final ErrorDto INVALID_SIGNATURE;

  /** Failure if transaction could not be added to pending transactions. */
  public static final ErrorDto SUBMIT_FAILURE;

  /** Invalid block identifier. */
  public static final ErrorDto INVALID_BLOCK_IDENTIFIER;

  /** Invalid call method. */
  public static final ErrorDto INVALID_CALL_ARGUMENT;

  /** Invalid transaction. */
  public static final ErrorDto INVALID_TRANSACTION;

  /** Transaction not in block. */
  public static final ErrorDto UNKNOWN_TRANSACTION;

  /** Unable to locate transaction necessary to determine operations. */
  public static final ErrorDto UNABLE_TO_FIND_TRANSACTION;

  /** Generic error. */
  public static final ErrorDto GENERIC_ERROR;

  private static ErrorDto createErrorDto(
      int code, String message, boolean retriable, List<ErrorDto> errorDtoAccumulator) {
    ErrorDto errorDto = new ErrorDto(code, message, retriable, null);
    errorDtoAccumulator.add(errorDto);
    return errorDto;
  }

  private final int code;
  private final String message;
  private final boolean retriable;
  private final String description;

  @SuppressWarnings("unused")
  private ErrorDto() {
    code = 0;
    message = null;
    retriable = false;
    description = null;
  }

  private ErrorDto(int code, String message, boolean retriable, String description) {
    this.code = code;
    this.message = message;
    this.retriable = retriable;
    this.description = description;
  }

  /**
   * Get the error code of this error.
   *
   * @return the error code.
   */
  public int getCode() {
    return code;
  }

  /**
   * Get the message of this error.
   *
   * @return the error message.
   */
  public String getMessage() {
    return message;
  }

  /**
   * Get additional information (if any) about what caused this error.
   *
   * @return description of the error.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Get a boolean indicating whether the request that caused this error can be safely retried.
   *
   * @return a boolean indicating if the request that caused this error can be retried.
   */
  @JsonProperty("retriable")
  public boolean isRetriable() {
    return retriable;
  }

  /**
   * Add a description to this error.
   *
   * @param description the description.
   * @return the error with a description.
   */
  @JsonIgnore
  public ErrorDto describe(String description) {
    return new ErrorDto(code, message, retriable, description);
  }

  /**
   * Sets an error to be retriable.
   *
   * @return the error as retriable.
   */
  @JsonIgnore
  public ErrorDto setRetriable() {
    return new ErrorDto(code, message, true, description);
  }

  /**
   * Create an error response from an error.
   *
   * @return the error response
   */
  @JsonIgnore
  public Response response() {
    return Response.status(500).entity(this).build();
  }

  @Override
  public String toString() {
    String errorString =
        "ErrorDto{" + "code=" + code + ", message='" + message + '\'' + ", retriable=" + retriable;
    if (Objects.isNull(description)) {
      return errorString + '}';
    } else {
      return errorString + ", description=" + description + '}';
    }
  }
}
