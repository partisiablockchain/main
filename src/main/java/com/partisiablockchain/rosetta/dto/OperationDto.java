package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO encapsulating a Rosetta operation. See <a
 * href="https://www.rosetta-api.org/docs/models/Operation.html">Operation</a>.
 */
public record OperationDto(
    @JsonProperty("operation_identifier") OperationIdentifierDto operationIdentifier,
    OperationDto.Type type,
    AccountIdentifierDto account,
    AmountDto amount,
    Status status,
    Metadata metadata) {

  /**
   * Metadata for the operation to provide an optional memo.
   *
   * @param memo optional memo
   * @param counterpart the counterpart of the operation
   */
  public record Metadata(String memo, AccountIdentifierDto counterpart) {}

  /** Operation types. */
  public enum Type {
    /** Operation ADD. */
    ADD,
    /** Operation DEDUCT. */
    DEDUCT,
    /** Operation BURN. */
    BURN,
    /** Operation VEST. */
    VEST,
    /** Operation DELEGATE (delegate staking). */
    DELEGATE,
    /** Operation RECEIVE (delegate staking). */
    RECEIVE,
    /** Operation RETRACT (delegate staking). */
    RETRACT,
    /** Operation RETURN (delegate staking). */
    RETURN
  }

  /** Operation statuses. */
  public enum Status {
    /** Operation succeeded. */
    SUCCESS,
    /** Operation failed. */
    FAILED
  }
}
