package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.partisiablockchain.rosetta.dto.call.CallRequest;

/**
 * DTO encapsulating a Rosetta call request. See <a
 * href="https://www.rosetta-api.org/docs/models/CallRequest.html">CallRequest</a>
 */
public record CallRequestDto(
    @JsonProperty("network_identifier") NetworkIdentifierDto networkIdentifier,
    String method,
    @JsonTypeInfo(
            include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
            property = "method",
            use = JsonTypeInfo.Id.NAME)
        CallRequest parameters) {

  /**
   * The get_shard_for_account call method. This method call expects a {@link
   * com.partisiablockchain.rosetta.dto.call.GetShardForAccountRequest GetShardForAccountRequest}
   * parameter request and returns a {@link
   * com.partisiablockchain.rosetta.dto.call.GetShardForAccountResponse GetShardForAccountResponse}.
   */
  public static final String GET_SHARD_FOR_ACCOUNT = "get_shard_for_account";

  /**
   * The get_block_from_transaction call method. This method call expects a {@link
   * com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionRequest
   * GetBlockFromTransactionRequest} parameter request and returns a {@link
   * com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionResponse
   * GetBlockFromTransactionResponse}
   */
  public static final String GET_BLOCK_FROM_TRANSACTION = "get_block_from_transaction";

  /**
   * The get_gas_for_account call method. This method call expects a {@link
   * com.partisiablockchain.rosetta.dto.call.GetGasForAccountRequest GetGasForAccountRequest}
   * parameter request and returns a {@link
   * com.partisiablockchain.rosetta.dto.call.GetGasForAccountResponse GetGasForAccountResponse}
   */
  public static final String GET_GAS_FOR_ACCOUNT = "get_gas_for_account";
}
