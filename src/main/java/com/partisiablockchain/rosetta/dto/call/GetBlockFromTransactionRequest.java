package com.partisiablockchain.rosetta.dto.call;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.rosetta.RosettaAdapter;
import com.partisiablockchain.rosetta.dto.CallRequestDto;

/**
 * Request type for the {@link CallRequestDto#GET_BLOCK_FROM_TRANSACTION get_block_from_transaction}
 * call method.
 *
 * @param transactionHash The hash identifier of the transaction.
 */
public record GetBlockFromTransactionRequest(@RosettaAdapter.Hash Hash transactionHash)
    implements CallRequest {}
