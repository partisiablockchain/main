package com.partisiablockchain.rosetta.dto.call;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.partisiablockchain.rosetta.dto.CallRequestDto;

/** The types of CallRequest that are supported for the /call Rosetta endpoint. */
@JsonSubTypes({
  @JsonSubTypes.Type(
      value = GetShardForAccountRequest.class,
      name = CallRequestDto.GET_SHARD_FOR_ACCOUNT),
  @JsonSubTypes.Type(
      value = GetBlockFromTransactionRequest.class,
      name = CallRequestDto.GET_BLOCK_FROM_TRANSACTION),
  @JsonSubTypes.Type(
      value = GetGasForAccountRequest.class,
      name = CallRequestDto.GET_GAS_FOR_ACCOUNT)
})
public sealed interface CallRequest
    permits GetBlockFromTransactionRequest, GetGasForAccountRequest, GetShardForAccountRequest {}
