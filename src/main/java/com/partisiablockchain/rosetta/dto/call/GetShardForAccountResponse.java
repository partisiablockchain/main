package com.partisiablockchain.rosetta.dto.call;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.rosetta.dto.CallRequestDto;

/**
 * Response type for the {@link CallRequestDto#GET_SHARD_FOR_ACCOUNT get_shard_for_account} call
 * method.
 *
 * @param network The shard the account lives on.
 */
public record GetShardForAccountResponse(String network) implements CallResponse {}
