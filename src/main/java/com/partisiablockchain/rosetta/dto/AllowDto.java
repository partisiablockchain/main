package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * DTO encapsulating a Rosetta allow. See <a
 * href="https://www.rosetta-api.org/docs/models/Allow.html">Allow</a>.
 */
public record AllowDto(
    @JsonProperty("operation_statuses") List<OperationStatusDto> operationStatuses,
    @JsonProperty("operation_types") List<String> operationTypes,
    @JsonProperty("errors") List<ErrorDto> errors,
    @JsonProperty("balance_exemptions") List<BalanceExemptionDto> balanceExemptions,
    @JsonProperty("historical_balance_lookup") boolean historicalBalanceLookup,
    @JsonProperty("call_methods") List<String> callMethods,
    @JsonProperty("mempool_coins") boolean mempoolCoins) {

  /** The supported operations statuses. */
  public static final List<OperationStatusDto> OPERATION_STATUSES =
      List.of(new OperationStatusDto("SUCCESS", true), new OperationStatusDto("FAILED", false));

  /** The supported operation types. */
  public static final List<String> OPERATION_TYPES =
      List.of("ADD", "DEDUCT", "BURN", "VEST", "DELEGATE", "RECEIVE", "RETRACT", "RETURN");

  /** The enabled errors. */
  public static final List<ErrorDto> ERRORS = ErrorDto.errorDtos;

  /** The balance exemptions. For PBC this will always be empty. */
  public static final List<BalanceExemptionDto> BALANCE_EXEMPTIONS = List.of();

  /** If historical balance lookup is supported. */
  public static final boolean HISTORICAL_BALANCE_LOOKUP = false;

  /** The call methods. */
  public static final List<String> CALL_METHODS =
      List.of(CallRequestDto.GET_SHARD_FOR_ACCOUNT, CallRequestDto.GET_BLOCK_FROM_TRANSACTION);

  /** If mempool coins is supported. */
  public static final boolean MEMPOOL_COINS = false;

  /** An allow DTO with default values. This should be the only allow DTO in use. */
  public static AllowDto DEFAULT =
      new AllowDto(
          OPERATION_STATUSES,
          OPERATION_TYPES,
          ERRORS,
          BALANCE_EXEMPTIONS,
          HISTORICAL_BALANCE_LOOKUP,
          CALL_METHODS,
          MEMPOOL_COINS);
}
