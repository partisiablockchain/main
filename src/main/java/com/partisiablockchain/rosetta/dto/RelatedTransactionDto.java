package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * DTO encapsulating a related transaction. See <a
 * href="https://www.rosetta-api.org/docs/models/RelatedTransaction.html">RelatedTransaction</a>.
 */
public record RelatedTransactionDto(
    @JsonProperty("network_identifier") NetworkIdentifierDto networkIdentifier,
    @JsonProperty("transaction_identifier") TransactionIdentifierDto transactionIdentifier,
    Direction direction) {

  /**
   * Describes the "direction" of this related transaction. I.e., whether it's pointing to an
   * earlier transaction or a later one.
   */
  @JsonNaming(PropertyNamingStrategies.LowerCaseStrategy.class)
  public enum Direction {
    /** forward. */
    forward,
    /** backward. */
    backward
  }
}
