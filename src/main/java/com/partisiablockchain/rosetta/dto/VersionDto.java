package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Properties;

/**
 * DTO encapsulating a Rosetta Version object. See <a
 * href="https://www.rosetta-api.org/docs/models/Version.html">Version</a>.
 */
public record VersionDto(
    @JsonProperty("rosetta_version") String rosettaVersion,
    @JsonProperty("node_version") String nodeVersion) {

  /** The current version of Rosetta. */
  public static final String ROSETTA_VERSION = "1.4.12";

  /** The current node version. It should match the current version of main. */
  public static final String NODE_VERSION = getNodeVersionFromProperties("/project.properties");

  /** A version DTO with default values. This should be the only version DTO in use. */
  public static VersionDto DEFAULT = new VersionDto(ROSETTA_VERSION, NODE_VERSION);

  static String getNodeVersionFromProperties(String propertiesPath) {
    final Properties properties = new Properties();
    ExceptionConverter.run(
        () -> properties.load(VersionDto.class.getResourceAsStream(propertiesPath)),
        "Unable to get version");
    return properties.getProperty("version");
  }
}
