package com.partisiablockchain.rosetta.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.rosetta.RosettaAdapter;

/**
 * DTO encapsulating a Rosetta construction parse response. See <a
 * href="https://www.rosetta-api.org/docs/models/ConstructionParseRequest.html">ConstructionParseRequest</a>.
 */
@SuppressWarnings("ArrayRecordComponent")
public record ConstructionParseRequestDto(
    @JsonProperty("network_identifier") NetworkIdentifierDto networkIdentifier,
    boolean signed,
    @RosettaAdapter.HexBytes byte[] transaction) {}
