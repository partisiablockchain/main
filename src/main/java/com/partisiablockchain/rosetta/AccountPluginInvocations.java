package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Invocation bytes for the account plugin. */
public final class AccountPluginInvocations {

  @SuppressWarnings("unused")
  private AccountPluginInvocations() {}

  static final int ADD_MPC_TOKEN_BALANCE = 2;
  static final int DEDUCT_MPC_TOKEN_BALANCE = 3;
  static final int BURN_MPC_TOKENS = 10;
  static final int COMMIT_TRANSFER = 13;
  static final int CREATE_VESTING_ACCOUNT = 15;
  static final int COMMIT_DELEGATE_STAKES = 25;
}
