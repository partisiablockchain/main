package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Invocation bytes for the MPC token contract. */
public final class MpcTokenInvocations {

  @SuppressWarnings("unused")
  private MpcTokenInvocations() {}

  static final int TRANSFER = 3;
  static final int TRANSFER_SMALL_MEMO = 13;
  static final int TRANSFER_LARGE_MEMO = 23;
  static final int DELEGATE_STAKES = 24;
  static final int DELEGATE_STAKES_ON_BEHALF_OF = 46;
  static final int DELEGATE_STAKES_WITH_EXPIRATION = 77;
  static final int DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF = 79;
  static final int RETRACT_DELEGATE_STAKES = 25;
  static final int RETRACT_DELEGATED_STAKES_ON_BEHALF_OF = 47;
  static final int BYOC_TRANSFER_OLD = 29;
  static final int TRANSFER_ON_BEHALF_OF = 40;
  static final int TRANSFER_SMALL_MEMO_ON_BEHALF_OF = 44;
  static final int TRANSFER_LARGE_MEMO_ON_BEHALF_OF = 45;
}
