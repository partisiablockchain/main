package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.dto.AccountBalanceRequestDto;
import com.partisiablockchain.rosetta.dto.AccountBalanceResponseDto;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.BlockDto;
import com.partisiablockchain.rosetta.dto.BlockIdentifierDto;
import com.partisiablockchain.rosetta.dto.BlockRequestDto;
import com.partisiablockchain.rosetta.dto.BlockResponseDto;
import com.partisiablockchain.rosetta.dto.BlockTransactionRequestDto;
import com.partisiablockchain.rosetta.dto.BlockTransactionResponse;
import com.partisiablockchain.rosetta.dto.CallRequestDto;
import com.partisiablockchain.rosetta.dto.CallResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionMetadataResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionSubmitRequest;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.MempoolResponse;
import com.partisiablockchain.rosetta.dto.MempoolTransactionRequestDto;
import com.partisiablockchain.rosetta.dto.MempoolTransactionResponseDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.NetworkRequestDto;
import com.partisiablockchain.rosetta.dto.NetworkStatusResponseDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.OperationIdentifierDto;
import com.partisiablockchain.rosetta.dto.PartialBlockIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierResponse;
import com.partisiablockchain.rosetta.dto.call.CallRequest;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionRequest;
import com.partisiablockchain.rosetta.dto.call.GetBlockFromTransactionResponse;
import com.partisiablockchain.rosetta.dto.call.GetGasForAccountRequest;
import com.partisiablockchain.rosetta.dto.call.GetGasForAccountResponse;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountRequest;
import com.partisiablockchain.rosetta.dto.call.GetShardForAccountResponse;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.BetanetAddresses;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/** Rosetta data API. */
@Path("/rosetta/")
public final class RosettaOnlineResource {

  /** The name of the blockchain. */
  public static final String NETWORK_IDENTIFIER_BLOCKCHAIN = "PartisiaBlockchain";

  static final long VALID_TIME = 600_000L;

  private final ShardedBlockchain blockchain;

  /**
   * Create a new Rosetta data resource.
   *
   * @param blockchain the blockchain ledgers for each shard
   */
  public RosettaOnlineResource(Map<String, BlockchainLedger> blockchain) {
    this.blockchain = new ShardedBlockchain(new HashMap<>(blockchain));
  }

  /**
   * Endpoint for retrieving the status for a specific network. See <a
   * href="https://www.rosetta-api.org/docs/NetworkApi.html#networkstatus">/network/status</a>.
   *
   * @param networkRequestDto the NetworkRequest that is used to determine version information and
   *     allowed operations
   * @return A NetworkStatusResponse if the given NetworkIdentifier is valid, otherwise throw an
   *     exception
   */
  @POST
  @Path("/network/status")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public NetworkStatusResponseDto networkStatus(NetworkRequestDto networkRequestDto) {
    blockchain.validateNetworkIdentifier(networkRequestDto.networkIdentifier());

    String shard = networkRequestDto.networkIdentifier().subNetworkIdentifier().network();
    BlockchainLedger ledger = blockchain.getShard(shard);
    Block latestBlock = ledger.getLatestBlock();
    Block genesisBlock = ledger.getBlock(0);
    BlockIdentifierDto currentBlockIdentifier =
        new BlockIdentifierDto(latestBlock.getBlockTime(), latestBlock.identifier());
    BlockIdentifierDto genesisBlockIdentifier =
        new BlockIdentifierDto(genesisBlock.getBlockTime(), genesisBlock.identifier());
    return new NetworkStatusResponseDto(
        currentBlockIdentifier,
        latestBlock.getProductionTime(),
        genesisBlockIdentifier,
        genesisBlockIdentifier,
        NetworkStatusResponseDto.PEERS);
  }

  static String subChainToSubNetworkId(String subChainId) {
    return subChainId == null ? "Gov" : subChainId;
  }

  /**
   * Endpoint that returns a list of all transactions that have yet to be included in a block. See
   * <a href="https://www.rosetta-api.org/docs/MempoolApi.html#mempool">/mempool</a>.
   *
   * @param networkRequest network request containing which shard to get transactions from.
   * @return list of transactions.
   */
  @POST
  @Path("/mempool")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public MempoolResponse mempool(NetworkRequestDto networkRequest) {
    NetworkIdentifierDto networkIdentifierDto = networkRequest.networkIdentifier();
    blockchain.validateNetworkIdentifier(networkIdentifierDto);
    BlockchainLedger ledger =
        blockchain.getShard(networkIdentifierDto.subNetworkIdentifier().network());
    List<SignedTransaction> pendingTransactions = ledger.getPendingTransactions();
    List<TransactionIdentifierDto> transactionIdentifierList = new ArrayList<>();
    for (SignedTransaction transaction : pendingTransactions) {
      transactionIdentifierList.add(new TransactionIdentifierDto(transaction.identifier()));
    }
    return new MempoolResponse(transactionIdentifierList);
  }

  /**
   * Endpoint that returns a transaction containing its identifier and action type. See <a
   * href="https://www.rosetta-api.org/docs/MempoolApi.html#mempooltransaction">/mempool/transaction</a>.
   *
   * @param mempoolTransactionRequest contains the shard to look for the transaction and transaction
   *     identifier.
   * @return transaction dto
   */
  @POST
  @Path("/mempool/transaction/")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public MempoolTransactionResponseDto mempoolTransaction(
      MempoolTransactionRequestDto mempoolTransactionRequest) {
    NetworkIdentifierDto networkIdentifierDto = mempoolTransactionRequest.networkIdentifier();
    blockchain.validateNetworkIdentifier(networkIdentifierDto);
    BlockchainLedger ledger =
        blockchain.getShard(
            mempoolTransactionRequest.networkIdentifier().subNetworkIdentifier().network());
    Hash requestHash = mempoolTransactionRequest.transactionIdentifier().hash();
    SignedTransaction pendingTransaction = ledger.getPending().get(requestHash);

    if (pendingTransaction == null) {
      throw new WebApplicationException(
          ErrorDto.INVALID_TRANSACTION
              .setRetriable()
              .describe("Transaction does not exist yet")
              .response());
    }

    BlockchainAddress contractAddress = pendingTransaction.getTransaction().getTargetContract();
    boolean isMpcTokenContract = contractAddress.equals(BetanetAddresses.MPC_TOKEN_CONTRACT);
    boolean isMpcTokenMpc20Contract =
        contractAddress.equals(BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT);
    if (!isMpcTokenContract && !isMpcTokenMpc20Contract) {
      return new MempoolTransactionResponseDto(
          new TransactionDto(new TransactionIdentifierDto(requestHash), List.of(), null, null));
    }
    byte[] rpc = pendingTransaction.getTransaction().getRpc();

    SafeDataInputStream rpcStream = SafeDataInputStream.createFromBytes(rpc);

    List<Integer> validInvocations;
    if (isMpcTokenContract) {
      validInvocations =
          List.of(
              MpcTokenInvocations.TRANSFER,
              MpcTokenInvocations.TRANSFER_SMALL_MEMO,
              MpcTokenInvocations.TRANSFER_LARGE_MEMO,
              MpcTokenInvocations.TRANSFER_ON_BEHALF_OF,
              MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF,
              MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF);
    } else { // Contract address is BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT
      validInvocations =
          List.of(MpcTokenMpc20Invocations.TRANSFER, MpcTokenMpc20Invocations.TRANSFER_FROM);
    }
    int invocationByte = rpcStream.readUnsignedByte();

    if (!validInvocations.contains(invocationByte)) {
      return new MempoolTransactionResponseDto(
          new TransactionDto(new TransactionIdentifierDto(requestHash), List.of(), null, null));
    }

    BlockchainAddress sender = pendingTransaction.getSender();
    if (isMpcTokenContract) {
      if (invocationByte == MpcTokenInvocations.TRANSFER_ON_BEHALF_OF
          || invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF
          || invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
        sender = BlockchainAddress.read(rpcStream);
      }
    } else {
      if (invocationByte == MpcTokenMpc20Invocations.TRANSFER_FROM) {
        sender = BlockchainAddress.read(rpcStream);
      }
    }

    BlockchainAddress recipient = BlockchainAddress.read(rpcStream);
    String amount;
    if (isMpcTokenContract) {
      amount = String.valueOf(rpcStream.readLong());
    } else {
      amount = String.valueOf(Unsigned256.create(rpcStream.readBytes(16)).longValueExact());
    }

    String memo = null;

    if (invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO
        || invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF) {
      memo = Long.toString(rpcStream.readLong());
    } else if (invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO
        || invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
      memo = rpcStream.readString();
    }

    OperationDto.Metadata metadataSender =
        new OperationDto.Metadata(memo, new AccountIdentifierDto(sender));
    OperationDto.Metadata metadataRecipient =
        new OperationDto.Metadata(memo, new AccountIdentifierDto(recipient));
    List<OperationDto> operations =
        List.of(
            new OperationDto(
                new OperationIdentifierDto(0),
                OperationDto.Type.ADD,
                new AccountIdentifierDto(recipient),
                new AmountDto(amount, CurrencyDto.MPC),
                OperationDto.Status.SUCCESS,
                metadataSender),
            new OperationDto(
                new OperationIdentifierDto(1),
                OperationDto.Type.DEDUCT,
                new AccountIdentifierDto(sender),
                new AmountDto("-" + amount, CurrencyDto.MPC),
                OperationDto.Status.SUCCESS,
                metadataRecipient));

    return new MempoolTransactionResponseDto(
        new TransactionDto(new TransactionIdentifierDto(requestHash), operations, null, null));
  }

  /**
   * Endpoint for retrieving the balance of a specific account. See <a
   * href="https://www.rosetta-api.org/docs/AccountApi.html#accountbalance">/account/balance</a>
   *
   * @param request an AccountBalanceRequest containing an account identifier, network identifier
   *     and optionally a block identifier and list of currencies
   * @return A response for the AccountBalanceRequest with the balance(s) for the specified account
   */
  @POST
  @Path("/account/balance")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public AccountBalanceResponseDto getAccountBalance(AccountBalanceRequestDto request) {
    blockchain.validateNetworkIdentifier(request.networkIdentifierDto());
    BlockchainLedger ledger =
        blockchain.getShard(request.networkIdentifierDto().subNetworkIdentifier().network());

    Block block;
    PartialBlockIdentifierDto blockIdentifier = request.blockIdentifierDto();
    block = getBlockFromPartialBlockIdentifier(blockIdentifier, ledger);
    List<CurrencyDto> currencies = validateCurrencyList(request.currencies());
    ImmutableChainState state = ledger.getState(block.getBlockTime());
    ensureAccountExists(request.accountIdentifierDto().address(), ledger);
    StateSerializable localPluginAccount =
        state.getLocalAccountPluginState().getAccount(request.accountIdentifierDto().address());

    List<AmountDto> amounts = BalanceCalculator.compute(localPluginAccount, currencies);
    return new AccountBalanceResponseDto(
        new BlockIdentifierDto(block.getBlockTime(), block.identifier()), amounts);
  }

  /**
   * Checks for account existence in the specified ledger. Throws an exception if the account does
   * not exist.
   *
   * @param address The blockchain address of the account
   * @param ledger The ledger in which to look up the account
   */
  private void ensureAccountExists(BlockchainAddress address, BlockchainLedger ledger) {
    StateSerializable accountState = ledger.getChainState().getAccount(address);
    if (accountState == null) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_ACCOUNT.response());
    }
  }

  private List<CurrencyDto> validateCurrencyList(List<CurrencyDto> currencies) {
    if (currencies == null) {
      return List.of(CurrencyDto.MPC);
    } else {
      // The only valid non-null list of currencies that the user could supply is the singleton
      // list with the MPC currency DTO in it. This changes when we need to return BYOC balances.
      if (currencies.size() > 1 || !currencies.get(0).equals(CurrencyDto.MPC)) {
        throw new WebApplicationException(ErrorDto.UNKNOWN_CURRENCY.response());
      }
      return currencies;
    }
  }

  /**
   * Endpoint for call. See <a href="https://www.rosetta-api.org/docs/CallApi.html#call">/call</a>
   *
   * <p>The supported methods are the methods in {@link
   * com.partisiablockchain.rosetta.dto.AllowDto#CALL_METHODS CallMethods}. See each method
   * description in {@link CallRequestDto} for expected parameters and return type.
   *
   * @param request a request containing method-specific parameters.
   * @return a response with method-specific content.
   */
  @POST
  @Path("/call")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public CallResponseDto call(CallRequestDto request) {
    NetworkIdentifierDto networkIdentifierDto = request.networkIdentifier();
    blockchain.validateNetworkIdentifier(networkIdentifierDto);
    String method = request.method();
    String shard = networkIdentifierDto.subNetworkIdentifier().network();
    CallRequest callRequest = request.parameters();
    if (method.equals(CallRequestDto.GET_SHARD_FOR_ACCOUNT)) {
      GetShardForAccountRequest requestDto = (GetShardForAccountRequest) callRequest;
      GetShardForAccountResponse responseDto = getShardForAccount(requestDto.address());
      return new CallResponseDto(responseDto, false);
    } else if (method.equals(CallRequestDto.GET_BLOCK_FROM_TRANSACTION)) {
      GetBlockFromTransactionRequest requestDto = (GetBlockFromTransactionRequest) callRequest;
      GetBlockFromTransactionResponse responseDto =
          getBlockFromTransaction(requestDto.transactionHash(), shard);
      return new CallResponseDto(responseDto, true);
    } else if (method.equals(CallRequestDto.GET_GAS_FOR_ACCOUNT)) {
      GetGasForAccountRequest requestDto = (GetGasForAccountRequest) callRequest;
      GetGasForAccountResponse responseDto =
          getGasForAccount(
              requestDto.address(), networkIdentifierDto.subNetworkIdentifier().network());
      return new CallResponseDto(responseDto, false);
    } else {
      throw new WebApplicationException(ErrorDto.UNKNOWN_CALL_METHOD.response());
    }
  }

  private GetShardForAccountResponse getShardForAccount(BlockchainAddress address) {
    ImmutableChainState chainState = blockchain.getGovShard().getChainState();
    String route = chainState.getRoutingPlugin().route(chainState.getActiveShards(), address);
    return new GetShardForAccountResponse(shardToString(route));
  }

  private GetBlockFromTransactionResponse getBlockFromTransaction(
      Hash transactionHash, String shard) {
    BlockchainLedger ledger = blockchain.getShard(shard);
    ExecutedTransaction transaction = ledger.getTransaction(transactionHash);
    if (transaction == null) {
      throw new WebApplicationException(ErrorDto.INVALID_TRANSACTION.response());
    }
    Hash blockHash = transaction.getBlockHash();
    long blockTime = ledger.getBlock(blockHash).getBlockTime();
    return new GetBlockFromTransactionResponse(blockTime, blockHash);
  }

  GetGasForAccountResponse getGasForAccount(BlockchainAddress address, String shard) {
    BlockchainLedger ledger = blockchain.getShard(shard);
    ensureAccountExists(address, ledger);
    ImmutableChainState state = ledger.getChainState();
    StateSerializable globalAccountState = state.getGlobalPluginState(ChainPluginType.ACCOUNT);
    StateSerializable localAccountState = state.getLocalAccountPluginState().getAccount(address);
    long gas = BalanceCalculator.computeTotalGas(localAccountState, globalAccountState);
    return new GetGasForAccountResponse(gas);
  }

  private String shardToString(String route) {
    return Objects.requireNonNullElse(route, "Gov");
  }

  /**
   * Endpoint for retrieving a block. See <a
   * href="https://www.rosetta-api.org/docs/BlockApi.html#block">/block</a>.
   *
   * <p>Identifiers for all executed transactions in the block are returned as "other transactions"
   * in the block response. It must contain all balance-changing transactions.
   *
   * @param blockRequest the block request containing the network identifier and either the block
   *     hash or block time
   * @return the block response
   */
  @POST
  @Path("/block")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public BlockResponseDto getBlock(BlockRequestDto blockRequest) {
    blockchain.validateNetworkIdentifier(blockRequest.networkIdentifier());
    String subNetworkId = blockRequest.networkIdentifier().subNetworkIdentifier().network();
    BlockchainLedger blockchain = this.blockchain.getShard(subNetworkId);
    Block block = getBlockFromPartialBlockIdentifier(blockRequest.blockIdentifier(), blockchain);
    return createBlockResponse(blockchain, block);
  }

  /**
   * Populate a {@link BlockResponseDto} from a block.
   *
   * <p>It is recommended to use the genesis block for both block_identifier and
   * parent_block_identifier when populating the genesis block.
   *
   * <p>To ensure only executed transactions are included we look at the chain state that was
   * present after the execution of the block previous to the requested block.
   *
   * @param blockchain ledger to retrieve data from the blockchain
   * @param block the block to populate the response from
   * @return a populated block response
   */
  BlockResponseDto createBlockResponse(BlockchainLedger blockchain, Block block) {
    BlockIdentifierDto blockIdentifier =
        new BlockIdentifierDto(block.getBlockTime(), block.identifier());
    Block parentBlock;
    if (block.getBlockTime() == 0) {
      parentBlock = block;
    } else {
      parentBlock = blockchain.getBlock(block.getParentBlock());
    }
    BlockIdentifierDto parentBlockIdentifier =
        new BlockIdentifierDto(parentBlock.getBlockTime(), parentBlock.identifier());
    BlockDto blockDto =
        new BlockDto(blockIdentifier, parentBlockIdentifier, block.getProductionTime(), List.of());
    List<TransactionIdentifierDto> executedTransactions =
        getExecutedTransactions(blockchain.getState(block.getBlockTime()));
    return new BlockResponseDto(blockDto, executedTransactions);
  }

  private static List<TransactionIdentifierDto> getExecutedTransactions(
      ImmutableChainState chainState) {
    AvlTree<Hash, Boolean> transactionExecutionStatuses =
        chainState.getExecutedState().getExecutionStatus();
    return transactionExecutionStatuses.keySet().stream()
        .map(TransactionIdentifierDto::new)
        .toList();
  }

  /**
   * Endpoint for retrieving a transaction in a block. See <a
   * href="https://www.rosetta-api.org/docs/BlockApi.html#blocktransaction">/block/transaction</a>.
   *
   * <p>All balance-changing effects of the transactions must be indicated by the mapped operations.
   *
   * @param blockTransactionRequest the request containing the network-, block- and transaction
   *     identifier
   * @return the response containing transactions information including mapped operations and
   *     spawned events
   */
  @POST
  @Path("/block/transaction")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public BlockTransactionResponse getBlockTransaction(
      BlockTransactionRequestDto blockTransactionRequest) {
    blockchain.validateNetworkIdentifier(blockTransactionRequest.networkIdentifier());
    Hash transactionHash = blockTransactionRequest.transactionIdentifier().hash();
    BlockchainLedger ledger =
        blockchain.getShard(
            blockTransactionRequest.networkIdentifier().subNetworkIdentifier().network());
    Block block = getBlockFromBlockIdentifier(blockTransactionRequest.blockIdentifier(), ledger);
    checkTransactionExistsInBlock(transactionHash, ledger, block);
    String subNetwork =
        blockTransactionRequest.networkIdentifier().subNetworkIdentifier().network();
    TransactionDto transactionDto =
        TransactionParser.parse(transactionHash, subNetwork, blockchain);
    return new BlockTransactionResponse(transactionDto);
  }

  private void checkTransactionExistsInBlock(
      Hash transactionHash, BlockchainLedger ledger, Block block) {
    if (!ledger
        .getState(block.getBlockTime())
        .getExecutedState()
        .getExecutionStatus()
        .keySet()
        .contains(transactionHash)) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_TRANSACTION.response());
    }
  }

  static Block getBlockFromBlockIdentifier(
      BlockIdentifierDto blockIdentifier, BlockchainLedger ledger) {
    if (blockIdentifier == null || blockIdentifier.hash() == null) {
      throw new WebApplicationException(ErrorDto.INVALID_BLOCK_IDENTIFIER.response());
    }
    PartialBlockIdentifierDto partialBlockIdentifierDto =
        new PartialBlockIdentifierDto(blockIdentifier.index(), blockIdentifier.hash());
    return getBlockFromPartialBlockIdentifier(partialBlockIdentifierDto, ledger);
  }

  static Block getBlockFromPartialBlockIdentifier(
      PartialBlockIdentifierDto partialBlockIdentifier, BlockchainLedger blockchain) {
    if (partialBlockIdentifier == null
        || (partialBlockIdentifier.hash() == null && partialBlockIdentifier.index() == null)) {
      return blockchain.getLatestBlock();
    }
    Hash hash = partialBlockIdentifier.hash();
    Long index = partialBlockIdentifier.index();

    if (index != null) {
      if (index < 0) {
        throw new WebApplicationException(ErrorDto.INVALID_BLOCK_IDENTIFIER.response());
      }
      Block blockFromIndex = blockchain.getBlock(index);
      if (blockFromIndex == null) {
        throw new WebApplicationException(ErrorDto.UNKNOWN_BLOCK.response());
      }
      if (hash != null && !blockFromIndex.identifier().equals(hash)) {
        throw new WebApplicationException(ErrorDto.INVALID_BLOCK_IDENTIFIER.response());
      }
      return blockFromIndex;
    } else {
      Block blockFromHash = blockchain.getBlock(hash);
      if (blockFromHash == null) {
        throw new WebApplicationException(ErrorDto.UNKNOWN_BLOCK.response());
      }
      return blockFromHash;
    }
  }

  /**
   * A metadata endpoint, <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#request-3">Metadata</a>..
   *
   * @param request a request for metadata to construct a payload.
   * @return metadata to construct a payload.
   */
  @POST
  @Path("/construction/metadata")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ConstructionMetadataResponseDto metadata(ConstructionMetadataRequestDto request) {

    NetworkIdentifierDto networkIdentifier = request.networkIdentifier();
    blockchain.validateNetworkIdentifier(networkIdentifier);

    BlockchainAddress address = request.options().senderAddress();
    BlockchainLedger blockchainLedger =
        blockchain.getShard(networkIdentifier.subNetworkIdentifier().network());

    if (!blockchainLedger.getChainState().getAccounts().contains(address)) {
      throw new WebApplicationException(
          ErrorDto.UNKNOWN_ACCOUNT
              .describe(
                  String.format(
                      "Account was not found on the given shard %s",
                      networkIdentifier.subNetworkIdentifier().network()))
              .response());
    }

    long blockTime = blockchainLedger.getLatestBlock().getProductionTime();
    ImmutableChainState chainState = blockchainLedger.getChainState();
    AccountState accountState = chainState.getAccount(address);
    long nonce = accountState.getNonce();

    return new ConstructionMetadataResponseDto(
        new ConstructionMetadataResponseDto.Metadata(
            nonce,
            blockTime + VALID_TIME,
            request.options().memo() == null ? null : request.options().memo()));
  }

  /**
   * Submit a pre-signed transaction to the node. See <a
   * href=https://www.rosetta-api.org/docs/ConstructionApi.html#constructionsubmit>/construction/submit</a>
   *
   * @param request a request with a pre-signed transaction that is added to pending transactions.
   * @return a response with a signed transaction.
   */
  @POST
  @Path("/construction/submit")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public TransactionIdentifierResponse submit(ConstructionSubmitRequest request) {
    NetworkIdentifierDto networkIdentifierDto = request.networkIdentifier();
    blockchain.validateNetworkIdentifier(networkIdentifierDto);
    SignedTransaction signedTransaction =
        SignedTransaction.read(
            networkIdentifierDto.network(),
            SafeDataInputStream.createFromBytes(request.signedTransaction()));
    BlockchainLedger ledger =
        blockchain.getShard(networkIdentifierDto.subNetworkIdentifier().network());
    if (ledger.addPendingTransaction(signedTransaction)) {
      return new TransactionIdentifierResponse(
          new TransactionIdentifierDto(signedTransaction.identifier()));
    } else {
      throw new WebApplicationException(ErrorDto.SUBMIT_FAILURE.response());
    }
  }
}
