package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.AllowDto;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionCombineResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionDeriveResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionHashRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionOptionsDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionParseResponseDto;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsRequest;
import com.partisiablockchain.rosetta.dto.ConstructionPayloadsResponse;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessRequestDto;
import com.partisiablockchain.rosetta.dto.ConstructionPreprocessResponseDto;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.NetworkListResponseDto;
import com.partisiablockchain.rosetta.dto.NetworkOptionsResponseDto;
import com.partisiablockchain.rosetta.dto.NetworkRequestDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.OperationIdentifierDto;
import com.partisiablockchain.rosetta.dto.PublicKeyDto;
import com.partisiablockchain.rosetta.dto.SignatureDto;
import com.partisiablockchain.rosetta.dto.SigningPayloadDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierResponse;
import com.partisiablockchain.rosetta.dto.VersionDto;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Rosetta construction API. */
@Path("/rosetta/")
public final class RosettaOfflineResource {

  private static final Logger logger = LoggerFactory.getLogger(RosettaOfflineResource.class);

  private final String network;
  private final List<String> shards;

  /**
   * Creates an offline rosetta resource.
   *
   * @param shards shards
   * @param chainId chain id
   */
  public RosettaOfflineResource(List<String> shards, String chainId) {
    this.shards = shards.stream().map(s -> s == null ? "Gov" : s).toList();
    this.network = chainId;
  }

  /**
   * Endpoint for retrieving the list of networks. See <a
   * href="https://www.rosetta-api.org/docs/NetworkApi.html#networklist">/network/list</a>.
   *
   * @return a NetworkListResponse with the shards that are supported
   */
  @POST
  @Path("/network/list")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public NetworkListResponseDto networkList() {
    List<NetworkIdentifierDto> networkIdentifiers = new ArrayList<>();
    for (String shard : shards) {
      networkIdentifiers.add(
          new NetworkIdentifierDto(
              RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
              network,
              new SubNetworkIdentifierDto(shard)));
    }
    return new NetworkListResponseDto(networkIdentifiers);
  }

  /**
   * Endpoint for retrieving the options for a network. See <a
   * href="https://www.rosetta-api.org/docs/NetworkApi.html#networkoptions">/network/options</a>.
   *
   * @param request the NetworkRequest that is used to determine version information and allowed
   *     operations.
   * @return a NetworkOptionsResponse if the given NetworkIdentifier is valid, otherwise throw an
   *     exception
   */
  @POST
  @Path("/network/options")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public NetworkOptionsResponseDto networkOptions(NetworkRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    return new NetworkOptionsResponseDto(VersionDto.DEFAULT, AllowDto.DEFAULT);
  }

  /**
   * Endpoint that derives a PBC address from a public key. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructionderive">/construction/derive</a>.
   *
   * @param request a public key request that can be used to create a public key.
   * @return a public key DTO.
   */
  @POST
  @Path("/construction/derive")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ConstructionDeriveResponseDto derive(ConstructionDeriveRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    byte[] hexBytes = request.publicKey().hexBytes();
    String curveType = request.publicKey().curveType();
    if (!curveType.equals(PublicKeyDto.CURVE_TYPE)) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_CURVE_TYPE.response());
    }
    BlockchainPublicKey blockchainPublicKey = BlockchainPublicKey.fromEncodedEcPoint(hexBytes);
    BlockchainAddress address = blockchainPublicKey.createAddress();
    return new ConstructionDeriveResponseDto(new AccountIdentifierDto(address));
  }

  /**
   * Endpoint that returns the transaction hash for a signed transaction. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructionhash">/construction/hash</a>.
   *
   * @param request a request object to hash a signed transaction.
   * @return a response with a transaction hash.
   */
  @POST
  @Path("/construction/hash")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public TransactionIdentifierResponse hash(ConstructionHashRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    NetworkIdentifierDto networkIdentifierDto = request.networkIdentifier();
    SignedTransaction signedTransaction =
        SignedTransaction.read(
            networkIdentifierDto.network(),
            SafeDataInputStream.createFromBytes(request.signedTransaction()));
    return new TransactionIdentifierResponse(
        new TransactionIdentifierDto(signedTransaction.identifier()));
  }

  /**
   * Endpoint for preprocess. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructionpreprocess">/construction/preprocess</a>.
   *
   * @param request a request for metadata to construct a payload.
   * @return metadata to construct a payload.
   */
  @POST
  @Path("/construction/preprocess")
  @Produces(MediaType.APPLICATION_JSON)
  public ConstructionPreprocessResponseDto preprocess(ConstructionPreprocessRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    ValidatedTransfer validatedTransfer = validateTransfer(request.operations());
    boolean memoExists = request.metadata() != null && request.metadata().memo() != null;
    return new ConstructionPreprocessResponseDto(
        new ConstructionOptionsDto(
            validatedTransfer.sender.account().address(),
            memoExists ? request.metadata().memo() : null));
  }

  /**
   * Endpoint for payloads. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructionpayloads">/construction/payloads</a>.
   *
   * @param request a request for payload with an array of operations.
   * @return unsigned transaction and signing payloads.
   */
  @POST
  @Path("/construction/payloads")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public ConstructionPayloadsResponse constructionPayloads(ConstructionPayloadsRequest request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    ValidatedTransfer transfer = validateTransfer(request.operations());

    BlockchainAddress address = transfer.recipient().account().address();
    long amount = Long.parseLong(transfer.recipient().amount().value());
    byte[] rpc = createTransferRpc(address, amount, request.metadata().memo());

    long networkCost = (2L * rpc.length + 114 + 26) * 5;
    long cpuCost = 6 * 2_500;
    long totalCost = networkCost + cpuCost;

    byte[] innerPart =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeLong(request.metadata().nonce());
              stream.writeLong(request.metadata().validToTime());
              stream.writeLong(totalCost);
              BetanetAddresses.MPC_TOKEN_CONTRACT.write(stream);
              stream.writeDynamicBytes(rpc);
            });
    byte[] signingPayloadHex =
        Hash.create(
                s -> {
                  s.write(innerPart);
                  s.writeString(request.networkIdentifier().network());
                })
            .getBytes();

    AccountIdentifierDto sender = transfer.sender().account();
    byte[] unsignedTxWithSigner =
        SafeDataOutputStream.serialize(
            stream -> {
              sender.address().write(stream);
              stream.write(innerPart);
            });
    SigningPayloadDto signingPayload =
        new SigningPayloadDto(signingPayloadHex, sender, null, SignatureDto.SIGNATURE_TYPE);
    return new ConstructionPayloadsResponse(unsignedTxWithSigner, List.of(signingPayload));
  }

  private static byte[] createTransferRpc(BlockchainAddress recipient, long amount, String memo) {
    boolean memoExists = memo != null;
    if (!memoExists) {
      return SafeDataOutputStream.serialize(
          stream -> {
            stream.writeByte(MpcTokenInvocations.TRANSFER);
            recipient.write(stream);
            stream.writeLong(amount);
          });
    } else {
      // Determine if memo is small or large.
      try {
        long smallMemo = Long.parseLong(memo);
        return SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_SMALL_MEMO);
              recipient.write(stream);
              stream.writeLong(amount);
              stream.writeLong(smallMemo);
            });
      } catch (NumberFormatException e) {
        // Could not parse memo as a long. Must be a large memo.
        return SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(MpcTokenInvocations.TRANSFER_LARGE_MEMO);
              recipient.write(stream);
              stream.writeLong(amount);
              stream.writeString(memo);
            });
      }
    }
  }

  /**
   * Endpoint for parse. This is used as a sanity check before signing and before broadcast. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructionparse">/construction/parse</a>.
   *
   * @param request a request to parse a transaction.
   * @return list of operations.
   */
  @POST
  @Path("/construction/parse")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public ConstructionParseResponseDto constructionParse(ConstructionParseRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(request.transaction());
    AccountIdentifierDto sender;
    List<AccountIdentifierDto> signers = null;
    if (request.signed()) {
      // have to make another copy of the stream here because recovering the sender consumes the
      // entire thing.
      SafeDataInputStream stream = SafeDataInputStream.createFromBytes(request.transaction());
      //      Signature signature = createSignature(stream.readBytes(65));
      Signature signature = Signature.read(stream);
      SignedTransaction signedTransaction =
          SignedTransaction.read(
              request.networkIdentifier().network(),
              SafeDataInputStream.createFromBytes(
                  SafeDataOutputStream.serialize(
                      s -> {
                        signature.write(s);
                        s.write(stream.readAllBytes());
                      })));
      sender = new AccountIdentifierDto(signedTransaction.getSender());
      signers = List.of(sender);

      // skip signature in the stream object we parse below.
      inputStream.skipBytes(65);
    } else {
      sender = new AccountIdentifierDto(BlockchainAddress.read(inputStream));
    }
    // Nonce
    inputStream.readLong();
    // Valid to time
    inputStream.readLong();
    // Cost
    inputStream.readLong();
    BlockchainAddress blockchainAddress = BlockchainAddress.read(inputStream);
    if (!blockchainAddress.equals(BetanetAddresses.MPC_TOKEN_CONTRACT)) {
      throw new WebApplicationException(ErrorDto.INVALID_BLOCKCHAIN_ADDRESS.response());
    }
    // Length of rpc
    inputStream.readInt();

    List<Integer> validInvocationBytes =
        List.of(
            MpcTokenInvocations.TRANSFER,
            MpcTokenInvocations.TRANSFER_SMALL_MEMO,
            MpcTokenInvocations.TRANSFER_LARGE_MEMO);
    int invocationByte = inputStream.readUnsignedByte();
    if (!validInvocationBytes.contains(invocationByte)) {
      throw new WebApplicationException(ErrorDto.INVALID_INVOCATION_BYTE.response());
    }
    BlockchainAddress recipient = BlockchainAddress.read(inputStream);
    final String amount = String.valueOf(inputStream.readLong());
    OperationDto.Metadata addMetadata;
    OperationDto.Metadata deductMetadata;
    if (invocationByte == MpcTokenInvocations.TRANSFER_SMALL_MEMO) {
      String memo = Long.toString(inputStream.readLong());
      addMetadata = new OperationDto.Metadata(memo, sender);
      deductMetadata = new OperationDto.Metadata(memo, new AccountIdentifierDto(recipient));
    } else if (invocationByte == MpcTokenInvocations.TRANSFER_LARGE_MEMO) {
      String memo = inputStream.readString();
      addMetadata = new OperationDto.Metadata(memo, sender);
      deductMetadata = new OperationDto.Metadata(memo, new AccountIdentifierDto(recipient));
    } else {
      addMetadata = new OperationDto.Metadata(null, sender);
      deductMetadata = new OperationDto.Metadata(null, new AccountIdentifierDto(recipient));
    }
    return new ConstructionParseResponseDto(
        List.of(
            new OperationDto(
                new OperationIdentifierDto(0),
                OperationDto.Type.ADD,
                new AccountIdentifierDto(recipient),
                new AmountDto(amount, CurrencyDto.MPC),
                null,
                addMetadata),
            new OperationDto(
                new OperationIdentifierDto(1),
                OperationDto.Type.DEDUCT,
                sender,
                new AmountDto("-" + amount, CurrencyDto.MPC),
                null,
                deductMetadata)),
        signers);
  }

  record ValidatedTransfer(OperationDto sender, OperationDto recipient) {}

  static ValidatedTransfer validateTransfer(List<OperationDto> ops) {
    if (ops.size() != 2) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_OPERATION_TYPE.response());
    }
    OperationDto sender =
        ops.get(0).type().equals(OperationDto.Type.DEDUCT) ? ops.get(0) : ops.get(1);
    OperationDto recipient =
        ops.get(0).type().equals(OperationDto.Type.ADD) ? ops.get(0) : ops.get(1);
    validateSenderAndRecipientOps(sender, recipient);
    return new ValidatedTransfer(sender, recipient);
  }

  private static void validateSenderAndRecipientOps(OperationDto sender, OperationDto recipient) {
    if (sender.type().equals(recipient.type())) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_OPERATION_TYPE.response());
    }
    validateTransferAmount(sender, recipient);
    if (!sender.amount().currency().equals(recipient.amount().currency())) {
      throw new WebApplicationException(ErrorDto.UNKNOWN_OPERATION_TYPE.response());
    }
  }

  private static void validateTransferAmount(OperationDto sender, OperationDto recipient) {
    long senderAmount = Long.parseLong(sender.amount().value());
    long recipientAmount = Long.parseLong(recipient.amount().value());
    if (validateTransferAmounts(senderAmount, recipientAmount)
        || senderAmount + recipientAmount != 0) {
      throw new WebApplicationException(ErrorDto.INVALID_AMOUNT.response());
    }
  }

  static boolean validateTransferAmounts(long senderAmount, long recipientAmount) {
    return senderAmount >= 0 || recipientAmount <= 0;
  }

  /**
   * Endpoint that prepends a signature to an unsigned transaction. See <a
   * href="https://www.rosetta-api.org/docs/ConstructionApi.html#constructioncombine">/construction/combine</a>
   *
   * @param request a request containing the signature and unsigned transaction.
   * @return a signed transaction.
   */
  @POST
  @Path("/construction/combine")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public ConstructionCombineResponseDto combine(ConstructionCombineRequestDto request) {
    ShardedBlockchain.validateNetworkIdentifier(request.networkIdentifier(), network, shards);
    if (request.signatures().size() != 1) {
      throw new WebApplicationException(ErrorDto.INVALID_SIGNATURE.response());
    }

    SignatureDto signatureDto = request.signatures().get(0);
    if (!signatureDto.signatureType().equals(SignatureDto.SIGNATURE_TYPE)) {
      throw new WebApplicationException(ErrorDto.INVALID_SIGNATURE.response());
    }

    byte[] signatureHex = request.signatures().get(0).hexBytes();
    Signature signature = createSignature(signatureHex);
    SignerAndTransaction signerAndTransaction = splitTransaction(request.unsignedTransaction());
    byte[] signedTransaction =
        SafeDataOutputStream.serialize(
            rpc -> {
              signature.write(rpc);
              rpc.write(signerAndTransaction.unsignedTransaction());
            });

    checkIfWellformedTransaction(
        request.networkIdentifier().network(),
        signedTransaction,
        signerAndTransaction.signerAddress);

    return new ConstructionCombineResponseDto(signedTransaction);
  }

  /**
   * Splits the unsigned transaction bytes into sender blockchain address and the unsigned
   * transaction.
   *
   * @param unsignedTransaction unsigned transaction blob to split
   * @return blockchain address and unsigned transaction
   */
  static SignerAndTransaction splitTransaction(byte[] unsignedTransaction) {

    return new SignerAndTransaction(
        BlockchainAddress.read(
            SafeDataInputStream.createFromBytes(Arrays.copyOfRange(unsignedTransaction, 0, 21))),
        Arrays.copyOfRange(unsignedTransaction, 21, unsignedTransaction.length));
  }

  private Signature createSignature(byte[] signature) {
    try {
      return new Signature(
          signature[64], new BigInteger(1, signature, 0, 32), new BigInteger(1, signature, 32, 32));
    } catch (RuntimeException e) {
      logger.error("Could not create signature: " + e);
      throw new WebApplicationException(ErrorDto.INVALID_SIGNATURE.response());
    }
  }

  private void checkIfWellformedTransaction(
      String chainId, byte[] signedTransaction, BlockchainAddress sender) {
    SignedTransaction transaction;
    try {
      transaction =
          SignedTransaction.read(chainId, SafeDataInputStream.createFromBytes(signedTransaction));
    } catch (RuntimeException e) {
      throw new WebApplicationException(ErrorDto.INVALID_TRANSACTION.response());
    }
    if (!transaction.getSender().equals(sender)) {
      throw new WebApplicationException(ErrorDto.INVALID_SIGNATURE.response());
    }
  }

  /** Record for holding the signer address and the unsigned transaction. */
  @SuppressWarnings("ArrayRecordComponent")
  record SignerAndTransaction(BlockchainAddress signerAddress, byte[] unsignedTransaction) {}
}
