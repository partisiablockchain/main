package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import jakarta.ws.rs.WebApplicationException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.bouncycastle.util.encoders.DecoderException;
import org.bouncycastle.util.encoders.Hex;

/** Adapter for serializing and deserializing Rosetta-specific JSON. */
public final class RosettaAdapter {

  private RosettaAdapter() {}

  /** Annotation for serializing and deserializing a blockchain address. */
  @Retention(RetentionPolicy.RUNTIME)
  @JacksonAnnotationsInside
  @JsonSerialize(converter = BlockchainAddressToString.class)
  @JsonDeserialize(using = BlockchainAddressFromString.class)
  public @interface BlockchainAddress {}

  /** Annotation for serializing and deserializing a hash. */
  @Retention(RetentionPolicy.RUNTIME)
  @JacksonAnnotationsInside
  @JsonSerialize(using = ToStringSerializer.class)
  @JsonDeserialize(using = HashFromString.class)
  public @interface Hash {}

  /** Annotation for serializing and deserializing a list of hashes. */
  @Retention(RetentionPolicy.RUNTIME)
  @JacksonAnnotationsInside
  @JsonSerialize(contentUsing = ToStringSerializer.class)
  @JsonDeserialize(contentUsing = HashFromString.class)
  public @interface HashList {}

  /** Annotation for serializing and deserializing hex encoded bytes. */
  @Retention(RetentionPolicy.RUNTIME)
  @JacksonAnnotationsInside
  @JsonSerialize(converter = HexBytesToString.class)
  @JsonDeserialize(using = HexBytesFromString.class)
  public @interface HexBytes {}

  static final class BlockchainAddressToString
      extends StdConverter<com.partisiablockchain.BlockchainAddress, String> {

    @Override
    public String convert(com.partisiablockchain.BlockchainAddress value) {
      return value.writeAsString();
    }
  }

  @SuppressWarnings("serial")
  static final class BlockchainAddressFromString
      extends FromStringDeserializer<com.partisiablockchain.BlockchainAddress> {

    public BlockchainAddressFromString() {
      super(com.partisiablockchain.BlockchainAddress.class);
    }

    @Override
    protected com.partisiablockchain.BlockchainAddress _deserialize(
        String value, DeserializationContext context) {
      return com.partisiablockchain.BlockchainAddress.fromString(value);
    }
  }

  @SuppressWarnings("serial")
  static final class HashFromString
      extends FromStringDeserializer<com.partisiablockchain.crypto.Hash> {

    public HashFromString() {
      super(com.partisiablockchain.crypto.Hash.class);
    }

    @Override
    protected com.partisiablockchain.crypto.Hash _deserialize(
        String value, DeserializationContext context) {
      return com.partisiablockchain.crypto.Hash.fromString(value);
    }
  }

  static final class HexBytesToString extends StdConverter<byte[], String> {

    @Override
    public String convert(byte[] value) {
      return Hex.toHexString(value);
    }
  }

  @SuppressWarnings("serial")
  static final class HexBytesFromString extends FromStringDeserializer<byte[]> {

    public HexBytesFromString() {
      super(byte[].class);
    }

    @Override
    protected byte[] _deserialize(String value, DeserializationContext context) {
      try {
        return Hex.decode(value);
      } catch (DecoderException exception) {
        throw new WebApplicationException(ErrorDto.INVALID_HEX_BYTES.response());
      }
    }
  }
}
