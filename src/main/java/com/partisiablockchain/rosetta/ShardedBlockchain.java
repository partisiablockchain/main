package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import jakarta.ws.rs.WebApplicationException;
import java.util.List;
import java.util.Map;

/** A sharded blockchain. */
public final class ShardedBlockchain {

  /** The name of the blockchain. */
  public static final String NETWORK_IDENTIFIER_BLOCKCHAIN = "PartisiaBlockchain";

  /** The name of the governance shard. */
  public static final String GOV_SHARD = "Gov";

  private final Map<String, BlockchainLedger> shards;
  private final String chainId;

  /**
   * Create a new sharded blockchain from a mapping of shard names to ledgers. The map is assumed to
   * have a mapping from <code>null</code> to the governance shard.
   *
   * @param shards the blockchain ledgers for each shard
   */
  public ShardedBlockchain(Map<String, BlockchainLedger> shards) {
    this.shards = shards;
    this.chainId = shards.get(null).getChainId();
  }

  /**
   * Retrieves a ledger from a shard name.
   *
   * @param shardName a shard name.
   * @return a ledger or null if the shard name is unknown.
   */
  public BlockchainLedger getShard(String shardName) {
    if (shardName.equals(GOV_SHARD)) {
      return getGovShard();
    } else {
      return shards.get(shardName);
    }
  }

  /**
   * Returns the ledgers map.
   *
   * @return a map of all ledgers.
   */
  public Map<String, BlockchainLedger> getShards() {
    return shards;
  }

  /**
   * Returns the default ledger using the null key.
   *
   * @return the ledger that matches the null key.
   */
  public BlockchainLedger getGovShard() {
    return shards.get(null);
  }

  /**
   * Get the names of all known shards.
   *
   * @return a list of shard names.
   */
  public List<String> getShardNames() {
    return shards.keySet().stream().map(s -> s == null ? "Gov" : s).toList();
  }

  /**
   * Checks if a supplied Rosetta network identifier object is valid with respect to this sharded
   * blockchain.
   *
   * @param networkIdentifier the network identifier
   */
  public void validateNetworkIdentifier(NetworkIdentifierDto networkIdentifier) {
    validateNetworkIdentifier(networkIdentifier, this.chainId, getShardNames());
  }

  static void validateNetworkIdentifier(
      NetworkIdentifierDto networkIdentifier, String chainId, List<String> shards) {
    boolean validBlockchainName =
        networkIdentifier.blockchain().equals(NETWORK_IDENTIFIER_BLOCKCHAIN);
    boolean validNetwork = networkIdentifier.network().equals(chainId);

    if (!validBlockchainName) {
      throw new WebApplicationException(
          ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid blockchain name").response());
    }
    if (!validNetwork) {
      throw new WebApplicationException(
          ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("Invalid network").response());
    }
    SubNetworkIdentifierDto dto = networkIdentifier.subNetworkIdentifier();
    if (dto == null) {
      throw new WebApplicationException(
          ErrorDto.INVALID_NETWORK_IDENTIFIER.describe("No sub network identifier").response());
    }
    if (!shards.contains(dto.network())) {
      throw new WebApplicationException(
          ErrorDto.INVALID_NETWORK_IDENTIFIER
              .describe(
                  String.format(
                      "No Shard exists for the given network %s", networkIdentifier.network()))
              .response());
    }
  }
}
