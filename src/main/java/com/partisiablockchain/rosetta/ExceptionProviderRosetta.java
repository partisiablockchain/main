package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.server.rest.ExceptionToResponse;
import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.ExceptionMapper;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Exception mapper for Rosetta. */
@Priority(999)
public final class ExceptionProviderRosetta implements ExceptionMapper<Throwable> {

  private static final Logger logger = LoggerFactory.getLogger(ExceptionProviderRosetta.class);

  private final ExceptionToResponse delegate;
  private final UriInfo uriInfo;

  /**
   * Constructor.
   *
   * @param uriInfo request URI information.
   */
  @Inject
  public ExceptionProviderRosetta(@Context UriInfo uriInfo) {
    this.delegate = new ExceptionToResponse();
    this.uriInfo = uriInfo;
  }

  @Override
  public Response toResponse(Throwable exception) {
    boolean isRosetta =
        uriInfo.getRequestUri().getPath().toLowerCase(Locale.ROOT).contains("rosetta");
    if (isRosetta) {
      logger.info(
          "A generic exception occurred while using one of the rosetta endpoints", exception);
      return ErrorDto.GENERIC_ERROR.describe(exception.getMessage()).response();
    } else {
      return delegate.toResponse(exception);
    }
  }
}
