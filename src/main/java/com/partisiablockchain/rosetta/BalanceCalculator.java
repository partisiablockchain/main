package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.ArrayList;
import java.util.List;

/** Class for computing combined balance of an account on the account plugin. */
public final class BalanceCalculator {

  @SuppressWarnings("unused")
  private BalanceCalculator() {}

  private static final int DELEGATE_STAKES = 0;
  private static final int RETURN_DELEGATE_STAKES = 3;

  /**
   * Calculate balance for an account state.
   *
   * @param accountState account state
   * @param currencies list of currencies
   * @return list of calculated amounts
   */
  public static List<AmountDto> compute(
      StateSerializable accountState, List<CurrencyDto> currencies) {
    List<AmountDto> balances = new ArrayList<>();
    if (accountState != null) {
      StateAccessor accessor = StateAccessor.create(accountState);
      for (int i = 0; i < currencies.size(); i++) {
        balances.add(computeBalance(accessor));
      }
    }
    return balances;
  }

  /**
   * Calculates the total gas available for an account from the available BYOC on the account.
   *
   * @param accountStateLocal The local account plugin state of the account
   * @param accountStateGlobal The global account plugin state for accounts.
   * @return The sum of available gas for the account.
   */
  static long computeTotalGas(
      StateSerializable accountStateLocal, StateSerializable accountStateGlobal) {
    StateAccessor accountStateLocalAccessor = StateAccessor.create(accountStateLocal);
    StateAccessor accountStateGlobalAccessor = StateAccessor.create(accountStateGlobal);
    List<StateAccessor> accountCoins =
        accountStateLocalAccessor.get("accountCoins").getListElements();
    List<StateAccessor> globalCoins =
        accountStateGlobalAccessor.get("coins").get("coins").getListElements();
    long gas = 0L;
    for (int i = 0; i < accountCoins.size(); i++) {
      StateAccessor fraction = globalCoins.get(i).get("conversionRate");
      long numerator = fraction.get("numerator").longValue();
      long denominator = fraction.get("denominator").longValue();
      Unsigned256 accountCoinBalance = accountCoins.get(i).get("balance").cast(Unsigned256.class);
      long externalCoinToGas =
          accountCoinBalance
              .multiply(Unsigned256.create(numerator))
              .divide(Unsigned256.create(denominator))
              .longValueExact();
      gas += externalCoinToGas;
    }
    return gas;
  }

  private static AmountDto computeBalance(StateAccessor accessor) {
    long mpcTokens = accessor.get("mpcTokens").longValue();
    mpcTokens += accessor.get("stakedTokens").longValue();

    for (StateAccessor account : accessor.get("vestingAccounts").getListElements()) {
      mpcTokens += account.get("tokens").longValue() - account.get("releasedTokens").longValue();
    }

    // To support both the currently deployed and the newest version of the account plugin, the
    // following branching on the renamed field for stored pending transfers was necessary. Support
    // for delegated stakes, which has been introduced in the newest version of the account plugin,
    // has also been added.
    String transferOrTransactionsField;
    if (accessor.hasField("storedPendingTransactions")) {
      transferOrTransactionsField = "storedPendingTransactions";
    } else {
      transferOrTransactionsField = "storedPendingTransfers";

      mpcTokens += addStoredPendingStakeDelegations(accessor);
      for (StateAccessorAvlLeafNode entry :
          accessor.get("delegatedStakesFromOthers").getTreeLeaves()) {
        mpcTokens += entry.getValue().get("acceptedDelegatedStakes").longValue();
        mpcTokens += entry.getValue().get("pendingDelegatedStakes").longValue();
      }
      for (StateAccessorAvlLeafNode entry :
          accessor.get("pendingRetractedDelegatedStakes").getTreeLeaves()) {
        mpcTokens += entry.getValue().longValue();
      }
    }

    for (StateAccessorAvlLeafNode leaf :
        accessor.get(transferOrTransactionsField).getTreeLeaves()) {
      StateAccessor leafValue = leaf.getValue();
      if (leafValue.hasField("addTokensIfTransferSuccessful")) {
        boolean addTokensIfTransferSuccessful =
            leafValue.get("addTokensIfTransferSuccessful").booleanValue();
        if (!addTokensIfTransferSuccessful) {
          mpcTokens += leafValue.get("amount").longValue();
        }
      } else {
        int coinIndex = leafValue.get("coinIndex").intValue();
        if (coinIndex == -1
            && !leafValue.get("addTokensOrCoinsIfTransferSuccessful").booleanValue()) {
          mpcTokens += leafValue.get("amount").longValue();
        }
      }
    }

    for (StateAccessorAvlLeafNode leaf : accessor.get("pendingUnstakes").getTreeLeaves()) {
      mpcTokens += leaf.getValue().longValue();
    }

    return new AmountDto(Long.toString(mpcTokens), CurrencyDto.MPC);
  }

  @SuppressWarnings("EnumOrdinal")
  private static long addStoredPendingStakeDelegations(StateAccessor accessor) {
    long tokens = 0;
    for (StateAccessorAvlLeafNode entry :
        accessor.get("storedPendingStakeDelegations").getTreeLeaves()) {
      int delegationType = entry.getValue().get("delegationType").cast(Enum.class).ordinal();
      boolean isDelegateStakesOrRetractDelegatedStakes =
          delegationType == DELEGATE_STAKES || delegationType == RETURN_DELEGATE_STAKES;
      if (isDelegateStakesOrRetractDelegatedStakes) {
        tokens += entry.getValue().get("amount").longValue();
      }
    }
    return tokens;
  }
}
