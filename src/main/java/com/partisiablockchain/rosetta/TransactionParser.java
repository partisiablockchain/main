package com.partisiablockchain.rosetta;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.rosetta.dto.AccountIdentifierDto;
import com.partisiablockchain.rosetta.dto.AmountDto;
import com.partisiablockchain.rosetta.dto.CurrencyDto;
import com.partisiablockchain.rosetta.dto.ErrorDto;
import com.partisiablockchain.rosetta.dto.NetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.OperationDto;
import com.partisiablockchain.rosetta.dto.OperationIdentifierDto;
import com.partisiablockchain.rosetta.dto.RelatedTransactionDto;
import com.partisiablockchain.rosetta.dto.SubNetworkIdentifierDto;
import com.partisiablockchain.rosetta.dto.TransactionDto;
import com.partisiablockchain.rosetta.dto.TransactionIdentifierDto;
import com.partisiablockchain.server.BetanetAddresses;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import jakarta.ws.rs.WebApplicationException;
import java.util.List;
import java.util.Objects;

/** Class handling the parsing of transactions to operations. */
public final class TransactionParser {

  @SuppressWarnings("unused")
  private TransactionParser() {}

  /**
   * Parse a transaction to get the corresponding operation.
   *
   * @param transactionHash hash of the transaction to parse
   * @param subNetwork network identifier indicating the blockchain ledger of the transaction
   * @param ledgers blockchain ledgers for each shard
   * @return the resulting Rosetta transaction DTO
   */
  public static TransactionDto parse(
      Hash transactionHash, String subNetwork, ShardedBlockchain ledgers) {
    BlockchainLedger blockchain = ledgers.getShard(subNetwork);
    ExecutedTransaction transaction = blockchain.getTransaction(transactionHash);
    if (transaction != null) {
      TransactionIdentifierDto originating = null;
      if (transaction.getInner() instanceof ExecutableEvent evt) {
        originating = new TransactionIdentifierDto(evt.getEvent().getOriginatingTransaction());
      }
      TransactionIdentifierDto transactionIdentifier =
          new TransactionIdentifierDto(transactionHash);
      TransactionSearcher mpcShardSearcher = (t) -> findTransactionForContract(t, ledgers);
      OperationDto operation =
          getOperation(transaction.getInner(), mpcShardSearcher, transaction.didExecutionSucceed());
      List<OperationDto> operations = operation == null ? List.of() : List.of(operation);
      List<RelatedTransactionDto> relatedOperations =
          transaction.getEvents().stream()
              .map(blockchain::getEventTransaction)
              .map(
                  e -> {
                    String destinationShard = e.getEvent().getDestinationShard();
                    NetworkIdentifierDto shard =
                        shardToNetworkIdentifier(destinationShard, blockchain.getChainId());
                    return new RelatedTransactionDto(
                        // Rosetta's SDK currently ignores any related transaction with network
                        // information, and probably continues to do so until it supports sharding.
                        subNetwork.equals(shard.subNetworkIdentifier().network()) ? null : shard,
                        new TransactionIdentifierDto(e.identifier()),
                        RelatedTransactionDto.Direction.forward);
                  })
              .toList();
      return new TransactionDto(
          transactionIdentifier,
          operations,
          relatedOperations,
          new TransactionDto.Metadata(originating));
    } else {
      throw new WebApplicationException(ErrorDto.INVALID_TRANSACTION.response());
    }
  }

  private static NetworkIdentifierDto shardToNetworkIdentifier(
      String destinationShard, String chainId) {
    return new NetworkIdentifierDto(
        RosettaOnlineResource.NETWORK_IDENTIFIER_BLOCKCHAIN,
        chainId,
        new SubNetworkIdentifierDto(destinationShard == null ? "Gov" : destinationShard));
  }

  /**
   * Find a transaction executed within one of the ledgers. The method is assumed to only be called
   * with transactions that are guaranteed to exist assuming the ledgers are up-to-date with the
   * network (e.g. two-phase commit initiating transactions).
   *
   * @param transactionId the transaction to find
   * @param ledgers the ledgers to search within
   * @return the found transaction
   * @throws WebApplicationException with {@link ErrorDto#UNABLE_TO_FIND_TRANSACTION} if the
   *     transaction could not be found
   */
  static ExecutedTransaction findTransactionForContract(
      Hash transactionId, ShardedBlockchain ledgers) {
    return ledgers.getShardNames().stream()
        .map(ledgers::getShard)
        .map(ledger -> ledger.getTransaction(transactionId))
        .filter(Objects::nonNull)
        .findFirst()
        .orElseThrow(
            () -> new WebApplicationException(ErrorDto.UNABLE_TO_FIND_TRANSACTION.response()));
  }

  private static OperationDto getOperation(
      ExecutableTransaction transaction,
      TransactionSearcher transactionSearcher,
      boolean executionSucceeded) {
    LocalPluginStateUpdate pluginUpdate = tryParseLocalPluginStateUpdate(transaction);
    if (pluginUpdate != null) {
      return getOperationForAccountPluginInvocation(
          pluginUpdate, transactionSearcher, executionSucceeded);
    } else {
      return null;
    }
  }

  private static OperationDto getOperationForAccountPluginInvocation(
      LocalPluginStateUpdate update,
      TransactionSearcher transactionSearcher,
      boolean executionSucceeded) {
    BlockchainAddress accountAddress = update.getContext();
    SafeDataInputStream updateRpc = SafeDataInputStream.createFromBytes(update.getRpc());
    int invocationByte = updateRpc.readUnsignedByte();

    if (invocationByte == AccountPluginInvocations.ADD_MPC_TOKEN_BALANCE) {
      long addAmount = updateRpc.readLong();
      return createOperation(accountAddress, addAmount, OperationDto.Type.ADD, executionSucceeded);
    } else if (invocationByte == AccountPluginInvocations.DEDUCT_MPC_TOKEN_BALANCE) {
      long deductAmount = updateRpc.readLong();
      return createOperation(
          accountAddress, -deductAmount, OperationDto.Type.DEDUCT, executionSucceeded);
    } else if (invocationByte == AccountPluginInvocations.BURN_MPC_TOKENS) {
      long burnAmount = updateRpc.readLong();
      return createOperation(
          accountAddress, -burnAmount, OperationDto.Type.BURN, executionSucceeded);
    } else if (invocationByte == AccountPluginInvocations.COMMIT_TRANSFER) {
      Hash transferId = Hash.read(updateRpc);
      return handleCommitTransferInvocation(
          transferId, transactionSearcher, accountAddress, executionSucceeded);
    } else if (invocationByte == AccountPluginInvocations.CREATE_VESTING_ACCOUNT) {
      long vestingAmount = updateRpc.readLong();
      return createOperation(
          accountAddress, vestingAmount, OperationDto.Type.VEST, executionSucceeded);
    } else if (invocationByte == AccountPluginInvocations.COMMIT_DELEGATE_STAKES) {
      Hash transferId = Hash.read(updateRpc);
      return handleCommitDelegateStakesInvocation(
          transferId, transactionSearcher, accountAddress, executionSucceeded);
    } else {
      return null;
    }
  }

  /**
   * Convert a COMMIT_TRANSFER invocation to the corresponding operation.
   *
   * <ol>
   *   <li>From a COMMIT_TRANSFER invocation the transactionId of the original TRANSFER invocation
   *       on MPC Token Contract or MPC-MPC20 Contract is found (need to check all shards).
   *   <li>With that TRANSFER invocation we read the transfer recipient and amount from RPC.
   *   <li>Then we compare the address of the recipient with the context address of the
   *       COMMIT_TRANSFER invocation on the account plugin to determine if:
   *       <ul>
   *         <li>context == recipient -> add operation
   *         <li>context != recipient -> deduct operation
   *       </ul>
   * </ol>
   *
   * @param transactionId transaction hash of the original TRANSFER invocation on MPC token contract
   * @param transactionSearcher searcher for transaction
   * @param commitSender account address of the COMMIT invocation on the account plugin
   * @param executionSucceeded whether the execution of the transaction succeeded or not
   */
  static OperationDto handleCommitTransferInvocation(
      Hash transactionId,
      TransactionSearcher transactionSearcher,
      BlockchainAddress commitSender,
      boolean executionSucceeded) {
    ExecutableEvent transactionEvent = findTransactionEvent(transactionId, transactionSearcher);
    if (transactionEvent == null) {
      return null;
    }

    BlockchainAddress contractAddress = transactionEvent.getEvent().getInner().target();
    SafeDataInputStream transferRpc = getTransactionRpc(transactionEvent);

    if (BetanetAddresses.MPC_TOKEN_CONTRACT.equals(contractAddress)) {
      return parseCommitTransactionOnMpcToken(
          transferRpc, transactionEvent, commitSender, executionSucceeded);
    } else if (contractAddress.equals(BetanetAddresses.MPC_TOKEN_MPC20_CONTRACT)) {
      return parseCommitTransactionOnMpcMpc20(
          transferRpc, transactionEvent, commitSender, executionSucceeded);
    } else {
      return null;
    }
  }

  /**
   * Parse a transfer on the MPC-MPC20 contract and create a Rosetta Operation representing the
   * transfer. The Operation is either of type ADD or DEDUCT depending on whether the address that
   * sent the COMMIT invocation corresponds to the address of the transfer recipient.
   *
   * @param transferRpc the RPC for the transfer transaction excluding the invocation byte
   * @param transactionEvent the transaction event on the contract
   * @param commitSender the address that sent the COMMIT invocation on the contract
   * @param executionSucceeded whether the execution of the transaction succeeded or not
   * @return A Rosetta Operation representing the transfer
   */
  private static OperationDto parseCommitTransactionOnMpcMpc20(
      SafeDataInputStream transferRpc,
      ExecutableEvent transactionEvent,
      BlockchainAddress commitSender,
      boolean executionSucceeded) {

    int transferInvocation = transferRpc.readUnsignedByte();
    if (transferInvocation != MpcTokenMpc20Invocations.TRANSFER
        && transferInvocation != MpcTokenMpc20Invocations.TRANSFER_FROM) {
      return null;
    }

    BlockchainAddress sender = getSenderAddress(transactionEvent);
    if (transferInvocation == MpcTokenMpc20Invocations.TRANSFER_FROM) {
      sender = BlockchainAddress.read(transferRpc);
    }

    BlockchainAddress receiver = BlockchainAddress.read(transferRpc);
    long transferAmount = Unsigned256.create(transferRpc.readBytes(16)).longValueExact();

    return createDepositOrWithdrawOperation(
        commitSender, transferAmount, null, sender, receiver, executionSucceeded);
  }

  /**
   * Parse a transfer on the MPC Token contract and create a Rosetta Operation representing the
   * transfer. The Operation is either of type ADD or DEDUCT depending on whether the address that
   * sent the COMMIT invocation corresponds to the address of the transfer recipient.
   *
   * @param transferRpc the RPC for the transfer transaction excluding the invocation byte
   * @param transactionEvent the transaction event on the contract
   * @param commitSender the address that sent the COMMIT invocation on the contract
   * @param executionSucceeded whether the execution of the transaction succeeded or not
   * @return A Rosetta Operation representing the transfer
   */
  private static OperationDto parseCommitTransactionOnMpcToken(
      SafeDataInputStream transferRpc,
      ExecutableEvent transactionEvent,
      BlockchainAddress commitSender,
      boolean executionSucceeded) {

    int transferInvocation = transferRpc.readUnsignedByte();
    if (transferInvocation != MpcTokenInvocations.TRANSFER
        && transferInvocation != MpcTokenInvocations.TRANSFER_SMALL_MEMO
        && transferInvocation != MpcTokenInvocations.TRANSFER_LARGE_MEMO
        && transferInvocation != MpcTokenInvocations.TRANSFER_ON_BEHALF_OF
        && transferInvocation != MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF
        && transferInvocation != MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
      return null;
    }

    BlockchainAddress sender = getSenderAddress(transactionEvent);
    if (transferInvocation == MpcTokenInvocations.TRANSFER_ON_BEHALF_OF
        || transferInvocation == MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF
        || transferInvocation == MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
      sender = BlockchainAddress.read(transferRpc);
    }

    BlockchainAddress receiver = BlockchainAddress.read(transferRpc);
    long transferAmount = transferRpc.readLong();

    String memo = null;
    if (transferInvocation == MpcTokenInvocations.TRANSFER_SMALL_MEMO
        || transferInvocation == MpcTokenInvocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF) {
      memo = Long.toString(transferRpc.readLong());
    } else if (transferInvocation == MpcTokenInvocations.TRANSFER_LARGE_MEMO
        || transferInvocation == MpcTokenInvocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF) {
      memo = transferRpc.readString();
    }

    return createDepositOrWithdrawOperation(
        commitSender, transferAmount, memo, sender, receiver, executionSucceeded);
  }

  /**
   * Create a Rosetta Operation with type ADD (deposit) or DEDUCT (withdraw) depending on whether
   * the receiver is the address corresponding to the sender of the COMMIT invocation.
   *
   * @param commitSender the address of the sender of the COMMIT invocation of the transfer
   * @param transferAmount the amount to transfer
   * @param memo the (optional) memo in the transfer
   * @param sender the sender of the transfer
   * @param receiver the receiver of the transfer
   * @param executionSucceeded whether the execution of the transaction succeeded or not
   * @return A Rosetta Operation representing the supplied information
   */
  private static OperationDto createDepositOrWithdrawOperation(
      BlockchainAddress commitSender,
      long transferAmount,
      String memo,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      boolean executionSucceeded) {
    boolean isDeposit = receiver.equals(commitSender);
    return isDeposit
        ? createOperation(
            commitSender, transferAmount, OperationDto.Type.ADD, memo, sender, executionSucceeded)
        : createOperation(
            commitSender,
            -transferAmount,
            OperationDto.Type.DEDUCT,
            memo,
            receiver,
            executionSucceeded);
  }

  /**
   * Searches for a transaction event, given a transaction identifier. If the transaction could not
   * be found, null is returned.
   *
   * @param transactionId The identifier of the transaction
   * @param transactionSearcher The strategy for finding a transaction across shards
   * @return The event of the transaction, or null.
   */
  private static ExecutableEvent findTransactionEvent(
      Hash transactionId, TransactionSearcher transactionSearcher) {
    ExecutedTransaction executedTransaction = transactionSearcher.search(transactionId);
    ExecutableTransaction inner = executedTransaction.getInner();
    if (inner instanceof ExecutableEvent event) {
      return event;
    }
    return null;
  }

  /**
   * Convert a COMMIT_DELEGATE_STAKES invocation to the corresponding operation.
   *
   * <ol>
   *   <li>From a COMMIT_DELEGATE_STAKES invocation we can find the transactionId of the original
   *       DELEGATE_STAKES or RETRACT_DELEGATE_STAKES invocation on MPC Token Contract (need to
   *       check all shards).
   *   <li>With DELEGATE invocation we read the recipient and amount from RPC.
   *   <li>Then we compare the address of the recipient with the context address of the
   *       COMMIT_DELEGATE_STAKES invocation on the account plugin to determine if DELEGATE_STAKES:
   *       <ul>
   *         <li>context == recipient -> receive operation
   *         <li>context != recipient -> delegate operation
   *       </ul>
   *   <li>Else if RETRACT_DELEGATE_STAKES then:
   *       <ul>
   *         <li>context == recipient -> return operation
   *         <li>context != recipient -> retract operation
   *       </ul>
   * </ol>
   *
   * @param transactionId transaction hash of the original DELEGATE invocation on MPC token contract
   * @param transactionSearcher searcher for transaction
   * @param accountAddress account address of the COMMIT invocation on the account plugin
   */
  static OperationDto handleCommitDelegateStakesInvocation(
      Hash transactionId,
      TransactionSearcher transactionSearcher,
      BlockchainAddress accountAddress,
      boolean executionSucceeded) {
    // Find the MPC Token Contract DELEGATE_STAKES or RETRACT_DELEGATE_STAKES transaction
    SafeDataInputStream delegateRpc =
        getTransactionRpc(transactionSearcher.search(transactionId).getInner());
    int delegateInvocation = delegateRpc.readUnsignedByte();

    BlockchainAddress sender = accountAddress;
    if (isOnBehalfOf(delegateInvocation)) {
      sender = BlockchainAddress.read(delegateRpc);
    }

    BlockchainAddress delegateRecipient = BlockchainAddress.read(delegateRpc);
    long delegateAmount = delegateRpc.readLong();
    boolean isDelegatee = delegateRecipient.equals(accountAddress);
    if (isDelegation(delegateInvocation)) {
      if (isDelegatee) {
        return createOperation(
            accountAddress, delegateAmount, OperationDto.Type.RECEIVE, executionSucceeded);
      } else {
        return createOperation(
            sender, -delegateAmount, OperationDto.Type.DELEGATE, executionSucceeded);
      }
    } else { // RETRACT_DELEGATE_STAKES
      if (isDelegatee) {
        return createOperation(
            accountAddress, -delegateAmount, OperationDto.Type.RETURN, executionSucceeded);
      } else {
        return createOperation(
            sender, delegateAmount, OperationDto.Type.RETRACT, executionSucceeded);
      }
    }
  }

  private static boolean isDelegation(int delegateInvocation) {
    return delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES
        || delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION
        || delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES_ON_BEHALF_OF
        || delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF;
  }

  private static boolean isOnBehalfOf(int delegateInvocation) {
    return delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES_ON_BEHALF_OF
        || delegateInvocation == MpcTokenInvocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF
        || delegateInvocation == MpcTokenInvocations.RETRACT_DELEGATED_STAKES_ON_BEHALF_OF;
  }

  private static SafeDataInputStream getTransactionRpc(
      ExecutableTransaction executableTransaction) {

    byte[] serialize = SafeDataOutputStream.serialize(executableTransaction::write);
    SafeDataInputStream transactionReader = SafeDataInputStream.createFromBytes(serialize);

    ExecutableEvent executableEvent = ExecutableEvent.read(transactionReader);
    EventTransaction.InnerTransaction inner =
        (EventTransaction.InnerTransaction) executableEvent.getEvent().getInner();
    InteractWithContractTransaction interact =
        (InteractWithContractTransaction) inner.getTransaction();

    return SafeDataInputStream.createFromBytes(interact.getRpc());
  }

  private static BlockchainAddress getSenderAddress(ExecutableTransaction executableTransaction) {
    byte[] serialize = SafeDataOutputStream.serialize(executableTransaction::write);
    SafeDataInputStream transactionReader = SafeDataInputStream.createFromBytes(serialize);

    ExecutableEvent executableEvent = ExecutableEvent.read(transactionReader);
    return ((EventTransaction.InnerTransaction) executableEvent.getEvent().getInner()).getSender();
  }

  interface TransactionSearcher {
    ExecutedTransaction search(Hash transactionId);
  }

  private static OperationDto createOperation(
      BlockchainAddress target, long amount, OperationDto.Type type, boolean executionSucceeded) {
    return createOperation(target, amount, type, null, null, executionSucceeded);
  }

  private static OperationDto createOperation(
      BlockchainAddress target,
      long amount,
      OperationDto.Type type,
      String memo,
      BlockchainAddress counterPart,
      boolean executionSucceeded) {
    AccountIdentifierDto account = new AccountIdentifierDto(target);
    AmountDto amountDto = new AmountDto(Long.toString(amount), CurrencyDto.MPC);
    OperationIdentifierDto operationIdentifier = new OperationIdentifierDto(0);
    AccountIdentifierDto accountIdentifierCounterPart =
        counterPart == null ? null : new AccountIdentifierDto(counterPart);
    OperationDto.Metadata metadata = new OperationDto.Metadata(memo, accountIdentifierCounterPart);
    OperationDto.Status status =
        executionSucceeded ? OperationDto.Status.SUCCESS : OperationDto.Status.FAILED;
    return new OperationDto(operationIdentifier, type, account, amountDto, status, metadata);
  }

  static LocalPluginStateUpdate tryParseLocalPluginStateUpdate(
      ExecutableTransaction innerTransaction) {
    if (innerTransaction instanceof ExecutableEvent) {
      EventTransaction.InnerEvent innerEvent =
          ((ExecutableEvent) innerTransaction).getEvent().getInner();
      EventTransaction.EventType eventType = innerEvent.getEventType();
      if (eventType.equals(EventTransaction.EventType.SYSTEM)) {
        InnerSystemEvent.SystemEventType systemEventType =
            ((InnerSystemEvent) innerEvent).getSystemEventType();
        if (systemEventType.equals(InnerSystemEvent.SystemEventType.UPDATE_LOCAL_PLUGIN_STATE)) {
          InnerSystemEvent.UpdateLocalPluginStateEvent updateLocalPluginStateEvent =
              (InnerSystemEvent.UpdateLocalPluginStateEvent) innerEvent;
          return parsePluginEvent(updateLocalPluginStateEvent);
        }
      }
    }
    return null;
  }

  private static LocalPluginStateUpdate parsePluginEvent(
      InnerSystemEvent.UpdateLocalPluginStateEvent pluginEvent) {
    byte[] serialize = SafeDataOutputStream.serialize(pluginEvent::write);
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(serialize);

    inputStream.readEnum(EventTransaction.EventType.values()); // ignored
    inputStream.readEnum(InnerSystemEvent.SystemEventType.values()); // ignored
    inputStream.readEnum(ChainPluginType.values());
    return LocalPluginStateUpdate.read(inputStream);
  }
}
