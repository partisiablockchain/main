package com.partisiablockchain.demo.init;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.server.ServerModule;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Server module responsible for propagating created events to other shards. */
public final class EventPropagation {

  private static final Logger logger = LoggerFactory.getLogger(EventPropagation.class);
  private final BlockchainClient client;

  private ScheduledExecutorService eventPropagation;

  /**
   * Creates an event propagation.
   *
   * @param client propagation config.
   */
  public EventPropagation(BlockchainClient client) {
    this.client = client;
  }

  /**
   * Creates an event propagation on the given blockchain ledger.
   *
   * @param blockchainLedger to propagate on
   * @param serverConfig register thread
   */
  public void create(BlockchainLedger blockchainLedger, ServerModule.ServerConfig serverConfig) {
    eventPropagation = ExecutorFactory.newScheduledSingleThread("EventPropagation");
    serverConfig.registerCloseable(eventPropagation::shutdownNow);

    blockchainLedger.attach(
        new Listener() {
          @Override
          public void newFinalBlock(Block block, ImmutableChainState state) {
            ExceptionLogger.handle(
                logger::warn,
                () -> processBlock(block, state, blockchainLedger),
                "Error while processing block");
          }
        });
  }

  /** Schedule a check of the supplied event. Will retry with increasing delay up to 30 seconds. */
  @SuppressWarnings("FutureReturnValueIgnored")
  void scheduleCheck(FloodableEvent event, int checkCount) {
    eventPropagation.schedule(
        () -> {
          if (!isExecuted(client, event) && pushEvent(this.client, event)) {
            scheduleCheck(event, checkCount + 1);
          }
        },
        Math.min(100 + 1_000L * checkCount, 30_000),
        TimeUnit.MILLISECONDS);
  }

  static boolean isExecuted(BlockchainContractClient client, FloodableEvent event) {
    try {
      ExecutableEvent executableEvent = event.getExecutableEvent();
      ExecutedTransaction executed =
          client.getTransaction(
              new ShardId(executableEvent.getEvent().getDestinationShard()),
              executableEvent.identifier());
      return executed != null;
    } catch (Exception e) {
      return false;
    }
  }

  void processBlock(Block block, ImmutableChainState state, BlockchainLedger blockchainLedger) {
    ImmutableChainState finalizedState = blockchainLedger.getState(block.getBlockTime() - 1);

    ExecutedState executedState = finalizedState.getExecutedState();
    Collection<Hash> eventTransactions = executedState.getEventTransactions();
    for (Hash eventTransaction : eventTransactions) {
      ExceptionLogger.handle(
          logger::warn,
          () -> {
            ExecutableEvent executableEvent =
                blockchainLedger.getEventTransaction(eventTransaction);
            String destinationShard = executableEvent.getEvent().getDestinationShard();
            boolean shouldPropagate =
                !Objects.equals(destinationShard, state.getExecutedState().getSubChainId());
            if (shouldPropagate) {
              pushEventToDestination(block, executableEvent, blockchainLedger);
            }
          },
          "Error in event propagation");
    }
  }

  void pushEventToDestination(
      Block block, ExecutableEvent executableEvent, BlockchainLedger blockchainLedger) {
    FloodableEvent event =
        FloodableEvent.create(
            executableEvent,
            blockchainLedger.getFinalizedBlock(block.getBlockTime()),
            blockchainLedger.getStateStorage());
    if (pushEvent(client, event)) {
      scheduleCheck(event, 0);
    }
  }

  /**
   * Try to push the supplied event.
   *
   * @param client client for pushing
   * @param event the event to push
   * @return true if a retry should be scheduled
   */
  static boolean pushEvent(BlockchainClient client, FloodableEvent event) {
    try {
      ExecutableEvent executableEvent = event.getExecutableEvent();
      String destinationShard = executableEvent.getEvent().getDestinationShard();
      logger.info("Propagating event {} to {}", executableEvent.identifier(), destinationShard);
      client.putEvent(
          new ShardId(destinationShard),
          new IncomingTransaction(SafeDataOutputStream.serialize(event)));
      return false;
    } catch (Exception e) {
      logger.debug("Unable to push event", e);
      return true;
    }
  }
}
