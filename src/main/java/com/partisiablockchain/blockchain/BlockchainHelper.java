package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.genesis.GenesisFile;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.storage.BlockStateStorage;
import java.io.File;

/** Helper class for creating an immutable chain state. */
public final class BlockchainHelper {

  /**
   * Creates an immutable chain state.
   *
   * @param storage block state storage
   * @param genesisFile genesis file
   * @return immutable chain state
   */
  public static ImmutableChainState getChainState(BlockStateStorage storage, String genesisFile) {
    StateStorageRaw stateStorage = storage.getStateStorage();
    ChainStateCache context = new ChainStateCache(stateStorage);

    GenesisFile genesisFileWrapper = new GenesisFile(new File(genesisFile));
    genesisFileWrapper.populateStateStorage(stateStorage);

    FinalBlock genesisBlock = genesisFileWrapper.readGenesisBlockFromZip();
    return new ImmutableChainState(context, storage.getState(genesisBlock.getBlock().getState()));
  }
}
