package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.ConditionalLogger;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import com.secata.tools.thread.ShutdownHook;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Class responsible for catching up events on boot and when relevant. */
public final class EventCatchUp {
  private static final Logger logger = LoggerFactory.getLogger(EventCatchUp.class);

  /**
   * Schedule recurring check of missing propagated events. Will run 30 seconds after called and
   * then recurring with a delay of 30 minutes.
   *
   * @param shutdownHook shutdown hook to register executor to be shutdown
   * @param ledgers enabled shards
   * @param conditionLogger factory for creation of conditional logger
   * @param initialDuration initial delay
   * @param delay delay after initial delay
   */
  @SuppressWarnings("FutureReturnValueIgnored")
  public static void run(
      ShutdownHook shutdownHook,
      Map<String, BlockchainLedger> ledgers,
      ConditionalLogger conditionLogger,
      int initialDuration,
      int delay) {
    ScheduledExecutorService executor = ExecutorFactory.newScheduled("EventCatchUp", 1);
    shutdownHook.register(executor);
    for (String from : ledgers.keySet()) {
      for (String to : ledgers.keySet()) {
        if (!Objects.equals(from, to)) {
          BlockchainLedger fromLedger = ledgers.get(from);
          BlockchainLedger toLedger = ledgers.get(to);
          executor.scheduleWithFixedDelay(
              () ->
                  ExceptionLogger.handle(
                      logger::warn,
                      () -> check(fromLedger, toLedger, conditionLogger),
                      "Error handling old events"),
              Duration.ofSeconds(initialDuration).toMillis(),
              Duration.ofMinutes(delay).toMillis(),
              TimeUnit.MILLISECONDS);
        }
      }
    }
  }

  static Set<Long> getAlreadyKnownEvents(BlockchainLedger toLedger, String from) {
    return toLedger.getPendingEvents().values().stream()
        .filter(e -> Objects.equals(e.getOriginShard(), from))
        .map(e -> e.getEvent().getNonce())
        .collect(Collectors.toSet());
  }

  static void check(
      BlockchainLedger fromLedger, BlockchainLedger toLedger, ConditionalLogger conditionLogger) {
    Set<Long> missingEvents = determineMissingEvents(fromLedger, toLedger);
    checkMissingEvents(fromLedger, toLedger, conditionLogger, missingEvents);
  }

  static void checkMissingEvents(
      BlockchainLedger fromLedger,
      BlockchainLedger toLedger,
      ConditionalLogger conditionLogger,
      Set<Long> missingEvents) {
    final String from = fromLedger.getChainState().getExecutedState().getSubChainId();
    final String to = toLedger.getChainState().getExecutedState().getSubChainId();
    logger.info("Trying to patch events {}", missingEvents);
    long latestBlockTime = fromLedger.getBlockTime();
    while (!missingEvents.isEmpty()) {
      // Get state for blockTime-1 as we need the state that has been finalized
      ImmutableChainState state = fromLedger.getState(latestBlockTime - 1);
      conditionLogger.handle(
          logger::warn,
          state == null,
          String.format(
              "Cannot find shard %s state for blocktime=%s - from block=%s, searching for"
                  + " events=%s",
              from, latestBlockTime, fromLedger.getBlock(latestBlockTime), missingEvents));

      if (getShardNonces(state, to).getOutbound() <= Collections.min(missingEvents)) {
        throw new RuntimeException(
            "Unable to find remaining events ("
                + missingEvents
                + "). Searched back to blocktime="
                + latestBlockTime);
      }

      Set<Hash> spawnedEvents = state.getExecutedState().getSpawnedEvents().keySet();
      for (Hash spawnedEvent : spawnedEvents) {
        ExecutableEvent eventTransaction = fromLedger.getEventTransaction(spawnedEvent);
        if (shouldAddEventToQueue(missingEvents, eventTransaction, to)) {
          logger.info("Adding event to queue {}", eventTransaction);
          boolean added =
              toLedger.addPendingEvent(
                  FloodableEvent.create(
                      eventTransaction,
                      fromLedger.getFinalizedBlock(latestBlockTime),
                      fromLedger.getStateStorage()));
          conditionLogger.handle(
              logger::warn,
              !added,
              String.format("Unable to add pending event %s", eventTransaction));
        }
      }
      latestBlockTime--;
    }
  }

  private static Set<Long> determineMissingEvents(
      BlockchainLedger fromLedger, BlockchainLedger toLedger) {
    final String from = fromLedger.getChainState().getExecutedState().getSubChainId();
    final String to = toLedger.getChainState().getExecutedState().getSubChainId();
    logger.info("Checking events from {} to {}", from, to);
    long outbound = getShardNonces(fromLedger, to).getOutbound();
    long inbound = getShardNonces(toLedger, from).getInbound();
    Set<Long> alreadyKnownEvents = getAlreadyKnownEvents(toLedger, from);
    Set<Long> missingEvents =
        LongStream.range(inbound, outbound).boxed().collect(Collectors.toSet());
    missingEvents.removeAll(alreadyKnownEvents);
    return missingEvents;
  }

  static boolean shouldAddEventToQueue(
      Set<Long> missingEvents, ExecutableEvent eventTransaction, String to) {
    boolean rightDestination =
        Objects.equals(eventTransaction.getEvent().getDestinationShard(), to);
    return rightDestination && missingEvents.remove(eventTransaction.getEvent().getNonce());
  }

  private static ShardNonces getShardNonces(BlockchainLedger fromLedger, String to) {
    ImmutableChainState chainState = fromLedger.getChainState();
    return getShardNonces(chainState, to);
  }

  private static ShardNonces getShardNonces(ImmutableChainState chainState, String to) {
    ShardNonces value = chainState.getShardNonces().getValue(new ShardId(to));
    return Objects.requireNonNullElse(value, ShardNonces.EMPTY);
  }
}
