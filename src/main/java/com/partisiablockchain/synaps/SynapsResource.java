package com.partisiablockchain.synaps;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/** Resource allowing access to KYC information with a Synaps contract on-chain. */
@Path("/synaps")
public final class SynapsResource {

  private final SynapsContractAccess contractAccess;

  /**
   * Create a new resource.
   *
   * @param contractAccess access to the contract state
   */
  @Inject
  public SynapsResource(SynapsContractAccess contractAccess) {
    this.contractAccess = contractAccess;
  }

  /**
   * Get kyc registration for individual from blockchain.
   *
   * @param serializedAddress the registered individual
   * @return the ky registration
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/kyc/{address}")
  public JsonNode getRegisteredKyc(@PathParam("address") String serializedAddress) {
    BlockchainAddress address = BlockchainAddress.fromString(serializedAddress);
    StateSerializable contractState = contractAccess.get();
    if (contractState == null) {
      throw new NotFoundException("Synaps contract does not exist");
    }

    StateAccessor stateAccessor = StateAccessor.create(contractState);
    if (!stateAccessor.hasField("kycRegistrations")) {
      throw new NotFoundException("Synaps contract does not support kyc registrations");
    }
    StateAccessor registrationAccessor =
        stateAccessor.get("kycRegistrations").getTreeValue(address);
    if (registrationAccessor == null) {
      throw new NotFoundException("User not registered");
    }
    Object registration = registrationAccessor.cast(Object.class);
    return StateObjectMapper.createObjectMapper().valueToTree(registration);
  }

  /** Interface for getting contract state from blockchain. */
  @FunctionalInterface
  public interface SynapsContractAccess {

    /**
     * Get the current state of the contract.
     *
     * @return current state of contract or null if not present
     */
    StateSerializable get();

    /**
     * Construct a new SynapsContractAccess that fetches information from a blockchain ledger.
     *
     * @param ledger the associated blockchain
     * @param address the address of the synaps contract
     * @return the created access
     */
    static SynapsContractAccess fromBlockchainLedger(
        BlockchainLedger ledger, BlockchainAddress address) {
      return () -> ledger.getChainState().getContractState(address);
    }
  }
}
