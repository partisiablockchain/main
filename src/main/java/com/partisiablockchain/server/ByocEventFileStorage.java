package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.monitor.DefaultEthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.tools.coverage.ExceptionLogger;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Store BYOC events in a JSON formatted file. */
final class ByocEventFileStorage implements ByocEventStorage {
  private static final Logger logger = LoggerFactory.getLogger(ByocEventFileStorage.class);
  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();

  private final File file;

  ByocEventFileStorage(File file) {
    this.file = file;
  }

  @Override
  public synchronized EthereumEventsCache readEvents() {
    ByocEventsView eventView = readOrCreateEventView();
    DefaultEthereumEventsCache cache = new DefaultEthereumEventsCache();
    cache.saveLatestCheckedBlock(eventView.getLatestCheckedBlock());
    cache.saveDeposits(eventView.getAllDeposits());
    cache.saveWithdrawals(eventView.getAllWithdrawals());
    return cache;
  }

  private ByocEventsView readOrCreateEventView() {
    if (file.exists()) {
      return readEventView();
    } else {
      return createEmptyEventCache();
    }
  }

  private ByocEventsView createEmptyEventCache() {
    ByocEventsView emptyCache = new ByocEventsView();
    writeEventView(emptyCache);
    return emptyCache;
  }

  private ByocEventsView readEventView() {
    try {
      return mapper.readValue(file, new TypeReference<>() {});
    } catch (Exception e) {
      logger.warn("Exception when reading from BYOC events file", e);
      return new ByocEventsView();
    }
  }

  @Override
  public synchronized void writeEvents(EthereumEventsCache updatedEvents) {
    ByocEventsView eventsView = new ByocEventsView();
    eventsView.setLatestCheckedBlock(updatedEvents.loadLatestCheckedBlock());
    for (DepositTransaction deposit : updatedEvents.loadAllDeposits()) {
      eventsView.putDeposit(deposit);
    }
    for (WithdrawalTransaction withdrawal : updatedEvents.loadAllWithdrawals()) {
      eventsView.putWithdrawal(withdrawal);
    }
    writeEventView(eventsView);
  }

  public void writeEventView(ByocEventsView events) {
    ExceptionLogger.handle(
        logger::warn,
        () -> {
          Path tmp = createTmpFile();
          mapper.writeValue(tmp.toFile(), events);
          Files.move(tmp, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        },
        "Could not write BYOC events to file " + file);
  }

  private Path createTmpFile() throws IOException {
    String tmpFileName = file.getCanonicalPath() + ".tmp";
    Path tmpPath = Path.of(tmpFileName);
    Files.deleteIfExists(tmpPath);
    return Files.createFile(tmpPath);
  }
}
