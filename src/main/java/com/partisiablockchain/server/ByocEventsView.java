package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.math.Unsigned256;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Serializable view of BYOC events. */
public final class ByocEventsView {
  private Unsigned256 latestCheckedBlock;
  private final Map<Long, DepositTransaction> deposits;

  @JsonDeserialize(keyUsing = InnerWithdrawalIdDeserializer.class)
  private final Map<InnerWithdrawalId, WithdrawalTransaction> withdrawals;

  ByocEventsView() {
    this.latestCheckedBlock = null;
    this.deposits = new HashMap<>();
    this.withdrawals = new HashMap<>();
  }

  /**
   * Set the number of the latest checked block in the view.
   *
   * @param latestCheckedBlock block number to set
   */
  public void setLatestCheckedBlock(Unsigned256 latestCheckedBlock) {
    this.latestCheckedBlock = latestCheckedBlock;
  }

  /**
   * Get the number of the latest check block from the view.
   *
   * @return block number of the latest checked block
   */
  public Unsigned256 getLatestCheckedBlock() {
    return latestCheckedBlock;
  }

  /**
   * Put the deposit into the view. If a topic with the same nonce exists, it is overwritten.
   *
   * @param deposit to put into the view
   */
  public void putDeposit(DepositTransaction deposit) {
    deposits.put(deposit.nonce(), deposit);
  }

  /**
   * Get all deposits transactions from view.
   *
   * @return all deposit transactions
   */
  public List<DepositTransaction> getAllDeposits() {
    return new ArrayList<>(deposits.values());
  }

  /**
   * Put the withdrawal into the view.
   *
   * @param withdrawal to add
   */
  public void putWithdrawal(WithdrawalTransaction withdrawal) {
    InnerWithdrawalId id =
        new InnerWithdrawalId(withdrawal.oracleNonce(), withdrawal.withdrawalNonce());
    withdrawals.put(id, withdrawal);
  }

  /**
   * Get all withdrawal transactions from the view.
   *
   * @return all withdrawal transactions
   */
  public List<WithdrawalTransaction> getAllWithdrawals() {
    return new ArrayList<>(withdrawals.values());
  }

  record InnerWithdrawalId(long oracleNonce, long withdrawalNonce) {

    @Override
    public String toString() {
      return "%d-%d".formatted(oracleNonce(), withdrawalNonce());
    }
  }

  static final class InnerWithdrawalIdDeserializer extends KeyDeserializer {

    @Override
    public Object deserializeKey(String key, DeserializationContext deserializationContext) {
      String[] keys = key.split("-", 2);
      long oracleNonce = Long.parseLong(keys[0]);
      long withdrawalNonce = Long.parseLong(keys[1]);
      return new InnerWithdrawalId(oracleNonce, withdrawalNonce);
    }
  }
}
