package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.SenderAuthenticationKeyPair;
import com.partisiablockchain.blockchain.BlockchainHelper;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.EventCatchUp;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.consensus.fasttrack.FastTrackNode;
import com.partisiablockchain.consensus.fasttrack.FastTrackProducerConfigDto;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Server;
import com.partisiablockchain.demo.byoc.monitor.ByocProcessRunner;
import com.partisiablockchain.demo.byoc.monitor.ContractStateHandler;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import com.partisiablockchain.demo.byoc.monitor.Ethereum;
import com.partisiablockchain.demo.byoc.monitor.PeerDatabase;
import com.partisiablockchain.demo.byoc.monitor.SystemTimeProvider;
import com.partisiablockchain.demo.byoc.monitor.SystemTimeProviderImpl;
import com.partisiablockchain.demo.byoc.monitor.VersionInformation;
import com.partisiablockchain.demo.byoc.settings.DepositSettings;
import com.partisiablockchain.demo.byoc.settings.DisputeSettings;
import com.partisiablockchain.demo.byoc.settings.EthereumSettings;
import com.partisiablockchain.demo.byoc.settings.EvmPriceOracleChainConfig;
import com.partisiablockchain.demo.byoc.settings.LargeOracleSettings;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.demo.byoc.settings.Settings;
import com.partisiablockchain.demo.byoc.settings.WithdrawalSettings;
import com.partisiablockchain.demo.init.EventPropagation;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.main.voting.VotingState;
import com.partisiablockchain.oracle.EvmOracleChainConfig;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.partisiablockchain.rosetta.ExceptionProviderRosetta;
import com.partisiablockchain.rosetta.RosettaOfflineResource;
import com.partisiablockchain.rosetta.RosettaOnlineResource;
import com.partisiablockchain.rosetta.dto.VersionDto;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto;
import com.partisiablockchain.server.CompositeNodeConfigDto.BlockProducerConfigDto.Chains;
import com.partisiablockchain.server.rest.chain.ChainController;
import com.partisiablockchain.server.rest.chain.LedgerChainService;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import com.partisiablockchain.server.rest.shard.LedgerShardService;
import com.partisiablockchain.server.rest.shard.ShardController;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.server.tcp.server.TcpModule;
import com.partisiablockchain.storage.BlockStateStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.synaps.SynapsResource;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.node.CombinedRealNode;
import com.partisiablockchain.zk.real.node.shared.EngineConfig;
import com.partisiablockchain.zk.real.node.shared.RealPeerDatabase;
import com.partisiablockchain.zk.real.node.shared.TransactionSender;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.coverage.WithResource;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.rest.OpenApiResource;
import com.secata.tools.rest.RestServer;
import com.secata.tools.thread.ExecutorFactory;
import com.secata.tools.thread.ShutdownHook;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Main entrypoint for starting a sharded blockchain in a single process. */
public final class ShardedMain {

  private static final Logger logger = LoggerFactory.getLogger(ShardedMain.class);

  /** Threshold for being considered outdated on block production. */
  public static final Duration TIMEOUT_FOR_BLOCK_PRODUCTION = Duration.ofMinutes(15);

  private static final ObjectMapper mapper =
      StateObjectMapper.createObjectMapper()
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  static void updatePeerDatabaseFromNodeConfig(
      CompositeNodeConfigDto nodeConfig, PersistentPeerDatabase peerDatabase) {
    nodeConfig
        .getProducerPeers()
        .forEach(
            peer ->
                peerDatabase.updateBlockProducerEndpoint(
                    peer.key(), peerDatabase.convertAddress(peer.address())));
  }

  static FastTrackProducerConfigDto createFastTrackConfigDtoIfProducer(
      boolean isProducer,
      CompositeNodeConfigDto config,
      Optional<KeyPair> networkKey,
      AdditionalStatusProviders additionalStatusProviders) {
    if (isProducer) {
      if (config == null) {
        return null;
      } else {
        FastTrackProducerConfigDto fastTrackConfig = new FastTrackProducerConfigDto();
        fastTrackConfig.producerKey = networkKey.get().getPrivateKey().toString(16);
        fastTrackConfig.producerBlsKey = config.producerConfig.finalizationKey;
        fastTrackConfig.timeout = getTimeout(config);
        fastTrackConfig.networkDelay = getNetworkDelay(config);
        fastTrackConfig.registerProvider = additionalStatusProviders;
        return fastTrackConfig;
      }
    }
    return null;
  }

  static List<Address> getTcpReaderAddresses(
      CompositeNodeConfigDto nodeConfig, List<String> shards) {
    return IntStream.range(0, shards.size() + 1)
        .mapToObj(i -> new Address("localhost", nodeConfig.floodingPort + 50 + i))
        .toList();
  }

  /**
   * Run the sharded chain.
   *
   * @param args Expects 3 arguments: 'path to node config json', 'path to storage directory'
   * @throws Exception if unable to start
   */
  public static void main(String[] args) throws Exception {
    BetanetConfigFiles betanetConfigFiles = BetanetConfigFiles.createFromArgs(args);
    final RootDirectory dataDirectory = betanetConfigFiles.getDataDirectory();
    logger.info("Booting Composite PBC blockchain");
    CompositeNodeConfigDto nodeConfig = betanetConfigFiles.getCompositeNodeConfig();
    validateConfig(
        betanetConfigFiles.getDataDir(),
        Path.of(nodeConfig.getGenesisFilePath("/genesis/genesis_config")),
        (fileName, properties) -> logger.info("{}: {}", fileName, properties));
    PersistentPeerDatabase peerDatabase =
        createPeerDatabase(
            getPeersFromFile(nodeConfig.getGenesisFilePath("/genesis/peers.txt")), dataDirectory);
    var threadDumpModule =
        new ThreadDumpModule(betanetConfigFiles.getDataDir(), nodeConfig.periodicThreadDumpsConfig);
    AdditionalStatusProviders additionalStatusProviders = new AdditionalStatusProvidersImpl();
    if (nodeConfig.getOverrideOrDefault("rosetta_online", true)) {
      runShardedMain(
          nodeConfig,
          dataDirectory,
          new Main.CollectingServerConfig(),
          betanetConfigFiles,
          peerDatabase,
          isProducer(nodeConfig),
          threadDumpModule,
          new ShutdownHook(),
          additionalStatusProviders,
          TransactionSenderProvider.REST_SENDER,
          new SystemTimeProviderImpl());
    } else {
      runShardedMainOffline(
          betanetConfigFiles.getDataDirectory(),
          new Main.CollectingServerConfig(),
          nodeConfig,
          new ShutdownHook(),
          additionalStatusProviders);
    }
  }

  static void runShardedMainOffline(
      RootDirectory rootDirectory,
      Main.CollectingServerConfig serverConfig,
      CompositeNodeConfigDto nodeConfig,
      ShutdownHook shutdownHook,
      AdditionalStatusProviders additionalStatusProviders) {
    RootDirectory governanceDir = rootDirectory.createSub("/__governance");
    String genesisFilePath = nodeConfig.getGenesisFilePath("/genesis/__governance.zip");

    ImmutableChainState state =
        BlockchainHelper.getChainState(new BlockStateStorage(governanceDir), genesisFilePath);
    serverConfig.registerRestComponent(
        new RosettaOfflineResource(
            state.getActiveShards().stream().toList(), state.getExecutedState().getChainId()));
    RestServer restServer = createRestServer(nodeConfig, serverConfig);
    Main.startApp(restServer, serverConfig.closeable, shutdownHook);
    additionalStatusProviders.register(new NodeProvider());
  }

  static boolean isProducer(CompositeNodeConfigDto nodeConfig) {
    return nodeConfig.producerConfig != null;
  }

  static void runShardedMain(
      CompositeNodeConfigDto nodeConfig,
      RootDirectory dataDirectory,
      Main.CollectingServerConfig serverConfig,
      BetanetConfigFiles betanetConfigFiles,
      PersistentPeerDatabase peerDatabase,
      boolean isProducer,
      ThreadDumpModule threadDumpModule,
      ShutdownHook shutdownHook,
      AdditionalStatusProviders additionalStatusProviders,
      TransactionSenderProvider transactionSenderProvider,
      SystemTimeProvider systemTimeProvider) {

    updatePeerDatabaseFromNodeConfig(nodeConfig, peerDatabase);
    Optional<KeyPair> networkKey = networkKey(nodeConfig);
    updatePeerDatabaseIfProducer(isProducer, networkKey, nodeConfig, peerDatabase);

    Map<String, BlockchainLedger> ledgers = new HashMap<>();
    additionalStatusProviders.register(new NodeProvider());

    RootDirectory governanceDir =
        setupLedger(
            nodeConfig,
            dataDirectory,
            peerDatabase,
            serverConfig,
            networkKey,
            "/__governance",
            "/genesis/__governance.zip",
            1,
            ledgers,
            null,
            additionalStatusProviders,
            isProducer,
            systemTimeProvider);

    BlockchainLedger governanceChain = ledgers.get(null);
    List<String> shards = List.copyOf(getShards(governanceChain));

    // TCP shard readers are used only for internal use to allow REAL nodes to listen for updates on
    // all shards.
    // Therefore, the ports need not be in the range of publicly available ports.
    List<Address> tcpReaders = getTcpReaderAddresses(nodeConfig, shards);

    // Configure event propagation config (rest always enabled)
    EventPropagation eventPropagation =
        new EventPropagation(createEventPropagationModule(nodeConfig, shards));
    eventPropagation.create(governanceChain, serverConfig);

    SingleThreadedTransactionSender sender;
    if (isProducer) {
      BigInteger accountKey =
          readPrivateKey(() -> nodeConfig.producerConfig.accountKey).orElseThrow();
      sender =
          transactionSenderProvider.createTransactionClient(
              serverConfig,
              createClient(new KeyPair(accountKey), nodeConfig.restAddress(), shards));
    } else {
      sender = null;
    }

    FastTrackProducerConfigDto fastTrackConfig =
        createFastTrackConfigDtoIfProducer(
            isProducer, nodeConfig, networkKey, additionalStatusProviders);

    registerFastTrackAndTcpReadersIfProducer(
        isProducer,
        sender,
        fastTrackConfig,
        tcpReaders,
        0,
        governanceDir,
        governanceChain,
        serverConfig);
    registerCombinedRealNodeIfProducer(
        isProducer,
        nodeConfig,
        serverConfig,
        peerDatabase,
        networkKey,
        governanceDir,
        governanceChain,
        sender,
        tcpReaders);

    Map<String, BlockchainResource> resources = new HashMap<>();
    Map<String, MetricsResource> metrics = new HashMap<>();

    for (int i = 0; i < shards.size(); i++) {
      String shard = shards.get(i);
      int shardOffset = i + 1;
      logger.info("Starting shard: {}", shard);

      RootDirectory subDir =
          setupLedger(
              nodeConfig,
              dataDirectory,
              peerDatabase,
              serverConfig,
              networkKey,
              shard,
              "genesis/" + shard + ".zip",
              shardOffset + 1,
              ledgers,
              shard,
              additionalStatusProviders,
              isProducer,
              systemTimeProvider);
      BlockchainLedger blockchain = ledgers.get(shard);

      registerFastTrackAndTcpReadersIfProducer(
          isProducer,
          sender,
          fastTrackConfig,
          tcpReaders,
          shardOffset,
          subDir,
          blockchain,
          serverConfig);

      eventPropagation.create(blockchain, serverConfig);

      resources.put(shard, new BlockchainResource(blockchain));
      metrics.put(shard, new MetricsResource(blockchain));
    }

    registerRestComponents(serverConfig, governanceChain, resources, metrics);

    threadDumpModule.register(serverConfig);

    runRosetta(serverConfig, ledgers);
    runSynaps(serverConfig, ledgers);
    runChainController(serverConfig, ledgers);
    runShardController(serverConfig, ledgers);

    RestServer restServer = createRestServer(nodeConfig, serverConfig);
    logger.info(
        "Starting PBC with {} shards, listening on {} with {}, currently {} is alive {}",
        shards.size(),
        nodeConfig.restPort,
        isRestServerRunning(restServer),
        governanceChain.getLatestBlock().getProductionTime(),
        Main.isRecentProductionTime(governanceChain.getLatestBlock().getProductionTime()));
    Main.startApp(restServer, serverConfig.closeable, shutdownHook);

    runByocAndSystemUpdate(
        isProducer,
        betanetConfigFiles,
        nodeConfig,
        peerDatabase,
        ledgers,
        governanceChain,
        shards,
        sender,
        resources,
        dataDirectory.createSub("byoc"),
        additionalStatusProviders,
        shutdownHook);

    reportNodeVersionIfOutOfDate(
        isProducer,
        sender,
        governanceChain,
        shards,
        resources,
        nodeConfig,
        SemanticVersion.readSemanticVersion(VersionDto.NODE_VERSION),
        systemTimeProvider);
  }

  /**
   * Sends version of this software to bp-orchestration contract if the version registered in
   * bp-orchestration contract is not the same as the version currently running.
   *
   * @param isProducer whether this node is a producer
   * @param sender transaction sender
   * @param governanceChain this node's ledger of the blockchain
   * @param shards shards of ledger
   * @param resources REST api for each shard
   * @param nodeConfig node's config containing pbc account address
   * @param currentVersion current software version of node
   * @param systemTimeProvider provide current system time in millis
   */
  static void reportNodeVersionIfOutOfDate(
      boolean isProducer,
      SingleThreadedTransactionSender sender,
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources,
      CompositeNodeConfigDto nodeConfig,
      SemanticVersion currentVersion,
      SystemTimeProvider systemTimeProvider) {
    if (currentVersion == null) {
      return;
    }
    if (isProducer && isStillAlive(governanceChain, systemTimeProvider.getSystemTime())) {
      BlockchainAddress pbcAccount = nodeConfig.getProducerPbcAccount();
      BpOrchestrationContractState.BlockProducerDto producerInformationRegisteredInBpo =
          getRegisteredProducerInformation(governanceChain, shards, resources, pbcAccount);
      if (producerInformationRegisteredInBpo != null
          && !currentVersion.equals(producerInformationRegisteredInBpo.nodeVersion())) {
        NodeVersionReporter.reportNodeVersion(sender, currentVersion);
      }
    }
  }

  /**
   * Fetches the producer information registered for this node in the BpOrchestration contract from
   * the ledger.
   *
   * @param governanceChain this nodes ledger
   * @param shards shards of ledger
   * @param resources REST api for each shard
   * @param pbcAccount account of node
   * @return producer information registered in this node's ledger
   */
  private static BpOrchestrationContractState.BlockProducerDto getRegisteredProducerInformation(
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources,
      BlockchainAddress pbcAccount) {
    BpOrchestrationContractState bpoContractState =
        getBpoContractState(governanceChain, shards, resources);
    return bpoContractState.blockProducers().getValue(pbcAccount);
  }

  static BpOrchestrationContractState getBpoContractState(
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources) {
    BlockchainAddress bpOrchestration = BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION;
    String route =
        governanceChain
            .getChainState()
            .getRoutingPlugin()
            .route(FixedList.create(shards), bpOrchestration);
    JsonNode serializedContract =
        resources
            .get(route)
            .getContract(
                bpOrchestration.writeAsString(), -1, true, BlockchainResource.StateOutput.JSON)
            .serializedContract();
    return ExceptionConverter.call(
        () -> mapper.convertValue(serializedContract, BpOrchestrationContractState.class),
        "Unable to deserialize");
  }

  private static RestServer createRestServer(
      CompositeNodeConfigDto nodeConfig, Main.CollectingServerConfig serverConfig) {
    ResourceConfig resourceConfig = Main.createConfigForBlockchainNode(serverConfig);
    return new RestServer(nodeConfig.restPort, resourceConfig);
  }

  static void runRosetta(
      Main.CollectingServerConfig serverConfig, Map<String, BlockchainLedger> ledgers) {
    serverConfig.registerRestComponent(
        new RosettaOfflineResource(
            new ArrayList<>(ledgers.keySet()), ledgers.get(null).getChainId()));
    serverConfig.registerRestComponent(new RosettaOnlineResource(ledgers));
    serverConfig.registerRestComponent(ExceptionProviderRosetta.class);
  }

  static void runSynaps(
      Main.CollectingServerConfig serverConfig, Map<String, BlockchainLedger> ledgers) {
    ImmutableChainState chainState = ledgers.get(null).getChainState();
    String shard =
        chainState
            .getRoutingPlugin()
            .route(chainState.getActiveShards(), BetanetAddresses.SYNAPS_KYC);
    SynapsResource.SynapsContractAccess contractAccess =
        SynapsResource.SynapsContractAccess.fromBlockchainLedger(
            ledgers.get(shard), BetanetAddresses.SYNAPS_KYC);
    serverConfig.registerRestComponent(SynapsResource.class);
    serverConfig.registerRestComponent(
        new AbstractBinder() {
          @Override
          protected void configure() {
            bind(contractAccess).to(SynapsResource.SynapsContractAccess.class);
          }
        });
  }

  static void runChainController(
      Main.CollectingServerConfig serverConfig, Map<String, BlockchainLedger> ledgers) {
    serverConfig.registerRestComponent(ChainController.class);
    serverConfig.registerRestComponent(
        new AbstractBinder() {
          @Override
          protected void configure() {
            bind(new LedgerChainService(ledgers))
                .to(com.partisiablockchain.server.rest.chain.ChainService.class);
          }
        });
  }

  static void runShardController(
      Main.CollectingServerConfig serverConfig, Map<String, BlockchainLedger> ledgers) {
    serverConfig.registerRestComponent(ShardController.class);
    serverConfig.registerRestComponent(
        new AbstractBinder() {
          @Override
          protected void configure() {
            bind(new LedgerShardService(ledgers))
                .to(com.partisiablockchain.server.rest.shard.ShardService.class);
          }
        });
  }

  static RootDirectory setupLedger(
      CompositeNodeConfigDto nodeConfig,
      RootDirectory dataDirectory,
      PersistentPeerDatabase peerDatabase,
      Main.CollectingServerConfig serverConfig,
      Optional<KeyPair> networkKey,
      String dataSubDir,
      String genesisFilePath,
      int offset,
      Map<String, BlockchainLedger> ledgers,
      String ledgerKey,
      AdditionalStatusProviders additionalStatusProvider,
      boolean isProducer,
      SystemTimeProvider systemTimeProvider) {
    NetworkConfig governanceNetwork =
        createNetworkConfig(peerDatabase, nodeConfig, offset, isProducer);

    RootDirectory governanceDir = dataDirectory.createSub(dataSubDir);

    BlockchainLedger chainLedger =
        createLedger(
            networkKey,
            governanceNetwork,
            governanceDir,
            nodeConfig.getGenesisFilePath(genesisFilePath),
            ledgers,
            additionalStatusProvider::register);

    ledgers.put(ledgerKey, chainLedger);
    serverConfig.registerCloseable(chainLedger);
    addRestAliveCheck(serverConfig, chainLedger, systemTimeProvider);

    return governanceDir;
  }

  static void runByocAndSystemUpdate(
      boolean isProducer,
      BetanetConfigFiles betanetConfigFiles,
      CompositeNodeConfigDto nodeConfig,
      PersistentPeerDatabase peerDatabase,
      Map<String, BlockchainLedger> ledgers,
      BlockchainLedger governanceChain,
      List<String> shards,
      SingleThreadedTransactionSender sender,
      Map<String, BlockchainResource> resources,
      RootDirectory byocDirectory,
      AdditionalStatusProviders additionalStatusProviders,
      ShutdownHook shutdownHook) {
    if (isProducer) {
      BigInteger oracleSigningKey = readPrivateKey(() -> nodeConfig.networkKey).orElseThrow();
      BlockchainAddress pbcAccount = nodeConfig.getProducerPbcAccount();

      List<ByocConfiguration> byocConfiguration =
          getByocConfiguration(governanceChain, shards, resources);
      ContractStateHandler contractStateHandler =
          new ContractStateHandler(determineActiveContracts(byocConfiguration));
      Client.createShardClient(
          null,
          getTcpReaderAddresses(nodeConfig, shards),
          contractStateHandler,
          governanceChain.getStateStorage(),
          Collections.emptySet());

      for (ByocConfiguration bridge : byocConfiguration) {
        String evmAddress =
            getEthereumByocContract(governanceChain, shards, resources, bridge.outgoing());

        EvmConfig config = getEvmConfig(nodeConfig, bridge.chain());
        if (config != null) {
          ByocConfig ethereumByocConfig =
              new ByocConfig(
                  ("byoc." + bridge.chain() + "." + bridge.symbol()).toLowerCase(Locale.ROOT),
                  config.url(),
                  evmAddress,
                  BigInteger.valueOf(config.blockValidity()),
                  bridge.incoming(),
                  bridge.outgoing());
          ethereumByocConfig.runByocOracleServices(
              byocDirectory,
              nodeConfig,
              shards,
              oracleSigningKey,
              pbcAccount,
              sender,
              shutdownHook,
              contractStateHandler,
              additionalStatusProviders);
        }
      }

      runPriceOracleService(
          nodeConfig,
          shards,
          pbcAccount,
          sender,
          shutdownHook,
          determinePriceOracleAddresses(byocConfiguration),
          contractStateHandler);

      Hash largeOracleDatabaseKey = computeLargeOracleDatabaseKey(oracleSigningKey);
      runLargeOracleService(
          nodeConfig,
          shards,
          largeOracleDatabaseKey,
          oracleSigningKey,
          pbcAccount,
          sender,
          peerDatabase,
          additionalStatusProviders,
          betanetConfigFiles.getDataDir(),
          shutdownHook,
          contractStateHandler);

      EventCatchUp.run(shutdownHook, ledgers, ShardedMain::logIfTrue, 30, 30);
      runSystemUpdate(
          nodeConfig,
          governanceChain,
          shards,
          sender,
          resources,
          nodeConfig.getGenesisFilePath("/update_votes.txt"),
          shutdownHook);
    }
  }

  /** Used to construct BYOC configs for the BYOC-related services. */
  record EvmConfig(String url, int blockValidity) {
    /**
     * Default constructor.
     *
     * @param url An external chain endpoint for a chain. See <a
     *     href=https://ethereum.org/en/developers/docs/apis/json-rpc>Ethereum API docs</a> for
     *     information on endpoints.
     * @param blockValidity Used for getting the latest block on the external chain, being treated
     *     as finalized, i.e. the latest finalized block will be the latest block on the chain minus
     *     <code>blockValidity</code>
     */
    EvmConfig(String url, int blockValidity) {
      this.url = url;
      this.blockValidity = blockValidity;
    }
  }

  /**
   * Contruct an EvmConfig for the specified chain, given the block producer's config.
   *
   * @param nodeConfig The block producer's config, containing the endpoint for the chain
   * @param chain The external chain
   * @return An EvmConfig for the supplied chain with the node's endpoint
   */
  static EvmConfig getEvmConfig(CompositeNodeConfigDto nodeConfig, String chain) {
    if (!Chains.getKnownChains().contains(chain)) {
      logger.warn("Unknown BYOC chain '{}' in orchestrator", chain);
      return null;
    }
    BlockProducerConfigDto producerConfig = nodeConfig.producerConfig;
    String endpoint = producerConfig.getChainEndpoint(chain);
    int blockValidity = getBlockValidityForChain(chain);
    return new EvmConfig(endpoint, blockValidity);
  }

  /**
   * Returns the block validity for the specified chain. Defaults to 20.
   *
   * @param chain The external chain to get the block validity depth for.
   * @return The block validity
   */
  private static int getBlockValidityForChain(String chain) {
    if (chain.equals(Chains.ETHEREUM) || chain.equals(Chains.SEPOLIA)) {
      return 10;
    } else {
      return 20;
    }
  }

  static List<BlockchainAddress> determinePriceOracleAddresses(
      List<ByocConfiguration> configuration) {
    List<BlockchainAddress> oracles = new ArrayList<>();
    for (ByocConfiguration bridge : configuration) {
      if (bridge.priceOracle() != null) {
        oracles.add(bridge.priceOracle());
      }
    }
    return oracles;
  }

  static List<BlockchainAddress> determineActiveContracts(List<ByocConfiguration> configuration) {
    List<BlockchainAddress> contracts =
        new ArrayList<>(
            List.of(
                BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
                BetanetAddresses.LARGE_ORACLE_CONTRACT));
    for (ByocConfiguration bridge : configuration) {
      contracts.add(bridge.incoming());
      contracts.add(bridge.outgoing());
    }
    contracts.addAll(determinePriceOracleAddresses(configuration));
    return contracts;
  }

  static Hash computeLargeOracleDatabaseKey(BigInteger oracleSigningKey) {
    return Hash.create(
        s -> {
          s.writeString("large-oracle-database-key");
          s.write(oracleSigningKey.toByteArray());
        });
  }

  static void logIfTrue(Consumer<String> logger, boolean condition, String message) {
    if (condition) {
      logger.accept(message);
    }
  }

  static void registerCombinedRealNodeIfProducer(
      boolean isProducer,
      CompositeNodeConfigDto nodeConfig,
      Main.CollectingServerConfig serverConfig,
      PersistentPeerDatabase peerDatabase,
      Optional<KeyPair> networkKey,
      RootDirectory governanceDir,
      BlockchainLedger governanceChain,
      SingleThreadedTransactionSender sender,
      List<Address> tcpReaders) {
    if (isProducer) {
      EngineConfig<
              SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
          realEngineConfig =
              buildBinaryRealEngineConfig(nodeConfig, peerDatabase, networkKey.get(), tcpReaders);
      List<EvmOracleChainConfig> oracleConfig = buildEvmOracleChainConfigs(nodeConfig);
      CombinedRealNode.register(
          governanceDir,
          governanceChain.getStateStorage(),
          serverConfig,
          realEngineConfig,
          oracleConfig,
          createTransactionSender(sender));
    }
  }

  static void registerFastTrackAndTcpReadersIfProducer(
      boolean isProducer,
      SingleThreadedTransactionSender sender,
      FastTrackProducerConfigDto fastTrackConfig,
      List<Address> tcpReaders,
      int shardOffset,
      RootDirectory subDir,
      BlockchainLedger blockchain,
      Main.CollectingServerConfig config) {
    if (isProducer) {
      FastTrackNode.register(
          subDir, blockchain, config, fastTrackConfig, sender::sendFeeDistributionTransaction);

      int shardTcpReaderPort = tcpReaders.get(shardOffset).port();
      registerTcpReader(config, subDir, blockchain, shardTcpReaderPort);
    }
  }

  static String isRestServerRunning(RestServer restServer) {
    return restServer != null ? restServer.isRunning() + "" : "";
  }

  static void registerRestComponents(
      Main.CollectingServerConfig serverConfig,
      BlockchainLedger governanceChain,
      Map<String, BlockchainResource> resources,
      Map<String, MetricsResource> metrics) {
    serverConfig.registerRestComponent(new BlockchainResource(governanceChain));
    serverConfig.registerRestComponent(new MetricsResource(governanceChain));
    serverConfig.registerRestComponent(new ShardResource(resources, metrics));
    serverConfig.registerRestComponent(OpenApiResource.class);
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  static void runSystemUpdate(
      CompositeNodeConfigDto nodeConfig,
      BlockchainLedger governanceChain,
      List<String> shards,
      SingleThreadedTransactionSender sender,
      Map<String, BlockchainResource> resources,
      String updateVotes,
      ShutdownHook shutdownHook) {
    ScheduledExecutorService executor = ExecutorFactory.newScheduled("SystemUpdate", 1);
    shutdownHook.register(executor);
    Map<Hash, Boolean> automaticVoteUpdateHashes = readAutomaticVotes(updateVotes, nodeConfig);
    executor.scheduleWithFixedDelay(
        () ->
            ExceptionLogger.handle(
                logger::warn,
                () ->
                    voteForSystemUpdatePolls(
                        nodeConfig,
                        governanceChain,
                        shards,
                        resources,
                        sender,
                        automaticVoteUpdateHashes),
                "Error voting for system updates."),
        30,
        Duration.ofHours(1).toSeconds(),
        TimeUnit.SECONDS);
  }

  static Map<Hash, Boolean> readAutomaticVotes(String resource, CompositeNodeConfigDto config) {
    try {
      return WithResource.apply(
          () -> ShardedMain.class.getResourceAsStream(resource),
          inputStream -> {
            if (inputStream == null) {
              return Map.of();
            } else {
              return new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                  .lines()
                  .collect(
                      Collectors.toMap(
                          Hash::fromString,
                          voteId ->
                              config.getOverrideOrDefault("systemupdate.vote." + voteId, true)));
            }
          },
          "Unable to read votes");
    } catch (Exception e) {
      logger.warn("Malformed " + resource + ".", e);
      return Map.of();
    }
  }

  private static void voteForSystemUpdatePolls(
      CompositeNodeConfigDto nodeConfig,
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources,
      SingleThreadedTransactionSender sender,
      Map<Hash, Boolean> automaticVoteUpdateHashes) {
    BigInteger pbcAccountKey =
        readPrivateKey(() -> nodeConfig.producerConfig.accountKey).orElseThrow();
    final BlockchainAddress address = new KeyPair(pbcAccountKey).getPublic().createAddress();

    VotingState votingContractState = getVotingContractState(governanceChain, shards, resources);
    Map<Hash, Boolean> votesToCast =
        determineVotesToCast(
            automaticVoteUpdateHashes, address, StateAccessor.create(votingContractState));

    for (Map.Entry<Hash, Boolean> entry : votesToCast.entrySet()) {
      Hash pollId = entry.getKey();
      boolean myVote = entry.getValue();
      logger.info("Voting {} for system update on poll {}", myVote, pollId);
      vote(pollId, sender, myVote);
    }
  }

  @SuppressWarnings("unchecked")
  static Map<Hash, Boolean> determineVotesToCast(
      Map<Hash, Boolean> automaticVoteUpdateHashes,
      BlockchainAddress address,
      StateAccessor contractState) {
    AvlTree<BlockchainAddress, Long> activeCommitteeVotes =
        contractState.get("activeCommitteeVotes").get("activeCommitteeVotes").cast(AvlTree.class);
    Map<Hash, Boolean> votesToCast = new HashMap<>();
    if (activeCommitteeVotes.containsKey(address)) {
      List<StateAccessorAvlLeafNode> pollEntries = contractState.get("polls").getTreeLeaves();
      for (StateAccessorAvlLeafNode pollEntry : pollEntries) {
        Hash pollId = pollEntry.getKey().cast(Hash.class);
        StateAccessor poll = pollEntry.getValue();
        if (poll.get("votes").getTreeValue(address) == null) {
          Hash pollHash = determinePollHash(poll);
          if (automaticVoteUpdateHashes.containsKey(pollHash)) {
            boolean myVote = automaticVoteUpdateHashes.get(pollHash);
            votesToCast.put(pollId, myVote);
          }
        }
      }
    }
    return votesToCast;
  }

  @SuppressWarnings("EnumOrdinal")
  static Hash determinePollHash(StateAccessor poll) {
    BlockchainAddress proposer = poll.get("owner").blockchainAddressValue();
    StateAccessor pendingUpdate = poll.get("pendingUpdate");
    int pollUpdateType = pendingUpdate.get("pollUpdateType").cast(Enum.class).ordinal();
    LargeByteArray updateContent = pendingUpdate.get("updateRpc").cast(LargeByteArray.class);
    Hash pollHash =
        Hash.create(
            s -> {
              proposer.write(s);
              s.writeByte(pollUpdateType);
              s.write(updateContent.getData());
            });
    return pollHash;
  }

  private static VotingState getVotingContractState(
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources) {
    BlockchainAddress systemUpdateAddress = BetanetAddresses.SYSTEM_UPDATE;
    String route =
        governanceChain
            .getChainState()
            .getRoutingPlugin()
            .route(FixedList.create(shards), systemUpdateAddress);
    JsonNode serializedContract =
        resources
            .get(route)
            .getContract(
                systemUpdateAddress.writeAsString(), -1, true, BlockchainResource.StateOutput.JSON)
            .serializedContract();
    return deserializeVotingContractState(serializedContract);
  }

  static VotingState deserializeVotingContractState(JsonNode serializedContract) {
    return ExceptionConverter.call(
        () -> mapper.convertValue(serializedContract, VotingState.class), "Unable to deserialize");
  }

  private static void vote(Hash pollId, SingleThreadedTransactionSender sender, boolean vote) {
    sender.sendTransaction(
        BetanetAddresses.SYSTEM_UPDATE,
        stream -> {
          stream.writeByte(2); // VotingContract.Invocation.VOTE
          pollId.write(stream);
          stream.writeBoolean(vote);
        });
  }

  private static PersistentPeerDatabase createPeerDatabase(
      Map<BlockchainPublicKey, Address> peers, RootDirectory dataDirectory) {
    return new PersistentPeerDatabase(peers, dataDirectory.createFile("peers.json"));
  }

  static TransactionSender createTransactionSender(SingleThreadedTransactionSender sender) {
    return sender::sendAndWaitForEvents;
  }

  static BlockchainTransactionClient createClient(KeyPair key, String url, List<String> shards) {
    // Wait a long time as the event is guaranteed to be included if the chain is running
    BlockchainTransactionClient client =
        BlockchainTransactionClient.create(
            url,
            new SenderAuthenticationKeyPair(key),
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            Duration.ofDays(7).toMillis());
    return client;
  }

  interface TransactionSenderProvider {
    TransactionSenderProvider REST_SENDER = SingleThreadedTransactionSender::new;

    SingleThreadedTransactionSender createTransactionClient(
        Main.CollectingServerConfig serverConfig,
        BlockchainTransactionClient blockchainTransactionClient);
  }

  record ByocConfig(
      String configPrefix,
      String ethereumUrl,
      String ethereumAddress,
      BigInteger ethereumBlockValidity,
      BlockchainAddress incoming,
      BlockchainAddress outgoing) {

    private void runDisputeService(
        CompositeNodeConfigDto nodeConfig,
        List<String> shards,
        BlockchainAddress pbcAccount,
        DataSource datasource,
        SingleThreadedTransactionSender sender,
        ShutdownHook shutdownHook,
        BlockchainAddress incomingContract,
        BlockchainAddress outgoingContract,
        ContractStateHandler contractStateHandler) {
      Settings settings =
          DisputeSettings.builder()
              .setTransactionCost(0)
              .setRequestInterval(getEthRequestInterval(nodeConfig))
              .setDelayOnError(getDelayOnError(nodeConfig))
              .setShards(shards)
              .setDataSource(datasource)
              .setTransactionSender(sender::sendAndWaitForEvents)
              .setPbcAccount(pbcAccount)
              .setPbcEndpoint(nodeConfig.restAddress())
              .setLargeOracleContract(BetanetAddresses.LARGE_ORACLE_CONTRACT)
              .setDepositContract(incomingContract)
              .setWithdrawContract(outgoingContract)
              .setContractStateHandler(contractStateHandler)
              .build();
      Server.run(settings, shutdownHook);
    }

    private EthereumSettings createEthereumSettings(
        CompositeNodeConfigDto nodeConfig,
        List<String> shards,
        BlockchainAddress pbcAccount,
        String ethereumAddress,
        SingleThreadedTransactionSender sender,
        String ethereumUrl,
        BigInteger ethereumBlockValidityDepth,
        ContractStateHandler contractStateHandler,
        PersistentByocEventCache eventCache) {
      return EthereumSettings.builder()
          .setEthereumBlockValidityDepth(ethereumBlockValidityDepth)
          .setTransactionCost(0)
          .setRequestInterval(getEthRequestInterval(nodeConfig))
          .setDelayOnError(getDelayOnError(nodeConfig))
          .setPbcEndpoint(nodeConfig.restAddress())
          .setPbcAccount(pbcAccount)
          .setShards(shards)
          .setEthereumEndpoint(ethereumUrl)
          .setEthereumByocContract(ethereumAddress)
          .setTransactionSender(sender::sendAndWaitForEvents)
          .setContractStateHandler(contractStateHandler)
          .setEthereumEventsCache(eventCache)
          .build();
    }

    private void runDepositService(
        CompositeNodeConfigDto nodeConfig,
        List<String> shards,
        BlockchainAddress pbcAccount,
        DataSource datasource,
        SingleThreadedTransactionSender sender,
        ShutdownHook shutdownHook,
        BlockchainAddress incomingContract,
        ContractStateHandler contractStateHandler) {

      Settings settings =
          DepositSettings.builder()
              .setTransactionCost(0)
              .setRequestInterval(getDepositRequestInterval(nodeConfig))
              .setDelayOnError(getDelayOnError(nodeConfig))
              .setPbcEndpoint(nodeConfig.restAddress())
              .setPbcAccount(pbcAccount)
              .setShards(shards)
              .setDepositContractAddress(incomingContract)
              .setDepositDataSource(datasource)
              .setTransactionSender(sender::sendAndWaitForEvents)
              .setContractStateHandler(contractStateHandler)
              .build();
      Server.run(settings, shutdownHook);
    }

    int getDepositRequestInterval(CompositeNodeConfigDto config) {
      return (int) config.getOverrideOrDefault(configPrefix() + ".deposit.check_interval", 5_000);
    }

    int getEthRequestInterval(CompositeNodeConfigDto config) {
      return (int)
          config.getOverrideOrDefault(configPrefix() + ".deposit.eth.check_interval", 300_000);
    }

    int getDelayOnError(CompositeNodeConfigDto config) {
      return (int) config.getOverrideOrDefault(configPrefix() + ".delay_on_error", 60_000);
    }

    private void runWithdrawalService(
        CompositeNodeConfigDto nodeConfig,
        List<String> shards,
        BigInteger oracleSigningKey,
        BlockchainAddress pbcAccount,
        SingleThreadedTransactionSender sender,
        BlockchainAddress outgoingContract,
        ShutdownHook shutdownHook,
        ContractStateHandler contractStateHandler) {
      Settings settings =
          WithdrawalSettings.builder()
              .setTransactionCost(0)
              .setRequestInterval(getWithdrawalCheckInterval(nodeConfig))
              .setDelayOnError(getDelayOnError(nodeConfig))
              .setPbcEndpoint(nodeConfig.restAddress())
              .setOracleSigningKey(new KeyPair(oracleSigningKey))
              .setPbcAccount(pbcAccount)
              .setShards(shards)
              .setWithdrawalContractAddress(outgoingContract)
              .setTransactionSender(sender::sendAndWaitForEvents)
              .setContractStateHandler(contractStateHandler)
              .build();
      Server.run(settings, shutdownHook);
    }

    int getWithdrawalCheckInterval(CompositeNodeConfigDto config) {
      return (int)
          config.getOverrideOrDefault(configPrefix() + ".withdrawal.check_interval", 300_000);
    }

    PersistentByocEventCache createPersistentByocEventCache(RootDirectory byocDirectory) {
      ByocEventFileStorage storage =
          new ByocEventFileStorage(byocDirectory.createFile(configPrefix + ".json"));
      return new PersistentByocEventCache(storage);
    }

    void runByocOracleServices(
        RootDirectory byocDirectory,
        CompositeNodeConfigDto nodeConfig,
        List<String> shards,
        BigInteger oracleSigningKey,
        BlockchainAddress pbcAccount,
        SingleThreadedTransactionSender sender,
        ShutdownHook shutdownHook,
        ContractStateHandler contractStateHandler,
        AdditionalStatusProviders additionalStatusProviders) {
      if (ethereumAddress() == null) {
        logger.info("BYOC ({}) contract not yet deployed", configPrefix());
        return;
      }
      boolean ethereumConfigMissing = ethereumUrl() == null || ethereumUrl().isEmpty();
      if (ethereumConfigMissing) {
        logger.warn("Missing web3j endpoint from node configuration");
      } else {
        PersistentByocEventCache eventCache = createPersistentByocEventCache(byocDirectory);
        additionalStatusProviders.register(
            new LatestExternalBlockProvider(configPrefix(), eventCache));
        EthereumSettings ethereumSettings =
            createEthereumSettings(
                nodeConfig,
                shards,
                pbcAccount,
                ethereumAddress(),
                sender,
                ethereumUrl(),
                ethereumBlockValidity(),
                contractStateHandler,
                eventCache);
        Ethereum ethereum = Ethereum.create(ethereumSettings);
        ByocProcessRunner processRunner =
            new ByocProcessRunner(
                ethereum,
                ethereumSettings.getDelayOnError(),
                ethereumSettings.getRequestInterval());
        shutdownHook.register(processRunner);
        boolean depositEnabled =
            nodeConfig.getOverrideOrDefault(configPrefix() + ".deposit.enabled", true);
        if (depositEnabled) {
          runDepositService(
              nodeConfig,
              shards,
              pbcAccount,
              ethereum,
              sender,
              shutdownHook,
              incoming(),
              contractStateHandler);
        }
        boolean disputeEnabled =
            nodeConfig.getOverrideOrDefault(configPrefix() + ".dispute.enabled", false);
        if (disputeEnabled) {
          runDisputeService(
              nodeConfig,
              shards,
              pbcAccount,
              ethereum,
              sender,
              shutdownHook,
              incoming(),
              outgoing(),
              contractStateHandler);
        }
      }
      boolean withdrawalEnabled =
          nodeConfig.getOverrideOrDefault(configPrefix() + ".withdrawal.enabled", true);
      if (withdrawalEnabled) {
        runWithdrawalService(
            nodeConfig,
            shards,
            oracleSigningKey,
            pbcAccount,
            sender,
            outgoing(),
            shutdownHook,
            contractStateHandler);
      }
    }
  }

  private static void runLargeOracleService(
      CompositeNodeConfigDto nodeConfig,
      List<String> shards,
      Hash largeOracleDatabaseKey,
      BigInteger oracleSigningKey,
      BlockchainAddress pbcAccount,
      SingleThreadedTransactionSender sender,
      PeerDatabase peers,
      AdditionalStatusProviders additionalStatusProviders,
      File dataDirectory,
      ShutdownHook shutdownHook,
      ContractStateHandler contractStateHandler) {
    File largeOracleDatabase = new File(dataDirectory, "large-oracle-database.db");
    File largeOracleBackupDatabase = new File(dataDirectory, "large-oracle-backup-database.db");
    Settings settings =
        LargeOracleSettings.builder()
            .setTransactionCost(0)
            .setRequestInterval(getLargeOracleCheckInterval(nodeConfig))
            .setDelayOnError(getLargeOracleDelayOnError(nodeConfig))
            .setPbcEndpoint(nodeConfig.restAddress())
            .setSigningKey(new KeyPair(oracleSigningKey))
            .setPbcAccount(pbcAccount)
            .setShards(shards)
            .setBpOrchestrationContract(BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION)
            .setLargeOracleContract(BetanetAddresses.LARGE_ORACLE_CONTRACT)
            .setLargeOraclePort(nodeConfig.floodingPort)
            .setTransactionSender(sender::sendAndWaitForEvents)
            .setPeerDatabase(peers)
            .setMyEndpoint(
                new com.partisiablockchain.demo.byoc.Address(
                    nodeConfig.producerConfig.host, nodeConfig.floodingPort))
            .setLargeOracleDatabaseName(largeOracleDatabase.getAbsolutePath())
            .setLargeOracleDatabaseBackupName(largeOracleBackupDatabase.getAbsolutePath())
            .setLargeOracleDatabaseKey(largeOracleDatabaseKey.getBytes())
            .setPreSignatureCount(10)
            .setVersionInformation(new VersionInformation("3.0.0"))
            .setFinalizationKey(
                new BlsKeyPair(new BigInteger(nodeConfig.producerConfig.finalizationKey, 16))
                    .getPublicKey())
            .setContractStateHandler(contractStateHandler)
            .setAdditionalStatusProvider(additionalStatusProviders)
            .build();
    Server.run(settings, shutdownHook);
  }

  static int getLargeOracleCheckInterval(CompositeNodeConfigDto config) {
    return (int) config.getOverrideOrDefault("byoc.large_oracle.check_interval", 120_000);
  }

  static int getLargeOracleDelayOnError(CompositeNodeConfigDto config) {
    return (int) config.getOverrideOrDefault("byoc.large_oracle.delay_on_error", 60_000);
  }

  private static void runPriceOracleService(
      CompositeNodeConfigDto nodeConfig,
      List<String> shards,
      BlockchainAddress pbcAccount,
      SingleThreadedTransactionSender sender,
      ShutdownHook shutdownHook,
      List<BlockchainAddress> priceOracleContracts,
      ContractStateHandler contractStateHandler) {

    List<EvmPriceOracleChainConfig> priceOracleEvmChains = createEvmPriceOracleConfigs(nodeConfig);

    PriceOracleSettings.Builder settingsBuilder =
        PriceOracleSettings.builder()
            .setTransactionCost(0)
            .setRequestInterval(getPriceOracleCheckInterval(nodeConfig))
            .setDelayOnError(getPriceOracleDelayOnError(nodeConfig))
            .setShards(shards)
            .setTransactionSender(sender::sendAndWaitForEvents)
            .setPbcAccount(pbcAccount)
            .setPbcEndpoint(nodeConfig.restAddress())
            .setLargeOracleContract(BetanetAddresses.LARGE_ORACLE_CONTRACT)
            .setPriceOracleContracts(priceOracleContracts)
            .setContractStateHandler(contractStateHandler)
            .setChainConfigs(priceOracleEvmChains);

    PriceOracleSettings settings = settingsBuilder.build();
    Server.run(settings, shutdownHook);
  }

  static int getPriceOracleCheckInterval(CompositeNodeConfigDto config) {
    return (int) config.getOverrideOrDefault("price_oracle.check_interval", 120_000);
  }

  static int getPriceOracleDelayOnError(CompositeNodeConfigDto config) {
    return (int) config.getOverrideOrDefault("price_oracle.delay_on_error", 60_000);
  }

  static List<ByocConfiguration> getByocConfiguration(
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources) {
    String route =
        governanceChain
            .getChainState()
            .getRoutingPlugin()
            .route(FixedList.create(shards), BetanetAddresses.BYOC_ORCHESTRATOR);

    BlockchainResource blockchainResource = resources.get(route);
    JsonNode byocBridgesNode;
    try {
      byocBridgesNode =
          blockchainResource.traverseContractState(
              BetanetAddresses.BYOC_ORCHESTRATOR.writeAsString(),
              new TraversePath(List.of(new FieldTraverse("byocBridges"))));

      record TokenBridge(String chain, BlockchainAddress incoming, BlockchainAddress outgoing) {}

      record ByocOrchestratorBridge(
          String chain,
          BlockchainAddress incoming,
          BlockchainAddress outgoing,
          BlockchainAddress priceOracle,
          List<TokenBridge> tokenBridges) {}

      // Create wrapper since the object mapper are unable to deserialize AvlTree directly
      record Wrapper(AvlTree<String, ByocOrchestratorBridge> tree) {}

      AvlTree<String, ByocOrchestratorBridge> symbolAndBridge =
          mapper
              .treeToValue(mapper.createObjectNode().set("tree", byocBridgesNode), Wrapper.class)
              .tree();

      List<ByocConfiguration> activeByocConfigurations = new ArrayList<>();
      for (String coinSymbol : symbolAndBridge.keySet()) {
        ByocOrchestratorBridge bridge = symbolAndBridge.getValue(coinSymbol);
        activeByocConfigurations.add(
            new ByocConfiguration(
                bridge.chain(),
                coinSymbol,
                bridge.incoming(),
                bridge.outgoing(),
                bridge.priceOracle()));
        if (bridge.tokenBridges() != null) {
          for (TokenBridge tokenBridge : bridge.tokenBridges()) {
            activeByocConfigurations.add(
                new ByocConfiguration(
                    tokenBridge.chain(),
                    coinSymbol,
                    tokenBridge.incoming(),
                    tokenBridge.outgoing(),
                    null));
          }
        }
      }
      return activeByocConfigurations;
    } catch (Exception e) {
      logger.info("Unable to resolve BYOC orchestrator contract, using default configuration", e);
      return List.of(
          new ByocConfiguration(
              "Ethereum",
              "ETH",
              BetanetAddresses.BYOC_INCOMING,
              BetanetAddresses.BYOC_OUTGOING,
              BetanetAddresses.PO_ETH_USD),
          new ByocConfiguration(
              "Polygon",
              "USDC",
              BetanetAddresses.BYOC_POLYGON_USDC_INCOMING,
              BetanetAddresses.BYOC_POLYGON_USDC_OUTGOING,
              null),
          new ByocConfiguration(
              "BnbSmartChain",
              "BNB",
              BetanetAddresses.BYOC_BNB_SMARTCHAIN_BNB_INCOMING,
              BetanetAddresses.BYOC_BNB_SMARTCHAIN_BNB_OUTGOING,
              BetanetAddresses.PO_BNB_USD),
          new ByocConfiguration(
              "Ethereum",
              "PARTI",
              BetanetAddresses.BYOC_ETHEREUM_PARTI_INCOMING,
              BetanetAddresses.BYOC_ETHEREUM_PARTI_OUTGOING,
              null));
    }
  }

  static String getEthereumByocContract(
      BlockchainLedger governanceChain,
      List<String> shards,
      Map<String, BlockchainResource> resources,
      BlockchainAddress byocOutgoingContract) {
    String route =
        governanceChain
            .getChainState()
            .getRoutingPlugin()
            .route(FixedList.create(shards), byocOutgoingContract);

    BlockchainResource blockchainResource = resources.get(route);
    JsonNode jsonNode;
    try {
      jsonNode =
          blockchainResource.traverseContractState(
              byocOutgoingContract.writeAsString(),
              new TraversePath(
                  List.of(new FieldTraverse("byocContract"), new FieldTraverse("identifier"))));
    } catch (Exception e) {
      logger.info("Unable to get BYOC address");
      return null;
    }
    LargeByteArray ethereumByocContract =
        ExceptionConverter.call(
            () ->
                StateObjectMapper.createObjectMapper().treeToValue(jsonNode, LargeByteArray.class),
            "Unable to deserialize");

    String byocAddress = "0x" + Hex.toHexString(ethereumByocContract.getData());
    if ("0x97a2b3856bebd8fd0b65841481777a4666d1e7cd".equals(byocAddress)) {
      logger.info("Ignoring misconfigured BYOC contract");
      return null;
    }

    return byocAddress;
  }

  static EngineConfig<
          SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      buildBinaryRealEngineConfig(
          CompositeNodeConfigDto nodeConfig,
          RealPeerDatabase peerDatabase,
          KeyPair engineKey,
          List<Address> shardReaders) {
    BlockchainAddress nodeIdentifier = nodeConfig.getProducerPbcAccount();
    String hostname = nodeConfig.producerConfig.host;
    int port = nodeConfig.floodingPort + PersistentPeerDatabase.REAL_PORT_OFFSET;
    String realNetworkAddress = hostname + ":" + port;
    return EngineConfig.createBinaryConfig(
        engineKey,
        nodeIdentifier,
        BetanetAddresses.ZK_NODE_REGISTRY,
        BetanetAddresses.REAL_PREPROCESS,
        realNetworkAddress,
        peerDatabase,
        shardReaders);
  }

  static List<EvmPriceOracleChainConfig> createEvmPriceOracleConfigs(
      CompositeNodeConfigDto nodeConfigDto) {
    return buildEvmOracleChainConfigs(nodeConfigDto).stream()
        .map(
            config ->
                new EvmPriceOracleChainConfig(
                    config.chainId(), config.web3Endpoint(), config.blockValidityThreshold()))
        .toList();
  }

  static List<EvmOracleChainConfig> buildEvmOracleChainConfigs(CompositeNodeConfigDto nodeConfig) {
    List<EvmOracleChainConfig> oracleChainConfigs = new ArrayList<>();
    for (String chainId : Chains.getKnownChains()) {
      EvmOracleChainConfig chainConfig = buildEvmOracleChainConfig(nodeConfig, chainId);
      if (chainConfig != null) {
        oracleChainConfigs.add(chainConfig);
      }
    }
    return oracleChainConfigs;
  }

  private static EvmOracleChainConfig buildEvmOracleChainConfig(
      CompositeNodeConfigDto config, String chainId) {
    String endpoint = config.producerConfig.getChainEndpoint(chainId);
    return buildEvmOracleChainConfig(config, chainId, endpoint, getBlockValidityForChain(chainId));
  }

  private static EvmOracleChainConfig buildEvmOracleChainConfig(
      CompositeNodeConfigDto config,
      String chainId,
      String web3Endpoint,
      int defaultBlockValidity) {
    if (web3Endpoint == null || web3Endpoint.isEmpty()) {
      return null;
    } else {
      return new EvmOracleChainConfig(
          chainId,
          web3Endpoint,
          evmOracleBlockValidity(config, chainId.toLowerCase(Locale.ENGLISH), defaultBlockValidity),
          evmOracleMaxBlockRange(config, chainId.toLowerCase(Locale.ENGLISH), 5_000),
          evmOracleLogReaderInterval(config, chainId.toLowerCase(Locale.ENGLISH), 900_000));
    }
  }

  private static BigInteger evmOracleBlockValidity(
      CompositeNodeConfigDto config, String chainId, int defaultValue) {
    String configurationKey = "evm.oracle.%s.block_validity".formatted(chainId);
    long blockValidity = config.getOverrideOrDefault(configurationKey, defaultValue);
    return BigInteger.valueOf(blockValidity);
  }

  private static BigInteger evmOracleMaxBlockRange(
      CompositeNodeConfigDto config, String chainId, int defaultValue) {
    String configurationKey = "evm.oracle.%s.max_block_range".formatted(chainId);
    long maxBlockRange = config.getOverrideOrDefault(configurationKey, defaultValue);
    return BigInteger.valueOf(maxBlockRange);
  }

  private static long evmOracleLogReaderInterval(
      CompositeNodeConfigDto config, String chainId, int defaultValue) {
    String configurationKey = "evm.oracle.%s.check_interval".formatted(chainId);
    return config.getOverrideOrDefault(configurationKey, defaultValue);
  }

  static void addRestAliveCheck(
      Main.CollectingServerConfig serverConfig,
      BlockchainLedger blockchainLedger,
      SystemTimeProvider systemTimeProvider) {
    serverConfig.addRestAliveCheck(
        () -> isStillAlive(blockchainLedger, systemTimeProvider.getSystemTime()));
  }

  /**
   * Check if the node is considered alive on block production time. A node is alive if the latest
   * block production time is within {@code TIMEOUT_FOR_BLOCK_PRODUCTION}.
   *
   * @param blockchainLedger nodes blockchain ledger
   * @param systemTimeMillis current time of node in millis
   * @return true if the node is alive
   */
  static boolean isStillAlive(BlockchainLedger blockchainLedger, long systemTimeMillis) {
    long blockAge = systemTimeMillis - blockchainLedger.getLatestBlock().getProductionTime();
    return blockAge < TIMEOUT_FOR_BLOCK_PRODUCTION.toMillis();
  }

  static BlockchainClient createEventPropagationModule(
      CompositeNodeConfigDto nodeConfig, List<String> shards) {
    String baseRest = nodeConfig.restAddress();
    return BlockchainClient.create(baseRest, shards.size());
  }

  static void registerTcpReader(
      Main.CollectingServerConfig serverConfig,
      RootDirectory directory,
      BlockchainLedger chain,
      int port) {
    TcpModule rcpReaderModule = new TcpModule();
    TcpModule.TcpModuleConfig tcpConfig = new TcpModule.TcpModuleConfig();
    tcpConfig.port = port;
    rcpReaderModule.register(directory, chain, serverConfig, tcpConfig);
  }

  static void updatePeerDatabaseIfProducer(
      boolean isProducer,
      Optional<KeyPair> networkKey,
      CompositeNodeConfigDto nodeConfig,
      PeerDatabase peerDatabase) {
    if (isProducer) {
      if (networkKey.isEmpty()) {
        throw new RuntimeException("Unable to register fast track modules without network key");
      }
      String host = nodeConfig.producerConfig.host;
      if (host == null || host.isBlank()) {
        throw new RuntimeException("Unable to start producer node without producerConfig.host");
      }
      peerDatabase.updateBlockProducerEndpoint(
          networkKey.get().getPublic(),
          new com.partisiablockchain.demo.byoc.Address(host, nodeConfig.floodingPort));
    }
  }

  static long getTimeout(CompositeNodeConfigDto config) {
    return config.getOverrideOrDefault("fasttrack.producer.timeout", 30_000L);
  }

  static long getNetworkDelay(CompositeNodeConfigDto config) {
    return config.getOverrideOrDefault("fasttrack.producer.network_delay", 15_000L);
  }

  private static FixedList<String> getShards(BlockchainLedger governanceBlockchain) {
    return governanceBlockchain.getChainState().getActiveShards();
  }

  static void validateConfig(
      File dataDirectory, Path genesisConfigPath, ResourceList.PropertiesFileConsumer consumer)
      throws IOException {
    List<String> genesisConfig = Files.readAllLines(genesisConfigPath);
    File activeConfig = new File(dataDirectory, "genesis_config");
    List<String> currentConfig = safeRead(activeConfig);

    logger.info(
        "Booting network. Configuration of current storage is {}, active configuration is {}",
        genesisConfig,
        currentConfig);

    if (currentConfig == null) {
      Files.copy(genesisConfigPath, activeConfig.toPath());
    } else if (!Objects.equals(genesisConfig, currentConfig)) {
      throw new IllegalStateException("Config in storage does not match config of current image");
    }
    ResourceList.consumeGitProperties(consumer);
  }

  static List<String> safeRead(File chainId) {
    if (chainId.exists()) {
      return ExceptionConverter.call(
          () -> Files.readAllLines(chainId.toPath()), "Unable to read file");
    } else {
      return null;
    }
  }

  static Map<String, String> createEventMap(String baseUrl, List<String> shards) {
    Map<String, String> map = new HashMap<>();
    for (String shard : shards) {
      map.put(shard, baseUrl + "/shards/" + shard);
    }
    return map;
  }

  static NetworkConfig createNetworkConfig(
      PersistentPeerDatabase peers, CompositeNodeConfigDto config, int offset, boolean isProducer) {
    List<Supplier<Address>> addressSuppliers = new ArrayList<>();

    Collection<com.partisiablockchain.demo.byoc.Address> peerDatabaseValues = peers.getPeers();
    List<Address> knownPeers = config.getKnownPeers();
    addressSuppliers.add(
        Main.KnownPeers.create(
            Stream.concat(
                    knownPeers.stream(),
                    peerDatabaseValues.stream().map(a -> new Address(a.getHost(), a.getPort())))
                .distinct()
                .map(address -> new Address(address.hostname(), address.port() + offset))
                .collect(Collectors.toList())));

    Address nodeAddress;
    if (isProducer) {
      nodeAddress = new Address(config.producerConfig.host, config.floodingPort + offset);
    } else {
      nodeAddress = new Address("localhost", config.floodingPort + offset);
    }
    return new NetworkConfig(
        nodeAddress, Main.joinSuppliers(addressSuppliers), config.getPersistentPeer(offset));
  }

  static Map<BlockchainPublicKey, Address> getPeersFromFile(String path) {
    List<String> peers = safeRead(new File(path));
    if (peers == null) {
      return Map.of();
    } else {
      Map<BlockchainPublicKey, Address> result = new HashMap<>();
      for (String peer : peers) {
        String[] split = peer.split("\t", -1);
        if (split.length == 3) {
          result.put(
              BlockchainPublicKey.fromEncodedEcPoint(Hex.decode(split[2])), toAddress(split[0]));
        }
      }
      return result;
    }
  }

  static Address toAddress(String s) {
    String[] split = s.split(":", -1);
    if (split.length == 2) {
      return new Address(split[0], Integer.parseInt(split[1]));
    } else {
      return new Address(s, 9888);
    }
  }

  private static Optional<KeyPair> networkKey(CompositeNodeConfigDto config) {
    return readPrivateKey(() -> config.networkKey).map(KeyPair::new);
  }

  static Optional<BigInteger> readPrivateKey(Supplier<String> configField) {
    String hexKey = configField.get();
    if (hexKey == null || hexKey.isEmpty()) {
      return Optional.empty();
    } else {
      BigInteger privateKey = new BigInteger(hexKey, 16);
      return Optional.of(privateKey);
    }
  }

  static BlockchainLedger createLedger(
      Optional<KeyPair> networkKey,
      NetworkConfig network,
      RootDirectory rootDir,
      String genesisFile,
      Map<String, BlockchainLedger> ledgers,
      Consumer<AdditionalStatusProvider> registerProviders) {
    return networkKey
        .map(
            key ->
                BlockchainLedger.createBlockChain(
                    network,
                    rootDir,
                    genesisFile,
                    key,
                    (shardId, eventId) -> ledgers.get(shardId).getEventTransaction(eventId),
                    registerProviders))
        .orElseGet(
            () ->
                BlockchainLedger.createBlockChain(
                    network,
                    rootDir,
                    genesisFile,
                    (shardId, eventId) -> ledgers.get(shardId).getEventTransaction(eventId),
                    registerProviders));
  }
}
