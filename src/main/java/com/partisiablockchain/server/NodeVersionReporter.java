package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Send {@code UPDATE_NODE_VERSION} transaction to BpOrchestration contract. */
public final class NodeVersionReporter {
  private static final int UPDATE_NODE_VERSION_INVOCATION = 15;

  /**
   * Send an {@code UPDATE_NODE_VERSION} transaction to BpOrchestration contract with the node
   * software version.
   *
   * @param sender transaction sender
   * @param nodeVersion Semantic node software version to report
   */
  public static void reportNodeVersion(
      SingleThreadedTransactionSender sender, SemanticVersion nodeVersion) {
    sender.sendTransaction(
        BetanetAddresses.BLOCK_PRODUCER_ORCHESTRATION,
        stream -> {
          stream.writeByte(UPDATE_NODE_VERSION_INVOCATION);
          stream.writeInt(nodeVersion.major());
          stream.writeInt(nodeVersion.minor());
          stream.writeInt(nodeVersion.patch());
        });
  }

  /** Static only class. */
  private NodeVersionReporter() {}
}
