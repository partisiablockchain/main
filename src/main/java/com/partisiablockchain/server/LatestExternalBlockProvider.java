package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import java.util.Map;

/** Status provider for the latest block from an external chain. */
public final class LatestExternalBlockProvider implements AdditionalStatusProvider {
  private final EthereumEventsCache ethereumEventsCache;
  private final String chain;

  /**
   * Constructs a new status provider for getting the latest block from an external chain.
   *
   * @param chain The external chain
   * @param ethereumEventsCache The event cache to get the latest block from
   */
  public LatestExternalBlockProvider(String chain, EthereumEventsCache ethereumEventsCache) {
    this.ethereumEventsCache = ethereumEventsCache;
    this.chain = chain;
  }

  @Override
  public String getName() {
    return "LatestExternalBlockProvider-%s".formatted(chain);
  }

  @Override
  public Map<String, String> getStatus() {
    Unsigned256 latestBlock = ethereumEventsCache.loadLatestCheckedBlock();
    String key = "latestBlockTime";
    String value;
    if (latestBlock == null) {
      value = "-1";
    } else {
      value = latestBlock.toString();
    }
    return Map.of(key, value);
  }
}
