package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.tools.coverage.ExceptionLogger;
import java.io.File;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Class for synchronized reading and writing of peer database files. */
final class PeersFile {

  private static final Logger logger = LoggerFactory.getLogger(PeersFile.class);
  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private final File peersFile;

  PeersFile(File peersFile) {
    this.peersFile = peersFile;
  }

  synchronized Map<Hash, Address> readPeers() {
    try {
      return mapper.readValue(peersFile, new TypeReference<>() {});
    } catch (Exception e) {
      logger.info("Exception when reading from peer file " + e);
      return Map.of();
    }
  }

  synchronized void writePeers(Map<Hash, Address> updatedPeers) {
    ExceptionLogger.handle(
        logger::info,
        () -> mapper.writeValue(peersFile, updatedPeers),
        "Could not write peers to file " + peersFile);
  }

  boolean exists() {
    return peersFile.exists();
  }
}
