package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;

/** Decorate in-memory ethereum BYOC events cache with a persistence layer. */
public final class PersistentByocEventCache implements EthereumEventsCache {

  private final ByocEventStorage eventStorage;
  private final EthereumEventsCache innerCache;

  PersistentByocEventCache(ByocEventStorage eventStorage) {
    this.eventStorage = eventStorage;
    this.innerCache = eventStorage.readEvents();
  }

  @Override
  public DepositTransaction loadDeposit(long depositNonce) {
    return innerCache.loadDeposit(depositNonce);
  }

  @Override
  public List<DepositTransaction> loadAllDeposits() {
    return innerCache.loadAllDeposits();
  }

  @Override
  public WithdrawalTransaction loadWithdrawal(DataSource.WithdrawalId withdrawalId) {
    return innerCache.loadWithdrawal(withdrawalId);
  }

  @Override
  public List<WithdrawalTransaction> loadAllWithdrawals() {
    return innerCache.loadAllWithdrawals();
  }

  @Override
  public Unsigned256 loadLatestCheckedBlock() {
    return innerCache.loadLatestCheckedBlock();
  }

  @Override
  public void saveDeposits(List<DepositTransaction> deposits) {
    innerCache.saveDeposits(deposits);
  }

  @Override
  public void saveWithdrawals(List<WithdrawalTransaction> withdrawals) {
    innerCache.saveWithdrawals(withdrawals);
  }

  @Override
  public void saveLatestCheckedBlock(Unsigned256 blockNumber) {
    innerCache.saveLatestCheckedBlock(blockNumber);
    eventStorage.writeEvents(innerCache);
  }
}
