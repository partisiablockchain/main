package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;

/** Addresses deployed in genesis. */
public final class BetanetAddresses {

  private BetanetAddresses() {}

  /** Address system updates contract. 04c5f00d7c6d70c3d0919fd7f81c7b9bfe16063620 */
  public static final BlockchainAddress SYSTEM_UPDATE =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "SystemUpdate");

  /** Address for bp orchestration contract. 04203b77743ad0ca831df9430a6be515195733ad91 */
  public static final BlockchainAddress BLOCK_PRODUCER_ORCHESTRATION =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "block-producer-orchestration-contract");

  /** Address for large oracle contract. 04f1ab744630e57fb9cfcd42e6ccbf386977680014 */
  public static final BlockchainAddress LARGE_ORACLE_CONTRACT =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "large-oracle-contract");

  /** Address for BYOC orchestrator contract. 0458ff0a290e2fe847b23a364925799d1c53c8b36b */
  public static final BlockchainAddress BYOC_ORCHESTRATOR =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-orchestrator");

  /** Address for small oracle BYOC deposit contract. 045dbd4c13df987d7fb4450e54bcd94b34a80f2351 */
  public static final BlockchainAddress BYOC_INCOMING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-incoming");

  /**
   * Address for small oracle BYOC withdrawal contract. 043b1822925da011657f9ab3d6ff02cf1e0bfe0146
   */
  public static final BlockchainAddress BYOC_OUTGOING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-outgoing");

  /** Address for small oracle BYOC deposit contract. 042f2f190765e27f175424783a1a272e2a983ef372 */
  public static final BlockchainAddress BYOC_POLYGON_USDC_INCOMING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-polygon-usdc-incoming");

  /**
   * Address for small oracle BYOC withdrawal contract. 04adfe4aaacc824657e49a59bdc8f14df87aa8531a
   */
  public static final BlockchainAddress BYOC_POLYGON_USDC_OUTGOING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-polygon-usdc-outgoing");

  /**
   * Address for small oracle BYOC withdrawal contract for BNB bridged from BNB Smart Chain.
   * 044bd689e5fe2995d679e946a2046f69f022be7c10
   */
  public static final BlockchainAddress BYOC_BNB_SMARTCHAIN_BNB_OUTGOING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-bnb-smartchain-bnb-outgoing");

  /**
   * Address for small oracle BYOC deposit contract for BNB bridged from BNB Smart Chain.
   * 047e1c96cd53943d1e0712c48d022fb461140e6b9f
   */
  public static final BlockchainAddress BYOC_BNB_SMARTCHAIN_BNB_INCOMING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-bnb-smartchain-bnb-incoming");

  /**
   * Address for small oracle BYOC withdrawal contract for PARTI from Ethereum.
   * 04b77d898aaf0818fb302326784281e6d77fae018d
   */
  public static final BlockchainAddress BYOC_ETHEREUM_PARTI_OUTGOING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-ethereum-parti-outgoing");

  /**
   * Address for small oracle BYOC deposit contract for PARTI from Ethereum.
   * 041545a7897b5be366a86cc40befec18959df88416
   */
  public static final BlockchainAddress BYOC_ETHEREUM_PARTI_INCOMING =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "byoc-ethereum-parti-incoming");

  /**
   * Address for the public WASM contract deploy contract.
   * 0197a0e238e924025bad144aa0c4913e46308f9a4d
   */
  public static final BlockchainAddress PUB_DEPLOY_WASM =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "pub-deploy-wasm");

  /**
   * Address allowing a root user to deploy new contracts.
   * 010b0d2e236ff42492650adebfd6f76418d26dc594
   */
  public static final BlockchainAddress ROOT_DEPLOY =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "RootDeploy");

  /** The fee distribution contract. 04fe17d1009372c8ed3ac5b790b32e349359c2c7e9 */
  public static final BlockchainAddress FEE_DISTRIBUTION =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "FeeDistribution");

  /** The Mpc token contract. 01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881 */
  public static final BlockchainAddress MPC_TOKEN_CONTRACT =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "MpcTokenContract");

  /** The mpc-token-mpc20 contract. 01615beb1c2bf57e45fcd1c4e67ef35b8735a685b1 */
  public static final BlockchainAddress MPC_TOKEN_MPC20_CONTRACT =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "MpcTokenMpc20Contract");

  /** The mpc-wrap contract. 017d9dacdd01f0b2bd4de40da37f545e89b7faa149 */
  public static final BlockchainAddress MPC_WRAP_CONTRACT =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "mpc-wrap");

  /** Contract controlling the ecosystem tokens. 01ad44bb0277a8df16408006c375a6fa015bb22c97 */
  public static final BlockchainAddress FOUNDATION_ECOSYSTEM =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationEcosystem");

  /** Contract controlling the ecosystem tokens. 012635f1c0a9bffd59853c6496e1c26ebda0e2b4da */
  public static final BlockchainAddress FOUNDATION_SALE =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationSale");

  /** Contract controlling the ecosystem tokens. 0135edec2c9fed33f45cf2538dc06ba139c4bb8f62 */
  public static final BlockchainAddress FOUNDATION_TEAM =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationTeamTokens");

  /** Contract controlling the ecosystem tokens. 016fe3c61bf8f8bd623deebb74cd80fcedc646b605 */
  public static final BlockchainAddress FOUNDATION_RESERVE =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "FoundationReserve");

  /** Address of the ZK node registry contract. 01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65 */
  public static final BlockchainAddress ZK_NODE_REGISTRY =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "ZkNodeRegistry");

  /**
   * Address of the REAL contract deployment contract. 018bc1ccbb672b87710327713c97d43204905082cb
   */
  public static final BlockchainAddress REAL_DEPLOY =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "RealDeploy");

  /**
   * Address of the REAL preprocess orchestration contract.
   * 01385fedf807390c3dedf42ba51208bc51292e2657
   */
  public static final BlockchainAddress REAL_PREPROCESS =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "RealPreprocess");

  /** Synaps KYC contract. 014aeb24bb43eb1d62c0cebf2a1318e63e35e53f96 */
  public static final BlockchainAddress SYNAPS_KYC =
      createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, "SynapsKyc");

  /**
   * Address for the price oracle contract for ETH/USD. 0485010babcdb7aa56a0da57a840d81e2ea5f5705d
   */
  public static final BlockchainAddress PO_ETH_USD =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "po-eth-usd");

  /**
   * Address for the price oracle contract for BNB/USD. 049abfc6e763e8115e886fd1f7811944f43b533c39
   */
  public static final BlockchainAddress PO_BNB_USD =
      createAddress(BlockchainAddress.Type.CONTRACT_GOV, "po-bnb-usd");

  static BlockchainAddress createAddress(BlockchainAddress.Type type, String address) {
    return BlockchainAddress.fromHash(type, Hash.create(s -> s.writeString(address)));
  }
}
