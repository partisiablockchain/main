package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** A version number <code>major.minor.patch</code> used for versioning the node software. */
@Immutable
public record SemanticVersion(int major, int minor, int patch) implements StateSerializable {

  /**
   * Converts a string of semantic version to {@link SemanticVersion}. Returns null if version is
   * invalid. A node version is considered valid if it starts with "x.y.z" where x,y,z is positive
   * integers. Any characters after "x.y.z" is removed.
   *
   * @param nodeVersion string of semantic version
   * @return nodeVersion as {@link SemanticVersion}
   */
  static SemanticVersion readSemanticVersion(String nodeVersion) {
    Matcher matcher = Pattern.compile("^([0-9]+)\\.([0-9]+)\\.([0-9]+)").matcher(nodeVersion);
    if (matcher.find()) {
      return new SemanticVersion(
          Integer.parseInt(matcher.group(1)),
          Integer.parseInt(matcher.group(2)),
          Integer.parseInt(matcher.group(3)));
    } else {
      return null;
    }
  }
}
