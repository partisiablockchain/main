package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Configuration for an entire node, including list of modules. */
@SuppressWarnings("WeakerAccess")
public final class CompositeNodeConfigDto {

  private static final Logger logger = LoggerFactory.getLogger(CompositeNodeConfigDto.class);

  /** Port used for rest. */
  public int restPort;

  /**
   * Base port used for flooding. Will use the next N ports where N is number of shards in the
   * blockchain.
   */
  public int floodingPort;

  /** Ips for other peers in the network. */
  public List<String> knownPeers;

  /** Key used for network encryption - and as key in any configured fast track. */
  public String networkKey;

  /** Optional configuration to enable block production. */
  public BlockProducerConfigDto producerConfig;

  /** Optional configuration to enable periodical thread dumps. */
  public ThreadDumpModule.Config periodicThreadDumpsConfig;

  /** Optional map for default values that can be overridden. */
  public Map<String, String> overrides;

  String restAddress() {
    return "http://localhost:" + this.restPort;
  }

  long getOverrideOrDefault(String key, long defaultValue) {
    Map<String, String> overrides = overrides();
    String override = overrides.get(key);
    if (override != null) {
      return Long.parseLong(override);
    } else {
      return defaultValue;
    }
  }

  boolean getOverrideOrDefault(String key, boolean defaultValue) {
    Map<String, String> overrides = overrides();
    String override = overrides.get(key);
    if (override != null) {
      return Boolean.parseBoolean(override);
    } else {
      return defaultValue;
    }
  }

  String getGenesisFilePath(String file) {
    Map<String, String> overrides = overrides();
    String override = overrides.get("genesisPath");
    if (override != null) {
      return override + file;
    } else {
      return file;
    }
  }

  List<Address> getKnownPeers() {
    if (knownPeers != null) {
      return knownPeers.stream()
          .map(s -> s.split(":", -1))
          .map(s -> new Address(s[s.length - 2], Integer.parseInt(s[s.length - 1])))
          .collect(Collectors.toList());
    } else {
      return List.of();
    }
  }

  List<ProducerPeer> getProducerPeers() {
    if (knownPeers != null) {
      return knownPeers.stream()
          .map(s -> s.split(":", -1))
          .filter(s -> s.length == 3)
          .map(
              s ->
                  new ProducerPeer(
                      BlockchainPublicKey.fromEncodedEcPoint(Hex.decode(s[0])),
                      new Address(s[1], Integer.parseInt(s[2]))))
          .collect(Collectors.toList());
    } else {
      return List.of();
    }
  }

  Address getPersistentPeer(int portOffset) {
    String configAddress = overrides().get("flooding.persistent.peer");
    if (configAddress == null) {
      return null;
    }
    try {
      Address address = Address.parseAddress(configAddress);
      return new Address(address.hostname(), address.port() + portOffset);
    } catch (Exception e) {
      logger.warn("Malformed persistent peer: " + configAddress);
      return null;
    }
  }

  private Map<String, String> overrides() {
    return Objects.requireNonNullElseGet(this.overrides, Map::of);
  }

  /**
   * Configuration of a block producer. The config is in JSON format, and should look similar to the
   * following example:
   *
   * <pre>
   * {
   *   "restPort" : 8080,
   *   "floodingPort" : 9888,
   *   "knownPeers" : [
   *     "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe169eb:localhost:8111"
   *     ],
   *   "networkKey" : "72935980cdbcc7a25a29105028d44e81193d5196f19054024cedb412a1030c7b",
   *   "producerConfig" : {
   *     "accountKey" : "02d396245fc8b3a57aa3f50819b5c3e88ccd38d4a3d02147311140f7adfbe16a",
   *     "finalizationKey" : "131fee502908bf7ae5d89544d549b929bf89dd3312bee7c4360becaddeeb4929",
   *     "host" : "13.44.46.218",
   *     "chainEndpoints" : {
   *       "Ethereum" : "https://example.com/Ethereum",
   *       "Polygon" : "https://example.com/Polygon",
   *       "BnbSmartChain" : "https://example.com/BNB"
   *     }
   *   }
   * }
   * </pre>
   */
  public static final class BlockProducerConfigDto {
    /** IDs for chains that are used. */
    public static final class Chains {
      /** Ethereum ID. */
      public static final String ETHEREUM = "Ethereum";

      /** Polygon ID. */
      public static final String POLYGON = "Polygon";

      /** BNB SmartChain ID. */
      public static final String BNB_SMART_CHAIN = "BnbSmartChain";

      /** Sepolia ID. */
      static final String SEPOLIA = "Sepolia";

      private Chains() {}

      static List<String> getKnownChains() {
        return List.of(ETHEREUM, POLYGON, BNB_SMART_CHAIN, SEPOLIA);
      }
    }

    /** Key of the node operator account. */
    public String accountKey;

    /** BLS key for finalization. */
    public String finalizationKey;

    /** API url to ethereum. */
    public String ethereumUrl;

    /** Web3 API url to Polygon. */
    public String polygonUrl;

    /** Web3 API url to BNB smart chain (BSC). */
    public String bnbSmartChainUrl;

    /** Public accessible host of this node. */
    public String host;

    /**
     * Map of chains and their endpoints. An example entry for Ethereum could be:
     *
     * <pre>"Ethereum", "https://eth-endpoint-example.com/"</pre>
     *
     * <p>See {@link Chains} for valid chains and the <a
     * href=https://ethereum.org/en/developers/docs/apis/json-rpc>Ethereum API docs</a> for more
     * information on valid endpoints.
     */
    public Map<String, String> chainConfigs;

    /**
     * Add the chain and endpoint to the map of chain configs for this producer config.
     *
     * @param chain The chain ID, e.g. "Ethereum"
     * @param endpoint The endpoint for the chain, e.g. "https://eth-endpoint-example.com/"
     */
    public void addChainEndpoint(String chain, String endpoint) {
      Map<String, String> updatedChainConfig =
          this.chainConfigs != null ? this.chainConfigs : new HashMap<>();
      updatedChainConfig.put(chain, endpoint);
      this.chainConfigs = updatedChainConfig;
    }

    /**
     * Get the chain endpoint from the block producer's config, using the new chain config style. If
     * the chain config is not initialized, null is returned.
     *
     * @param chain The chain to get the endpoint for
     * @return The chain endpoint if it exists, or null
     */
    private String getEndpointNew(String chain) {
      return chainConfigs != null ? chainConfigs.get(chain) : null;
    }

    /**
     * Get the chain endpoint in this config from the old config style, or null if it is not
     * present.
     *
     * @param chain The chain to get the endpoint for. Can either be Ethereum, Polygon or
     *     BnbSmartChain
     * @return The chain endpoint or null if it was not found
     */
    private String getEndpointOld(String chain) {
      if (Chains.ETHEREUM.equals(chain)) {
        return ethereumUrl;
      } else if (Chains.POLYGON.equals(chain)) {
        return polygonUrl;
      } else if (Chains.BNB_SMART_CHAIN.equals(chain)) {
        return bnbSmartChainUrl;
      } else {
        return null;
      }
    }

    /**
     * Get the external chain endpoint URL from the block producer's config and the supplied chain.
     * Backwards compatible with the old chain configurations. Logs a warning if the chain was
     * configured in both the old and the new chain configurations, and prioritizes the newer
     * configuration map. Returns null if the chain was not configured in the config.
     *
     * @param chain The external chain to get the endpoint of
     * @return The endpoint URL, or null if no endpoint was found for the chain in the config
     */
    public String getChainEndpoint(String chain) {
      return getChainEndpoint(chain, logger);
    }

    String getChainEndpoint(String chain, Logger logger) {
      String endPoint = getEndpointNew(chain);
      String oldEndpoint = getEndpointOld(chain);
      validateEndpoints(endPoint, oldEndpoint, logger, chain);
      return endPoint == null ? oldEndpoint : endPoint;
    }

    /**
     * Logs a warning if both the new and old endpoint contains a value.
     *
     * @param endPoint The chain endpoint from the newer config map style
     * @param oldEndpoint The chain endpoint from the old config style
     * @param logger The logger to log the message
     * @param chain The chain for which the endpoint(s) are retrieved
     */
    private static void validateEndpoints(
        String endPoint, String oldEndpoint, Logger logger, String chain) {
      if (endPoint != null && oldEndpoint != null) {
        logger.warn(
            "Warning: Multiple endpoints was found for the chain '{}'. The configuration field"
                + " '{}' should no longer be used. Please update your block producer"
                + " configuration to only use the 'chainConfigs' map with an entry for '{}'.",
            chain,
            getOldConfigFieldFromChain(chain),
            chain);
      }
    }

    /**
     * Get the name of the record field of the old chain config given the chain. Only supports
     * Ethereum, Polygon and BnbSmartChain.
     */
    static String getOldConfigFieldFromChain(String chain) {
      if (Chains.ETHEREUM.equals(chain)) {
        return "ethereumUrl";
      } else if (Chains.POLYGON.equals(chain)) {
        return "polygonUrl";
      } else {
        return "bnbSmartChainUrl";
      }
    }
  }

  /**
   * Get the blockchain address of the producer of this node. Throws an error if node is not
   * configured with an account key.
   *
   * @return blockchain address
   */
  BlockchainAddress getProducerPbcAccount() {
    return new KeyPair(new BigInteger(producerConfig.accountKey, 16)).getPublic().createAddress();
  }

  record ProducerPeer(BlockchainPublicKey key, Address address) {}
}
