package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.monitor.PeerDatabase;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.zk.real.node.shared.RealPeerDatabase;
import com.partisiablockchain.zknetwork.ConnectionAddress;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/** Peer database persisting peer information to disk. */
public final class PersistentPeerDatabase implements PeerDatabase, RealPeerDatabase {

  static final int REAL_PORT_OFFSET = 9;
  private final PeersFile peersFile;
  private final Map<Hash, com.partisiablockchain.demo.byoc.Address> peers;
  private List<BlockchainPublicKey> registeredPeers = new ArrayList<>();

  /**
   * Create a persistent peer database.
   *
   * <p>Caches the current peers in memory and floods to disk on write.
   *
   * <p>If a peer occurs in both the initial peers from config and an existing peer database file
   * with different endpoints, we assume the endpoint from peer database file is most up to date and
   * use that.
   *
   * @param initialPeers initial peers
   * @param peersDatabaseFile peers file
   */
  public PersistentPeerDatabase(
      Map<BlockchainPublicKey, Address> initialPeers, File peersDatabaseFile) {
    peersFile = new PeersFile(peersDatabaseFile);

    peers = new ConcurrentHashMap<>(convertAddresses(initialPeers));
    if (peersFile.exists()) {
      peers.putAll(peersFile.readPeers());
    }
    peersFile.writePeers(peers);
  }

  private Map<Hash, com.partisiablockchain.demo.byoc.Address> convertAddresses(
      Map<BlockchainPublicKey, Address> floodingAddressPeers) {
    return floodingAddressPeers.entrySet().stream()
        .collect(
            Collectors.toMap(
                entry -> hashPublicKey(entry.getKey()), entry -> convertAddress(entry.getValue())));
  }

  com.partisiablockchain.demo.byoc.Address convertAddress(Address floodingAddress) {
    return new com.partisiablockchain.demo.byoc.Address(
        floodingAddress.hostname(), floodingAddress.port());
  }

  @Override
  public void setRegisteredBlockProducers(List<BlockchainPublicKey> updatedRegisteredPeers) {
    registeredPeers = List.copyOf(updatedRegisteredPeers);
  }

  @Override
  public boolean isRegisteredBlockProducer(BlockchainPublicKey blockchainPublicKey) {
    return registeredPeers.contains(blockchainPublicKey);
  }

  @Override
  public com.partisiablockchain.demo.byoc.Address getBlockProducerEndpoint(
      BlockchainPublicKey blockchainPublicKey) {
    return peers.get(hashPublicKey(blockchainPublicKey));
  }

  @Override
  public List<BlockchainPublicKey> getBlockProducersWithEndpoint() {
    return registeredPeers.stream().filter(p -> peers.get(hashPublicKey(p)) != null).toList();
  }

  @Override
  public boolean containsBlockProducer(BlockchainPublicKey blockchainPublicKey) {
    return peers.containsKey(hashPublicKey(blockchainPublicKey));
  }

  @Override
  public void updateBlockProducerEndpoint(
      BlockchainPublicKey blockchainPublicKey,
      com.partisiablockchain.demo.byoc.Address updatedAddress) {
    peers.put(hashPublicKey(blockchainPublicKey), updatedAddress);
    peersFile.writePeers(peers);
  }

  Hash hashPublicKey(BlockchainPublicKey publicKey) {
    return Hash.create(publicKey::write);
  }

  Collection<com.partisiablockchain.demo.byoc.Address> getPeers() {
    return peers.values();
  }

  @Override
  public ConnectionAddress getPeerRealAddress(BlockchainPublicKey blockchainPublicKey) {
    com.partisiablockchain.demo.byoc.Address address =
        peers.get(hashPublicKey(blockchainPublicKey));
    if (address == null) {
      return null;
    } else {
      int port = address.getPort() + REAL_PORT_OFFSET;
      return new ConnectionAddress(address.getHost(), port);
    }
  }
}
