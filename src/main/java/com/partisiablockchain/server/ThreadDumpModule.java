package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A small module that creates thread dump of the current JVM and stores it on disk. */
public final class ThreadDumpModule {

  private static final Logger logger = LoggerFactory.getLogger(ThreadDumpModule.class);
  private static final DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd__HH-mm");

  private final Path outputFolder;
  private final Config config;

  ThreadDumpModule(File dataDir, Config nullableConfig) {
    this.outputFolder = dataDir.toPath().resolve("threaddumps");
    this.config = Objects.requireNonNullElseGet(nullableConfig, Config::new);
  }

  void register(Main.CollectingServerConfig serverConfig) {
    if (config.isEnabled()) {
      ExceptionConverter.run(
          () -> Files.createDirectories(outputFolder),
          "Could not create thread dump output folder");

      ScheduledExecutorService scheduler = ExecutorFactory.newScheduledSingleThread("ThreadDump");
      serverConfig.registerCloseable(scheduler::shutdownNow);

      setSchedulerToDumpThreads(scheduler, outputFolder);
    }
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  void setSchedulerToDumpThreads(ScheduledExecutorService scheduler, Path path) {
    scheduler.scheduleAtFixedRate(
        () -> ExceptionLogger.handle(logger::warn, () -> toFile(path), "Could not dump threads."),
        config.determineInitialDelay(),
        config.determinePeriod(),
        TimeUnit.MILLISECONDS);
  }

  static void toFile(Path parentFolder) {
    var timestamp = formatter.format(LocalDateTime.now(ZoneId.systemDefault()));
    var filename = String.format("threads-%s.txt", timestamp);
    Path outputFile = parentFolder.resolve(filename);

    ExceptionConverter.run(() -> Files.write(outputFile, threadDump()), "IO error");
  }

  static byte[] threadDump() {
    StringBuilder threadDump = new StringBuilder(System.lineSeparator());
    ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();
    boolean objectMonitorSupported = threadBean.isObjectMonitorUsageSupported();
    boolean synchronizerUsageSupported = threadBean.isSynchronizerUsageSupported();
    for (ThreadInfo threadInfo :
        threadBean.dumpAllThreads(objectMonitorSupported, synchronizerUsageSupported)) {
      threadDump.append(threadInfo.toString());
    }
    return threadDump.toString().getBytes(StandardCharsets.UTF_8);
  }

  /** The config class for the thread dump module. */
  @SuppressWarnings("unused")
  public static final class Config {

    /** True to enable. */
    public boolean enabled;

    /** Delay before first stack dump. */
    public Long initialDelay;

    /** Interval between dumps. */
    public Long period;

    /**
     * Gets true for enabled.
     *
     * @return the true for enabled.
     */
    public boolean isEnabled() {
      return enabled;
    }

    /**
     * Gets the delay before first stack dump.
     *
     * @return the delay before first stack dump.
     */
    long determineInitialDelay() {
      return Objects.requireNonNullElse(initialDelay, 30_000L);
    }

    /**
     * Gets the interval between dumps.
     *
     * @return the interval between dumps.
     */
    long determinePeriod() {
      return Objects.requireNonNullElse(period, 5 * 60_000L);
    }
  }
}
