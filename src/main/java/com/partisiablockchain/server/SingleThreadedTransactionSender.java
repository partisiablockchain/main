package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.BlockchainTransactionClient;
import com.partisiablockchain.api.transactionclient.Transaction;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A transaction sender in a single thread. */
public final class SingleThreadedTransactionSender {
  private static final Logger logger =
      LoggerFactory.getLogger(SingleThreadedTransactionSender.class);
  private static final long DEFAULT_FEE_DISTRIBUTION_DELAY = Duration.ofMinutes(5).toMillis();
  private static final Duration DEFAULT_TRANSACTION_RETRY_DURATION = Duration.ofHours(2);

  private final ScheduledExecutorService executor;
  private final BlockchainTransactionClient client;
  private final long maximumFeeDistributionDelay;
  private final Duration transactionRetryDuration;
  private final TimeSupport timeSupport;

  /**
   * Create a transaction sender.
   *
   * @param config the server configuration
   * @param client the sharded client to send from
   */
  public SingleThreadedTransactionSender(
      ServerModule.ServerConfig config, BlockchainTransactionClient client) {
    this(
        config,
        client,
        DEFAULT_FEE_DISTRIBUTION_DELAY,
        DEFAULT_TRANSACTION_RETRY_DURATION,
        TimeSupport.SYSTEM_TIME);
  }

  /**
   * Create a transaction sender.
   *
   * @param config the server configuration
   * @param client the sharded client to send from
   * @param maximumFeeDistributionDelay maximum delay of transactions sent to fee distribution in
   *     milliseconds.
   * @param transactionRetryDuration the time to wait for a transaction to be included in a block
   * @param timeSupport supplier of the current system time in milliseconds
   */
  SingleThreadedTransactionSender(
      ServerModule.ServerConfig config,
      BlockchainTransactionClient client,
      long maximumFeeDistributionDelay,
      Duration transactionRetryDuration,
      TimeSupport timeSupport) {
    this.client = client;

    ScheduledExecutorService thread =
        ExecutorFactory.newScheduledSingleThread("rewards-transaction-sender");
    config.registerCloseable(thread::shutdown);

    this.executor = thread;
    this.maximumFeeDistributionDelay = maximumFeeDistributionDelay;
    this.transactionRetryDuration = transactionRetryDuration;
    this.timeSupport = timeSupport;
  }

  void sendFeeDistributionTransaction(DataStreamSerializable serializable) {
    long delay = sampleFeeDistributionDelay(maximumFeeDistributionDelay);
    sendDelayed(BetanetAddresses.FEE_DISTRIBUTION, serializable, delay);
  }

  static long sampleFeeDistributionDelay(long maximumWait) {
    return (long) (Math.random() * maximumWait);
  }

  void sendTransaction(BlockchainAddress contractAddress, DataStreamSerializable serializable) {
    sendDelayed(contractAddress, serializable, 0L);
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void sendDelayed(
      BlockchainAddress contractAddress, DataStreamSerializable serializable, long delay) {
    executor.schedule(
        () ->
            ExceptionLogger.handle(
                logger::warn,
                () -> {
                  Transaction transaction =
                      Transaction.create(
                          contractAddress, SafeDataOutputStream.serialize(serializable));
                  client.waitForInclusionInBlock(client.signAndSend(transaction, 0));
                },
                "Error sending transaction"),
        delay,
        TimeUnit.MILLISECONDS);
  }

  /**
   * Sends the given {@link Transaction} with the given cost, and waits for completion of it and
   * it's spawned events.
   *
   * @param transaction Transaction to send.
   * @param cost Amount of gas to send the transaction with.
   */
  void sendAndWaitForEvents(Transaction transaction, long cost) {
    Instant includeBefore = timeSupport.currentInstant().plus(transactionRetryDuration);
    ExecutedTransaction eventDto =
        ExceptionConverter.call(
            () ->
                executor
                    .submit(() -> client.ensureInclusionInBlock(transaction, cost, includeBefore))
                    .get(),
            "Error sending transaction");
    client.waitForSpawnedEvents(eventDto);
  }

  /**
   * Sends the given transaction with the given cost, and waits for completion of it and it's
   * spawned events.
   *
   * <p>Uses the DTO of the old transcation sender, but this can be converted easily to the new
   * sender.
   *
   * @param transaction Transaction to send.
   * @param cost Amount of gas to send the transaction with.
   */
  void sendAndWaitForEvents(
      com.partisiablockchain.client.transaction.Transaction transaction, long cost) {
    sendAndWaitForEvents(Transaction.create(transaction.getAddress(), transaction.getRpc()), cost);
  }

  interface TimeSupport {

    TimeSupport SYSTEM_TIME = Instant::now;

    Instant currentInstant();
  }
}
