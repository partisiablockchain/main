package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.server.storage.StorageRocksDb;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.File;

final class BetanetConfigFiles {

  static final ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);

  private final File dataDir;
  private final File nodeConfigFile;

  private BetanetConfigFiles(String nodeConfigFile, String dataDir) {
    this.nodeConfigFile = new File(nodeConfigFile);
    this.dataDir = new File(dataDir);
  }

  /**
   * Creates the config files from two string argument. The files as
   *
   * <ol>
   *   <li>Node config file
   *   <li>Data directory for resuming previous operations
   * </ol>
   *
   * <p>Due to backwards compatibility three arguments are allowed as
   *
   * <ol>
   *   <li>Node config file
   *   <li>Ignored
   *   <li>Data directory for resuming previous operations
   * </ol>
   *
   * @param args the arguments
   * @return the config files
   */
  static BetanetConfigFiles createFromArgs(String[] args) {
    if (args.length != 3 && args.length != 2) {
      throw new IllegalArgumentException("Expected 2 or 3 arguments, received " + args.length);
    } else {
      String nodeConfig;
      String storageDirectory;
      if (args.length == 2) {
        nodeConfig = args[0];
        storageDirectory = args[1];
      } else {
        nodeConfig = args[0];
        storageDirectory = args[2];
      }
      if (!new File(nodeConfig).isFile()) {
        throw new IllegalArgumentException(nodeConfig + " is not a file");
      } else if (!new File(storageDirectory).isDirectory()) {
        throw new IllegalArgumentException(storageDirectory + " is not a directory");
      } else {
        return new BetanetConfigFiles(nodeConfig, storageDirectory);
      }
    }
  }

  CompositeNodeConfigDto getCompositeNodeConfig() {
    return read(CompositeNodeConfigDto.class, nodeConfigFile);
  }

  RootDirectory getDataDirectory() {
    return new RootDirectory(getDataDir(), StorageRocksDb::new);
  }

  File getDataDir() {
    return dataDir;
  }

  private <T> T read(Class<T> valueType, File file) {
    return ExceptionConverter.call(
        () -> mapper.readValue(file, valueType), "Unable to read config file: " + file);
  }
}
