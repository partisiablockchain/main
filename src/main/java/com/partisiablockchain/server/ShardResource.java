package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import java.util.Map;

/** Resource for accessing sub resources per shard. */
@Path("/shards")
public final class ShardResource {

  private final Map<String, BlockchainResource> resources;
  private final Map<String, MetricsResource> metrics;

  ShardResource(Map<String, BlockchainResource> resources, Map<String, MetricsResource> metrics) {
    this.resources = resources;
    this.metrics = metrics;
  }

  /**
   * Get the blockchain resource for a shard.
   *
   * @param shardId the shard to access
   * @return the blockchain resource
   */
  @Path("/{shardId}/blockchain")
  public BlockchainResource shardResource(@PathParam("shardId") String shardId) {
    return resources.get(shardId);
  }

  /**
   * Get the metrics resource for a shard.
   *
   * @param shardId the shard to access
   * @return the metrics resource
   */
  @Path("/{shardId}/metrics")
  public MetricsResource metricsResource(@PathParam("shardId") String shardId) {
    return metrics.get(shardId);
  }
}
