# Changelog

## v2.9.0 (31/05/2022)

Mainnet release

## v2.9.1 (09/06/2022)

#### Changes

- PAR-2387: Report signature metrics from proof of justification (fast-track-node:2.9.1)
- PAR-4196: Remove openssl from docker image
- PAR-4197: Producers experiencing SIGSEGV (Update JRE to Temurin 17.0.3+7)
