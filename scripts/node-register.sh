#!/usr/bin/env bash

set -e

function docker_compose() {
  if docker compose &> /dev/null; then
    docker compose "$@"
  elif docker-compose &> /dev/null; then
    docker-compose "$@"
  else
    return 1
  fi
}

function container_id() {
  docker_compose ps -q pbc
}

function container_net() {
  id=$(container_id)
  docker inspect --format "{{lower .HostConfig.NetworkMode}}" "$id"
}

function container_name() {
  id=$(container_id)
  name=$(docker inspect --format "{{lower .Name}}" "$id")
  echo "${name:1}"
}

function docker_compose_exists() {
  if docker_compose &> /dev/null; then
    return 0
  else
    return 1
  fi
}

function jq_docker() {
  docker run --rm -i ghcr.io/jqlang/jq:1.7.1 "$@"
}

function check_status() {
  CODE=$(docker run --rm --net="$(container_net)" curlimages/curl "http://$(container_name):8080" -o /dev/null -w '%{http_code}\n' -s || true)
  if [[ $CODE == "204" ]]; then
    echo "Your node is up to date with the rest of the network."
  else
    echo "Your node is NOT up to date."
  fi
}

function is_running() {
  docker_compose exec pbc bash -c 'exit 255' &> /dev/null || ret=$?
  if [[ $ret -eq 255 ]]; then
    return 0
  else
    return 1
  fi
}

function add_logback_to_storage() {
  LOGBACK_CONF='<configuration debug="false"><statusListener class="ch.qos.logback.core.status.NopStatusListener" /></configuration>'
  if is_running; then
    docker_compose exec pbc /bin/bash -c "mkdir -p /storage/cli && echo '$LOGBACK_CONF'>/storage/cli/logback-test.xml"
  else
    docker_compose run --rm --entrypoint /bin/bash pbc -c "mkdir -p /storage/cli && echo '$LOGBACK_CONF'>/storage/cli/logback-test.xml" 2> /dev/null
  fi
}

function exec_java() {
  docker_compose exec pbc bash -c "unset JAVA_TOOL_OPTIONS; java -cp /storage/cli:\"\$(cat /app/jib-classpath-file)\" $*"
}

function run_as_root() {
  docker_compose run --rm --user 0:0 --entrypoint /bin/bash pbc -c "unset JAVA_TOOL_OPTIONS; java -cp /storage/cli:\"\$(cat /app/jib-classpath-file)\" $*"
}

if ! docker_compose_exists; then
  echo "Error: Docker Compose V1 or V2 could not be found. Please install it."
  exit 1
fi

function validate_kyc() {
  session_id=$1
  synaps_response=$(curl -s -H 'Session-Id: '"$session_id" https://synaps.partisiablockchain.com/kyc)
  address=$(jq_docker -r '.partisia_blockchain_address' <<< "$synaps_response")
  status=$(jq_docker -r '.status' <<< "$synaps_response")

  if [ "$status" = "APPROVED" ]; then
    echo "Your KYC registration is already approved."
    exit 0
  fi

  onchain_info=$(curl -s 'https://reader.partisiablockchain.com/synaps/kyc/'"$address")
  if [[ -z $onchain_info ]]; then
    echo "No KYC registration was found on-chain."
    exit 0
  fi

  synaps_fmt=$(jq_docker -r '{name: .name, address: .address, city: .city, country: .country_numeric}' <<< "$synaps_response")
  onchain_fmt=$(jq_docker -r '{name: .name, address: .address, city: .city, country: .country}' <<< "$onchain_info")
  diff_result=$(diff <(echo "$onchain_fmt") <(echo "$synaps_fmt") || true)

  if [[ -n "$diff_result" ]]; then
    echo "Your KYC registration is invalid. Please correct the following differences on-chain:"
    echo "$diff_result"
  else
    echo "Your KYC registration is valid!"
  fi
}

add_logback_to_storage

if [[ $1 == "create-config" ]]; then
  if is_running; then
    echo "Your node seems to be running, please close it first."
    exit 1
  fi

  run_as_root com.partisiablockchain.configgenerator.CliConfigGeneratorMain /conf/config.json
  docker_compose run --user 0:0 --rm --entrypoint /bin/bash pbc -c "chmod -R 500 /conf && chown 1500:1500 /conf/config.json || true"
elif [[ $1 == "register-node" ]]; then
  if ! is_running; then
    echo "Your node does not seem to be running, please start it first."
    exit 1
  else
    check_status
    exec_java com.partisiablockchain.configgenerator.RegisterNodeCli /conf/config.json
  fi
elif [[ $1 == "report-active" ]]; then
  if ! is_running; then
    echo "Your node does not seem to be running, please start it first."
    exit 1
  else
    check_status
    exec_java com.partisiablockchain.configgenerator.RegisterAsActiveCli /conf/config.json
  fi
elif [[ $1 == "compute-rewards" ]]; then
  if [ -z "$2" ]; then
    echo "Usage $0 $1 <rewards-quarter>"
    exit 1
  fi
  if ! is_running; then
    echo "Your node does not seem to be running, please start it first."
    exit 1
  else
    check_status
    exec_java com.partisiablockchain.rewards.RewardsCli "$2"
  fi
elif [[ $1 == "status" ]]; then
  if ! is_running; then
    echo "Your node does not seem to be running."
    exit 1
  else
    check_status
  fi
elif [[ $1 == "validate-kyc" ]]; then
  if [ -z "$2" ]; then
    echo "Usage $0 $1 <session-id>"
    exit 1
  fi
  validate_kyc "$2"
else
  echo "Usage $0 (create-config | register-node | status | validate-kyc | report-active | compute-rewards)"
  exit 1
fi
